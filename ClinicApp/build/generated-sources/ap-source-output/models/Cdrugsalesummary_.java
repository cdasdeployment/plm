package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Cdrugsaledetail;
import models.Client;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Cdrugsalesummary.class)
public class Cdrugsalesummary_ { 

    public static volatile SingularAttribute<Cdrugsalesummary, String> transactioncode;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> amountreceived;
    public static volatile SingularAttribute<Cdrugsalesummary, String> description;
    public static volatile SingularAttribute<Cdrugsalesummary, String> hmo;
    public static volatile SingularAttribute<Cdrugsalesummary, String> precode;
    public static volatile SingularAttribute<Cdrugsalesummary, String> paymentmode;
    public static volatile SingularAttribute<Cdrugsalesummary, String> midcode;
    public static volatile SingularAttribute<Cdrugsalesummary, String> cardname;
    public static volatile SingularAttribute<Cdrugsalesummary, String> clientname;
    public static volatile SingularAttribute<Cdrugsalesummary, Date> servicedatetime;
    public static volatile SingularAttribute<Cdrugsalesummary, Client> client;
    public static volatile SingularAttribute<Cdrugsalesummary, Integer> id;
    public static volatile SingularAttribute<Cdrugsalesummary, Character> seqcode;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> hmodiscount;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> salediscountamount;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> hmodiscountamount;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> salediscount;
    public static volatile SingularAttribute<Cdrugsalesummary, String> authcode;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> amountchange;
    public static volatile SingularAttribute<Cdrugsalesummary, String> cardexpiry;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> totalamount;
    public static volatile SingularAttribute<Cdrugsalesummary, String> cardnumber;
    public static volatile ListAttribute<Cdrugsalesummary, Cdrugsaledetail> cdrugsaledetailList;
    public static volatile SingularAttribute<Cdrugsalesummary, Double> saletotal;
    public static volatile SingularAttribute<Cdrugsalesummary, String> checknumber;

}