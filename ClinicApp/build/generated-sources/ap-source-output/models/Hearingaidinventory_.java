package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Hearingaiditem;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Hearingaidinventory.class)
public class Hearingaidinventory_ { 

    public static volatile SingularAttribute<Hearingaidinventory, Date> servicetimestamp;
    public static volatile SingularAttribute<Hearingaidinventory, Hearingaiditem> hearingaid;
    public static volatile SingularAttribute<Hearingaidinventory, Integer> purchase;
    public static volatile SingularAttribute<Hearingaidinventory, Double> buying;
    public static volatile SingularAttribute<Hearingaidinventory, Double> selling;
    public static volatile SingularAttribute<Hearingaidinventory, Integer> id;
    public static volatile SingularAttribute<Hearingaidinventory, Integer> pullout;
    public static volatile SingularAttribute<Hearingaidinventory, Integer> returned;
    public static volatile SingularAttribute<Hearingaidinventory, Integer> updated;
    public static volatile SingularAttribute<Hearingaidinventory, String> remarks;

}