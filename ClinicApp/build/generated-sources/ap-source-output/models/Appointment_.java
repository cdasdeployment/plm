package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Client;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Appointment.class)
public class Appointment_ { 

    public static volatile SingularAttribute<Appointment, Date> servicetimestamp;
    public static volatile SingularAttribute<Appointment, Date> appointmentdate;
    public static volatile SingularAttribute<Appointment, String> session;
    public static volatile SingularAttribute<Appointment, Boolean> isUpdated;
    public static volatile SingularAttribute<Appointment, Integer> index;
    public static volatile SingularAttribute<Appointment, Client> client;
    public static volatile SingularAttribute<Appointment, Date> appointmenttime;
    public static volatile SingularAttribute<Appointment, Integer> id;
    public static volatile SingularAttribute<Appointment, String> department;
    public static volatile SingularAttribute<Appointment, Boolean> isPriority;
    public static volatile SingularAttribute<Appointment, String> serviceprocedure;

}