package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.AccHearingaidinventory;
import models.Acchearingaidsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(AccHearingaiditem.class)
public class AccHearingaiditem_ { 

    public static volatile SingularAttribute<AccHearingaiditem, String> serialnumber;
    public static volatile SingularAttribute<AccHearingaiditem, String> purchasedate;
    public static volatile SingularAttribute<AccHearingaiditem, String> supplier;
    public static volatile ListAttribute<AccHearingaiditem, Acchearingaidsaledetail> acchearingaidsaledetailList;
    public static volatile SingularAttribute<AccHearingaiditem, Double> buying;
    public static volatile SingularAttribute<AccHearingaiditem, String> model;
    public static volatile SingularAttribute<AccHearingaiditem, Double> selling;
    public static volatile SingularAttribute<AccHearingaiditem, Integer> reorder;
    public static volatile SingularAttribute<AccHearingaiditem, Integer> id;
    public static volatile ListAttribute<AccHearingaiditem, AccHearingaidinventory> accHearingaidinventoryList;
    public static volatile SingularAttribute<AccHearingaiditem, Integer> stocks;

}