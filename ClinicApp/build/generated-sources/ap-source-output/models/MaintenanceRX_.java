package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.ChartDetails;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(MaintenanceRX.class)
public class MaintenanceRX_ { 

    public static volatile SingularAttribute<MaintenanceRX, String> note2;
    public static volatile SingularAttribute<MaintenanceRX, String> note3;
    public static volatile SingularAttribute<MaintenanceRX, String> gender;
    public static volatile SingularAttribute<MaintenanceRX, String> nextcheckup;
    public static volatile SingularAttribute<MaintenanceRX, Date> issuedate;
    public static volatile SingularAttribute<MaintenanceRX, String> preparation3;
    public static volatile SingularAttribute<MaintenanceRX, String> generic1;
    public static volatile SingularAttribute<MaintenanceRX, String> generic2;
    public static volatile SingularAttribute<MaintenanceRX, String> generic3;
    public static volatile SingularAttribute<MaintenanceRX, String> filepath;
    public static volatile SingularAttribute<MaintenanceRX, String> preparation1;
    public static volatile SingularAttribute<MaintenanceRX, String> preparation2;
    public static volatile SingularAttribute<MaintenanceRX, Integer> id;
    public static volatile SingularAttribute<MaintenanceRX, String> quantity1;
    public static volatile SingularAttribute<MaintenanceRX, String> quantity2;
    public static volatile SingularAttribute<MaintenanceRX, String> brand3;
    public static volatile SingularAttribute<MaintenanceRX, String> brand2;
    public static volatile SingularAttribute<MaintenanceRX, String> brand1;
    public static volatile SingularAttribute<MaintenanceRX, String> quantity3;
    public static volatile SingularAttribute<MaintenanceRX, String> direction1;
    public static volatile SingularAttribute<MaintenanceRX, String> direction2;
    public static volatile SingularAttribute<MaintenanceRX, String> direction3;
    public static volatile SingularAttribute<MaintenanceRX, String> note1;
    public static volatile SingularAttribute<MaintenanceRX, ChartDetails> chart;
    public static volatile SingularAttribute<MaintenanceRX, String> age;
    public static volatile SingularAttribute<MaintenanceRX, String> remarks;

}