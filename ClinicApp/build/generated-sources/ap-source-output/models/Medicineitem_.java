package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Druginventory;
import models.Drugsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Medicineitem.class)
public class Medicineitem_ { 

    public static volatile SingularAttribute<Medicineitem, Boolean> isrx;
    public static volatile SingularAttribute<Medicineitem, Double> capital;
    public static volatile SingularAttribute<Medicineitem, String> purchasemonth;
    public static volatile SingularAttribute<Medicineitem, String> expiremonth;
    public static volatile SingularAttribute<Medicineitem, String> usage;
    public static volatile ListAttribute<Medicineitem, Drugsaledetail> drugsaledetailList;
    public static volatile SingularAttribute<Medicineitem, String> purchase;
    public static volatile SingularAttribute<Medicineitem, Integer> reorder;
    public static volatile SingularAttribute<Medicineitem, Boolean> isActive;
    public static volatile SingularAttribute<Medicineitem, String> brandname;
    public static volatile SingularAttribute<Medicineitem, Integer> stocks;
    public static volatile SingularAttribute<Medicineitem, String> preparation;
    public static volatile SingularAttribute<Medicineitem, String> mtype;
    public static volatile SingularAttribute<Medicineitem, String> expireyear;
    public static volatile ListAttribute<Medicineitem, Druginventory> druginventoryCollection;
    public static volatile SingularAttribute<Medicineitem, Double> price;
    public static volatile SingularAttribute<Medicineitem, String> genericname;
    public static volatile SingularAttribute<Medicineitem, String> supplier;
    public static volatile SingularAttribute<Medicineitem, String> buying;
    public static volatile SingularAttribute<Medicineitem, String> expiration;
    public static volatile SingularAttribute<Medicineitem, Double> selling;
    public static volatile SingularAttribute<Medicineitem, String> purchaseyear;
    public static volatile SingularAttribute<Medicineitem, Integer> id;

}