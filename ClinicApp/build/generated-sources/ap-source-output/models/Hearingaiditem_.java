package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Hearingaidinventory;
import models.Hearingaidsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Hearingaiditem.class)
public class Hearingaiditem_ { 

    public static volatile SingularAttribute<Hearingaiditem, String> purchasedate;
    public static volatile SingularAttribute<Hearingaiditem, String> serialnumber;
    public static volatile SingularAttribute<Hearingaiditem, String> supplier;
    public static volatile SingularAttribute<Hearingaiditem, Double> buying;
    public static volatile ListAttribute<Hearingaiditem, Hearingaidsaledetail> hearingaidsaledetailList;
    public static volatile SingularAttribute<Hearingaiditem, String> model;
    public static volatile SingularAttribute<Hearingaiditem, Double> selling;
    public static volatile SingularAttribute<Hearingaiditem, Integer> reorder;
    public static volatile SingularAttribute<Hearingaiditem, Integer> id;
    public static volatile ListAttribute<Hearingaiditem, Hearingaidinventory> hearingaidinventoryList;
    public static volatile SingularAttribute<Hearingaiditem, Integer> stocks;

}