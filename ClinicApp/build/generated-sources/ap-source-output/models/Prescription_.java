package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Client;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Prescription.class)
public class Prescription_ { 

    public static volatile SingularAttribute<Prescription, String> quantity1;
    public static volatile SingularAttribute<Prescription, String> quantity2;
    public static volatile SingularAttribute<Prescription, String> brand3;
    public static volatile SingularAttribute<Prescription, String> gender;
    public static volatile SingularAttribute<Prescription, String> brand2;
    public static volatile SingularAttribute<Prescription, String> brand1;
    public static volatile SingularAttribute<Prescription, String> nextcheckup;
    public static volatile SingularAttribute<Prescription, String> quantity3;
    public static volatile SingularAttribute<Prescription, Date> issuedate;
    public static volatile SingularAttribute<Prescription, String> preparation3;
    public static volatile SingularAttribute<Prescription, String> generic1;
    public static volatile SingularAttribute<Prescription, String> direction1;
    public static volatile SingularAttribute<Prescription, String> generic2;
    public static volatile SingularAttribute<Prescription, String> direction2;
    public static volatile SingularAttribute<Prescription, String> filepath;
    public static volatile SingularAttribute<Prescription, String> generic3;
    public static volatile SingularAttribute<Prescription, String> direction3;
    public static volatile SingularAttribute<Prescription, String> preparation1;
    public static volatile SingularAttribute<Prescription, String> preparation2;
    public static volatile SingularAttribute<Prescription, Client> client;
    public static volatile SingularAttribute<Prescription, Integer> id;
    public static volatile SingularAttribute<Prescription, String> remarks;
    public static volatile SingularAttribute<Prescription, String> age;

}