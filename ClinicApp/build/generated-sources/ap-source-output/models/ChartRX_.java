package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.ChartDetails;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(ChartRX.class)
public class ChartRX_ { 

    public static volatile SingularAttribute<ChartRX, String> note2;
    public static volatile SingularAttribute<ChartRX, String> note3;
    public static volatile SingularAttribute<ChartRX, String> gender;
    public static volatile SingularAttribute<ChartRX, String> nextcheckup;
    public static volatile SingularAttribute<ChartRX, Date> issuedate;
    public static volatile SingularAttribute<ChartRX, String> preparation3;
    public static volatile SingularAttribute<ChartRX, String> generic1;
    public static volatile SingularAttribute<ChartRX, String> generic2;
    public static volatile SingularAttribute<ChartRX, String> generic3;
    public static volatile SingularAttribute<ChartRX, String> filepath;
    public static volatile SingularAttribute<ChartRX, String> preparation1;
    public static volatile SingularAttribute<ChartRX, String> preparation2;
    public static volatile SingularAttribute<ChartRX, Integer> id;
    public static volatile SingularAttribute<ChartRX, String> quantity1;
    public static volatile SingularAttribute<ChartRX, String> quantity2;
    public static volatile SingularAttribute<ChartRX, String> brand3;
    public static volatile SingularAttribute<ChartRX, String> brand2;
    public static volatile SingularAttribute<ChartRX, String> brand1;
    public static volatile SingularAttribute<ChartRX, String> quantity3;
    public static volatile SingularAttribute<ChartRX, String> direction1;
    public static volatile SingularAttribute<ChartRX, String> direction2;
    public static volatile SingularAttribute<ChartRX, String> direction3;
    public static volatile SingularAttribute<ChartRX, String> note1;
    public static volatile SingularAttribute<ChartRX, ChartDetails> chart;
    public static volatile SingularAttribute<ChartRX, String> age;
    public static volatile SingularAttribute<ChartRX, String> remarks;

}