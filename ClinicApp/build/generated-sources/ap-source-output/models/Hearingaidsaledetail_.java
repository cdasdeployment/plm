package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Hearingaiditem;
import models.Hearingaidsalesummary;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Hearingaidsaledetail.class)
public class Hearingaidsaledetail_ { 

    public static volatile SingularAttribute<Hearingaidsaledetail, Integer> quantity;
    public static volatile SingularAttribute<Hearingaidsaledetail, String> transactioncode;
    public static volatile SingularAttribute<Hearingaidsaledetail, String> description;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> hmodiscount;
    public static volatile SingularAttribute<Hearingaidsaledetail, String> hmo;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> hmodiscountamount;
    public static volatile SingularAttribute<Hearingaidsaledetail, Hearingaiditem> hearingaid;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> total;
    public static volatile SingularAttribute<Hearingaidsaledetail, Hearingaidsalesummary> salesummary;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> price;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> itemdiscount;
    public static volatile SingularAttribute<Hearingaidsaledetail, String> clientname;
    public static volatile SingularAttribute<Hearingaidsaledetail, Integer> client;
    public static volatile SingularAttribute<Hearingaidsaledetail, Date> servicedatetime;
    public static volatile SingularAttribute<Hearingaidsaledetail, Integer> id;
    public static volatile SingularAttribute<Hearingaidsaledetail, Double> itemdiscountamount;

}