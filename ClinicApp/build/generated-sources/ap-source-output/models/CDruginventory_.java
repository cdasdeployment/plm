package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.CMedicineitem;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(CDruginventory.class)
public class CDruginventory_ { 

    public static volatile SingularAttribute<CDruginventory, Date> servicetimestamp;
    public static volatile SingularAttribute<CDruginventory, Integer> purchase;
    public static volatile SingularAttribute<CDruginventory, Double> buying;
    public static volatile SingularAttribute<CDruginventory, Double> selling;
    public static volatile SingularAttribute<CDruginventory, Integer> id;
    public static volatile SingularAttribute<CDruginventory, Integer> pullout;
    public static volatile SingularAttribute<CDruginventory, Integer> returned;
    public static volatile SingularAttribute<CDruginventory, Integer> updated;
    public static volatile SingularAttribute<CDruginventory, String> remarks;
    public static volatile SingularAttribute<CDruginventory, CMedicineitem> drug;

}