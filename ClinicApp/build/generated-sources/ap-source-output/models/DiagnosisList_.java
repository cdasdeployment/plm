package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.ChartDetails;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(DiagnosisList.class)
public class DiagnosisList_ { 

    public static volatile SingularAttribute<DiagnosisList, String> diagnosis;
    public static volatile SingularAttribute<DiagnosisList, Integer> id;
    public static volatile SingularAttribute<DiagnosisList, ChartDetails> chart;

}