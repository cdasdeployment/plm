package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.ChartDetails;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(BillingDetail.class)
public class BillingDetail_ { 

    public static volatile SingularAttribute<BillingDetail, Double> amount;
    public static volatile SingularAttribute<BillingDetail, String> charge;
    public static volatile SingularAttribute<BillingDetail, Integer> id;
    public static volatile SingularAttribute<BillingDetail, String> HMO;
    public static volatile SingularAttribute<BillingDetail, ChartDetails> chart;

}