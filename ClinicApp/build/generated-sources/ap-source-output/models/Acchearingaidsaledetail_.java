package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.AccHearingaiditem;
import models.Acchearingaidsalesummary;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Acchearingaidsaledetail.class)
public class Acchearingaidsaledetail_ { 

    public static volatile SingularAttribute<Acchearingaidsaledetail, Integer> quantity;
    public static volatile SingularAttribute<Acchearingaidsaledetail, String> transactioncode;
    public static volatile SingularAttribute<Acchearingaidsaledetail, String> description;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> hmodiscount;
    public static volatile SingularAttribute<Acchearingaidsaledetail, String> hmo;
    public static volatile SingularAttribute<Acchearingaidsaledetail, AccHearingaiditem> acchearingaid;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> hmodiscountamount;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> total;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Acchearingaidsalesummary> salesummary;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> price;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> itemdiscount;
    public static volatile SingularAttribute<Acchearingaidsaledetail, String> clientname;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Integer> client;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Date> servicedatetime;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Integer> id;
    public static volatile SingularAttribute<Acchearingaidsaledetail, Double> itemdiscountamount;

}