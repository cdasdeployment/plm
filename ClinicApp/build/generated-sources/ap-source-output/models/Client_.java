package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Acchearingaidsalesummary;
import models.Appointment;
import models.Cdrugsalesummary;
import models.ChartDetails;
import models.Drugsalesummary;
import models.Hearingaidsalesummary;
import models.PatientChart;
import models.Prescription;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-12-27T14:33:37")
@StaticMetamodel(Client.class)
public class Client_ { 

    public static volatile SingularAttribute<Client, String> insurance;
    public static volatile SingularAttribute<Client, String> occupation;
    public static volatile ListAttribute<Client, Hearingaidsalesummary> hearingaidsalesummaryList;
    public static volatile SingularAttribute<Client, String> profilepic;
    public static volatile ListAttribute<Client, Appointment> appointmentList;
    public static volatile SingularAttribute<Client, Boolean> isActive;
    public static volatile SingularAttribute<Client, String> debitcard;
    public static volatile SingularAttribute<Client, String> otherallergies;
    public static volatile SingularAttribute<Client, String> smokingfrequency;
    public static volatile SingularAttribute<Client, String> province;
    public static volatile ListAttribute<Client, PatientChart> patientChartList;
    public static volatile SingularAttribute<Client, String> coughcolds;
    public static volatile SingularAttribute<Client, Date> lastcheckup;
    public static volatile SingularAttribute<Client, String> diabetesmellitus;
    public static volatile SingularAttribute<Client, Integer> id;
    public static volatile ListAttribute<Client, Acchearingaidsalesummary> acchearingaidsalesummaryList;
    public static volatile ListAttribute<Client, Cdrugsalesummary> cdrugsalesummaryList;
    public static volatile SingularAttribute<Client, String> foodallergy;
    public static volatile SingularAttribute<Client, Integer> months;
    public static volatile SingularAttribute<Client, String> town;
    public static volatile SingularAttribute<Client, String> asthma;
    public static volatile ListAttribute<Client, Drugsalesummary> drugsalesummaryList;
    public static volatile SingularAttribute<Client, String> lastprocedure;
    public static volatile SingularAttribute<Client, String> lastname;
    public static volatile SingularAttribute<Client, Date> surgerydate;
    public static volatile SingularAttribute<Client, String> contactinfo;
    public static volatile SingularAttribute<Client, String> maintenanceMeds;
    public static volatile SingularAttribute<Client, String> latexallergy;
    public static volatile SingularAttribute<Client, String> status;
    public static volatile SingularAttribute<Client, Date> birthday;
    public static volatile SingularAttribute<Client, String> civilstatus;
    public static volatile SingularAttribute<Client, String> emergencyname;
    public static volatile SingularAttribute<Client, String> firstname;
    public static volatile SingularAttribute<Client, String> gender;
    public static volatile SingularAttribute<Client, Date> checkupdate;
    public static volatile ListAttribute<Client, Prescription> prescriptionList;
    public static volatile SingularAttribute<Client, String> emergencyaddress;
    public static volatile SingularAttribute<Client, String> smokingsticksperday;
    public static volatile SingularAttribute<Client, String> smoking;
    public static volatile SingularAttribute<Client, String> company;
    public static volatile SingularAttribute<Client, String> streetaddress;
    public static volatile SingularAttribute<Client, String> email;
    public static volatile SingularAttribute<Client, String> emergencycontact;
    public static volatile SingularAttribute<Client, String> surgerydone;
    public static volatile SingularAttribute<Client, String> alcoholic;
    public static volatile SingularAttribute<Client, String> alcoholicfrequency;
    public static volatile SingularAttribute<Client, String> brgy;
    public static volatile SingularAttribute<Client, String> creditcard;
    public static volatile ListAttribute<Client, ChartDetails> chartDetailsList;
    public static volatile SingularAttribute<Client, String> medicineallergy;
    public static volatile SingularAttribute<Client, String> alcoholiclength;
    public static volatile SingularAttribute<Client, Integer> days;
    public static volatile SingularAttribute<Client, String> cancer;
    public static volatile SingularAttribute<Client, String> hypertension;
    public static volatile SingularAttribute<Client, Integer> age;
    public static volatile SingularAttribute<Client, String> surgery;

}