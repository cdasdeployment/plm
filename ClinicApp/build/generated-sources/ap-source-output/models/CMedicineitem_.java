package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.CDruginventory;
import models.Cdrugsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(CMedicineitem.class)
public class CMedicineitem_ { 

    public static volatile SingularAttribute<CMedicineitem, Double> capital;
    public static volatile SingularAttribute<CMedicineitem, String> purchasemonth;
    public static volatile SingularAttribute<CMedicineitem, String> expiremonth;
    public static volatile SingularAttribute<CMedicineitem, String> purchase;
    public static volatile SingularAttribute<CMedicineitem, Integer> reorder;
    public static volatile SingularAttribute<CMedicineitem, String> brandname;
    public static volatile SingularAttribute<CMedicineitem, Integer> stocks;
    public static volatile SingularAttribute<CMedicineitem, String> preparation;
    public static volatile SingularAttribute<CMedicineitem, String> mtype;
    public static volatile SingularAttribute<CMedicineitem, String> musage;
    public static volatile SingularAttribute<CMedicineitem, String> expireyear;
    public static volatile SingularAttribute<CMedicineitem, Double> price;
    public static volatile SingularAttribute<CMedicineitem, String> genericname;
    public static volatile SingularAttribute<CMedicineitem, String> supplier;
    public static volatile SingularAttribute<CMedicineitem, String> buying;
    public static volatile SingularAttribute<CMedicineitem, String> expiration;
    public static volatile SingularAttribute<CMedicineitem, Double> selling;
    public static volatile ListAttribute<CMedicineitem, CDruginventory> cdruginventoryList;
    public static volatile SingularAttribute<CMedicineitem, String> purchaseyear;
    public static volatile ListAttribute<CMedicineitem, Cdrugsaledetail> cdrugsaledetailList;
    public static volatile SingularAttribute<CMedicineitem, Integer> id;

}