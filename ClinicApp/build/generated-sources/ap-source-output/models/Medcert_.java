package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Medcert.class)
public class Medcert_ { 

    public static volatile SingularAttribute<Medcert, Date> servicetimestamp;
    public static volatile SingularAttribute<Medcert, Integer> client;
    public static volatile SingularAttribute<Medcert, String> dateissued;
    public static volatile SingularAttribute<Medcert, Integer> id;
    public static volatile SingularAttribute<Medcert, String> message;

}