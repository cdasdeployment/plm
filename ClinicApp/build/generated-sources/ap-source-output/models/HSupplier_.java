package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(HSupplier.class)
public class HSupplier_ { 

    public static volatile SingularAttribute<HSupplier, String> suppliername;
    public static volatile SingularAttribute<HSupplier, String> supplieragent;
    public static volatile SingularAttribute<HSupplier, String> suppliercontact;
    public static volatile SingularAttribute<HSupplier, String> supplieraddress;
    public static volatile SingularAttribute<HSupplier, Integer> id;

}