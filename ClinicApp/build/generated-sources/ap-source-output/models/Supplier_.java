package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Supplier.class)
public class Supplier_ { 

    public static volatile SingularAttribute<Supplier, String> suppliername;
    public static volatile SingularAttribute<Supplier, String> supplieragent;
    public static volatile SingularAttribute<Supplier, String> suppliercontact;
    public static volatile SingularAttribute<Supplier, String> supplieraddress;
    public static volatile SingularAttribute<Supplier, Integer> id;

}