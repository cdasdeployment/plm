package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Acchearingaidsaledetail;
import models.Client;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Acchearingaidsalesummary.class)
public class Acchearingaidsalesummary_ { 

    public static volatile SingularAttribute<Acchearingaidsalesummary, String> transactioncode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> amountreceived;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> description;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> hmo;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> precode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> paymentmode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> midcode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> cardname;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> clientname;
    public static volatile ListAttribute<Acchearingaidsalesummary, Acchearingaidsaledetail> acchearingaidsaledetailList;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Date> servicedatetime;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Client> client;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Integer> id;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Character> seqcode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> hmodiscount;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> salediscountamount;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> hmodiscountamount;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> salediscount;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> authcode;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> amountchange;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> cardexpiry;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> totalamount;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> cardnumber;
    public static volatile SingularAttribute<Acchearingaidsalesummary, Double> saletotal;
    public static volatile SingularAttribute<Acchearingaidsalesummary, String> checknumber;

}