package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.AccHearingaiditem;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(AccHearingaidinventory.class)
public class AccHearingaidinventory_ { 

    public static volatile SingularAttribute<AccHearingaidinventory, Date> servicetimestamp;
    public static volatile SingularAttribute<AccHearingaidinventory, AccHearingaiditem> hearingaid;
    public static volatile SingularAttribute<AccHearingaidinventory, Integer> purchase;
    public static volatile SingularAttribute<AccHearingaidinventory, Double> buying;
    public static volatile SingularAttribute<AccHearingaidinventory, Double> selling;
    public static volatile SingularAttribute<AccHearingaidinventory, Integer> id;
    public static volatile SingularAttribute<AccHearingaidinventory, Integer> pullout;
    public static volatile SingularAttribute<AccHearingaidinventory, Integer> returned;
    public static volatile SingularAttribute<AccHearingaidinventory, Integer> updated;
    public static volatile SingularAttribute<AccHearingaidinventory, String> remarks;

}