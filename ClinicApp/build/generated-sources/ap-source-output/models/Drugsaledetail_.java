package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Drugsalesummary;
import models.Medicineitem;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Drugsaledetail.class)
public class Drugsaledetail_ { 

    public static volatile SingularAttribute<Drugsaledetail, Integer> quantity;
    public static volatile SingularAttribute<Drugsaledetail, String> transactioncode;
    public static volatile SingularAttribute<Drugsaledetail, String> description;
    public static volatile SingularAttribute<Drugsaledetail, Double> hmodiscount;
    public static volatile SingularAttribute<Drugsaledetail, String> hmo;
    public static volatile SingularAttribute<Drugsaledetail, Double> hmodiscountamount;
    public static volatile SingularAttribute<Drugsaledetail, Medicineitem> drug;
    public static volatile SingularAttribute<Drugsaledetail, Double> total;
    public static volatile SingularAttribute<Drugsaledetail, Drugsalesummary> salesummary;
    public static volatile SingularAttribute<Drugsaledetail, Double> price;
    public static volatile SingularAttribute<Drugsaledetail, Double> itemdiscount;
    public static volatile SingularAttribute<Drugsaledetail, String> clientname;
    public static volatile SingularAttribute<Drugsaledetail, Integer> client;
    public static volatile SingularAttribute<Drugsaledetail, Date> servicedatetime;
    public static volatile SingularAttribute<Drugsaledetail, Integer> id;
    public static volatile SingularAttribute<Drugsaledetail, Double> itemdiscountamount;

}