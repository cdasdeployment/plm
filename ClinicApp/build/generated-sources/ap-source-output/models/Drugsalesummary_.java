package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Client;
import models.Drugsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Drugsalesummary.class)
public class Drugsalesummary_ { 

    public static volatile SingularAttribute<Drugsalesummary, String> transactioncode;
    public static volatile SingularAttribute<Drugsalesummary, Double> amountreceived;
    public static volatile ListAttribute<Drugsalesummary, Drugsaledetail> drugsaledetailList;
    public static volatile SingularAttribute<Drugsalesummary, String> description;
    public static volatile SingularAttribute<Drugsalesummary, String> hmo;
    public static volatile SingularAttribute<Drugsalesummary, String> precode;
    public static volatile SingularAttribute<Drugsalesummary, String> paymentmode;
    public static volatile SingularAttribute<Drugsalesummary, String> midcode;
    public static volatile SingularAttribute<Drugsalesummary, String> cardname;
    public static volatile SingularAttribute<Drugsalesummary, String> clientname;
    public static volatile SingularAttribute<Drugsalesummary, Date> servicedatetime;
    public static volatile SingularAttribute<Drugsalesummary, Client> client;
    public static volatile SingularAttribute<Drugsalesummary, Integer> id;
    public static volatile SingularAttribute<Drugsalesummary, String> seqcode;
    public static volatile SingularAttribute<Drugsalesummary, Double> hmodiscount;
    public static volatile SingularAttribute<Drugsalesummary, Double> salediscountamount;
    public static volatile SingularAttribute<Drugsalesummary, Double> hmodiscountamount;
    public static volatile SingularAttribute<Drugsalesummary, Double> salediscount;
    public static volatile SingularAttribute<Drugsalesummary, String> authcode;
    public static volatile SingularAttribute<Drugsalesummary, Double> amountchange;
    public static volatile SingularAttribute<Drugsalesummary, String> cardexpiry;
    public static volatile SingularAttribute<Drugsalesummary, Double> totalamount;
    public static volatile SingularAttribute<Drugsalesummary, String> cardnumber;
    public static volatile SingularAttribute<Drugsalesummary, Double> saletotal;
    public static volatile SingularAttribute<Drugsalesummary, String> checknumber;

}