package models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.TransactionLogPK;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(TransactionLog.class)
public class TransactionLog_ { 

    public static volatile SingularAttribute<TransactionLog, TransactionLogPK> transactionLogPK;
    public static volatile SingularAttribute<TransactionLog, String> transactioninfo;

}