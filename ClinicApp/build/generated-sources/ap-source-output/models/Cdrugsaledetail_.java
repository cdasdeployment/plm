package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.CMedicineitem;
import models.Cdrugsalesummary;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Cdrugsaledetail.class)
public class Cdrugsaledetail_ { 

    public static volatile SingularAttribute<Cdrugsaledetail, CMedicineitem> cdrug;
    public static volatile SingularAttribute<Cdrugsaledetail, Integer> quantity;
    public static volatile SingularAttribute<Cdrugsaledetail, String> transactioncode;
    public static volatile SingularAttribute<Cdrugsaledetail, String> description;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> hmodiscount;
    public static volatile SingularAttribute<Cdrugsaledetail, String> hmo;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> hmodiscountamount;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> total;
    public static volatile SingularAttribute<Cdrugsaledetail, Cdrugsalesummary> salesummary;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> price;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> itemdiscount;
    public static volatile SingularAttribute<Cdrugsaledetail, String> clientname;
    public static volatile SingularAttribute<Cdrugsaledetail, Integer> client;
    public static volatile SingularAttribute<Cdrugsaledetail, Date> servicedatetime;
    public static volatile SingularAttribute<Cdrugsaledetail, Integer> id;
    public static volatile SingularAttribute<Cdrugsaledetail, Double> itemdiscountamount;

}