package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Medicineitem;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Druginventory.class)
public class Druginventory_ { 

    public static volatile SingularAttribute<Druginventory, Date> servicetimestamp;
    public static volatile SingularAttribute<Druginventory, Integer> purchase;
    public static volatile SingularAttribute<Druginventory, Double> buying;
    public static volatile SingularAttribute<Druginventory, Double> selling;
    public static volatile SingularAttribute<Druginventory, Integer> id;
    public static volatile SingularAttribute<Druginventory, Integer> pullout;
    public static volatile SingularAttribute<Druginventory, Integer> returned;
    public static volatile SingularAttribute<Druginventory, Integer> updated;
    public static volatile SingularAttribute<Druginventory, String> remarks;
    public static volatile SingularAttribute<Druginventory, Medicineitem> drug;

}