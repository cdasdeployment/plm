package models;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import models.Client;
import models.Hearingaidsaledetail;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2017-08-22T15:28:25")
@StaticMetamodel(Hearingaidsalesummary.class)
public class Hearingaidsalesummary_ { 

    public static volatile SingularAttribute<Hearingaidsalesummary, String> transactioncode;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> amountreceived;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> description;
    public static volatile ListAttribute<Hearingaidsalesummary, Hearingaidsaledetail> hearingaidsaledetailList;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> hmo;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> precode;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> paymentmode;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> midcode;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> cardname;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> clientname;
    public static volatile SingularAttribute<Hearingaidsalesummary, Date> servicedatetime;
    public static volatile SingularAttribute<Hearingaidsalesummary, Client> client;
    public static volatile SingularAttribute<Hearingaidsalesummary, Integer> id;
    public static volatile SingularAttribute<Hearingaidsalesummary, Character> seqcode;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> hmodiscount;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> salediscountamount;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> hmodiscountamount;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> salediscount;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> authcode;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> amountchange;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> cardexpiry;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> totalamount;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> cardnumber;
    public static volatile SingularAttribute<Hearingaidsalesummary, Double> saletotal;
    public static volatile SingularAttribute<Hearingaidsalesummary, String> checknumber;

}