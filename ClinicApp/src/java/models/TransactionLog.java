/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "transactionlog")
@NamedQueries({
    @NamedQuery(name = "TransactionLog.findAll", query = "SELECT t FROM TransactionLog t")})
public class TransactionLog implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TransactionLogPK transactionLogPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "TRANSACTIONINFO")
    private String transactioninfo;

    public TransactionLog() {
    }

    public TransactionLog(TransactionLogPK transactionLogPK) {
        this.transactionLogPK = transactionLogPK;
    }

    public TransactionLog(TransactionLogPK transactionLogPK, String transactioninfo) {
        this.transactionLogPK = transactionLogPK;
        this.transactioninfo = transactioninfo;
    }

    public TransactionLog(int prefix, String midfix, String seqfix) {
        this.transactionLogPK = new TransactionLogPK(prefix, midfix, seqfix);
    }

    public TransactionLogPK getTransactionLogPK() {
        return transactionLogPK;
    }

    public void setTransactionLogPK(TransactionLogPK transactionLogPK) {
        this.transactionLogPK = transactionLogPK;
    }

    public String getTransactioninfo() {
        return transactioninfo;
    }

    public void setTransactioninfo(String transactioninfo) {
        this.transactioninfo = transactioninfo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (transactionLogPK != null ? transactionLogPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionLog)) {
            return false;
        }
        TransactionLog other = (TransactionLog) object;
        if ((this.transactionLogPK == null && other.transactionLogPK != null) || (this.transactionLogPK != null && !this.transactionLogPK.equals(other.transactionLogPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.TransactionLog[ transactionLogPK=" + transactionLogPK + " ]";
    }
    
}
