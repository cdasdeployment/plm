/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "cmedicineitem")
@NamedQueries({
    @NamedQuery(name = "CMedicineitem.findAll", query = "SELECT c FROM CMedicineitem c")})
public class CMedicineitem implements Serializable {

    @Size(max = 45)
    @Column(name = "PURCHASEYEAR")
    private String purchaseyear;
    @Size(max = 45)
    @Column(name = "PURCHASEMONTH")
    private String purchasemonth;
    @Size(max = 45)
    @Column(name = "EXPIREMONTH")
    private String expiremonth;
    @Size(max = 45)
    @Column(name = "EXPIREYEAR")
    private String expireyear;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cdrug", fetch = FetchType.LAZY)
    private List<Cdrugsaledetail> cdrugsaledetailList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "GENERICNAME")
    private String genericname;
    @Size(max = 255)
    @Column(name = "BRANDNAME")
    private String brandname;
    @Size(max = 255)
    @Column(name = "PREPARATION")
    private String preparation;
    @Size(max = 45)
    @Column(name = "MUSAGE")
    private String musage;
    @Size(max = 45)
    @Column(name = "MTYPE")
    private String mtype;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRICE")
    private Double price;
    @Column(name = "STOCKS")
    private Integer stocks;
    @Column(name = "REORDER")
    private Integer reorder;
    @Size(max = 255)
    @Column(name = "SUPPLIER")
    private String supplier;
    @Size(max = 45)
    @Column(name = "EXPIRATION")
    private String expiration;
    @Size(max = 45)
    @Column(name = "PURCHASE")
    private String purchase;
    @Column(name = "SELLING")
    private Double selling;
    @Size(max = 45)
    @Column(name = "BUYING")
    private String buying;
    @Column(name = "CAPITAL")
    private Double capital;
    @OneToMany(mappedBy = "drug", fetch = FetchType.LAZY)
    private List<CDruginventory> cdruginventoryList;

    public CMedicineitem() {
    }

    public CMedicineitem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGenericname() {
        return genericname;
    }

    public void setGenericname(String genericname) {
        this.genericname = genericname;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public String getPreparation() {
        return preparation;
    }

    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    public String getMusage() {
        return musage;
    }

    public void setMusage(String musage) {
        this.musage = musage;
    }

    public String getMtype() {
        return mtype;
    }

    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getStocks() {
        return stocks;
    }

    public void setStocks(Integer stocks) {
        this.stocks = stocks;
    }

    public Integer getReorder() {
        return reorder;
    }

    public void setReorder(Integer reorder) {
        this.reorder = reorder;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getPurchase() {
        return purchase;
    }

    public void setPurchase(String purchase) {
        this.purchase = purchase;
    }

    public Double getSelling() {
        return selling;
    }

    public void setSelling(Double selling) {
        this.selling = selling;
    }

    public String getBuying() {
        return buying;
    }

    public void setBuying(String buying) {
        this.buying = buying;
    }

    public Double getCapital() {
        return capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public List<CDruginventory> getCdruginventoryList() {
        return cdruginventoryList;
    }

    public void setCdruginventoryList(List<CDruginventory> cdruginventoryList) {
        this.cdruginventoryList = cdruginventoryList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CMedicineitem)) {
            return false;
        }
        CMedicineitem other = (CMedicineitem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Cmedicineitem[ id=" + id + " ]";
    }

    public List<Cdrugsaledetail> getCdrugsaledetailList() {
        return cdrugsaledetailList;
    }

    public void setCdrugsaledetailList(List<Cdrugsaledetail> cdrugsaledetailList) {
        this.cdrugsaledetailList = cdrugsaledetailList;
    }

    public String getPurchaseyear() {
        return purchaseyear;
    }

    public void setPurchaseyear(String purchaseyear) {
        this.purchaseyear = purchaseyear;
    }

    public String getPurchasemonth() {
        return purchasemonth;
    }

    public void setPurchasemonth(String purchasemonth) {
        this.purchasemonth = purchasemonth;
    }

    public String getExpiremonth() {
        return expiremonth;
    }

    public void setExpiremonth(String expiremonth) {
        this.expiremonth = expiremonth;
    }

    public String getExpireyear() {
        return expireyear;
    }

    public void setExpireyear(String expireyear) {
        this.expireyear = expireyear;
    }
    
}
