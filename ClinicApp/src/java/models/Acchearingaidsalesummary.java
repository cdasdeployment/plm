/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "acchearingaidsalesummary")
@NamedQueries({
    @NamedQuery(name = "Acchearingaidsalesummary.findAll", query = "SELECT a FROM Acchearingaidsalesummary a")})
public class Acchearingaidsalesummary implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 11)
    @Column(name = "TRANSACTIONCODE")
    private String transactioncode;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 255)
    @Column(name = "CLIENTNAME")
    private String clientname;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SALETOTAL")
    private Double saletotal;
    @Column(name = "SALEDISCOUNT")
    private Double salediscount;
    @Column(name = "SALEDISCOUNTAMOUNT")
    private Double salediscountamount;
    @Column(name = "SERVICEDATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date servicedatetime;
    @Size(max = 45)
    @Column(name = "HMO")
    private String hmo;
    @Column(name = "HMODISCOUNT")
    private Double hmodiscount;
    @Column(name = "HMODISCOUNTAMOUNT")
    private Double hmodiscountamount;
    @Column(name = "TOTALAMOUNT")
    private Double totalamount;
    @Column(name = "AMOUNTRECEIVED")
    private Double amountreceived;
    @Column(name = "AMOUNTCHANGE")
    private Double amountchange;
    @Size(max = 4)
    @Column(name = "PRECODE")
    private String precode;
    @Size(max = 4)
    @Column(name = "MIDCODE")
    private String midcode;
    @Column(name = "SEQCODE")
    private Character seqcode;
    @Size(max = 255)
    @Column(name = "PAYMENTMODE")
    private String paymentmode;
    @Size(max = 255)
    @Column(name = "CARDNAME")
    private String cardname;
    @Size(max = 255)
    @Column(name = "CARDNUMBER")
    private String cardnumber;
    @Size(max = 255)
    @Column(name = "CARDEXPIRY")
    private String cardexpiry;
    @Size(max = 255)
    @Column(name = "AUTHCODE")
    private String authcode;
    @Size(max = 255)
    @Column(name = "CHECKNUMBER")
    private String checknumber;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "salesummary", fetch = FetchType.LAZY)
    private List<Acchearingaidsaledetail> acchearingaidsaledetailList;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Client client;

    public Acchearingaidsalesummary() {
    }

    public Acchearingaidsalesummary(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransactioncode() {
        return transactioncode;
    }

    public void setTransactioncode(String transactioncode) {
        this.transactioncode = transactioncode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public Double getSaletotal() {
        return saletotal;
    }

    public void setSaletotal(Double saletotal) {
        this.saletotal = saletotal;
    }

    public Double getSalediscount() {
        return salediscount;
    }

    public void setSalediscount(Double salediscount) {
        this.salediscount = salediscount;
    }

    public Double getSalediscountamount() {
        return salediscountamount;
    }

    public void setSalediscountamount(Double salediscountamount) {
        this.salediscountamount = salediscountamount;
    }

    public Date getServicedatetime() {
        return servicedatetime;
    }

    public void setServicedatetime(Date servicedatetime) {
        this.servicedatetime = servicedatetime;
    }

    public String getHmo() {
        return hmo;
    }

    public void setHmo(String hmo) {
        this.hmo = hmo;
    }

    public Double getHmodiscount() {
        return hmodiscount;
    }

    public void setHmodiscount(Double hmodiscount) {
        this.hmodiscount = hmodiscount;
    }

    public Double getHmodiscountamount() {
        return hmodiscountamount;
    }

    public void setHmodiscountamount(Double hmodiscountamount) {
        this.hmodiscountamount = hmodiscountamount;
    }

    public Double getTotalamount() {
        return totalamount;
    }

    public void setTotalamount(Double totalamount) {
        this.totalamount = totalamount;
    }

    public Double getAmountreceived() {
        return amountreceived;
    }

    public void setAmountreceived(Double amountreceived) {
        this.amountreceived = amountreceived;
    }

    public Double getAmountchange() {
        return amountchange;
    }

    public void setAmountchange(Double amountchange) {
        this.amountchange = amountchange;
    }

    public String getPrecode() {
        return precode;
    }

    public void setPrecode(String precode) {
        this.precode = precode;
    }

    public String getMidcode() {
        return midcode;
    }

    public void setMidcode(String midcode) {
        this.midcode = midcode;
    }

    public Character getSeqcode() {
        return seqcode;
    }

    public void setSeqcode(Character seqcode) {
        this.seqcode = seqcode;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public String getCardname() {
        return cardname;
    }

    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getCardexpiry() {
        return cardexpiry;
    }

    public void setCardexpiry(String cardexpiry) {
        this.cardexpiry = cardexpiry;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getChecknumber() {
        return checknumber;
    }

    public void setChecknumber(String checknumber) {
        this.checknumber = checknumber;
    }

    public List<Acchearingaidsaledetail> getAcchearingaidsaledetailList() {
        return acchearingaidsaledetailList;
    }

    public void setAcchearingaidsaledetailList(List<Acchearingaidsaledetail> acchearingaidsaledetailList) {
        this.acchearingaidsaledetailList = acchearingaidsaledetailList;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acchearingaidsalesummary)) {
            return false;
        }
        Acchearingaidsalesummary other = (Acchearingaidsalesummary) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Acchearingaidsalesummary[ id=" + id + " ]";
    }
    
}
