/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "prescription")
@NamedQueries({
    @NamedQuery(name = "Prescription.findAll", query = "SELECT p FROM Prescription p")})
public class Prescription implements Serializable {

    @Lob
    @Size(max = 2147483647)
    @Column(name = "FILEPATH")
    private String filepath;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "REMARKS")
    private String remarks;
    @Size(max = 45)
    @Column(name = "NEXTCHECKUP")
    private String nextcheckup;

    @Column(name = "ISSUEDATE")
    @Temporal(TemporalType.DATE)
    private Date issuedate;
    @Size(max = 45)
    @Column(name = "AGE")
    private String age;
    @Size(max = 45)
    @Column(name = "GENDER")
    private String gender;
    @Size(max = 255)
    @Column(name = "GENERIC1")
    private String generic1;
    @Size(max = 255)
    @Column(name = "BRAND1")
    private String brand1;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PREPARATION1")
    private String preparation1;
    @Size(max = 45)
    @Column(name = "QUANTITY1")
    private String quantity1;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DIRECTION1")
    private String direction1;
    @Size(max = 255)
    @Column(name = "GENERIC2")
    private String generic2;
    @Size(max = 255)
    @Column(name = "BRAND2")
    private String brand2;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PREPARATION2")
    private String preparation2;
    @Size(max = 45)
    @Column(name = "QUANTITY2")
    private String quantity2;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DIRECTION2")
    private String direction2;
    @Size(max = 45)
    @Column(name = "GENERIC3")
    private String generic3;
    @Size(max = 45)
    @Column(name = "BRAND3")
    private String brand3;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PREPARATION3")
    private String preparation3;
    @Size(max = 45)
    @Column(name = "QUANTITY3")
    private String quantity3;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DIRECTION3")
    private String direction3;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    public Prescription() {
    }

    public Prescription(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Prescription)) {
            return false;
        }
        Prescription other = (Prescription) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Prescription[ id=" + id + " ]";
    }

    public Date getIssuedate() {
        return issuedate;
    }

    public void setIssuedate(Date issuedate) {
        this.issuedate = issuedate;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGeneric1() {
        return generic1;
    }

    public void setGeneric1(String generic1) {
        this.generic1 = generic1;
    }

    public String getBrand1() {
        return brand1;
    }

    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    public String getPreparation1() {
        return preparation1;
    }

    public void setPreparation1(String preparation1) {
        this.preparation1 = preparation1;
    }

    public String getQuantity1() {
        return quantity1;
    }

    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    public String getDirection1() {
        return direction1;
    }

    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    public String getGeneric2() {
        return generic2;
    }

    public void setGeneric2(String generic2) {
        this.generic2 = generic2;
    }

    public String getBrand2() {
        return brand2;
    }

    public void setBrand2(String brand2) {
        this.brand2 = brand2;
    }

    public String getPreparation2() {
        return preparation2;
    }

    public void setPreparation2(String preparation2) {
        this.preparation2 = preparation2;
    }

    public String getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    public String getDirection2() {
        return direction2;
    }

    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    public String getGeneric3() {
        return generic3;
    }

    public void setGeneric3(String generic3) {
        this.generic3 = generic3;
    }

    public String getBrand3() {
        return brand3;
    }

    public void setBrand3(String brand3) {
        this.brand3 = brand3;
    }

    public String getPreparation3() {
        return preparation3;
    }

    public void setPreparation3(String preparation3) {
        this.preparation3 = preparation3;
    }

    public String getQuantity3() {
        return quantity3;
    }

    public void setQuantity3(String quantity3) {
        this.quantity3 = quantity3;
    }

    public String getDirection3() {
        return direction3;
    }

    public void setDirection3(String direction3) {
        this.direction3 = direction3;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNextcheckup() {
        return nextcheckup;
    }

    public void setNextcheckup(String nextcheckup) {
        this.nextcheckup = nextcheckup;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

}
