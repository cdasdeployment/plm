/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "patientchart")
@NamedQueries({
    @NamedQuery(name = "PatientChart.findAll", query = "SELECT p FROM PatientChart p")})
public class PatientChart implements Serializable {

    @Transient
    private String isShavingStr;
    @Transient
    private String isWaxingStr;
    @Transient
    private String isElectroStr;
    @Transient
    private String isPluckStr;
    @Transient
    private String isTweezeStr;
    @Transient
    private String isStringStr;
    @Transient
    private String isDepilatoriesStr;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "PRODUCTSUSED")
    private String productsused;

    @Size(max = 45)
    @Column(name = "COUGHCOLDS")
    private String coughcolds;
    @Column(name = "AGE")
    private Integer age;

    @Size(max = 45)
    @Column(name = "E_PROCEDUREDONEBY")
    private String eProceduredoneby;
    @Size(max = 45)
    @Column(name = "H_PROCEDUREDONEBY")
    private String hProceduredoneby;
    @Size(max = 45)
    @Column(name = "C_PROCEDUREDONEBY")
    private String cProceduredoneby;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARING_ASSESSMENT")
    private String hearingAssessment;
    @Size(max = 45)
    @Column(name = "PREVIOUSFACIALTREATMENT")
    private String previousfacialtreatment;
    @Size(max = 45)
    @Column(name = "CHEMICALPEEL")
    private String chemicalpeel;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SKINPRODUCTS")
    private String skinproducts;
    @Column(name = "IS_SHAVING")
    private Boolean isShaving;
    @Column(name = "IS_WAXING")
    private Boolean isWaxing;
    @Column(name = "IS_ELECTRO")
    private Boolean isElectro;
    @Column(name = "IS_PLUCK")
    private Boolean isPluck;
    @Column(name = "IS_TWEEZE")
    private Boolean isTweeze;
    @Column(name = "IS_STRING")
    private Boolean isString;
    @Column(name = "IS_DEPILATORIES")
    private Boolean isDepilatories;
    @Size(max = 45)
    @Column(name = "SUNSCREEN_USE")
    private String sunscreenUse;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SUNSCREEN_NOTES")
    private String sunscreenNotes;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DOCTORORDER")
    private String doctororder;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "LEFT_EAR_ASSESSMENT")
    private String leftEarAssessment;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "RIGHT_EAR_ASSESSMENT")
    private String rightEarAssessment;

    @Size(max = 45)
    @Column(name = "HEARING_LOSS_DURATION")
    private String hearingLossDuration;

    @Size(max = 45)
    @Column(name = "HEARING_LOSS_ONSET")
    private String hearingLossOnset;

    @Size(max = 45)
    @Column(name = "TYMP_VOLUME_RIGHT")
    private String tympVolumeRight;

    @Size(max = 45)
    @Column(name = "TYMP_TYPE_RIGHT")
    private String tympTypeRight;

    @Size(max = 45)
    @Column(name = "HEARING_AID_USE")
    private String hearingAidUse;

    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARING_AID_NOTE")
    private String hearingAidNote;

    @Size(max = 45)
    @Column(name = "HEARINGLOSSNOTICE")
    private String hearinglossnotice;
    @Size(max = 45)
    @Column(name = "BETTEREAR")
    private String betterear;
    @Size(max = 45)
    @Column(name = "NOISEEXPOSURE")
    private String noiseexposure;
    @Size(max = 45)
    @Column(name = "TINNITUS")
    private String tinnitus;
    @Size(max = 45)
    @Column(name = "TINNITUSEAR")
    private String tinnitusear;
    @Size(max = 45)
    @Column(name = "EARFAMILYHISTORY")
    private String earfamilyhistory;
    @Size(max = 45)
    @Column(name = "EARSURGERY")
    private String earsurgery;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "EARSURGERYNOTE")
    private String earsurgerynote;
    @Size(max = 45)
    @Column(name = "TYMP_STATIC_LEFT")
    private String tympStaticLeft;
    @Size(max = 45)
    @Column(name = "TYMP_STATIC_RIGHT")
    private String tympStaticRight;
    @Size(max = 45)
    @Column(name = "TYMP_MIDDLE_LEFT")
    private String tympMiddleLeft;
    @Size(max = 45)
    @Column(name = "TYMP_MIDDLE_RIGHT")
    private String tympMiddleRight;
    @Size(max = 45)
    @Column(name = "TYMP_VOLUME_LEFT")
    private String tympVolumeLeft;
    @Size(max = 45)
    @Column(name = "TYMP_TYPE_LEFT")
    private String tympTypeLeft;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT1")
    private String audioAcLeft1;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT2")
    private String audioAcLeft2;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT3")
    private String audioAcLeft3;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT4")
    private String audioAcLeft4;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT5")
    private String audioAcLeft5;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT6")
    private String audioAcLeft6;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT_AVG")
    private String audioAcLeftAvg;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT1")
    private String audioAcRight1;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT2")
    private String audioAcRight2;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT3")
    private String audioAcRight3;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT4")
    private String audioAcRight4;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT5")
    private String audioAcRight5;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT6")
    private String audioAcRight6;
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT_AVG")
    private String audioAcRightAvg;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT1")
    private String audioBcLeft1;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT2")
    private String audioBcLeft2;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT3")
    private String audioBcLeft3;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT4")
    private String audioBcLeft4;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT5")
    private String audioBcLeft5;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT6")
    private String audioBcLeft6;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT_AVG")
    private String audioBcLeftAvg;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT1")
    private String audioBcRight1;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT2")
    private String audioBcRight2;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT3")
    private String audioBcRight3;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT4")
    private String audioBcRight4;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT5")
    private String audioBcRight5;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT6")
    private String audioBcRight6;
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT_AVG")
    private String audioBcRightAvg;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARINGASSESSMENT")
    private String hearingassessment;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARINGRECOMMENDATION")
    private String hearingrecommendation;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 45)
    @Column(name = "HEIGHT")
    private String height;
    @Size(max = 45)
    @Column(name = "WEIGHT")
    private String weight;
    @Size(max = 45)
    @Column(name = "BLOODPRESSURE")
    private String bloodpressure;
    @Size(max = 45)
    @Column(name = "PULSE")
    private String pulse;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MAINTENANCEMEDS")
    private String maintenancemeds;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "RECENTMEDS")
    private String recentmeds;
    @Size(max = 45)
    @Column(name = "REFERREDBY")
    private String referredby;
    @Column(name = "RECORDDATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date recorddatetime;
    @Column(name = "DATERECORDED")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRecord;
    @Column(name = "DATEOPEN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOpen;
    @Column(name = "DATEUPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CHIEFCOMPLAINT")
    private String chiefcomplaint;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DIAGNOSIS")
    private String diagnosis;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ILLNESSHISTORY")
    private String illnesshistory;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PROCEDURES")
    private String procedures;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PHYSICALEXAM")
    private String physicalexam;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ADDITIONALNOTES")
    private String additionalNotes;
    @Column(name = "CHARTTYPE")
    private Integer charttype;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVE")
    private int active;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Client client;

    public PatientChart() {
    }

    public PatientChart(Integer id) {
        this.id = id;
    }

    public PatientChart(Integer id, int active) {
        this.id = id;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(String bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getMaintenancemeds() {
        return maintenancemeds;
    }

    public void setMaintenancemeds(String maintenancemeds) {
        this.maintenancemeds = maintenancemeds;
    }

    public String getRecentmeds() {
        return recentmeds;
    }

    public void setRecentmeds(String recentmeds) {
        this.recentmeds = recentmeds;
    }

    public String getReferredby() {
        return referredby;
    }

    public void setReferredby(String referredby) {
        this.referredby = referredby;
    }

    public Date getRecorddatetime() {
        return recorddatetime;
    }

    public void setRecorddatetime(Date recorddatetime) {
        this.recorddatetime = recorddatetime;
    }

    public String getChiefcomplaint() {
        return chiefcomplaint;
    }

    public void setChiefcomplaint(String chiefcomplaint) {
        this.chiefcomplaint = chiefcomplaint;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getIllnesshistory() {
        return illnesshistory;
    }

    public void setIllnesshistory(String illnesshistory) {
        this.illnesshistory = illnesshistory;
    }

    public String getProcedures() {
        return procedures;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    public String getPhysicalexam() {
        return physicalexam;
    }

    public void setPhysicalexam(String physicalexam) {
        this.physicalexam = physicalexam;
    }

    public Integer getCharttype() {
        return charttype;
    }

    public void setCharttype(Integer charttype) {
        this.charttype = charttype;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PatientChart)) {
            return false;
        }
        PatientChart other = (PatientChart) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.PatientChart[ id=" + id + " ]";
    }

    /**
     * @return the additionalNotes
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * @param additionalNotes the additionalNotes to set
     */
    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    /**
     * @return the dateUpdate
     */
    public Date getDateUpdate() {
        return dateUpdate;
    }

    /**
     * @param dateUpdate the dateUpdate to set
     */
    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    /**
     * @return the dateOpen
     */
    public Date getDateOpen() {
        return dateOpen;
    }

    /**
     * @param dateOpen the dateOpen to set
     */
    public void setDateOpen(Date dateOpen) {
        this.dateOpen = dateOpen;
    }

    /**
     * @return the dateRecord
     */
    public Date getDateRecord() {
        return dateRecord;
    }

    /**
     * @param dateRecord the dateRecord to set
     */
    public void setDateRecord(Date dateRecord) {
        this.dateRecord = dateRecord;
    }

    public String getHearinglossnotice() {
        return hearinglossnotice;
    }

    public void setHearinglossnotice(String hearinglossnotice) {
        this.hearinglossnotice = hearinglossnotice;
    }

    public String getBetterear() {
        return betterear;
    }

    public void setBetterear(String betterear) {
        this.betterear = betterear;
    }

    public String getNoiseexposure() {
        return noiseexposure;
    }

    public void setNoiseexposure(String noiseexposure) {
        this.noiseexposure = noiseexposure;
    }

    public String getTinnitus() {
        return tinnitus;
    }

    public void setTinnitus(String tinnitus) {
        this.tinnitus = tinnitus;
    }

    public String getTinnitusear() {
        return tinnitusear;
    }

    public void setTinnitusear(String tinnitusear) {
        this.tinnitusear = tinnitusear;
    }

    public String getEarfamilyhistory() {
        return earfamilyhistory;
    }

    public void setEarfamilyhistory(String earfamilyhistory) {
        this.earfamilyhistory = earfamilyhistory;
    }

    public String getEarsurgery() {
        return earsurgery;
    }

    public void setEarsurgery(String earsurgery) {
        this.earsurgery = earsurgery;
    }

    public String getEarsurgerynote() {
        return earsurgerynote;
    }

    public void setEarsurgerynote(String earsurgerynote) {
        this.earsurgerynote = earsurgerynote;
    }

    public String getTympStaticLeft() {
        return tympStaticLeft;
    }

    public void setTympStaticLeft(String tympStaticLeft) {
        this.tympStaticLeft = tympStaticLeft;
    }

    public String getTympStaticRight() {
        return tympStaticRight;
    }

    public void setTympStaticRight(String tympStaticRight) {
        this.tympStaticRight = tympStaticRight;
    }

    public String getTympMiddleLeft() {
        return tympMiddleLeft;
    }

    public void setTympMiddleLeft(String tympMiddleLeft) {
        this.tympMiddleLeft = tympMiddleLeft;
    }

    public String getTympMiddleRight() {
        return tympMiddleRight;
    }

    public void setTympMiddleRight(String tympMiddleRight) {
        this.tympMiddleRight = tympMiddleRight;
    }

    public String getTympVolumeLeft() {
        return tympVolumeLeft;
    }

    public void setTympVolumeLeft(String tympVolumeLeft) {
        this.tympVolumeLeft = tympVolumeLeft;
    }

    public String getTympTypeLeft() {
        return tympTypeLeft;
    }

    public void setTympTypeLeft(String tympTypeLeft) {
        this.tympTypeLeft = tympTypeLeft;
    }

    public String getAudioAcLeft1() {
        return audioAcLeft1;
    }

    public void setAudioAcLeft1(String audioAcLeft1) {
        this.audioAcLeft1 = audioAcLeft1;
    }

    public String getAudioAcLeft2() {
        return audioAcLeft2;
    }

    public void setAudioAcLeft2(String audioAcLeft2) {
        this.audioAcLeft2 = audioAcLeft2;
    }

    public String getAudioAcLeft3() {
        return audioAcLeft3;
    }

    public void setAudioAcLeft3(String audioAcLeft3) {
        this.audioAcLeft3 = audioAcLeft3;
    }

    public String getAudioAcLeft4() {
        return audioAcLeft4;
    }

    public void setAudioAcLeft4(String audioAcLeft4) {
        this.audioAcLeft4 = audioAcLeft4;
    }

    public String getAudioAcLeft5() {
        return audioAcLeft5;
    }

    public void setAudioAcLeft5(String audioAcLeft5) {
        this.audioAcLeft5 = audioAcLeft5;
    }

    public String getAudioAcLeft6() {
        return audioAcLeft6;
    }

    public void setAudioAcLeft6(String audioAcLeft6) {
        this.audioAcLeft6 = audioAcLeft6;
    }

    public String getAudioAcLeftAvg() {
        return audioAcLeftAvg;
    }

    public void setAudioAcLeftAvg(String audioAcLeftAvg) {
        this.audioAcLeftAvg = audioAcLeftAvg;
    }

    public String getAudioAcRight1() {
        return audioAcRight1;
    }

    public void setAudioAcRight1(String audioAcRight1) {
        this.audioAcRight1 = audioAcRight1;
    }

    public String getAudioAcRight2() {
        return audioAcRight2;
    }

    public void setAudioAcRight2(String audioAcRight2) {
        this.audioAcRight2 = audioAcRight2;
    }

    public String getAudioAcRight3() {
        return audioAcRight3;
    }

    public void setAudioAcRight3(String audioAcRight3) {
        this.audioAcRight3 = audioAcRight3;
    }

    public String getAudioAcRight4() {
        return audioAcRight4;
    }

    public void setAudioAcRight4(String audioAcRight4) {
        this.audioAcRight4 = audioAcRight4;
    }

    public String getAudioAcRight5() {
        return audioAcRight5;
    }

    public void setAudioAcRight5(String audioAcRight5) {
        this.audioAcRight5 = audioAcRight5;
    }

    public String getAudioAcRight6() {
        return audioAcRight6;
    }

    public void setAudioAcRight6(String audioAcRight6) {
        this.audioAcRight6 = audioAcRight6;
    }

    public String getAudioAcRightAvg() {
        return audioAcRightAvg;
    }

    public void setAudioAcRightAvg(String audioAcRightAvg) {
        this.audioAcRightAvg = audioAcRightAvg;
    }

    public String getAudioBcLeft1() {
        return audioBcLeft1;
    }

    public void setAudioBcLeft1(String audioBcLeft1) {
        this.audioBcLeft1 = audioBcLeft1;
    }

    public String getAudioBcLeft2() {
        return audioBcLeft2;
    }

    public void setAudioBcLeft2(String audioBcLeft2) {
        this.audioBcLeft2 = audioBcLeft2;
    }

    public String getAudioBcLeft3() {
        return audioBcLeft3;
    }

    public void setAudioBcLeft3(String audioBcLeft3) {
        this.audioBcLeft3 = audioBcLeft3;
    }

    public String getAudioBcLeft4() {
        return audioBcLeft4;
    }

    public void setAudioBcLeft4(String audioBcLeft4) {
        this.audioBcLeft4 = audioBcLeft4;
    }

    public String getAudioBcLeft5() {
        return audioBcLeft5;
    }

    public void setAudioBcLeft5(String audioBcLeft5) {
        this.audioBcLeft5 = audioBcLeft5;
    }

    public String getAudioBcLeft6() {
        return audioBcLeft6;
    }

    public void setAudioBcLeft6(String audioBcLeft6) {
        this.audioBcLeft6 = audioBcLeft6;
    }

    public String getAudioBcLeftAvg() {
        return audioBcLeftAvg;
    }

    public void setAudioBcLeftAvg(String audioBcLeftAvg) {
        this.audioBcLeftAvg = audioBcLeftAvg;
    }

    public String getAudioBcRight1() {
        return audioBcRight1;
    }

    public void setAudioBcRight1(String audioBcRight1) {
        this.audioBcRight1 = audioBcRight1;
    }

    public String getAudioBcRight2() {
        return audioBcRight2;
    }

    public void setAudioBcRight2(String audioBcRight2) {
        this.audioBcRight2 = audioBcRight2;
    }

    public String getAudioBcRight3() {
        return audioBcRight3;
    }

    public void setAudioBcRight3(String audioBcRight3) {
        this.audioBcRight3 = audioBcRight3;
    }

    public String getAudioBcRight4() {
        return audioBcRight4;
    }

    public void setAudioBcRight4(String audioBcRight4) {
        this.audioBcRight4 = audioBcRight4;
    }

    public String getAudioBcRight5() {
        return audioBcRight5;
    }

    public void setAudioBcRight5(String audioBcRight5) {
        this.audioBcRight5 = audioBcRight5;
    }

    public String getAudioBcRight6() {
        return audioBcRight6;
    }

    public void setAudioBcRight6(String audioBcRight6) {
        this.audioBcRight6 = audioBcRight6;
    }

    public String getAudioBcRightAvg() {
        return audioBcRightAvg;
    }

    public void setAudioBcRightAvg(String audioBcRightAvg) {
        this.audioBcRightAvg = audioBcRightAvg;
    }

    public String getHearingassessment() {
        return hearingassessment;
    }

    public void setHearingassessment(String hearingassessment) {
        this.hearingassessment = hearingassessment;
    }

    public String getHearingrecommendation() {
        return hearingrecommendation;
    }

    public void setHearingrecommendation(String hearingrecommendation) {
        this.hearingrecommendation = hearingrecommendation;
    }

    public String getTympTypeRight() {
        return tympTypeRight;
    }

    public void setTympTypeRight(String tympTypeRight) {
        this.tympTypeRight = tympTypeRight;
    }

    public String getTympVolumeRight() {
        return tympVolumeRight;
    }

    public void setTympVolumeRight(String tympVolumeRight) {
        this.tympVolumeRight = tympVolumeRight;
    }

    /**
     * @return the hearingAidUse
     */
    public String getHearingAidUse() {
        return hearingAidUse;
    }

    /**
     * @param hearingAidUse the hearingAidUse to set
     */
    public void setHearingAidUse(String hearingAidUse) {
        this.hearingAidUse = hearingAidUse;
    }

    /**
     * @return the hearingAidNote
     */
    public String getHearingAidNote() {
        return hearingAidNote;
    }

    /**
     * @param hearingAidNote the hearingAidNote to set
     */
    public void setHearingAidNote(String hearingAidNote) {
        this.hearingAidNote = hearingAidNote;
    }

    /**
     * @return the hearingLossDuration
     */
    public String getHearingLossDuration() {
        return hearingLossDuration;
    }

    /**
     * @param hearingLossDuration the hearingLossDuration to set
     */
    public void setHearingLossDuration(String hearingLossDuration) {
        this.hearingLossDuration = hearingLossDuration;
    }

    /**
     * @return the hearingLossOnset
     */
    public String getHearingLossOnset() {
        return hearingLossOnset;
    }

    /**
     * @param hearingLossOnset the hearingLossOnset to set
     */
    public void setHearingLossOnset(String hearingLossOnset) {
        this.hearingLossOnset = hearingLossOnset;
    }

    public String getLeftEarAssessment() {
        return leftEarAssessment;
    }

    public void setLeftEarAssessment(String leftEarAssessment) {
        this.leftEarAssessment = leftEarAssessment;
    }

    public String getRightEarAssessment() {
        return rightEarAssessment;
    }

    public void setRightEarAssessment(String rightEarAssessment) {
        this.rightEarAssessment = rightEarAssessment;
    }

    public String getHearingAssessment() {
        return hearingAssessment;
    }

    public void setHearingAssessment(String hearingAssessment) {
        this.hearingAssessment = hearingAssessment;
    }

    public String getPreviousfacialtreatment() {
        return previousfacialtreatment;
    }

    public void setPreviousfacialtreatment(String previousfacialtreatment) {
        this.previousfacialtreatment = previousfacialtreatment;
    }

    public String getChemicalpeel() {
        return chemicalpeel;
    }

    public void setChemicalpeel(String chemicalpeel) {
        this.chemicalpeel = chemicalpeel;
    }

    public String getSkinproducts() {
        return skinproducts;
    }

    public void setSkinproducts(String skinproducts) {
        this.skinproducts = skinproducts;
    }

    public Boolean getIsShaving() {
        return isShaving;
    }

    public void setIsShaving(Boolean isShaving) {
        this.isShaving = isShaving;
    }

    public Boolean getIsWaxing() {
        return isWaxing;
    }

    public void setIsWaxing(Boolean isWaxing) {
        this.isWaxing = isWaxing;
    }

    public Boolean getIsElectro() {
        return isElectro;
    }

    public void setIsElectro(Boolean isElectro) {
        this.isElectro = isElectro;
    }

    public Boolean getIsPluck() {
        return isPluck;
    }

    public void setIsPluck(Boolean isPluck) {
        this.isPluck = isPluck;
    }

    public Boolean getIsTweeze() {
        return isTweeze;
    }

    public void setIsTweeze(Boolean isTweeze) {
        this.isTweeze = isTweeze;
    }

    public Boolean getIsString() {
        return isString;
    }

    public void setIsString(Boolean isString) {
        this.isString = isString;
    }

    public Boolean getIsDepilatories() {
        return isDepilatories;
    }

    public void setIsDepilatories(Boolean isDepilatories) {
        this.isDepilatories = isDepilatories;
    }

    public String getSunscreenUse() {
        return sunscreenUse;
    }

    public void setSunscreenUse(String sunscreenUse) {
        this.sunscreenUse = sunscreenUse;
    }

    public String getSunscreenNotes() {
        return sunscreenNotes;
    }

    public void setSunscreenNotes(String sunscreenNotes) {
        this.sunscreenNotes = sunscreenNotes;
    }

    public String getDoctororder() {
        return doctororder;
    }

    public void setDoctororder(String doctororder) {
        this.doctororder = doctororder;
    }

    public String getEProceduredoneby() {
        return eProceduredoneby;
    }

    public void setEProceduredoneby(String eProceduredoneby) {
        this.eProceduredoneby = eProceduredoneby;
    }

    public String getHProceduredoneby() {
        return hProceduredoneby;
    }

    public void setHProceduredoneby(String hProceduredoneby) {
        this.hProceduredoneby = hProceduredoneby;
    }

    public String getCProceduredoneby() {
        return cProceduredoneby;
    }

    public void setCProceduredoneby(String cProceduredoneby) {
        this.cProceduredoneby = cProceduredoneby;
    }

    public String getCoughcolds() {
        return coughcolds;
    }

    public void setCoughcolds(String coughcolds) {
        this.coughcolds = coughcolds;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getProductsused() {
        return productsused;
    }

    public void setProductsused(String productsused) {
        this.productsused = productsused;
    }

    /**
     * @return the isShavingStr
     */
    public String getIsShavingStr() {
        return isShavingStr;
    }

    /**
     * @param isShavingStr the isShavingStr to set
     */
    public void setIsShavingStr(String isShavingStr) {
        this.isShavingStr = isShavingStr;
    }

    /**
     * @return the isWaxingStr
     */
    public String getIsWaxingStr() {
        return isWaxingStr;
    }

    /**
     * @param isWaxingStr the isWaxingStr to set
     */
    public void setIsWaxingStr(String isWaxingStr) {
        this.isWaxingStr = isWaxingStr;
    }

    /**
     * @return the isElectroStr
     */
    public String getIsElectroStr() {
        if (isElectro == null) {
            isElectroStr = "";
        } else if (isElectro.booleanValue() == false) {
            isElectroStr = "None";
        } else if (isElectro.booleanValue() == true) {
            isElectroStr = "Yes";
        }

        return isElectroStr;
    }

    /**
     * @param isElectroStr the isElectroStr to set
     */
    public void setIsElectroStr(String isElectroStr) {
        this.isElectroStr = isElectroStr;
    }

    /**
     * @return the isPluckStr
     */
    public String getIsPluckStr() {
        if (isPluck == null) {
            isPluckStr = "";
        } else if (isPluck.booleanValue() == false) {
            isPluckStr = "None";
        } else if (isPluck.booleanValue() == true) {
            isPluckStr = "Yes";
        }
        return isPluckStr;
    }

    /**
     * @param isPluckStr the isPluckStr to set
     */
    public void setIsPluckStr(String isPluckStr) {
        this.isPluckStr = isPluckStr;
    }

    /**
     * @return the isTweezeStr
     */
    public String getIsTweezeStr() {
        if (isTweeze == null) {
            isTweezeStr = "";
        } else if (isTweeze.booleanValue() == false) {
            isTweezeStr = "None";
        } else if (isTweeze.booleanValue() == true) {
            isPluckStr = "Yes";
        }
        return isTweezeStr;
    }

    /**
     * @param isTweezeStr the isTweezeStr to set
     */
    public void setIsTweezeStr(String isTweezeStr) {
        this.isTweezeStr = isTweezeStr;
    }

    /**
     * @return the isStringStr
     */
    public String getIsStringStr() {
        if (isString == null) {
            isStringStr = "";
        } else if (isString.booleanValue() == false) {
            isStringStr = "None";
        } else if (isString.booleanValue() == true) {
            isStringStr = "Yes";
        }
        return isStringStr;
    }

    /**
     * @param isStringStr the isStringStr to set
     */
    public void setIsStringStr(String isStringStr) {
        this.isStringStr = isStringStr;
    }

    /**
     * @return the isDepilatoriesStr
     */
    public String getIsDepilatoriesStr() {
        if (isDepilatories == null) {
            isDepilatoriesStr = "";
        } else if (isDepilatories.booleanValue() == false) {
            isDepilatoriesStr = "None";
        } else if (isDepilatories.booleanValue() == true) {
            isStringStr = "Yes";
        }
        return isDepilatoriesStr;
    }

    /**
     * @param isDepilatoriesStr the isDepilatoriesStr to set
     */
    public void setIsDepilatoriesStr(String isDepilatoriesStr) {
        this.isDepilatoriesStr = isDepilatoriesStr;
    }

}
