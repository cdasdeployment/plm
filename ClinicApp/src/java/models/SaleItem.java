/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author apple
 */
public interface SaleItem {
    
    public String getItemDescription();
    public Double getItemPrice();
    public Integer getItemStock();
    public String getItemCategory();
    public String getItemModelPreparation();
    
}
