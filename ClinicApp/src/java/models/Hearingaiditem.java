/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "hearingaiditem")
@NamedQueries({
    @NamedQuery(name = "Hearingaiditem.findAll", query = "SELECT h FROM Hearingaiditem h")})
public class Hearingaiditem implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hearingaid", fetch = FetchType.LAZY)
    private List<Hearingaidsaledetail> hearingaidsaledetailList;

    @Size(max = 45)
    @Column(name = "PURCHASEDATE")
    private String purchasedate;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "SERIALNUMBER")
    private String serialnumber;
    @Size(max = 45)
    @Column(name = "MODEL")
    private String model;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BUYING")
    private Double buying;
    @Column(name = "SELLING")
    private Double selling;
    @Size(max = 45)
    @Column(name = "SUPPLIER")
    private String supplier;
    @Column(name = "STOCKS")
    private Integer stocks;
    @Column(name = "REORDER")
    private Integer reorder;
    @OneToMany(mappedBy = "hearingaid", fetch = FetchType.LAZY)
    private List<Hearingaidinventory> hearingaidinventoryList;

    public Hearingaiditem() {
    }

    public Hearingaiditem(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSerialnumber() {
        return serialnumber;
    }

    public void setSerialnumber(String serialnumber) {
        this.serialnumber = serialnumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getBuying() {
        return buying;
    }

    public void setBuying(Double buying) {
        this.buying = buying;
    }

    public Double getSelling() {
        return selling;
    }

    public void setSelling(Double selling) {
        this.selling = selling;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public Integer getStocks() {
        return stocks;
    }

    public void setStocks(Integer stocks) {
        this.stocks = stocks;
    }

    public Integer getReorder() {
        return reorder;
    }

    public void setReorder(Integer reorder) {
        this.reorder = reorder;
    }

    public List<Hearingaidinventory> getHearingaidinventoryList() {
        return hearingaidinventoryList;
    }

    public void setHearingaidinventoryList(List<Hearingaidinventory> hearingaidinventoryList) {
        this.hearingaidinventoryList = hearingaidinventoryList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hearingaiditem)) {
            return false;
        }
        Hearingaiditem other = (Hearingaiditem) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Hearingaiditem[ id=" + id + " ]";
    }

    public String getPurchasedate() {
        return purchasedate;
    }

    public void setPurchasedate(String purchasedate) {
        this.purchasedate = purchasedate;
    }

    public List<Hearingaidsaledetail> getHearingaidsaledetailList() {
        return hearingaidsaledetailList;
    }

    public void setHearingaidsaledetailList(List<Hearingaidsaledetail> hearingaidsaledetailList) {
        this.hearingaidsaledetailList = hearingaidsaledetailList;
    }
    
}
