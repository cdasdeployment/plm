/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "cdrugsaledetail")
@NamedQueries({
    @NamedQuery(name = "Cdrugsaledetail.findAll", query = "SELECT c FROM Cdrugsaledetail c")})
public class Cdrugsaledetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 11)
    @Column(name = "TRANSACTIONCODE")
    private String transactioncode;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "QUANTITY")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PRICE")
    private Double price;
    @Column(name = "TOTAL")
    private Double total;
    @Column(name = "ITEMDISCOUNT")
    private Double itemdiscount;
    @Column(name = "ITEMDISCOUNTAMOUNT")
    private Double itemdiscountamount;
    @Size(max = 45)
    @Column(name = "HMO")
    private String hmo;
    @Column(name = "HMODISCOUNT")
    private Double hmodiscount;
    @Column(name = "HMODISCOUNTAMOUNT")
    private Double hmodiscountamount;
    @Column(name = "CLIENT")
    private Integer client;
    @Size(max = 255)
    @Column(name = "CLIENTNAME")
    private String clientname;
    @Column(name = "SERVICEDATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date servicedatetime;
    @JoinColumn(name = "SALESUMMARY", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cdrugsalesummary salesummary;
    @JoinColumn(name = "CDRUG", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CMedicineitem cdrug;

    public Cdrugsaledetail() {
    }

    public Cdrugsaledetail(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTransactioncode() {
        return transactioncode;
    }

    public void setTransactioncode(String transactioncode) {
        this.transactioncode = transactioncode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getItemdiscount() {
        return itemdiscount;
    }

    public void setItemdiscount(Double itemdiscount) {
        this.itemdiscount = itemdiscount;
    }

    public Double getItemdiscountamount() {
        return itemdiscountamount;
    }

    public void setItemdiscountamount(Double itemdiscountamount) {
        this.itemdiscountamount = itemdiscountamount;
    }

    public String getHmo() {
        return hmo;
    }

    public void setHmo(String hmo) {
        this.hmo = hmo;
    }

    public Double getHmodiscount() {
        return hmodiscount;
    }

    public void setHmodiscount(Double hmodiscount) {
        this.hmodiscount = hmodiscount;
    }

    public Double getHmodiscountamount() {
        return hmodiscountamount;
    }

    public void setHmodiscountamount(Double hmodiscountamount) {
        this.hmodiscountamount = hmodiscountamount;
    }

    public Integer getClient() {
        return client;
    }

    public void setClient(Integer client) {
        this.client = client;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public Date getServicedatetime() {
        return servicedatetime;
    }

    public void setServicedatetime(Date servicedatetime) {
        this.servicedatetime = servicedatetime;
    }

    public Cdrugsalesummary getSalesummary() {
        return salesummary;
    }

    public void setSalesummary(Cdrugsalesummary salesummary) {
        this.salesummary = salesummary;
    }

    public CMedicineitem getCdrug() {
        return cdrug;
    }

    public void setCdrug(CMedicineitem cdrug) {
        this.cdrug = cdrug;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cdrugsaledetail)) {
            return false;
        }
        Cdrugsaledetail other = (Cdrugsaledetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Cdrugsaledetail[ id=" + id + " ]";
    }
    
}
