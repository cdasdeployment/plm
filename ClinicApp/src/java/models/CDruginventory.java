/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "cdruginventory")
@NamedQueries({
    @NamedQuery(name = "CDruginventory.findAll", query = "SELECT c FROM CDruginventory c")})
public class CDruginventory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "REMARKS")
    private String remarks;
    @Column(name = "SERVICETIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date servicetimestamp;
    @Column(name = "PULLOUT")
    private Integer pullout;
    @Column(name = "PURCHASE")
    private Integer purchase;
    @Column(name = "RETURNED")
    private Integer returned;
    @Column(name = "UPDATED")
    private Integer updated;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "BUYING")
    private Double buying;
    @Column(name = "SELLING")
    private Double selling;
    @JoinColumn(name = "DRUG", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private CMedicineitem drug;

    public CDruginventory() {
    }

    public CDruginventory(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Date getServicetimestamp() {
        return servicetimestamp;
    }

    public void setServicetimestamp(Date servicetimestamp) {
        this.servicetimestamp = servicetimestamp;
    }

    public Integer getPullout() {
        return pullout;
    }

    public void setPullout(Integer pullout) {
        this.pullout = pullout;
    }

    public Integer getPurchase() {
        return purchase;
    }

    public void setPurchase(Integer purchase) {
        this.purchase = purchase;
    }

    public Integer getReturned() {
        return returned;
    }

    public void setReturned(Integer returned) {
        this.returned = returned;
    }

    public Integer getUpdated() {
        return updated;
    }

    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    public Double getBuying() {
        return buying;
    }

    public void setBuying(Double buying) {
        this.buying = buying;
    }

    public Double getSelling() {
        return selling;
    }

    public void setSelling(Double selling) {
        this.selling = selling;
    }

    public CMedicineitem getDrug() {
        return drug;
    }

    public void setDrug(CMedicineitem drug) {
        this.drug = drug;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CDruginventory)) {
            return false;
        }
        CDruginventory other = (CDruginventory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Cdruginventory[ id=" + id + " ]";
    }
    
}
