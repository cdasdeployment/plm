/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "chartdetails")
@NamedQueries({
    @NamedQuery(name = "ChartDetails.findAll", query = "SELECT c FROM ChartDetails c")})
public class ChartDetails implements Serializable {

    @Size(max = 45)
    @Column(name = "GENERIC4")
    private String generic4 = "";
    @Size(max = 45)
    @Column(name = "GENERIC5")
    private String generic5 = "";
    @Size(max = 45)
    @Column(name = "GENERIC6")
    private String generic6 = "";
    @Size(max = 45)
    @Column(name = "BRAND4")
    private String brand4 = "";
    @Size(max = 45)
    @Column(name = "BRAND5")
    private String brand5 = "";
    @Size(max = 45)
    @Column(name = "BRAND6")
    private String brand6 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION4")
    private String preparation4 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION5")
    private String preparation5 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION6")
    private String preparation6 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY4")
    private String quantity4 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY5")
    private String quantity5 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY6")
    private String quantity6 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION4")
    private String direction4 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION5")
    private String direction5 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION6")
    private String direction6 = "";

    @Lob
    @Size(max = 2147483647)
    @Column(name = "ADMITTINGORDERS2")
    private String admittingorders2 = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ADMITTINGORDERS1")
    private String admittingorders1 = "";
    @Column(name = "ADMITTINGDATE1")
    @Temporal(TemporalType.DATE)
    private Date admittingDate1 = new Date();
    @Column(name = "ADMITTINGDATE2")
    @Temporal(TemporalType.DATE)
    private Date admittingDate2 = new Date();
    @Size(max = 512)
    @Column(name = "MEDCERTMESSAGE")
    private String medcertMessage = "";
    @Size(max = 8)
    @Column(name = "CHARTCODE")
    private String chartCode = "";
    @Size(max = 45)
    @Column(name = "GENERIC1")
    private String generic1 = "";
    @Size(max = 45)
    @Column(name = "GENERIC2")
    private String generic2 = "";
    @Size(max = 45)
    @Column(name = "GENERIC3")
    private String generic3 = "";
    @Size(max = 45)
    @Column(name = "BRAND1")
    private String brand1 = "";
    @Size(max = 45)
    @Column(name = "BRAND2")
    private String brand2 = "";
    @Size(max = 45)
    @Column(name = "BRAND3")
    private String brand3 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION1")
    private String preparation1 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION2")
    private String preparation2 = "";
    @Size(max = 45)
    @Column(name = "PREPARATION3")
    private String preparation3 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY1")
    private String quantity1 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY2")
    private String quantity2 = "";
    @Size(max = 45)
    @Column(name = "QUANTITY3")
    private String quantity3 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION1")
    private String direction1 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION2")
    private String direction2 = "";
    @Size(max = 45)
    @Column(name = "DIRECTION3")
    private String direction3 = "";

    @Size(max = 45)
    @Column(name = "MGENERIC1")
    private String mgeneric1 = "";
    @Size(max = 45)
    @Column(name = "MGENERIC2")
    private String mgeneric2 = "";
    @Size(max = 45)
    @Column(name = "MGENERIC3")
    private String mgeneric3 = "";
    @Size(max = 45)
    @Column(name = "MBRAND1")
    private String mbrand1 = "";
    @Size(max = 45)
    @Column(name = "MBRAND2")
    private String mbrand2 = "";
    @Size(max = 45)
    @Column(name = "MBRAND3")
    private String mbrand3 = "";
    @Size(max = 45)
    @Column(name = "MPREPARATION1")
    private String mpreparation1 = "";
    @Size(max = 45)
    @Column(name = "MPREPARATION2")
    private String mpreparation2 = "";
    @Size(max = 45)
    @Column(name = "MPREPARATION3")
    private String mpreparation3 = "";
    @Size(max = 45)
    @Column(name = "MQUANTITY1")
    private String mquantity1 = "";
    @Size(max = 45)
    @Column(name = "MQUANTITY2")
    private String mquantity2 = "";
    @Size(max = 45)
    @Column(name = "MQUANTITY3")
    private String mquantity3 = "";
    @Size(max = 45)
    @Column(name = "MDIRECTION1")
    private String mdirection1 = "";
    @Size(max = 45)
    @Column(name = "MDIRECTION2")
    private String mdirection2 = "";
    @Size(max = 45)
    @Column(name = "MDIRECTION3")
    private String mdirection3 = "";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private int id;
    @Size(max = 45)
    @Column(name = "HEIGHT")
    private String height = "";
    @Size(max = 45)
    @Column(name = "WEIGHT")
    private String weight = "";
    @Size(max = 45)
    @Column(name = "BLOODPRESSURE")
    private String bloodpressure = "";
    @Size(max = 45)
    @Column(name = "PULSE")
    private String pulse = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MAINTENANCEMEDS")
    private String maintenancemeds = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "RECENTMEDS")
    private String recentmeds = "";
    @Size(max = 45)
    @Column(name = "REFERREDBY")
    private String referredby = "";
    @Column(name = "RECORDDATETIME")
    @Temporal(TemporalType.DATE)
    private Date recorddatetime = new Date();
    @Column(name = "NEXTCHECKUP")
    @Temporal(TemporalType.DATE)
    private Date nextcheckup;
    @Column(name = "LABREQUESTDATE")
    @Temporal(TemporalType.DATE)
    private Date labrequestDate = new Date();
    @Column(name = "ADMITTINGDATE")
    @Temporal(TemporalType.DATE)
    private Date admittingDate = new Date();
    @Column(name = "REFERRALDATE")
    @Temporal(TemporalType.DATE)
    private Date referralDate = new Date();
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CHIEFCOMPLAINT")
    private String chiefcomplaint = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DIAGNOSIS")
    private String diagnosis = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PROCEDURES")
    private String procedures = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PHYSICALEXAM")
    private String physicalexam = "";
    @Column(name = "CHARTTYPE")
    private int charttype = 1;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ADDITIONALNOTES")
    private String additionalnotes = "";
    @Basic(optional = false)
    @NotNull
    @Column(name = "ACTIVE")
    private int active = 1;
    @Column(name = "DATERECORDED")
    @Temporal(TemporalType.DATE)
    private Date daterecorded = new Date();
    @Column(name = "DATEOPEN")
    @Temporal(TemporalType.DATE)
    private Date dateopen = new Date();
    @Column(name = "DATEUPDATE")
    @Temporal(TemporalType.DATE)
    private Date dateupdate = new Date();
    @Size(max = 45)
    @Column(name = "HEARINGLOSSNOTICE")
    private String hearinglossnotice = "";
    @Size(max = 45)
    @Column(name = "BETTEREAR")
    private String betterear = "";
    @Size(max = 45)
    @Column(name = "NOISEEXPOSURE")
    private String noiseexposure = "";
    @Size(max = 45)
    @Column(name = "TINNITUS")
    private String tinnitus = "";
    @Size(max = 45)
    @Column(name = "TINNITUSEAR")
    private String tinnitusear = "";
    @Size(max = 45)
    @Column(name = "EARFAMILYHISTORY")
    private String earfamilyhistory = "";
    @Size(max = 45)
    @Column(name = "EARSURGERY")
    private String earsurgery = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "EARSURGERYNOTE")
    private String earsurgerynote = "";
    @Size(max = 45)
    @Column(name = "TYMP_STATIC_LEFT")
    private String tympStaticLeft = "";
    @Size(max = 45)
    @Column(name = "TYMP_STATIC_RIGHT")
    private String tympStaticRight = "";
    @Size(max = 45)
    @Column(name = "TYMP_MIDDLE_LEFT")
    private String tympMiddleLeft = "";
    @Size(max = 45)
    @Column(name = "TYMP_MIDDLE_RIGHT")
    private String tympMiddleRight = "";
    @Size(max = 45)
    @Column(name = "TYMP_VOLUME_LEFT")
    private String tympVolumeLeft = "";
    @Size(max = 45)
    @Column(name = "TYMP_VOLUME_RIGHT")
    private String tympVolumeRight = "";
    @Size(max = 45)
    @Column(name = "TYMP_TYPE_LEFT")
    private String tympTypeLeft = "";
    @Size(max = 45)
    @Column(name = "TYMP_TYPE_RIGHT")
    private String tympTypeRight = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT1")
    private String audioAcLeft1 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT2")
    private String audioAcLeft2 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT3")
    private String audioAcLeft3 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT4")
    private String audioAcLeft4 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT5")
    private String audioAcLeft5 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT6")
    private String audioAcLeft6 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_LEFT_AVG")
    private String audioAcLeftAvg = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT1")
    private String audioAcRight1 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT2")
    private String audioAcRight2 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT3")
    private String audioAcRight3 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT4")
    private String audioAcRight4 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT5")
    private String audioAcRight5 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT6")
    private String audioAcRight6 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_AC_RIGHT_AVG")
    private String audioAcRightAvg = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT1")
    private String audioBcLeft1 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT2")
    private String audioBcLeft2 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT3")
    private String audioBcLeft3 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT4")
    private String audioBcLeft4 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT5")
    private String audioBcLeft5 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT6")
    private String audioBcLeft6 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_LEFT_AVG")
    private String audioBcLeftAvg = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT1")
    private String audioBcRight1 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT2")
    private String audioBcRight2 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT3")
    private String audioBcRight3 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT4")
    private String audioBcRight4 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT5")
    private String audioBcRight5 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT6")
    private String audioBcRight6 = "";
    @Size(max = 45)
    @Column(name = "AUDIO_BC_RIGHT_AVG")
    private String audioBcRightAvg = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARINGASSESSMENT")
    private String hearingassessment = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARINGRECOMMENDATION")
    private String hearingrecommendation = "";
    @Size(max = 45)
    @Column(name = "HEARING_AID_USE")
    private String hearingAidUse = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARING_AID_NOTE")
    private String hearingAidNote = "";
    @Size(max = 45)
    @Column(name = "HEARING_LOSS_DURATION")
    private String hearingLossDuration = "";
    @Size(max = 45)
    @Column(name = "HEARING_LOSS_ONSET")
    private String hearingLossOnset = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "LEFT_EAR_ASSESSMENT")
    private String leftEarAssessment = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "RIGHT_EAR_ASSESSMENT")
    private String rightEarAssessment = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEARING_ASSESSMENT")
    private String hearingAssessment = "";
    @Size(max = 45)
    @Column(name = "PREVIOUSFACIALTREATMENT")
    private String previousfacialtreatment = "";
    @Size(max = 45)
    @Column(name = "CHEMICALPEEL")
    private String chemicalpeel = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SKINPRODUCTS")
    private String skinproducts = "";
    @Column(name = "UPDATED")
    private Boolean isUpdated = false;
    @Column(name = "IS_SHAVING")
    private Boolean isShaving = false;
    @Column(name = "IS_WAXING")
    private Boolean isWaxing = false;
    @Column(name = "IS_ELECTRO")
    private Boolean isElectro = false;
    @Column(name = "IS_PLUCK")
    private Boolean isPluck = false;
    @Column(name = "IS_TWEEZE")
    private Boolean isTweeze = false;
    @Column(name = "IS_STRING")
    private Boolean isString = false;
    @Column(name = "IS_DEPILATORIES")
    private Boolean isDepilatories = false;
    @Size(max = 45)
    @Column(name = "SUNSCREEN_USE")
    private String sunscreenUse = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SUNSCREEN_NOTES")
    private String sunscreenNotes = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "DOCTORORDER")
    private String doctororder = "";
    @Size(max = 45)
    @Column(name = "E_PROCEDUREDONEBY")
    private String eProceduredoneby = "";
    @Size(max = 45)
    @Column(name = "H_PROCEDUREDONEBY")
    private String hProceduredoneby = "";
    @Size(max = 45)
    @Column(name = "C_PROCEDUREDONEBY")
    private String cProceduredoneby = "";
    @Size(max = 45)
    @Column(name = "COUGHCOLDS")
    private String coughcolds = "";
    @Column(name = "AGE")
    private Integer age = 0;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PRODUCTSUSED")
    private String productsused = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "FAMILYHISTORY")
    private String familyhistory = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MEDICALHISTORY")
    private String medicalhistory = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ILLNESSHISTORY")
    private String illnesshistory = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "FOLLOWUP")
    private String followup = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "SKINMUSCULOSKELETAL")
    private String skinmusculoskeletal = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "HEENTNNECK")
    private String heentnneck = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CHESTHEARTLUNGS")
    private String chestheartlungs = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ABDOMENURINARY")
    private String abdomenurinary = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PELVIC")
    private String pelvic = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "NEUROLOGIC")
    private String neurologic = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "CHARTNOTES")
    private String chartnotes = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ADMITTINGORDERS")
    private String admittingorders = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "LABREQUEST")
    private String labrequest = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "LABREQUESTNOTE")
    private String labrequestnote = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MEDCERTDIAGNOSIS")
    private String medcertdiagnosis = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "ANCILLIARYTESTS")
    private String ancilliarytests = "";
    @Lob
    @Size(max = 2147483647)
    @Column(name = "RECOMMENDATION")
    private String recommendation = "";
    @Column(name = "PRESCRIPTIONMEDS")
    private Integer prescriptionmeds = 0;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "REFERRALNOTE")
    private String referralnote = "";
    @Column(name = "MAINTENANCERX")
    private Integer maintenancerx = 0;
    @Size(max = 45)
    @Column(name = "INSURANCEPROVIDER")
    private String insuranceprovider = "";
    @Column(name = "BILLING")
    private Integer billing = 0;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "chart", fetch = FetchType.LAZY)
    private List<DiagnosisList> diagnosisListList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "chart", fetch = FetchType.LAZY)
    private List<BillingDetail> billingDetailList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "chart", fetch = FetchType.LAZY)
    private List<ChartRX> chartRXList;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Client client;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "chart", fetch = FetchType.LAZY)
    private List<MaintenanceRX> maintenanceRXList;

    public ChartDetails() {
    }

    public ChartDetails(Integer id) {
        this.id = id;
    }

    public ChartDetails(Integer id, int active) {
        this.id = id;
        this.active = active;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(String bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getMaintenancemeds() {
        return maintenancemeds;
    }

    public void setMaintenancemeds(String maintenancemeds) {
        this.maintenancemeds = maintenancemeds;
    }

    public String getRecentmeds() {
        return recentmeds;
    }

    public void setRecentmeds(String recentmeds) {
        this.recentmeds = recentmeds;
    }

    public String getReferredby() {
        return referredby;
    }

    public void setReferredby(String referredby) {
        this.referredby = referredby;
    }

    public Date getRecorddatetime() {
        return recorddatetime;
    }

    public void setRecorddatetime(Date recorddatetime) {
        this.recorddatetime = recorddatetime;
    }

    public String getChiefcomplaint() {
        return chiefcomplaint;
    }

    public void setChiefcomplaint(String chiefcomplaint) {
        this.chiefcomplaint = chiefcomplaint;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getProcedures() {
        return procedures;
    }

    public void setProcedures(String procedures) {
        this.procedures = procedures;
    }

    public String getPhysicalexam() {
        return physicalexam;
    }

    public void setPhysicalexam(String physicalexam) {
        this.physicalexam = physicalexam;
    }

    public Integer getCharttype() {
        return charttype;
    }

    public void setCharttype(Integer charttype) {
        this.charttype = charttype;
    }

    public String getAdditionalnotes() {
        return additionalnotes;
    }

    public void setAdditionalnotes(String additionalnotes) {
        this.additionalnotes = additionalnotes;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Date getDaterecorded() {
        return daterecorded;
    }

    public void setDaterecorded(Date daterecorded) {
        this.daterecorded = daterecorded;
    }

    public Date getDateopen() {
        return dateopen;
    }

    public void setDateopen(Date dateopen) {
        this.dateopen = dateopen;
    }

    public Date getDateupdate() {
        return dateupdate;
    }

    public void setDateupdate(Date dateupdate) {
        this.dateupdate = dateupdate;
    }

    public String getHearinglossnotice() {
        return hearinglossnotice;
    }

    public void setHearinglossnotice(String hearinglossnotice) {
        this.hearinglossnotice = hearinglossnotice;
    }

    public String getBetterear() {
        return betterear;
    }

    public void setBetterear(String betterear) {
        this.betterear = betterear;
    }

    public String getNoiseexposure() {
        return noiseexposure;
    }

    public void setNoiseexposure(String noiseexposure) {
        this.noiseexposure = noiseexposure;
    }

    public String getTinnitus() {
        return tinnitus;
    }

    public void setTinnitus(String tinnitus) {
        this.tinnitus = tinnitus;
    }

    public String getTinnitusear() {
        return tinnitusear;
    }

    public void setTinnitusear(String tinnitusear) {
        this.tinnitusear = tinnitusear;
    }

    public String getEarfamilyhistory() {
        return earfamilyhistory;
    }

    public void setEarfamilyhistory(String earfamilyhistory) {
        this.earfamilyhistory = earfamilyhistory;
    }

    public String getEarsurgery() {
        return earsurgery;
    }

    public void setEarsurgery(String earsurgery) {
        this.earsurgery = earsurgery;
    }

    public String getEarsurgerynote() {
        return earsurgerynote;
    }

    public void setEarsurgerynote(String earsurgerynote) {
        this.earsurgerynote = earsurgerynote;
    }

    public String getTympStaticLeft() {
        return tympStaticLeft;
    }

    public void setTympStaticLeft(String tympStaticLeft) {
        this.tympStaticLeft = tympStaticLeft;
    }

    public String getTympStaticRight() {
        return tympStaticRight;
    }

    public void setTympStaticRight(String tympStaticRight) {
        this.tympStaticRight = tympStaticRight;
    }

    public String getTympMiddleLeft() {
        return tympMiddleLeft;
    }

    public void setTympMiddleLeft(String tympMiddleLeft) {
        this.tympMiddleLeft = tympMiddleLeft;
    }

    public String getTympMiddleRight() {
        return tympMiddleRight;
    }

    public void setTympMiddleRight(String tympMiddleRight) {
        this.tympMiddleRight = tympMiddleRight;
    }

    public String getTympVolumeLeft() {
        return tympVolumeLeft;
    }

    public void setTympVolumeLeft(String tympVolumeLeft) {
        this.tympVolumeLeft = tympVolumeLeft;
    }

    public String getTympVolumeRight() {
        return tympVolumeRight;
    }

    public void setTympVolumeRight(String tympVolumeRight) {
        this.tympVolumeRight = tympVolumeRight;
    }

    public String getTympTypeLeft() {
        return tympTypeLeft;
    }

    public void setTympTypeLeft(String tympTypeLeft) {
        this.tympTypeLeft = tympTypeLeft;
    }

    public String getTympTypeRight() {
        return tympTypeRight;
    }

    public void setTympTypeRight(String tympTypeRight) {
        this.tympTypeRight = tympTypeRight;
    }

    public String getAudioAcLeft1() {
        return audioAcLeft1;
    }

    public void setAudioAcLeft1(String audioAcLeft1) {
        this.audioAcLeft1 = audioAcLeft1;
    }

    public String getAudioAcLeft2() {
        return audioAcLeft2;
    }

    public void setAudioAcLeft2(String audioAcLeft2) {
        this.audioAcLeft2 = audioAcLeft2;
    }

    public String getAudioAcLeft3() {
        return audioAcLeft3;
    }

    public void setAudioAcLeft3(String audioAcLeft3) {
        this.audioAcLeft3 = audioAcLeft3;
    }

    public String getAudioAcLeft4() {
        return audioAcLeft4;
    }

    public void setAudioAcLeft4(String audioAcLeft4) {
        this.audioAcLeft4 = audioAcLeft4;
    }

    public String getAudioAcLeft5() {
        return audioAcLeft5;
    }

    public void setAudioAcLeft5(String audioAcLeft5) {
        this.audioAcLeft5 = audioAcLeft5;
    }

    public String getAudioAcLeft6() {
        return audioAcLeft6;
    }

    public void setAudioAcLeft6(String audioAcLeft6) {
        this.audioAcLeft6 = audioAcLeft6;
    }

    public String getAudioAcLeftAvg() {
        return audioAcLeftAvg;
    }

    public void setAudioAcLeftAvg(String audioAcLeftAvg) {
        this.audioAcLeftAvg = audioAcLeftAvg;
    }

    public String getAudioAcRight1() {
        return audioAcRight1;
    }

    public void setAudioAcRight1(String audioAcRight1) {
        this.audioAcRight1 = audioAcRight1;
    }

    public String getAudioAcRight2() {
        return audioAcRight2;
    }

    public void setAudioAcRight2(String audioAcRight2) {
        this.audioAcRight2 = audioAcRight2;
    }

    public String getAudioAcRight3() {
        return audioAcRight3;
    }

    public void setAudioAcRight3(String audioAcRight3) {
        this.audioAcRight3 = audioAcRight3;
    }

    public String getAudioAcRight4() {
        return audioAcRight4;
    }

    public void setAudioAcRight4(String audioAcRight4) {
        this.audioAcRight4 = audioAcRight4;
    }

    public String getAudioAcRight5() {
        return audioAcRight5;
    }

    public void setAudioAcRight5(String audioAcRight5) {
        this.audioAcRight5 = audioAcRight5;
    }

    public String getAudioAcRight6() {
        return audioAcRight6;
    }

    public void setAudioAcRight6(String audioAcRight6) {
        this.audioAcRight6 = audioAcRight6;
    }

    public String getAudioAcRightAvg() {
        return audioAcRightAvg;
    }

    public void setAudioAcRightAvg(String audioAcRightAvg) {
        this.audioAcRightAvg = audioAcRightAvg;
    }

    public String getAudioBcLeft1() {
        return audioBcLeft1;
    }

    public void setAudioBcLeft1(String audioBcLeft1) {
        this.audioBcLeft1 = audioBcLeft1;
    }

    public String getAudioBcLeft2() {
        return audioBcLeft2;
    }

    public void setAudioBcLeft2(String audioBcLeft2) {
        this.audioBcLeft2 = audioBcLeft2;
    }

    public String getAudioBcLeft3() {
        return audioBcLeft3;
    }

    public void setAudioBcLeft3(String audioBcLeft3) {
        this.audioBcLeft3 = audioBcLeft3;
    }

    public String getAudioBcLeft4() {
        return audioBcLeft4;
    }

    public void setAudioBcLeft4(String audioBcLeft4) {
        this.audioBcLeft4 = audioBcLeft4;
    }

    public String getAudioBcLeft5() {
        return audioBcLeft5;
    }

    public void setAudioBcLeft5(String audioBcLeft5) {
        this.audioBcLeft5 = audioBcLeft5;
    }

    public String getAudioBcLeft6() {
        return audioBcLeft6;
    }

    public void setAudioBcLeft6(String audioBcLeft6) {
        this.audioBcLeft6 = audioBcLeft6;
    }

    public String getAudioBcLeftAvg() {
        return audioBcLeftAvg;
    }

    public void setAudioBcLeftAvg(String audioBcLeftAvg) {
        this.audioBcLeftAvg = audioBcLeftAvg;
    }

    public String getAudioBcRight1() {
        return audioBcRight1;
    }

    public void setAudioBcRight1(String audioBcRight1) {
        this.audioBcRight1 = audioBcRight1;
    }

    public String getAudioBcRight2() {
        return audioBcRight2;
    }

    public void setAudioBcRight2(String audioBcRight2) {
        this.audioBcRight2 = audioBcRight2;
    }

    public String getAudioBcRight3() {
        return audioBcRight3;
    }

    public void setAudioBcRight3(String audioBcRight3) {
        this.audioBcRight3 = audioBcRight3;
    }

    public String getAudioBcRight4() {
        return audioBcRight4;
    }

    public void setAudioBcRight4(String audioBcRight4) {
        this.audioBcRight4 = audioBcRight4;
    }

    public String getAudioBcRight5() {
        return audioBcRight5;
    }

    public void setAudioBcRight5(String audioBcRight5) {
        this.audioBcRight5 = audioBcRight5;
    }

    public String getAudioBcRight6() {
        return audioBcRight6;
    }

    public void setAudioBcRight6(String audioBcRight6) {
        this.audioBcRight6 = audioBcRight6;
    }

    public String getAudioBcRightAvg() {
        return audioBcRightAvg;
    }

    public void setAudioBcRightAvg(String audioBcRightAvg) {
        this.audioBcRightAvg = audioBcRightAvg;
    }

    public String getHearingassessment() {
        return hearingassessment;
    }

    public void setHearingassessment(String hearingassessment) {
        this.hearingassessment = hearingassessment;
    }

    public String getHearingrecommendation() {
        return hearingrecommendation;
    }

    public void setHearingrecommendation(String hearingrecommendation) {
        this.hearingrecommendation = hearingrecommendation;
    }

    public String getHearingAidUse() {
        return hearingAidUse;
    }

    public void setHearingAidUse(String hearingAidUse) {
        this.hearingAidUse = hearingAidUse;
    }

    public String getHearingAidNote() {
        return hearingAidNote;
    }

    public void setHearingAidNote(String hearingAidNote) {
        this.hearingAidNote = hearingAidNote;
    }

    public String getHearingLossDuration() {
        return hearingLossDuration;
    }

    public void setHearingLossDuration(String hearingLossDuration) {
        this.hearingLossDuration = hearingLossDuration;
    }

    public String getHearingLossOnset() {
        return hearingLossOnset;
    }

    public void setHearingLossOnset(String hearingLossOnset) {
        this.hearingLossOnset = hearingLossOnset;
    }

    public String getLeftEarAssessment() {
        return leftEarAssessment;
    }

    public void setLeftEarAssessment(String leftEarAssessment) {
        this.leftEarAssessment = leftEarAssessment;
    }

    public String getRightEarAssessment() {
        return rightEarAssessment;
    }

    public void setRightEarAssessment(String rightEarAssessment) {
        this.rightEarAssessment = rightEarAssessment;
    }

    public String getHearingAssessment() {
        return hearingAssessment;
    }

    public void setHearingAssessment(String hearingAssessment) {
        this.hearingAssessment = hearingAssessment;
    }

    public String getPreviousfacialtreatment() {
        return previousfacialtreatment;
    }

    public void setPreviousfacialtreatment(String previousfacialtreatment) {
        this.previousfacialtreatment = previousfacialtreatment;
    }

    public String getChemicalpeel() {
        return chemicalpeel;
    }

    public void setChemicalpeel(String chemicalpeel) {
        this.chemicalpeel = chemicalpeel;
    }

    public String getSkinproducts() {
        return skinproducts;
    }

    public void setSkinproducts(String skinproducts) {
        this.skinproducts = skinproducts;
    }

    public Boolean getIsShaving() {
        return isShaving;
    }

    public void setIsShaving(Boolean isShaving) {
        this.isShaving = isShaving;
    }

    public Boolean getIsWaxing() {
        return isWaxing;
    }

    public void setIsWaxing(Boolean isWaxing) {
        this.isWaxing = isWaxing;
    }

    public Boolean getIsElectro() {
        return isElectro;
    }

    public void setIsElectro(Boolean isElectro) {
        this.isElectro = isElectro;
    }

    public Boolean getIsPluck() {
        return isPluck;
    }

    public void setIsPluck(Boolean isPluck) {
        this.isPluck = isPluck;
    }

    public Boolean getIsTweeze() {
        return isTweeze;
    }

    public void setIsTweeze(Boolean isTweeze) {
        this.isTweeze = isTweeze;
    }

    public Boolean getIsString() {
        return isString;
    }

    public void setIsString(Boolean isString) {
        this.isString = isString;
    }

    public Boolean getIsDepilatories() {
        return isDepilatories;
    }

    public void setIsDepilatories(Boolean isDepilatories) {
        this.isDepilatories = isDepilatories;
    }

    public String getSunscreenUse() {
        return sunscreenUse;
    }

    public void setSunscreenUse(String sunscreenUse) {
        this.sunscreenUse = sunscreenUse;
    }

    public String getSunscreenNotes() {
        return sunscreenNotes;
    }

    public void setSunscreenNotes(String sunscreenNotes) {
        this.sunscreenNotes = sunscreenNotes;
    }

    public String getDoctororder() {
        return doctororder;
    }

    public void setDoctororder(String doctororder) {
        this.doctororder = doctororder;
    }

    public String getEProceduredoneby() {
        return eProceduredoneby;
    }

    public void setEProceduredoneby(String eProceduredoneby) {
        this.eProceduredoneby = eProceduredoneby;
    }

    public String getHProceduredoneby() {
        return hProceduredoneby;
    }

    public void setHProceduredoneby(String hProceduredoneby) {
        this.hProceduredoneby = hProceduredoneby;
    }

    public String getCProceduredoneby() {
        return cProceduredoneby;
    }

    public void setCProceduredoneby(String cProceduredoneby) {
        this.cProceduredoneby = cProceduredoneby;
    }

    public String getCoughcolds() {
        return coughcolds;
    }

    public void setCoughcolds(String coughcolds) {
        this.coughcolds = coughcolds;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getProductsused() {
        return productsused;
    }

    public void setProductsused(String productsused) {
        this.productsused = productsused;
    }

    public String getFamilyhistory() {
        return familyhistory;
    }

    public void setFamilyhistory(String familyhistory) {
        this.familyhistory = familyhistory;
    }

    public String getMedicalhistory() {
        return medicalhistory;
    }

    public void setMedicalhistory(String medicalhistory) {
        this.medicalhistory = medicalhistory;
    }

    public String getIllnesshistory() {
        return illnesshistory;
    }

    public void setIllnesshistory(String illnesshistory) {
        this.illnesshistory = illnesshistory;
    }

    public String getFollowup() {
        return followup;
    }

    public void setFollowup(String followup) {
        this.followup = followup;
    }

    public String getSkinmusculoskeletal() {
        return skinmusculoskeletal;
    }

    public void setSkinmusculoskeletal(String skinmusculoskeletal) {
        this.skinmusculoskeletal = skinmusculoskeletal;
    }

    public String getHeentnneck() {
        return heentnneck;
    }

    public void setHeentnneck(String heentnneck) {
        this.heentnneck = heentnneck;
    }

    public String getChestheartlungs() {
        return chestheartlungs;
    }

    public void setChestheartlungs(String chestheartlungs) {
        this.chestheartlungs = chestheartlungs;
    }

    public String getAbdomenurinary() {
        return abdomenurinary;
    }

    public void setAbdomenurinary(String abdomenurinary) {
        this.abdomenurinary = abdomenurinary;
    }

    public String getPelvic() {
        return pelvic;
    }

    public void setPelvic(String pelvic) {
        this.pelvic = pelvic;
    }

    public String getNeurologic() {
        return neurologic;
    }

    public void setNeurologic(String neurologic) {
        this.neurologic = neurologic;
    }

    public String getChartnotes() {
        return chartnotes;
    }

    public void setChartnotes(String chartnotes) {
        this.chartnotes = chartnotes;
    }

    public String getAdmittingorders() {
        return admittingorders;
    }

    public void setAdmittingorders(String admittingorders) {
        this.admittingorders = admittingorders;
    }

    public String getLabrequest() {
        return labrequest;
    }

    public void setLabrequest(String labrequest) {
        this.labrequest = labrequest;
    }

    public String getLabrequestnote() {
        return labrequestnote;
    }

    public void setLabrequestnote(String labrequestnote) {
        this.labrequestnote = labrequestnote;
    }

    public String getMedcertdiagnosis() {
        return medcertdiagnosis;
    }

    public void setMedcertdiagnosis(String medcertdiagnosis) {
        this.medcertdiagnosis = medcertdiagnosis;
    }

    public String getAncilliarytests() {
        return ancilliarytests;
    }

    public void setAncilliarytests(String ancilliarytests) {
        this.ancilliarytests = ancilliarytests;
    }

    public String getRecommendation() {
        return recommendation;
    }

    public void setRecommendation(String recommendation) {
        this.recommendation = recommendation;
    }

    public Integer getPrescriptionmeds() {
        return prescriptionmeds;
    }

    public void setPrescriptionmeds(Integer prescriptionmeds) {
        this.prescriptionmeds = prescriptionmeds;
    }

    public String getReferralnote() {
        return referralnote;
    }

    public void setReferralnote(String referralnote) {
        this.referralnote = referralnote;
    }

    public Integer getMaintenancerx() {
        return maintenancerx;
    }

    public void setMaintenancerx(Integer maintenancerx) {
        this.maintenancerx = maintenancerx;
    }

    public String getInsuranceprovider() {
        return insuranceprovider;
    }

    public void setInsuranceprovider(String insuranceprovider) {
        this.insuranceprovider = insuranceprovider;
    }

    public Integer getBilling() {
        return billing;
    }

    public void setBilling(Integer billing) {
        this.billing = billing;
    }

    public List<DiagnosisList> getDiagnosisListList() {
        return diagnosisListList;
    }

    public void setDiagnosisListList(List<DiagnosisList> diagnosisListList) {
        this.diagnosisListList = diagnosisListList;
    }

    public List<BillingDetail> getBillingDetailList() {
        return billingDetailList;
    }

    public void setBillingDetailList(List<BillingDetail> billingDetailList) {
        this.billingDetailList = billingDetailList;
    }

    public List<ChartRX> getChartRXList() {
        return chartRXList;
    }

    public void setChartRXList(List<ChartRX> chartRXList) {
        this.chartRXList = chartRXList;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<MaintenanceRX> getMaintenanceRXList() {
        return maintenanceRXList;
    }

    public void setMaintenanceRXList(List<MaintenanceRX> maintenanceRXList) {
        this.maintenanceRXList = maintenanceRXList;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ChartDetails other = (ChartDetails) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.id;
        return hash;
    }

    @Override
    public String toString() {
        return "models.ChartDetails[ id=" + id + " ]";
    }

    /**
     * @return the nextcheckup
     */
    public Date getNextcheckup() {
       return nextcheckup;
    }

    /**
     * @param nextcheckup the nextcheckup to set
     */
    public void setNextcheckup(Date nextcheckup) {
        this.nextcheckup = nextcheckup;
    }

    /**
     * @return the labrequestDate
     */
    public Date getLabrequestDate() {
        return labrequestDate;
    }

    /**
     * @param labrequestDate the labrequestDate to set
     */
    public void setLabrequestDate(Date labrequestDate) {
        this.labrequestDate = labrequestDate;
    }

    /**
     * @return the admittingDate
     */
    public Date getAdmittingDate() {
        return admittingDate;
    }

    /**
     * @param admittingDate the admittingDate to set
     */
    public void setAdmittingDate(Date admittingDate) {
        this.admittingDate = admittingDate;
    }

    /**
     * @return the referralDate
     */
    public Date getReferralDate() {
        return referralDate;
    }

    /**
     * @param referralDate the referralDate to set
     */
    public void setReferralDate(Date referralDate) {
        this.referralDate = referralDate;
    }

    /**
     * @return the generic1
     */
    public String getGeneric1() {
        return generic1;
    }

    /**
     * @param generic1 the generic1 to set
     */
    public void setGeneric1(String generic1) {
        this.generic1 = generic1;
    }

    /**
     * @return the generic2
     */
    public String getGeneric2() {
        return generic2;
    }

    /**
     * @param generic2 the generic2 to set
     */
    public void setGeneric2(String generic2) {
        this.generic2 = generic2;
    }

    /**
     * @return the generic3
     */
    public String getGeneric3() {
        return generic3;
    }

    /**
     * @param generic3 the generic3 to set
     */
    public void setGeneric3(String generic3) {
        this.generic3 = generic3;
    }

    /**
     * @return the brand1
     */
    public String getBrand1() {
        return brand1;
    }

    /**
     * @param brand1 the brand1 to set
     */
    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    /**
     * @return the brand2
     */
    public String getBrand2() {
        return brand2;
    }

    /**
     * @param brand2 the brand2 to set
     */
    public void setBrand2(String brand2) {
        this.brand2 = brand2;
    }

    /**
     * @return the brand3
     */
    public String getBrand3() {
        return brand3;
    }

    /**
     * @param brand3 the brand3 to set
     */
    public void setBrand3(String brand3) {
        this.brand3 = brand3;
    }

    /**
     * @return the preparation1
     */
    public String getPreparation1() {
        return preparation1;
    }

    /**
     * @param preparation1 the preparation1 to set
     */
    public void setPreparation1(String preparation1) {
        this.preparation1 = preparation1;
    }

    /**
     * @return the preparation2
     */
    public String getPreparation2() {
        return preparation2;
    }

    /**
     * @param preparation2 the preparation2 to set
     */
    public void setPreparation2(String preparation2) {
        this.preparation2 = preparation2;
    }

    /**
     * @return the preparation3
     */
    public String getPreparation3() {
        return preparation3;
    }

    /**
     * @param preparation3 the preparation3 to set
     */
    public void setPreparation3(String preparation3) {
        this.preparation3 = preparation3;
    }

    /**
     * @return the quantity1
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * @param quantity1 the quantity1 to set
     */
    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    /**
     * @return the quantity2
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * @param quantity2 the quantity2 to set
     */
    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    /**
     * @return the quantity3
     */
    public String getQuantity3() {
        return quantity3;
    }

    /**
     * @param quantity3 the quantity3 to set
     */
    public void setQuantity3(String quantity3) {
        this.quantity3 = quantity3;
    }

    /**
     * @return the direction1
     */
    public String getDirection1() {
        return direction1;
    }

    /**
     * @param direction1 the direction1 to set
     */
    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    /**
     * @return the direction2
     */
    public String getDirection2() {
        return direction2;
    }

    /**
     * @param direction2 the direction2 to set
     */
    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    /**
     * @return the direction3
     */
    public String getDirection3() {
        return direction3;
    }

    /**
     * @param direction3 the direction3 to set
     */
    public void setDirection3(String direction3) {
        this.direction3 = direction3;
    }

    /**
     * @return the mgeneric1
     */
    public String getMgeneric1() {
        return mgeneric1;
    }

    /**
     * @param mgeneric1 the mgeneric1 to set
     */
    public void setMgeneric1(String mgeneric1) {
        this.mgeneric1 = mgeneric1;
    }

    /**
     * @return the mgeneric2
     */
    public String getMgeneric2() {
        return mgeneric2;
    }

    /**
     * @param mgeneric2 the mgeneric2 to set
     */
    public void setMgeneric2(String mgeneric2) {
        this.mgeneric2 = mgeneric2;
    }

    /**
     * @return the mgeneric3
     */
    public String getMgeneric3() {
        return mgeneric3;
    }

    /**
     * @param mgeneric3 the mgeneric3 to set
     */
    public void setMgeneric3(String mgeneric3) {
        this.mgeneric3 = mgeneric3;
    }

    /**
     * @return the mbrand1
     */
    public String getMbrand1() {
        return mbrand1;
    }

    /**
     * @param mbrand1 the mbrand1 to set
     */
    public void setMbrand1(String mbrand1) {
        this.mbrand1 = mbrand1;
    }

    /**
     * @return the mbrand2
     */
    public String getMbrand2() {
        return mbrand2;
    }

    /**
     * @param mbrand2 the mbrand2 to set
     */
    public void setMbrand2(String mbrand2) {
        this.mbrand2 = mbrand2;
    }

    /**
     * @return the mbrand3
     */
    public String getMbrand3() {
        return mbrand3;
    }

    /**
     * @param mbrand3 the mbrand3 to set
     */
    public void setMbrand3(String mbrand3) {
        this.mbrand3 = mbrand3;
    }

    /**
     * @return the mpreparation1
     */
    public String getMpreparation1() {
        return mpreparation1;
    }

    /**
     * @param mpreparation1 the mpreparation1 to set
     */
    public void setMpreparation1(String mpreparation1) {
        this.mpreparation1 = mpreparation1;
    }

    /**
     * @return the mpreparation2
     */
    public String getMpreparation2() {
        return mpreparation2;
    }

    /**
     * @param mpreparation2 the mpreparation2 to set
     */
    public void setMpreparation2(String mpreparation2) {
        this.mpreparation2 = mpreparation2;
    }

    /**
     * @return the mpreparation3
     */
    public String getMpreparation3() {
        return mpreparation3;
    }

    /**
     * @param mpreparation3 the mpreparation3 to set
     */
    public void setMpreparation3(String mpreparation3) {
        this.mpreparation3 = mpreparation3;
    }

    /**
     * @return the mquantity1
     */
    public String getMquantity1() {
        return mquantity1;
    }

    /**
     * @param mquantity1 the mquantity1 to set
     */
    public void setMquantity1(String mquantity1) {
        this.mquantity1 = mquantity1;
    }

    /**
     * @return the mquantity2
     */
    public String getMquantity2() {
        return mquantity2;
    }

    /**
     * @param mquantity2 the mquantity2 to set
     */
    public void setMquantity2(String mquantity2) {
        this.mquantity2 = mquantity2;
    }

    /**
     * @return the mquantity3
     */
    public String getMquantity3() {
        return mquantity3;
    }

    /**
     * @param mquantity3 the mquantity3 to set
     */
    public void setMquantity3(String mquantity3) {
        this.mquantity3 = mquantity3;
    }

    /**
     * @return the mdirection1
     */
    public String getMdirection1() {
        return mdirection1;
    }

    /**
     * @param mdirection1 the mdirection1 to set
     */
    public void setMdirection1(String mdirection1) {
        this.mdirection1 = mdirection1;
    }

    /**
     * @return the mdirection2
     */
    public String getMdirection2() {
        return mdirection2;
    }

    /**
     * @param mdirection2 the mdirection2 to set
     */
    public void setMdirection2(String mdirection2) {
        this.mdirection2 = mdirection2;
    }

    /**
     * @return the mdirection3
     */
    public String getMdirection3() {
        return mdirection3;
    }

    /**
     * @param mdirection3 the mdirection3 to set
     */
    public void setMdirection3(String mdirection3) {
        this.mdirection3 = mdirection3;
    }

    /**
     * @return the chartCode
     */
    public String getChartCode() {
        return chartCode;
    }

    /**
     * @param chartCode the chartCode to set
     */
    public void setChartCode(String chartCode) {
        this.chartCode = chartCode;
    }

    /**
     * @return the medcertMessage
     */
    public String getMedCertMessage() {
        return medcertMessage;
    }

    /**
     * @param medcertMessage the medcertMessage to set
     */
    public void setMedCertMessage(String medcertMessage) {
        this.medcertMessage = medcertMessage;
    }

    /**
     * @return the admittingDate1
     */
    public Date getAdmittingDate1() {
        return admittingDate1;
    }

    /**
     * @param admittingDate1 the admittingDate1 to set
     */
    public void setAdmittingDate1(Date admittingDate1) {
        this.admittingDate1 = admittingDate1;
    }

    /**
     * @return the admittingDate2
     */
    public Date getAdmittingDate2() {
        return admittingDate2;
    }

    /**
     * @param admittingDate2 the admittingDate2 to set
     */
    public void setAdmittingDate2(Date admittingDate2) {
        this.admittingDate2 = admittingDate2;
    }

    /**
     * @return the admittingorders1
     */
    public String getAdmittingorders1() {
        return admittingorders1;
    }

    /**
     * @param admittingorders1 the admittingorders1 to set
     */
    public void setAdmittingorders1(String admittingorders1) {
        this.admittingorders1 = admittingorders1;
    }

    /**
     * @return the admittingorders2
     */
    public String getAdmittingorders2() {
        return admittingorders2;
    }

    /**
     * @param admittingorders2 the admittingorders2 to set
     */
    public void setAdmittingorders2(String admittingorders2) {
        this.admittingorders2 = admittingorders2;
    }

    /**
     * @return the generic4
     */
    public String getGeneric4() {
        return generic4;
    }

    /**
     * @param generic4 the generic4 to set
     */
    public void setGeneric4(String generic4) {
        this.generic4 = generic4;
    }

    /**
     * @return the generic5
     */
    public String getGeneric5() {
        return generic5;
    }

    /**
     * @param generic5 the generic5 to set
     */
    public void setGeneric5(String generic5) {
        this.generic5 = generic5;
    }

    /**
     * @return the generic6
     */
    public String getGeneric6() {
        return generic6;
    }

    /**
     * @param generic6 the generic6 to set
     */
    public void setGeneric6(String generic6) {
        this.generic6 = generic6;
    }

    /**
     * @return the brand4
     */
    public String getBrand4() {
        return brand4;
    }

    /**
     * @param brand4 the brand4 to set
     */
    public void setBrand4(String brand4) {
        this.brand4 = brand4;
    }

    /**
     * @return the brand5
     */
    public String getBrand5() {
        return brand5;
    }

    /**
     * @param brand5 the brand5 to set
     */
    public void setBrand5(String brand5) {
        this.brand5 = brand5;
    }

    /**
     * @return the brand6
     */
    public String getBrand6() {
        return brand6;
    }

    /**
     * @param brand6 the brand6 to set
     */
    public void setBrand6(String brand6) {
        this.brand6 = brand6;
    }

    /**
     * @return the preparation4
     */
    public String getPreparation4() {
        return preparation4;
    }

    /**
     * @param preparation4 the preparation4 to set
     */
    public void setPreparation4(String preparation4) {
        this.preparation4 = preparation4;
    }

    /**
     * @return the preparation5
     */
    public String getPreparation5() {
        return preparation5;
    }

    /**
     * @param preparation5 the preparation5 to set
     */
    public void setPreparation5(String preparation5) {
        this.preparation5 = preparation5;
    }

    /**
     * @return the preparation6
     */
    public String getPreparation6() {
        return preparation6;
    }

    /**
     * @param preparation6 the preparation6 to set
     */
    public void setPreparation6(String preparation6) {
        this.preparation6 = preparation6;
    }

    /**
     * @return the quantity4
     */
    public String getQuantity4() {
        return quantity4;
    }

    /**
     * @param quantity4 the quantity4 to set
     */
    public void setQuantity4(String quantity4) {
        this.quantity4 = quantity4;
    }

    /**
     * @return the quantity5
     */
    public String getQuantity5() {
        return quantity5;
    }

    /**
     * @param quantity5 the quantity5 to set
     */
    public void setQuantity5(String quantity5) {
        this.quantity5 = quantity5;
    }

    /**
     * @return the quantity6
     */
    public String getQuantity6() {
        return quantity6;
    }

    /**
     * @param quantity6 the quantity6 to set
     */
    public void setQuantity6(String quantity6) {
        this.quantity6 = quantity6;
    }

    /**
     * @return the direction4
     */
    public String getDirection4() {
        return direction4;
    }

    /**
     * @param direction4 the direction4 to set
     */
    public void setDirection4(String direction4) {
        this.direction4 = direction4;
    }

    /**
     * @return the direction5
     */
    public String getDirection5() {
        return direction5;
    }

    /**
     * @param direction5 the direction5 to set
     */
    public void setDirection5(String direction5) {
        this.direction5 = direction5;
    }

    /**
     * @return the direction6
     */
    public String getDirection6() {
        return direction6;
    }

    /**
     * @param direction6 the direction6 to set
     */
    public void setDirection6(String direction6) {
        this.direction6 = direction6;
    }

    /**
     * @return the isUpdated
     */
    public Boolean getIsUpdated() {
        return isUpdated;
    }

    /**
     * @param isUpdated the isUpdated to set
     */
    public void setIsUpdated(Boolean isUpdated) {
        this.isUpdated = isUpdated;
    }

}
