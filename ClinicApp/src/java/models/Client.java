/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "client")
@NamedQueries({
    @NamedQuery(name = "Client.findAll", query = "SELECT c FROM Client c")})
public class Client implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<ChartDetails> chartDetailsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<Hearingaidsalesummary> hearingaidsalesummaryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<Acchearingaidsalesummary> acchearingaidsalesummaryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<Cdrugsalesummary> cdrugsalesummaryList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<Drugsalesummary> drugsalesummaryList;

    @Size(max = 45)
    @Column(name = "COUGHCOLDS")
    private String coughcolds;

    @Size(max = 45)
    @Column(name = "LATEXALLERGY")
    private String latexallergy;
    @Column(name = "LASTCHECKUP")
    @Temporal(TemporalType.DATE)
    private Date lastcheckup;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "LASTPROCEDURE")
    private String lastprocedure;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<PatientChart> patientChartList;

    @Size(max = 255)
    @Column(name = "COMPANY")
    private String company = "";
    @Column(name = "AGE")
    private Integer age = 0;
    @Column(name = "MONTHS")
    private Integer months = 0;
    @Column(name = "DAYS")
    private Integer days;
    @Column(name = "CHECKUPDATE")
    @Temporal(TemporalType.DATE)
    private Date checkupdate = new Date();

    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private List<Prescription> prescriptionList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "client", fetch = FetchType.LAZY)
    private List<Appointment> appointmentList;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "LASTNAME")
    private String lastname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "FIRSTNAME")
    private String firstname;
    @Column(name = "BIRTHDAY")
    @Temporal(TemporalType.DATE)
    private Date birthday;
    @Size(max = 45)
    @Column(name = "GENDER")
    private String gender;
    @Size(max = 45)
    @Column(name = "CIVILSTATUS")
    private String civilstatus;
    @Size(max = 45)
    @Column(name = "OCCUPATION")
    private String occupation;
    @Size(max = 255)
    @Column(name = "CONTACTINFO")
    private String contactinfo;
    @Size(max = 255)
    @Column(name = "STREETADDRESS")
    private String streetaddress;
    @Size(max = 255)
    @Column(name = "BRGY")
    private String brgy;
    @Size(max = 255)
    @Column(name = "TOWN")
    private String town;
    @Size(max = 255)
    @Column(name = "PROVINCE")
    private String province;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "EMAIL")
    private String email;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "PROFILEPIC")
    private String profilepic;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "MAINTENANCEMEDS")
    private String maintenanceMeds;
    @Size(max = 255)
    @Column(name = "INSURANCE")
    private String insurance;
    @Size(max = 255)
    @Column(name = "CREDITCARD")
    private String creditcard;
    @Size(max = 255)
    @Column(name = "DEBITCARD")
    private String debitcard;
    @Size(max = 45)
    @Column(name = "ASTHMA")
    private String asthma;
    @Size(max = 255)
    @Column(name = "FOODALLERGY")
    private String foodallergy;
    @Size(max = 255)
    @Column(name = "MEDICINEALLERGY")
    private String medicineallergy;
    @Size(max = 255)
    @Column(name = "OTHERALLERGIES")
    private String otherallergies;
    @Size(max = 45)
    @Column(name = "HYPERTENSION")
    private String hypertension;
    @Size(max = 45)
    @Column(name = "DIABETESMELLITUS")
    private String diabetesmellitus;
    @Size(max = 45)
    @Column(name = "CANCER")
    private String cancer;
    @Size(max = 45)
    @Column(name = "SMOKING")
    private String smoking;
    @Size(max = 45)
    @Column(name = "SMOKINGSTICKSPERDAY")
    private String smokingsticksperday;
    @Size(max = 45)
    @Column(name = "SMOKINGFREQUENCY")
    private String smokingfrequency;
    @Size(max = 45)
    @Column(name = "ALCOHOLIC")
    private String alcoholic;
    @Size(max = 45)
    @Column(name = "ALCOHOLICLENGTH")
    private String alcoholiclength;
    @Size(max = 45)
    @Column(name = "ALCOHOLICFREQUENCY")
    private String alcoholicfrequency;
    @Size(max = 45)
    @Column(name = "SURGERY")
    private String surgery;
    @Size(max = 45)
    @Column(name = "SURGERYDONE")
    private String surgerydone;
    @Column(name = "SURGERYDATE")
    @Temporal(TemporalType.DATE)
    private Date surgerydate;
    @Size(max = 45)
    @Column(name = "EMERGENCYNAME")
    private String emergencyname;
    @Size(max = 255)
    @Column(name = "EMERGENCYADDRESS")
    private String emergencyaddress;
    @Size(max = 45)
    @Column(name = "EMERGENCYCONTACT")
    private String emergencycontact;
    @Size(max = 45)
    @Column(name = "STATUS")
    private String status;

    @Column(name = "ACTIVE")
    private Boolean isActive;
    @Transient
    private String ageString;
    @Transient
    private String ageValue;
    @Transient
    private String surgeryDateString;
    @Transient
    private List<PatientChart> entChartList = new ArrayList<PatientChart>();
    @Transient
    private List<PatientChart> hearingChartList = new ArrayList<PatientChart>();
    @Transient
    private List<PatientChart> cosmeticChartList = new ArrayList<PatientChart>();

    public Client() {
    }

    public Client(Integer id) {
        this.id = id;
    }

    public Client(Integer id, String lastname, String firstname) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastname() {
        return lastname.trim().toUpperCase();
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname.trim().toUpperCase();
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender.trim().toUpperCase();
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCivilstatus() {
        return civilstatus.trim().toUpperCase();
    }

    public void setCivilstatus(String civilstatus) {
        this.civilstatus = civilstatus;
    }

    public String getOccupation() {
        return occupation.trim().toUpperCase();
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getContactinfo() {
        return contactinfo.trim().toUpperCase();
    }

    public void setContactinfo(String contactinfo) {
        this.contactinfo = contactinfo;
    }

    public String getStreetaddress() {
        return streetaddress.trim().toUpperCase();
    }

    public void setStreetaddress(String streetaddress) {
        this.streetaddress = streetaddress;
    }

    public String getBrgy() {
        return brgy.trim().toUpperCase();
    }

    public void setBrgy(String brgy) {
        this.brgy = brgy;
    }

    public String getTown() {
        return town.trim().toUpperCase();
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getProvince() {
        return province.trim().toUpperCase();
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getEmail() {
        return email.trim().toLowerCase();
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getProfilepic() {
        return profilepic.trim().toLowerCase();
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    public String getInsurance() {
        return insurance.trim().toUpperCase();
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getCreditcard() {
        return creditcard.trim().toUpperCase();
    }

    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    public String getDebitcard() {
        return debitcard.trim().toUpperCase();
    }

    public void setDebitcard(String debitcard) {
        this.debitcard = debitcard;
    }

    public String getAsthma() {
        return asthma.trim().toUpperCase();
    }

    public void setAsthma(String asthma) {
        this.asthma = asthma;
    }

    public String getFoodallergy() {
        return foodallergy != null ? foodallergy.trim().toUpperCase() : "-";
    }

    public void setFoodallergy(String foodallergy) {
        this.foodallergy = foodallergy;
    }

    public String getMedicineallergy() {
        return medicineallergy != null ? medicineallergy.trim().toUpperCase() : "-";
    }

    public void setMedicineallergy(String medicineallergy) {
        this.medicineallergy = medicineallergy;
    }

    public String getOtherallergies() {
        return otherallergies != null ? otherallergies.trim().toUpperCase() : "-";
    }

    public void setOtherallergies(String otherallergies) {
        this.otherallergies = otherallergies;
    }

    public String getHypertension() {
        return hypertension != null ? hypertension.trim().toUpperCase() : "-";
    }

    public void setHypertension(String hypertension) {
        this.hypertension = hypertension;
    }

    public String getDiabetesmellitus() {
        return diabetesmellitus != null ? diabetesmellitus.trim().toUpperCase() : "-";
    }

    public void setDiabetesmellitus(String diabetesmellitus) {
        this.diabetesmellitus = diabetesmellitus;
    }

    public String getCancer() {
        return cancer != null ? cancer.trim().toUpperCase() : "-";
    }

    public void setCancer(String cancer) {
        this.cancer = cancer;
    }

    public String getSmoking() {
        return smoking != null ? smoking.trim().toUpperCase() : "";
    }

    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    public String getSmokingsticksperday() {
        return smokingsticksperday.trim().toUpperCase();
    }

    public void setSmokingsticksperday(String smokingsticksperday) {
        this.smokingsticksperday = smokingsticksperday;
    }

    public String getSmokingfrequency() {
        return smokingfrequency.trim().toUpperCase();
    }

    public void setSmokingfrequency(String smokingfrequency) {
        this.smokingfrequency = smokingfrequency;
    }

    public String getAlcoholic() {
        return alcoholic != null ? alcoholic.trim().toUpperCase() : "-";
    }

    public void setAlcoholic(String alcoholic) {
        this.alcoholic = alcoholic;
    }

    public String getAlcoholiclength() {
        return alcoholiclength.trim().toUpperCase();
    }

    public void setAlcoholiclength(String alcoholiclength) {
        this.alcoholiclength = alcoholiclength;
    }

    public String getAlcoholicfrequency() {
        return alcoholicfrequency;
    }

    public void setAlcoholicfrequency(String alcoholicfrequency) {
        this.alcoholicfrequency = alcoholicfrequency;
    }

    public String getSurgery() {
        return surgery;
    }

    public void setSurgery(String surgery) {
        this.surgery = surgery;
    }

    public String getSurgerydone() {
        String tmp = "-";
        if (surgerydone != null) {
            tmp = surgerydone.trim().toUpperCase();
        } else {
            tmp = "-";
        }
        if (surgeryDateString != null) {
            tmp = tmp + " (" + surgeryDateString.trim().toUpperCase() + ")";
        } else {
            tmp = "-";
        }
        return tmp;
    }

    public void setSurgerydone(String surgerydone) {
        this.surgerydone = surgerydone;
    }

    public Date getSurgerydate() {
        return surgerydate;
    }

    public void setSurgerydate(Date surgerydate) {
        this.surgerydate = surgerydate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Client)) {
            return false;
        }
        Client other = (Client) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Client[ id=" + id + " ]";
    }

    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
    }

    /**
     * @return the ageString
     */
    @Transient
    public String getAgeString() {
        ageString = "-";
        if (birthday != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(birthday);
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            ageString = (period.getYears() > 0 ? period.getYears() + " year(s) " : "") + (period.getMonths() > 0 ? period.getMonths() + " month(s) " : "") + (period.getDays() > 0 ? period.getDays() + " day(s)" : "");
        } else if (age != null) {
            ageString = age + " year(s)";
        }
        return ageString;
    }

    /**
     * @return the surgeryDateString
     */
    @Transient
    public String getSurgeryDateString() {
        if (surgerydate == null) {
            return "";
        }

        surgeryDateString = "";
        if (surgerydate != null) {
            surgeryDateString = new SimpleDateFormat("MMM dd, yyyy").format(surgerydate);
        }
        return surgeryDateString;
    }

    /**
     * @return the isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the emergencyaddress
     */
    public String getEmergencyaddress() {
        return emergencyaddress.trim().toUpperCase();
    }

    /**
     * @param emergencyaddress the emergencyaddress to set
     */
    public void setEmergencyaddress(String emergencyaddress) {
        this.emergencyaddress = emergencyaddress;
    }

    /**
     * @return the emergencyname
     */
    public String getEmergencyname() {
        return emergencyname.trim().toUpperCase();
    }

    /**
     * @param emergencyname the emergencyname to set
     */
    public void setEmergencyname(String emergencyname) {
        this.emergencyname = emergencyname;
    }

    /**
     * @return the emergencycontact
     */
    public String getEmergencycontact() {
        return emergencycontact.trim().toUpperCase();
    }

    /**
     * @param emergencycontact the emergencycontact to set
     */
    public void setEmergencycontact(String emergencycontact) {
        this.emergencycontact = emergencycontact;
    }

    public List<Prescription> getPrescriptionList() {
        return prescriptionList;
    }

    public void setPrescriptionList(List<Prescription> prescriptionList) {
        this.prescriptionList = prescriptionList;
    }

    public String getCompany() {
        return company.trim().toUpperCase();
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getAge() {
        if (this.getBirthday() != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(this.getBirthday());
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            this.setAge(period.getYears());
        }
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getCheckupdate() {
        return checkupdate;
    }

    public void setCheckupdate(Date checkupdate) {
        this.checkupdate = checkupdate;
    }

    public List<PatientChart> getPatientChartList() {
        return patientChartList;
    }

    public void setPatientChartList(List<PatientChart> patientChartList) {
        this.patientChartList = patientChartList;
    }

    /**
     * @return the entChartList
     */
    public List<PatientChart> getEntChartList() {
        entChartList.clear();
        for (PatientChart chart : patientChartList) {
            if (chart.getCharttype().equals(1)) {
                entChartList.add(chart);
            }
        }
        return entChartList;
    }

    /**
     * @param entChartList the entChartList to set
     */
    public void setEntChartList(List<PatientChart> entChartList) {
        this.entChartList = entChartList;
    }

    /**
     * @return the hearingChartList
     */
    public List<PatientChart> getHearingChartList() {
        hearingChartList.clear();
        for (PatientChart chart : patientChartList) {

            if (chart.getCharttype().equals(2)) {
                hearingChartList.add(chart);
            }

        }
        return hearingChartList;
    }

    /**
     * @param hearingChartList the hearingChartList to set
     */
    public void setHearingChartList(List<PatientChart> hearingChartList) {
        this.hearingChartList = hearingChartList;
    }

    /**
     * @return the cosmeticChartList
     */
    public List<PatientChart> getCosmeticChartList() {
        cosmeticChartList.clear();
        for (PatientChart chart : patientChartList) {

            if (chart.getCharttype().equals(3)) {
                cosmeticChartList.add(chart);
            }
        }
        return cosmeticChartList;
    }

    /**
     * @param cosmeticChartList the cosmeticChartList to set
     */
    public void setCosmeticChartList(List<PatientChart> cosmeticChartList) {
        this.cosmeticChartList = cosmeticChartList;
    }

    public String getLatexallergy() {
        return latexallergy != null ? latexallergy.trim().toUpperCase() : "";
    }

    public void setLatexallergy(String latexallergy) {
        this.latexallergy = latexallergy;
    }

    public Date getLastcheckup() {
        return lastcheckup;
    }

    public void setLastcheckup(Date lastcheckup) {
        this.lastcheckup = lastcheckup;
    }

    public String getLastprocedure() {
        return lastprocedure != null ? lastprocedure.trim().toUpperCase() : "-";
    }

    public void setLastprocedure(String lastprocedure) {
        this.lastprocedure = lastprocedure;
    }

    public String getCoughcolds() {
        return coughcolds != null ? coughcolds.trim().toUpperCase() : "-";
    }

    public void setCoughcolds(String coughcolds) {
        this.coughcolds = coughcolds;
    }

    public List<Hearingaidsalesummary> getHearingaidsalesummaryList() {
        return hearingaidsalesummaryList;
    }

    public void setHearingaidsalesummaryList(List<Hearingaidsalesummary> hearingaidsalesummaryList) {
        this.hearingaidsalesummaryList = hearingaidsalesummaryList;
    }

    public List<Acchearingaidsalesummary> getAcchearingaidsalesummaryList() {
        return acchearingaidsalesummaryList;
    }

    public void setAcchearingaidsalesummaryList(List<Acchearingaidsalesummary> acchearingaidsalesummaryList) {
        this.acchearingaidsalesummaryList = acchearingaidsalesummaryList;
    }

    public List<Cdrugsalesummary> getCdrugsalesummaryList() {
        return cdrugsalesummaryList;
    }

    public void setCdrugsalesummaryList(List<Cdrugsalesummary> cdrugsalesummaryList) {
        this.cdrugsalesummaryList = cdrugsalesummaryList;
    }

    public List<Drugsalesummary> getDrugsalesummaryList() {
        return drugsalesummaryList;
    }

    public void setDrugsalesummaryList(List<Drugsalesummary> drugsalesummaryList) {
        this.drugsalesummaryList = drugsalesummaryList;
    }

    public List<ChartDetails> getChartDetailsList() {
        return chartDetailsList;
    }

    public void setChartDetailsList(List<ChartDetails> chartDetailsList) {
        this.chartDetailsList = chartDetailsList;
    }

    /**
     * @return the months
     */
    public Integer getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(Integer months) {
        this.months = months;
    }

    /**
     * @return the days
     */
    public Integer getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Integer days) {
        this.days = days;
    }

    /**
     * @return the maintenanceMeds
     */
    public String getMaintenanceMeds() {
        if(maintenanceMeds==null){
            return "-";
        }
            
        return maintenanceMeds.trim().toUpperCase();
    }

    /**
     * @param maintenanceMeds the maintenanceMeds to set
     */
    public void setMaintenanceMeds(String maintenanceMeds) {
        this.maintenanceMeds = maintenanceMeds;
    }

    /**
     * @return the ageValue
     */
    public String getAgeValue() {
        return ageValue.trim().toUpperCase();
    }

    /**
     * @param ageValue the ageValue to set
     */
    public void setAgeValue(String ageValue) {
        this.ageValue = ageValue;
    }

}
