/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author apple
 */
@Entity
@Table(name = "appointment")
@NamedQueries({
    @NamedQuery(name = "Appointment.findAll", query = "SELECT a FROM Appointment a")})
public class Appointment implements Serializable {

    @Column(name = "ISPRIORITY")
    private Boolean isPriority = false;
    @Column(name = "UPDATED")
    private Boolean isUpdated = false;
    @Size(max = 45)
    @Column(name = "DEPARTMENT")
    private String department;
    @Column(name = "INDEXQ")
    private Integer index;

    @Size(max = 45)
    @Column(name = "SESSION")
    private String session;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "APPOINTMENTDATE")
    @Temporal(TemporalType.DATE)
    private Date appointmentdate;
    @Column(name = "APPOINTMENTTIME")
    @Temporal(TemporalType.TIME)
    private Date appointmenttime;
    @Size(max = 512)
    @Column(name = "SERVICEPROCEDURE")
    private String serviceprocedure;
    @Column(name = "SERVICETIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date servicetimestamp;
    @JoinColumn(name = "CLIENT", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Client client;
    @Transient
    private String timeQueued;

    public Appointment() {
    }

    public Appointment(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAppointmentdate() {
        return appointmentdate;
    }

    public void setAppointmentdate(Date appointmentdate) {
        this.appointmentdate = appointmentdate;
    }

    public Date getAppointmenttime() {
        return appointmenttime;
    }

    public void setAppointmenttime(Date appointmenttime) {
        this.appointmenttime = appointmenttime;
    }

    public String getProcedure() {
        return serviceprocedure;
    }

    public void setProcedure(String procedure) {
        this.serviceprocedure = procedure;
    }

    public Date getTimestamp() {
        return servicetimestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.servicetimestamp = timestamp;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointment)) {
            return false;
        }
        Appointment other = (Appointment) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.Appointment[ id=" + id + " ]";
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    /**
     * @return the timeQueued
     */
    public String getTimeQueued() {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");
        timeQueued = sdf.format(servicetimestamp);
        return timeQueued;
    }

    /**
     * @param timeQueued the timeQueued to set
     */
    public void setTimeQueued(String timeQueued) {
        this.timeQueued = timeQueued;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    /**
     * @return the isUpdated
     */
    public Boolean getIsUpdated() {
        return isUpdated;
    }

    /**
     * @param isUpdated the isUpdated to set
     */
    public void setIsUpdated(Boolean isUpdated) {
        this.isUpdated = isUpdated;
    }

    /**
     * @return the isPriority
     */
    public Boolean getIsPriority() {
        return isPriority;
    }

    /**
     * @param isPriority the isPriority to set
     */
    public void setIsPriority(Boolean isPriority) {
        this.isPriority = isPriority;
    }

}
