/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author apple
 */
@Embeddable
public class TransactionLogPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "PREFIX")
    private int prefix;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MIDFIX")
    private String midfix;
    @Basic(optional = false)
    @NotNull
    @Column(name = "SEQFIX")
    private String seqfix;

    public TransactionLogPK() {
    }

    public TransactionLogPK(int prefix, String midfix, String seqfix) {
        this.prefix = prefix;
        this.midfix = midfix;
        this.seqfix = seqfix;
    }

    public int getPrefix() {
        return prefix;
    }

    public void setPrefix(int prefix) {
        this.prefix = prefix;
    }

    public String getMidfix() {
        return midfix;
    }

    public void setMidfix(String midfix) {
        this.midfix = midfix;
    }

    public String getSeqfix() {
        return seqfix;
    }

    public void setSeqfix(String seqfix) {
        this.seqfix = seqfix;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) prefix;
        hash += midfix.hashCode();
        hash += seqfix.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionLogPK)) {
            return false;
        }
        TransactionLogPK other = (TransactionLogPK) object;
        if (this.prefix != other.prefix) {
            return false;
        }
        if (this.midfix.equals(other.midfix)) {
            return false;
        }
        if (this.seqfix.equals(other.seqfix)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "models.TransactionLogPK[ prefix=" + prefix + ", midfix=" + midfix + ", seqfix=" + seqfix + " ]";
    }
    
}
