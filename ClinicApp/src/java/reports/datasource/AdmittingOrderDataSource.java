/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports.datasource;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import models.ChartDetails;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author User
 */
public class AdmittingOrderDataSource implements JRDataSource {

    private Iterator<ChartDetails> i;
    private ChartDetails currentChart;
    private List<ChartDetails> admittingList;
    private String dateString;

    public AdmittingOrderDataSource(List<ChartDetails> admittingList) {
        this.admittingList = admittingList;
        i = this.admittingList.iterator();
    }

    @Override
    public boolean next() throws JRException {
        boolean hasNext = i.hasNext();
        if (hasNext) {
            currentChart = i.next();

        }
        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        String fieldName = jrf.getName();
        if (fieldName.equals("patient")) {
            return currentChart.getClient().getFirstname() + " " + currentChart.getClient().getLastname();
        } else if (fieldName.equals("agesex")) {
            if (currentChart.getClient().getAge() != null) {
                if (currentChart.getClient().getAge() == 0) {
                    if (currentChart.getClient().getMonths() > 0) {
                        return currentChart.getClient().getMonths() + " months old/" + currentChart.getClient().getGender().charAt(0);
                    }
                }
                if (currentChart.getClient().getAge() == 0) {
                    if (currentChart.getClient().getMonths() == 0) {
                        return currentChart.getClient().getDays() + " days old/" + currentChart.getClient().getGender().charAt(0);
                    }
                }
                return currentChart.getClient().getAge() + " years old/" + currentChart.getClient().getGender().charAt(0);
            } else {
                return "-";
            }
        } else if (fieldName.equals("issuedate")) {
            return new SimpleDateFormat("MMM dd, yyyy").format(currentChart.getRecorddatetime());
        } else if (fieldName.equals("message")) {
            return currentChart.getAdmittingorders();
        }

        return "";
    }

}
