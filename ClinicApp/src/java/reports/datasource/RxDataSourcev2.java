/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports.datasource;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import models.ChartDetails;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author apple
 */
public class RxDataSourcev2 implements JRDataSource {

    private Iterator<ChartDetails> i;
    private ChartDetails currentRx;
    private List<ChartDetails> rxList;

    public RxDataSourcev2(List<ChartDetails> rxList) {
        this.rxList = rxList;
        i = this.rxList.iterator();
    }

    @Override
    public boolean next() throws JRException {
        boolean hasNext = i.hasNext();
        if (hasNext) {
            currentRx = i.next();

        }
        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        String fieldName = jrf.getName();

        if (fieldName.equals("generic1")) {
            if (currentRx.getGeneric1() != null) {
                return (currentRx.getGeneric1() + " " + currentRx.getPreparation1().toLowerCase() + " #" + currentRx.getQuantity1()).trim().isEmpty() || (currentRx.getGeneric1() + " " + currentRx.getPreparation1().toLowerCase() + " #" + currentRx.getQuantity1()).trim().length() == 1 ? "" : currentRx.getGeneric1() + " " + currentRx.getPreparation1().toLowerCase() + " #" + currentRx.getQuantity1();
            }
        } else if (fieldName.equals("generic2")) {
            if (currentRx.getGeneric2() != null) {
                return (currentRx.getGeneric2() + " " + currentRx.getPreparation2().toLowerCase() + " #" + currentRx.getQuantity2()).trim().isEmpty() || (currentRx.getGeneric2() + " " + currentRx.getPreparation2().toLowerCase() + " #" + currentRx.getQuantity2()).trim().length() == 1 ? "" : currentRx.getGeneric2() + " " + currentRx.getPreparation2().toLowerCase() + " #" + currentRx.getQuantity2();
            }
        } else if (fieldName.equals("generic3")) {
            if (currentRx.getGeneric3() != null) {
                return (currentRx.getGeneric3() + " " + currentRx.getPreparation3().toLowerCase() + " #" + currentRx.getQuantity3()).trim().isEmpty() || (currentRx.getGeneric3() + " " + currentRx.getPreparation3().toLowerCase() + " #" + currentRx.getQuantity3()).trim().length() == 1 ? "" : currentRx.getGeneric3() + " " + currentRx.getPreparation3().toLowerCase() + " #" + currentRx.getQuantity3();
            }
        } else if (fieldName.equals("generic4")) {
            if (currentRx.getGeneric4() != null) {
                return (currentRx.getGeneric4() + " " + currentRx.getPreparation4().toLowerCase() + " #" + currentRx.getQuantity4()).trim().isEmpty() || (currentRx.getGeneric4() + " " + currentRx.getPreparation4().toLowerCase() + " #" + currentRx.getQuantity4()).trim().length() == 1 ? "" : currentRx.getGeneric4() + " " + currentRx.getPreparation4().toLowerCase() + " #" + currentRx.getQuantity4();
            }
        } else if (fieldName.equals("generic5")) {
            if (currentRx.getGeneric5() != null) {
                return (currentRx.getGeneric5() + " " + currentRx.getPreparation5().toLowerCase() + " #" + currentRx.getQuantity5()).trim().isEmpty() || (currentRx.getGeneric5() + " " + currentRx.getPreparation5().toLowerCase() + " #" + currentRx.getQuantity5()).trim().length() == 1 ? "" : currentRx.getGeneric5() + " " + currentRx.getPreparation5().toLowerCase() + " #" + currentRx.getQuantity5();
            }
        } else if (fieldName.equals("generic6")) {
            if (currentRx.getGeneric6() != null) {
                return (currentRx.getGeneric6() + " " + currentRx.getPreparation6().toLowerCase() + " #" + currentRx.getQuantity6()).trim().isEmpty() || (currentRx.getGeneric6() + " " + currentRx.getPreparation6().toLowerCase() + " #" + currentRx.getQuantity6()).trim().length() == 1 ? "" : currentRx.getGeneric6() + " " + currentRx.getPreparation6().toLowerCase() + " #" + currentRx.getQuantity6();
            }
        } else if (fieldName.equals("preparation1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getPreparation1().trim().isEmpty() ? "" : currentRx.getPreparation1().toLowerCase();
            }
        } else if (fieldName.equals("preparation2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getPreparation2().trim().isEmpty() ? "" : currentRx.getPreparation2().toLowerCase();
            }
        } else if (fieldName.equals("preparation3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getPreparation3().trim().isEmpty() ? "" : currentRx.getPreparation3().toLowerCase();
            }
        } else if (fieldName.equals("preparation4")) {
            if (currentRx.getGeneric4() != null) {
                return currentRx.getPreparation4().trim().isEmpty() ? "" : currentRx.getPreparation4().toLowerCase();
            }
        } else if (fieldName.equals("preparation5")) {
            if (currentRx.getGeneric5() != null) {
                return currentRx.getPreparation5().trim().isEmpty() ? "" : currentRx.getPreparation5().toLowerCase();
            }
        } else if (fieldName.equals("preparation6")) {
            if (currentRx.getGeneric6() != null) {
                return currentRx.getPreparation6().trim().isEmpty() ? "" : currentRx.getPreparation6().toLowerCase();
            }
        } else if (fieldName.equals("brand1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getBrand1().trim().isEmpty() ? "" : "(" + currentRx.getBrand1().toUpperCase() + ")";
            }
        } else if (fieldName.equals("brand2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getBrand2().trim().isEmpty() ? "" : "(" + currentRx.getBrand2().toUpperCase() + ")";
            }
        } else if (fieldName.equals("brand3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getBrand3().trim().isEmpty() ? "" : "(" + currentRx.getBrand3().toUpperCase() + ")";
            }
        } else if (fieldName.equals("brand4")) {
            if (currentRx.getGeneric4() != null) {
                return currentRx.getBrand4().trim().isEmpty() ? "" : "(" + currentRx.getBrand4().toUpperCase() + ")";
            }
        } else if (fieldName.equals("brand5")) {
            if (currentRx.getGeneric5() != null) {
                return currentRx.getBrand5().trim().isEmpty() ? "" : "(" + currentRx.getBrand5().toUpperCase() + ")";
            }
        } else if (fieldName.equals("brand6")) {
            if (currentRx.getGeneric6() != null) {
                return currentRx.getBrand6().trim().isEmpty() ? "" : "(" + currentRx.getBrand6().toUpperCase() + ")";
            }
        } else if (fieldName.equals("quantity1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getQuantity1();
            }
        } else if (fieldName.equals("quantity2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getQuantity2();
            }
        } else if (fieldName.equals("quantity3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getQuantity3();
            }
        } else if (fieldName.equals("quantity4")) {
            if (currentRx.getGeneric4() != null) {
                return currentRx.getQuantity4();
            }
        } else if (fieldName.equals("quantity5")) {
            if (currentRx.getGeneric6() != null) {
                return currentRx.getQuantity5();
            }
        } else if (fieldName.equals("quantity6")) {
            if (currentRx.getGeneric6() != null) {
                return currentRx.getQuantity6();
            }
        } else if (fieldName.equals("sig1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getDirection1().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection1() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("sig2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getDirection2().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection2() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("sig3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getDirection3().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection3() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("sig4")) {
            if (currentRx.getGeneric4() != null) {
                return currentRx.getDirection4().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection4() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("sig5")) {
            if (currentRx.getGeneric5() != null) {
                return currentRx.getDirection5().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection5() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("sig6")) {
            if (currentRx.getGeneric6() != null) {
                return currentRx.getDirection6().trim().isEmpty() ? "" : "Sig.:" + currentRx.getDirection6() + "\n\n______________________________________________________";
            }
        } else if (fieldName.equals("patient")) {
            return (currentRx.getClient().getFirstname() + " " + currentRx.getClient().getLastname()).toUpperCase();
        } else if (fieldName.equals("agesex")) {
            if (currentRx.getClient().getAge() != null) {
                if (currentRx.getClient().getAge() == 0) {
                    if (currentRx.getClient().getMonths() > 0) {
                        return currentRx.getClient().getMonths() + " months old/" + currentRx.getClient().getGender().charAt(0);
                    }
                }
                if (currentRx.getClient().getAge() == 0) {
                    if (currentRx.getClient().getMonths() == 0) {
                        return currentRx.getClient().getDays() + " days old/" + currentRx.getClient().getGender().charAt(0);
                    }
                }
                return currentRx.getClient().getAge() + " years old/" + currentRx.getClient().getGender().charAt(0);
            } else {
                return "-";
            }
        } else if (fieldName.equals("issuedate")) {
            if (currentRx.getRecorddatetime() != null) {
                return new SimpleDateFormat("MMM dd, yyyy").format(currentRx.getRecorddatetime());
            }
        } else if (fieldName.equals("nextcheckup")) {
            if (currentRx.getNextcheckup() != null) {
                return new SimpleDateFormat("MMM dd, yyyy").format(currentRx.getNextcheckup());
            }
        }
//

        return "";
    }

}
