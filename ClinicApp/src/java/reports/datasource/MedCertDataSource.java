/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports.datasource;

import java.util.Iterator;
import java.util.List;
import models.Medcert;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author apple
 */
public class MedCertDataSource implements JRDataSource{
    
    private Iterator<Medcert> i;
    private Medcert currentMedCert;
    private List<Medcert> certList;

    public MedCertDataSource(List<Medcert> certList) {
        this.certList = certList;
        i = this.certList.iterator();
    }
    
    
    
    @Override
    public boolean next() throws JRException {
        boolean hasNext = i.hasNext();
        if(hasNext){
            currentMedCert = i.next();
            
        }
        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        String fieldName = jrf.getName();
        if(fieldName.equals("message")){
            return currentMedCert.getMessage();
        }
        return "";
    }

    /**
     * @return the currentMedCert
     */
    public Medcert getCurrentMedCert() {
        return currentMedCert;
    }

    /**
     * @param currentMedCert the currentMedCert to set
     */
    public void setCurrentMedCert(Medcert currentMedCert) {
        this.currentMedCert = currentMedCert;
    }
    
}
