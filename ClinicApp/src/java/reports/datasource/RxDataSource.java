/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports.datasource;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import models.Prescription;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author apple
 */
public class RxDataSource implements JRDataSource {

    private Iterator<Prescription> i;
    private Prescription currentRx;
    private List<Prescription> rxList;

    public RxDataSource(List<Prescription> rxList) {
        this.rxList = rxList;
        i = this.rxList.iterator();
    }

    @Override
    public boolean next() throws JRException {
        boolean hasNext = i.hasNext();
        if (hasNext) {
            currentRx = i.next();

        }
        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        String fieldName = jrf.getName();

        if (fieldName.equals("generic1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getGeneric1() + " " + currentRx.getPreparation1().toLowerCase() + " #" + currentRx.getQuantity1();
            }
        } else if (fieldName.equals("generic2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getGeneric2() + " " + currentRx.getPreparation2().toLowerCase() + " #" + currentRx.getQuantity2();
            }
        } else if (fieldName.equals("generic3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getGeneric3() + " " + currentRx.getPreparation3().toLowerCase() + " #" + currentRx.getQuantity3();
            }
        } else if (fieldName.equals("preparation1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getPreparation1();
            }
        } else if (fieldName.equals("preparation2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getPreparation2();
            }
        } else if (fieldName.equals("preparation3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getPreparation3();
            }
        } else if (fieldName.equals("brand1")) {
            if (currentRx.getGeneric1() != null) {
                return "(" + currentRx.getBrand1() + ")";
            }
        } else if (fieldName.equals("brand2")) {
            if (currentRx.getGeneric2() != null) {
                return "(" + currentRx.getBrand2() + ")";
            }
        } else if (fieldName.equals("brand3")) {
            if (currentRx.getGeneric3() != null) {
                return "(" + currentRx.getBrand3() + ")";
            }
        } else if (fieldName.equals("quantity1")) {
            if (currentRx.getGeneric1() != null) {
                return currentRx.getQuantity1();
            }
        } else if (fieldName.equals("quantity2")) {
            if (currentRx.getGeneric2() != null) {
                return currentRx.getQuantity2();
            }
        } else if (fieldName.equals("quantity3")) {
            if (currentRx.getGeneric3() != null) {
                return currentRx.getQuantity3();
            }
        } else if (fieldName.equals("sig1")) {
            if (currentRx.getGeneric1() != null) {
                return "Sig.:" + currentRx.getDirection1();
            }
        } else if (fieldName.equals("sig2")) {
            if (currentRx.getGeneric2() != null) {
                return "Sig.:" + currentRx.getDirection2();
            }
        } else if (fieldName.equals("sig3")) {
            if (currentRx.getGeneric3() != null) {
                return "Sig.:" + currentRx.getDirection3();
            }
        } else if (fieldName.equals("patient")) {
            return currentRx.getClient().getFirstname() + " " + currentRx.getClient().getLastname();
        } else if (fieldName.equals("agesex")) {
            if (currentRx.getClient().getAge() != null) {
                if (currentRx.getClient().getAge() == 0) {
                    if (currentRx.getClient().getMonths() > 0) {
                        return currentRx.getClient().getMonths() + " months old/" + currentRx.getClient().getGender().charAt(0);
                    }
                }
                if (currentRx.getClient().getAge() == 0) {
                    if (currentRx.getClient().getMonths() == 0) {
                        return currentRx.getClient().getDays() + " days old/" + currentRx.getClient().getGender().charAt(0);
                    }
                }
                return currentRx.getClient().getAge() + " years old/" + currentRx.getClient().getGender().charAt(0);
            } else {
                return "-";
            }
        } else if (fieldName.equals("issuedate")) {
            return new SimpleDateFormat("MMM dd, yyyy").format(currentRx.getIssuedate());
        }

        return "";
    }

}
