/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package reports.datasource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import models.ChartDetails;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

/**
 *
 * @author apple
 */
public class MedCertDataSourcev2 implements JRDataSource{
    
    private Iterator<ChartDetails> i;
    private ChartDetails currentChart;
    private List<ChartDetails> rxList;
    private String dateString;

    public MedCertDataSourcev2(List<ChartDetails> rxList) {
        this.rxList = rxList;
        i = this.rxList.iterator();
    }

    @Override
    public boolean next() throws JRException {
        boolean hasNext = i.hasNext();
        if (hasNext) {
            currentChart = i.next();

        }
        return hasNext;
    }

    @Override
    public Object getFieldValue(JRField jrf) throws JRException {
        String fieldName = jrf.getName();
        if(fieldName.equals("message")){
            return currentChart.getMedCertMessage();
        }else if(fieldName.equals("diagnosis")){
            return currentChart.getMedcertdiagnosis();
        }else if(fieldName.equals("test")){
            return currentChart.getAncilliarytests();
        }else if(fieldName.equals("recommendation")){
            return currentChart.getRecommendation() + getDateString();
        }
        
        return "";
    }

    /**
     * @return the currentChart
     */
    public ChartDetails getCurrentChart() {
        return currentChart;
    }

    /**
     * @param currentChart the currentChart to set
     */
    public void setCurrentChart(ChartDetails currentChart) {
        this.currentChart = currentChart;
    }
    
    
    public String getDateString() {
        String d = "";
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());

        switch (c.get(Calendar.DAY_OF_MONTH)) {
            case 1:
                d = "1st";
                break;
            case 2:
                d = "2nd";
                break;
            case 3:
                d = "3rd";
                break;
            case 4:
                d = "4th";
                break;
            case 5:
                d = "5th";
                break;
            case 6:
                d = "6th";
                break;
            case 7:
                d = "7th";
                break;
            case 8:
                d = "8th";
                break;
            case 9:
                d = "9th";
                break;
            case 10:
                d = "10th";
                break;
            case 11:
                d = "11th";
                break;
            case 12:
                d = "12th";
                break;
            case 13:
                d = "13th";
                break;
            case 14:
                d = "16th";
                break;
            case 15:
                d = "17th";
                break;
            case 16:
                d = "16th";
                break;
            case 17:
                d = "17th";
                break;
            case 18:
                d = "18th";
                break;
            case 19:
                d = "19th";
                break;
            case 20:
                d = "20th";
                break;
            case 21:
                d = "21st";
                break;
            case 22:
                d = "22nd";
                break;
            case 23:
                d = "23rd";
                break;
            case 24:
                d = "24th";
                break;
            case 25:
                d = "25th";
                break;
            case 26:
                d = "26th";
                break;
            case 27:
                d = "27th";
                break;
            case 28:
                d = "28th";
                break;
            case 29:
                d = "29th";
                break;
            case 30:
                d = "30th";
                break;
            case 31:
                d = "31st";
                break;
        }
        dateString = "\n\nThis certification is issued for whatever purpose it may serve. \nGiven this " + d + " day of " + new SimpleDateFormat("MMMM").format(new Date()) + ", " + c.get(Calendar.YEAR) + ".";
        
        return dateString;
    }
    
}
