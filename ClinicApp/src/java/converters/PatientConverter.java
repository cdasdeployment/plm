/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;

/**
 *
 * @author apple
 */
@Named("patientConverter")
public class PatientConverter implements Converter {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string.trim().equals("")) {
            return null;
        } else {
            try {
                EntityManager em = factory.createEntityManager();
                return em.find(Client.class, Integer.parseInt(string));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o == null || o.equals("")) {
            return "";
        } else {
            return ((Client) o).getId().toString();
        }
    }

}
