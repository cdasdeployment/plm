/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package converters;

import controllers.jpa.MedicineitemJpaController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Medicineitem;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author apple
 */
@Named("medicineConverter")
public class MedicineConverter implements Converter {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        EntityManager em = factory.createEntityManager();
        //System.out.println("String parameter is: " + string);
        if (!StringUtils.isNumeric(string)) {
            if (string.trim().length() > 2) {
                MedicineitemJpaController medController = new MedicineitemJpaController(utx, factory);
                Medicineitem m = new Medicineitem();
                m.setBrandname("");
                m.setBuying("");
                m.setCapital(0.0);
                m.setExpiration("");
                m.setGenericname(string.trim().toUpperCase());
                m.setMtype("");
                m.setPreparation("");
                m.setPrice(0.0);
                m.setPurchase("");
                m.setSelling(0.0);
                m.setStocks(0);
                m.setSupplier("");
                m.setUsage("");
                m.setReorder(0);
                m.setIsrx(false);
                m.setIsActive(false);
                try {
                    medController.create(m);
                } catch (Exception ex) {
                    Logger.getLogger(MedicineConverter.class.getName()).log(Level.SEVERE, null, ex);
                }
                return em.find(Medicineitem.class, m.getId());
            }
        } else if (string.trim().equals("")) {
            return null;
        } else {
            try {
                return em.find(Medicineitem.class, Integer.parseInt(string));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o == null || o.equals("")) {
            return "";
        } else {
            return ((Medicineitem) o).getId().toString();
        }
    }

}
