/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.MedicineitemJpaController;
import controllers.jpa.SupplierJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Medicineitem;
import models.Supplier;

/**
 *
 * @author apple
 */
@ManagedBean(name = "medicineController")
@ViewScoped
public class MedicineController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String genericname;
    private String brandname;
    private String preparation;
    private String usage;
    private String mtype;
    private Double price = 0.0;
    private Integer stocks = 0;
    private Integer reorder = 0;
    private String supplier;
    private String expiration;
    private String purchase;
    private Double selling = 0.0;
    private String buying;
    private Double capital = 0.0;

    private ArrayList months = new ArrayList();
    private ArrayList years = new ArrayList();

    private String purchaseMonth = "-";
    private String purchaseYear = "-";

    private String expireMonth = "-";
    private String expireYear = "-";

    //inventory mgt
    private int update = 0;
    private int returned = 0;
    private int pullout = 0;
    private int purchased = 0;

    private List<String> supplierList = new ArrayList();
    private SupplierJpaController scontroller = new SupplierJpaController(utx, factory);
    private MedicineitemJpaController controller = new MedicineitemJpaController(utx, factory);
    private Medicineitem thisItem;
    private List<Medicineitem> drugList = new ArrayList();
    private String searchkey = "";

    @PostConstruct
    public void init() {
        scontroller = new SupplierJpaController(utx, factory);
        controller = new MedicineitemJpaController(utx, factory);
        supplierList = new ArrayList();
        drugList = new ArrayList();

        for (Supplier s : scontroller.findSupplierEntities()) {
            getSupplierList().add(s.getSuppliername());
        }

        drugList = controller.findMedicineitemEntities();

        //year
        int initYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int y = initYear; y >= (initYear - 1); y--) {
            years.add(y);
        }
        years.add("-");

        //months
        months.add("-");
        months.add("January");
        months.add("February");
        months.add("March");
        months.add("April");
        months.add("May");
        months.add("June");
        months.add("July");
        months.add("August");
        months.add("September");
        months.add("October");
        months.add("November");
        months.add("December");

    }

    public void refreshDrugList() {
        drugList = new ArrayList();
        controller = new MedicineitemJpaController(utx, factory);
        drugList = controller.findMedicineitemEntities();
    }

    public List<Medicineitem> search() {
        searchkey = searchkey.toLowerCase();
        System.out.println(searchkey);
        drugList.clear();
        controller = new MedicineitemJpaController(utx, factory);
        if (searchkey.trim().isEmpty()) {
            drugList = controller.findMedicineitemEntities();
        } else {
            EntityManager em = factory.createEntityManager();
            for (Medicineitem m : controller.findMedicineitemEntities()) {
                //System.out.println(s.getFirstname().toLowerCase().contains(searchKey) || s.getLastname().toLowerCase().contains(searchKey));
                if (m.getGenericname().toLowerCase().contains(searchkey) || m.getBrandname().toLowerCase().contains(searchkey)
                        || m.getSupplier().toLowerCase().contains(searchkey)) {
                    m = em.merge(m);
                    em.refresh(m);
                    drugList.add(m);
                }
            }
            em.close();
        }
        return drugList;
    }

    public void prepareForm() {
        genericname = "";
        brandname = "";
        preparation = "";
        usage = "";
        mtype = "";
        price = 0.0;
        stocks = 0;
        reorder = 0;
        supplier = "";
        expiration = "";
        purchase = "";
        selling = 0.0;
        buying = "";
        capital = 0.0;

    }

    public void prepareDrugInfo() {
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(thisItem));
            em.refresh(thisItem);
            em.close();
        }
    }

    public void update() {

        controller = new MedicineitemJpaController(utx, factory);
        try {
            thisItem.setGenericname(thisItem.getGenericname().toUpperCase());
            thisItem.setBrandname(thisItem.getBrandname().toUpperCase());
            thisItem.setReorder((int) (stocks * 0.20));
            controller.edit(thisItem);
            JsfUtil.addSuccessMessage("Item record updated!");
            refreshDrugList();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(MedicineController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MedicineController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        if (supplier == null) {
            JsfUtil.addErrorMessage("Please indicate supplier name (Required). Item not saved.");
            return;
        }

        if (supplier.isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate supplier name (Required). Item not saved.");
            return;
        }

        if (brandname.isEmpty() && genericname.isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate generic name/brand name (Required). Item not saved.");
            return;
        }

        if (selling <= 0) {
            JsfUtil.addErrorMessage("Selling price should be greater than zero (Required). Item not saved.");
            return;

        }
        if (stocks <= 0) {
            JsfUtil.addErrorMessage("Quantity in-stock should be greater than zero (Required). Item not saved.");
            return;
        }

        if (purchaseMonth == null || purchaseMonth.trim().isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate purchase month (Required). Item not saved.");
            return;
        }

        if (purchaseYear == null || purchaseYear.trim().isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate purchase year (Required). Item not saved.");
            return;
        }

        if (expireMonth == null || expireMonth.trim().isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate expiration month (Required). Item not saved.");
            return;
        }

        if (expireYear == null || expireYear.trim().isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate expiration year (Required). Item not saved.");
            return;
        }

        controller = new MedicineitemJpaController(utx, factory);
        Medicineitem newMedicine = new Medicineitem();
        newMedicine.setBrandname(brandname.trim().toUpperCase());
        newMedicine.setBuying(buying);
        newMedicine.setCapital(capital);
        newMedicine.setExpiration(expireMonth + " " + expireYear);
        newMedicine.setGenericname(genericname.trim().toUpperCase());
        newMedicine.setMtype(mtype);
        newMedicine.setPreparation(preparation);
        newMedicine.setPrice(price);
        newMedicine.setPurchase(purchaseMonth + " " + purchaseYear);
        newMedicine.setSelling(selling);
        newMedicine.setStocks(stocks);
        newMedicine.setSupplier(supplier);
        newMedicine.setUsage(usage);
        newMedicine.setReorder((int) (stocks * 0.20));
        newMedicine.setIsrx(true);
        newMedicine.setPurchasemonth(purchaseMonth);
        newMedicine.setPurchaseyear(purchaseYear);
        newMedicine.setExpiremonth(expireMonth);
        newMedicine.setExpireyear(expireYear);

        try {
            controller.create(newMedicine);
            JsfUtil.addSuccessMessage("New drug profile added!");
            refreshDrugList();
        } catch (Exception ex) {
            Logger.getLogger(MedicineController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the genericname
     */
    public String getGenericname() {
        return genericname;
    }

    /**
     * @param genericname the genericname to set
     */
    public void setGenericname(String genericname) {
        this.genericname = genericname;
    }

    /**
     * @return the brandname
     */
    public String getBrandname() {
        return brandname;
    }

    /**
     * @param brandname the brandname to set
     */
    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    /**
     * @return the preparation
     */
    public String getPreparation() {
        return preparation;
    }

    /**
     * @param preparation the preparation to set
     */
    public void setPreparation(String preparation) {
        this.preparation = preparation;
    }

    /**
     * @return the usage
     */
    public String getUsage() {
        return usage;
    }

    /**
     * @param usage the usage to set
     */
    public void setUsage(String usage) {
        this.usage = usage;
    }

    /**
     * @return the mtype
     */
    public String getMtype() {
        return mtype;
    }

    /**
     * @param mtype the mtype to set
     */
    public void setMtype(String mtype) {
        this.mtype = mtype;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return the stocks
     */
    public Integer getStocks() {
        return stocks;
    }

    /**
     * @param stocks the stocks to set
     */
    public void setStocks(Integer stocks) {
        this.stocks = stocks;
    }

    /**
     * @return the reorder
     */
    public Integer getReorder() {
        return reorder;
    }

    /**
     * @param reorder the reorder to set
     */
    public void setReorder(Integer reorder) {
        this.reorder = reorder;
    }

    /**
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * @param supplier the supplier to set
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * @return the expiration
     */
    public String getExpiration() {
        return expiration;
    }

    /**
     * @param expiration the expiration to set
     */
    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    /**
     * @return the purchase
     */
    public String getPurchase() {
        return purchase;
    }

    /**
     * @param purchase the purchase to set
     */
    public void setPurchase(String purchase) {
        this.purchase = purchase;
    }

    /**
     * @return the selling
     */
    public Double getSelling() {
        return selling;
    }

    /**
     * @param selling the selling to set
     */
    public void setSelling(Double selling) {
        this.selling = selling;
    }

    /**
     * @return the buying
     */
    public String getBuying() {
        return buying;
    }

    /**
     * @param buying the buying to set
     */
    public void setBuying(String buying) {
        this.buying = buying;
    }

    /**
     * @return the supplierList
     */
    public List<String> getSupplierList() {
        return supplierList;
    }

    /**
     * @param supplierList the supplierList to set
     */
    public void setSupplierList(List<String> supplierList) {
        this.supplierList = supplierList;
    }

    /**
     * @return the capital
     */
    public Double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(Double capital) {
        this.capital = capital;
    }

    /**
     * @return the thisItem
     */
    public Medicineitem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(Medicineitem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the drugList
     */
    public List<Medicineitem> getDrugList() {
        return drugList;
    }

    /**
     * @param drugList the drugList to set
     */
    public void setDrugList(List<Medicineitem> drugList) {
        this.drugList = drugList;
    }

    /**
     * @return the searchkey
     */
    public String getSearchkey() {
        return searchkey;
    }

    /**
     * @param searchkey the searchkey to set
     */
    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

    /**
     * @return the update
     */
    public int getUpdate() {
        return update;
    }

    /**
     * @param update the update to set
     */
    public void setUpdate(int update) {
        this.update = update;
    }

    /**
     * @return the returned
     */
    public int getReturned() {
        return returned;
    }

    /**
     * @param returned the returned to set
     */
    public void setReturned(int returned) {
        this.returned = returned;
    }

    /**
     * @return the pullout
     */
    public int getPullout() {
        return pullout;
    }

    /**
     * @param pullout the pullout to set
     */
    public void setPullout(int pullout) {
        this.pullout = pullout;
    }

    /**
     * @return the purchased
     */
    public int getPurchased() {
        return purchased;
    }

    /**
     * @param purchased the purchased to set
     */
    public void setPurchased(int purchased) {
        this.purchased = purchased;
    }

    /**
     * @return the months
     */
    public ArrayList getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(ArrayList months) {
        this.months = months;
    }

    /**
     * @return the years
     */
    public ArrayList getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(ArrayList years) {
        this.years = years;
    }

    /**
     * @return the purchaseMonth
     */
    public String getPurchaseMonth() {
        return purchaseMonth;
    }

    /**
     * @param purchaseMonth the purchaseMonth to set
     */
    public void setPurchaseMonth(String purchaseMonth) {
        this.purchaseMonth = purchaseMonth;
    }

    /**
     * @return the purchaseyear
     */
    public String getPurchaseYear() {
        return purchaseYear;
    }

    /**
     * @param purchaseyear the purchaseyear to set
     */
    public void setPurchaseYear(String purchaseyear) {
        this.purchaseYear = purchaseyear;
    }

    /**
     * @return the expireMonth
     */
    public String getExpireMonth() {
        return expireMonth;
    }

    /**
     * @param expireMonth the expireMonth to set
     */
    public void setExpireMonth(String expireMonth) {
        this.expireMonth = expireMonth;
    }

    /**
     * @return the expireYear
     */
    public String getExpireYear() {
        return expireYear;
    }

    /**
     * @param expireYear the expireYear to set
     */
    public void setExpireYear(String expireYear) {
        this.expireYear = expireYear;
    }

}
