/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.MedicineitemJpaController;
import controllers.jpa.PrescriptionJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.Config;
import controllers.util.JsfUtil;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.transaction.UserTransaction;
import models.Client;
import models.Medicineitem;
import models.Prescription;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.primefaces.event.SelectEvent;
import reports.datasource.RxDataSource;

/**
 *
 * @author apple
 */
@ManagedBean
@ViewScoped
public class PrescriptionController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Date issuedate = new Date();
    private String issuedateString = "";
    private String age;
    private String gender;
    private String generic1;
    private String brand1;
    private String preparation1;
    private String quantity1;
    private String direction1;
    private String generic2;
    private String brand2;
    private String preparation2;
    private String quantity2;
    private String direction2;
    private String generic3;
    private String brand3;
    private String preparation3;
    private String quantity3;
    private String direction3;
    private Client client;
    private String nextcheckup;
    private MedicineitemJpaController medController;
    private PrescriptionJpaController pController;
    private Medicineitem drug1;
    private Medicineitem drug2;
    private Medicineitem drug3;
    //searching
    private String searchKey = "";
    private String pdfViewPath = "";

    @PostConstruct
    public void init() {
        pController = new PrescriptionJpaController(utx, factory);
        medController = new MedicineitemJpaController(utx, factory);
        issuedate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        issuedateString = sdf.format(issuedate);
        age = "";
        gender = "";
        generic1 = "";
        brand1 = "";
        preparation1 = "";
        quantity1 = "";
        direction1 = "";
        generic2 = "";
        brand2 = "";
        preparation2 = "";
        quantity2 = "";
        direction2 = "";
        generic3 = "";
        brand3 = "";
        preparation3 = "";
        quantity3 = "";
        direction3 = "";
        pdfViewPath = "";
        drug1 = null;
        drug2 = null;
        drug3 = null;
        client = null;
    }

    public void newRxForm() {
        issuedate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        issuedateString = sdf.format(issuedate);
        age = "";
        gender = "";
        generic1 = "";
        brand1 = "";
        preparation1 = "";
        quantity1 = "";
        direction1 = "";
        generic2 = "";
        brand2 = "";
        preparation2 = "";
        quantity2 = "";
        direction2 = "";
        generic3 = "";
        brand3 = "";
        preparation3 = "";
        quantity3 = "";
        direction3 = "";
        pdfViewPath = "";
        drug1 = null;
        drug2 = null;
        drug3 = null;
        client = null;

    }

    private Prescription rx;

    public void downloadRx() {
        try {
            save();

            List<Prescription> list = new ArrayList<Prescription>();

            list.add(rx);
            RxDataSource dataSource = new RxDataSource(list);
            JasperPrint jprint = JasperFillManager.fillReport(Config.reportTemplateDirectory + File.separator + "rx.jasper", new HashMap(), dataSource);
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            File tempfile = new File(servletContext.getRealPath("/webapp") + File.separator + "TEMP");

            if (!tempfile.exists()) {
                tempfile.mkdir();
            }
            String pdfFile = tempfile + File.separator + "Rx" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(new Date()) + ".pdf";

            pdfViewPath = File.separator + "TEMP" + File.separator + "Rx" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(new Date()) + ".pdf";

            File f = new File(pdfFile);
            if (f.exists()) {
                f.delete();
            }
            if (f.createNewFile()) {
                JasperExportManager.exportReportToPdfFile(jprint, pdfFile);
                
                rx.setFilepath(pdfFile);
                pController = new PrescriptionJpaController(utx, factory);
                pController.edit(rx);
                
            }

        } catch (JRException ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void save() {

        if (client == null) {
            JsfUtil.addErrorMessage("Please specify patient.");
            return;
        }

        if (drug1 == null && drug2 == null && drug3 == null) {
            JsfUtil.addErrorMessage("Please specify medicines for prescription.");
            return;
        }

        rx = new Prescription();

        if (drug1 != null) {
            generic1 = drug1.getGenericname();
            brand1 = drug1.getBrandname();
            preparation1 = drug1.getPreparation() + " " + drug1.getMtype().toLowerCase();

            rx.setGeneric1(generic1);
            rx.setBrand1(brand1);
            rx.setPreparation1(preparation1);
            rx.setQuantity1(quantity1);
            rx.setDirection1(direction1);

        }

        if (drug2 != null) {
            generic1 = drug1.getGenericname();
            brand1 = drug1.getBrandname();
            preparation2 = drug2.getPreparation() + " " + drug2.getMtype().toLowerCase();

            rx.setGeneric2(generic2);
            rx.setBrand2(brand2);
            rx.setPreparation2(preparation2);
            rx.setQuantity2(quantity2);
            rx.setDirection2(direction2);

        }

        if (drug3 != null) {
            generic1 = drug1.getGenericname();
            brand1 = drug1.getBrandname();
            preparation3 = drug3.getPreparation() + " " + drug3.getMtype().toLowerCase();

            rx.setGeneric3(generic3);
            rx.setBrand3(brand3);
            rx.setPreparation3(preparation3);
            rx.setQuantity3(quantity3);
            rx.setDirection3(direction3);

        }

        rx.setIssuedate(issuedate);
        rx.setNextcheckup(nextcheckup);
        rx.setClient(client);
        rx.setAge(age);
        rx.setGender(gender.charAt(0)+"");

        pController = new PrescriptionJpaController(utx, factory);
        try {
            pController.create(rx);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(PrescriptionController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void handleSelect(SelectEvent event) {
        client = (Client) event.getObject();
        age = client.getAge() == null ? "" : client.getAge() + "";
        gender = client.getGender();
    }

    /**
     * @return the issuedate
     */
    public Date getIssuedate() {
        return issuedate;
    }

    /**
     * @param issuedate the issuedate to set
     */
    public void setIssuedate(Date issuedate) {
        this.issuedate = issuedate;
    }

    /**
     * @return the age
     */
    public String getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(String age) {
        this.age = age;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the generic1
     */
    public String getGeneric1() {
        return generic1;
    }

    /**
     * @param generic1 the generic1 to set
     */
    public void setGeneric1(String generic1) {
        this.generic1 = generic1;
    }

    /**
     * @return the brand1
     */
    public String getBrand1() {
        return brand1;
    }

    /**
     * @param brand1 the brand1 to set
     */
    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    /**
     * @return the preparation1
     */
    public String getPreparation1() {
        return preparation1;
    }

    /**
     * @param preparation1 the preparation1 to set
     */
    public void setPreparation1(String preparation1) {
        this.preparation1 = preparation1;
    }

    /**
     * @return the quantity1
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * @param quantity1 the quantity1 to set
     */
    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    /**
     * @return the direction1
     */
    public String getDirection1() {
        return direction1;
    }

    /**
     * @param direction1 the direction1 to set
     */
    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    /**
     * @return the generic2
     */
    public String getGeneric2() {
        return generic2;
    }

    /**
     * @param generic2 the generic2 to set
     */
    public void setGeneric2(String generic2) {
        this.generic2 = generic2;
    }

    /**
     * @return the brand2
     */
    public String getBrand2() {
        return brand2;
    }

    /**
     * @param brand2 the brand2 to set
     */
    public void setBrand2(String brand2) {
        this.brand2 = brand2;
    }

    /**
     * @return the preparation2
     */
    public String getPreparation2() {
        return preparation2;
    }

    /**
     * @param preparation2 the preparation2 to set
     */
    public void setPreparation2(String preparation2) {
        this.preparation2 = preparation2;
    }

    /**
     * @return the quantity2
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * @param quantity2 the quantity2 to set
     */
    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    /**
     * @return the direction2
     */
    public String getDirection2() {
        return direction2;
    }

    /**
     * @param direction2 the direction2 to set
     */
    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    /**
     * @return the generic3
     */
    public String getGeneric3() {
        return generic3;
    }

    /**
     * @param generic3 the generic3 to set
     */
    public void setGeneric3(String generic3) {
        this.generic3 = generic3;
    }

    /**
     * @return the brand3
     */
    public String getBrand3() {
        return brand3;
    }

    /**
     * @param brand3 the brand3 to set
     */
    public void setBrand3(String brand3) {
        this.brand3 = brand3;
    }

    /**
     * @return the preparation3
     */
    public String getPreparation3() {
        return preparation3;
    }

    /**
     * @param preparation3 the preparation3 to set
     */
    public void setPreparation3(String preparation3) {
        this.preparation3 = preparation3;
    }

    /**
     * @return the quantity3
     */
    public String getQuantity3() {
        return quantity3;
    }

    /**
     * @param quantity3 the quantity3 to set
     */
    public void setQuantity3(String quantity3) {
        this.quantity3 = quantity3;
    }

    /**
     * @return the direction3
     */
    public String getDirection3() {
        return direction3;
    }

    /**
     * @param direction3 the direction3 to set
     */
    public void setDirection3(String direction3) {
        this.direction3 = direction3;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the issuedateString
     */
    public String getIssuedateString() {
        return issuedateString;
    }

    /**
     * @param issuedateString the issuedateString to set
     */
    public void setIssuedateString(String issuedateString) {
        this.issuedateString = issuedateString;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }

    /**
     * @param searchKey the searchKey to set
     */
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    public List<Medicineitem> completeDrug(String q) {
        List<Medicineitem> suggestions = new ArrayList<Medicineitem>();
        EntityManager em = factory.createEntityManager();
        Query query = em.createQuery("SELECT m from Medicineitem m WHERE m.genericname LIKE '" + q.trim().toLowerCase() + "%' ORDER BY m.genericname").setMaxResults(5);
        //Query query = em.createQuery("SELECT c FROM Client c WHERE c.lastname LIKE '" + q.trim().toLowerCase() + "%' OR c.firstname LIKE '" + q.trim().toLowerCase() + "%' ORDER BY c.lastname,c.firstname").setMaxResults(5);
        for (Object c : query.getResultList()) {
            suggestions.add(((Medicineitem) c));
        }
        em.close();
        return suggestions;
    }

    public void handleSelectDrug1(SelectEvent event) {
        drug1 = (Medicineitem) event.getObject();
        generic1 = drug1.getGenericname();
        brand1 = drug1.getBrandname();
        preparation1 = drug1.getPreparation() + " " + drug1.getMtype();
    }

    public void handleSelectDrug2(SelectEvent event) {
       drug2 = (Medicineitem) event.getObject();
        generic2 = drug2.getGenericname();
        brand2 = drug2.getBrandname();
        preparation2 = drug2.getPreparation() + " " + drug2.getMtype();
    }

    public void handleSelectDrug3(SelectEvent event) {
        drug3 = (Medicineitem) event.getObject();
        generic3 = drug3.getGenericname();
        brand3 = drug3.getBrandname();
        preparation3 = drug3.getPreparation() + " " + drug3.getMtype();
    }

    /**
     * @return the drug1
     */
    public Medicineitem getDrug1() {
        return drug1;
    }

    /**
     * @param drug1 the drug1 to set
     */
    public void setDrug1(Medicineitem drug1) {
        this.drug1 = drug1;
    }

    /**
     * @return the drug2
     */
    public Medicineitem getDrug2() {
        return drug2;
    }

    /**
     * @param drug2 the drug2 to set
     */
    public void setDrug2(Medicineitem drug2) {
        this.drug2 = drug2;
    }

    /**
     * @return the drug3
     */
    public Medicineitem getDrug3() {
        return drug3;
    }

    /**
     * @param drug3 the drug3 to set
     */
    public void setDrug3(Medicineitem drug3) {
        this.drug3 = drug3;
    }

    /**
     * @return the pdfViewPath
     */
    public String getPdfViewPath() {
        return pdfViewPath;
    }

    /**
     * @param pdfViewPath the pdfViewPath to set
     */
    public void setPdfViewPath(String pdfViewPath) {
        this.pdfViewPath = pdfViewPath;
    }

    /**
     * @return the nextcheckup
     */
    public String getNextcheckup() {
        return nextcheckup;
    }

    /**
     * @param nextcheckup the nextcheckup to set
     */
    public void setNextcheckup(String nextcheckup) {
        this.nextcheckup = nextcheckup;
    }

}
