/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ChartDetailsJpaController;
import controllers.jpa.ClientJpaController;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Appointment;
import models.BillingDetail;
import models.ChartDetails;
import models.Client;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author apple
 */
@ManagedBean(name = "chartDetailController")
@ViewScoped
public class ChartDetailController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Client client;
    private String familyhistory = "";
    private String medicalhistory = "";
    private String illnesshistory = "";
    private String height = "";
    private String weight = "";
    private String bloodpressure = "";
    private String pulse = "";
    private String referredby = "";
    private Date recorddatetime = new Date();
    private String chiefcomplaint = "";
    private List<BillingDetail> billingList = new ArrayList<>();
    private double totalBilling = 0;

    private List<ChartDetails> chartList = new ArrayList<>();
    private ChartDetails thisChart;

    private ChartDetails selectedChart;

    public void saveInitialChart() {
        System.out.println(client);
        boolean isFound = false;
        if (client != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            System.out.println("client not null");
            ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
            if (client.getChartDetailsList().isEmpty()) {
                System.out.println("empty chart details");
                ChartDetails chartDetail = new ChartDetails();
                chartDetail.setClient(client);
                chartDetail.setFamilyhistory(familyhistory);
                chartDetail.setMedicalhistory(medicalhistory);
                chartDetail.setIllnesshistory(illnesshistory);
                chartDetail.setHeight(height);
                chartDetail.setWeight(weight);
                chartDetail.setBloodpressure(bloodpressure);
                chartDetail.setPulse(pulse);
                chartDetail.setReferredby(referredby);
                chartDetail.setRecorddatetime(recorddatetime);
                chartDetail.setChiefcomplaint(chiefcomplaint);
                chartDetail.setChartCode(simpleDateFormat.format(recorddatetime));
                chartDetail.setCharttype(1);
                try {
                    chartDetailController.create(chartDetail);
                    JsfUtil.addSuccessMessage("Initial chart created!");
                    isFound = true;
                } catch (Exception ex) {
                    Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("chart details not empty");
                for (ChartDetails cd : client.getChartDetailsList()) {
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                    String currentDateString = simpleDateFormat.format(new Date());
                    System.out.println(recordDateString + " --> " + currentDateString);
                    if (recordDateString.equals(currentDateString)) {
                        cd.setFamilyhistory(familyhistory);
                        cd.setMedicalhistory(medicalhistory);
                        cd.setIllnesshistory(illnesshistory);
                        cd.setHeight(height);
                        cd.setWeight(weight);
                        cd.setBloodpressure(bloodpressure);
                        cd.setPulse(pulse);
                        cd.setReferredby(referredby);
                        cd.setRecorddatetime(recorddatetime);
                        cd.setChiefcomplaint(chiefcomplaint);
                        cd.setChartCode(simpleDateFormat.format(recorddatetime));
                        cd.setCharttype(1);
                        try {
                            chartDetailController.edit(cd);
                            JsfUtil.addSuccessMessage("Initial chart updated!");
                            isFound = true;
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (RollbackFailureException ex) {
                            Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                }
                if (isFound == false) {
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    chartDetailController = new ChartDetailsJpaController(utx, factory);
                    ChartDetails chartDetail = new ChartDetails();
                    chartDetail.setClient(client);
                    chartDetail.setFamilyhistory(familyhistory.trim());
                    chartDetail.setMedicalhistory(medicalhistory.trim());
                    chartDetail.setIllnesshistory(illnesshistory.trim());
                    chartDetail.setHeight(height.trim());
                    chartDetail.setWeight(weight.trim());
                    chartDetail.setBloodpressure(bloodpressure.trim());
                    chartDetail.setPulse(pulse.trim());
                    chartDetail.setReferredby(referredby.trim());
                    chartDetail.setRecorddatetime(recorddatetime);
                    chartDetail.setChiefcomplaint(chiefcomplaint.trim());
                    chartDetail.setChartCode(simpleDateFormat.format(recorddatetime));
                    chartDetail.setCharttype(1);
                    try {
                        chartDetailController.create(chartDetail);
                        JsfUtil.addSuccessMessage("Initial chart created!");
                    } catch (Exception ex) {
                        Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void prepareSelectedChart() {
        if (thisChart != null) {
            EntityManager em = factory.createEntityManager();
            thisChart = em.merge(thisChart);
            em.refresh(thisChart);
            em.close();

        }
    }

    public void prepareClientChart() {
        if (client != null) {
            //update birthday start
            if (client.getBirthday() != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(client.getBirthday());
                LocalDate today = LocalDate.now();
                LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

                Period period = Period.between(bday, today);
                client.setAge(period.getYears());
                client.setMonths(period.getMonths());
                client.setDays(period.getDays());
                ClientJpaController clienController = new ClientJpaController(utx, factory);
                try {
                    clienController.edit(client);
                } catch (NonexistentEntityException ex) {
                    Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (RollbackFailureException ex) {
                    Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            //update birthday end

            EntityManager em = factory.createEntityManager();
            client = em.merge(client);
            em.refresh(client);
            em.close();

            if (client.getChartDetailsList().isEmpty()) {
                familyhistory = "";
                medicalhistory = "";
                illnesshistory = "";
                height = "";
                weight = "";
                bloodpressure = "";
                pulse = "";
                referredby = "";
                recorddatetime = new Date();
                chiefcomplaint = "";
                ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                ChartDetails chartDetail = new ChartDetails();
                chartDetail.setClient(client);
                chartDetail.setFamilyhistory(familyhistory);
                chartDetail.setMedicalhistory(medicalhistory);
                chartDetail.setIllnesshistory(illnesshistory);
                chartDetail.setHeight(height);
                chartDetail.setWeight(weight);
                chartDetail.setBloodpressure(bloodpressure);
                chartDetail.setPulse(pulse);
                chartDetail.setReferredby(referredby);
                chartDetail.setRecorddatetime(recorddatetime);
                chartDetail.setChiefcomplaint(chiefcomplaint);
                chartDetail.setAge(client.getAge());
                chartDetail.setChartCode(simpleDateFormat.format(recorddatetime));
                try {
                    chartDetailController.create(chartDetail);
                    em = factory.createEntityManager();
                    client = em.merge(client);
                    em.refresh(client);
                    em.close();

                    setChartList(new ArrayList<>());
                    for (ChartDetails cd : client.getChartDetailsList()) {
                        getChartList().add(cd);
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                setChartList(new ArrayList<>());
                for (ChartDetails cd : client.getChartDetailsList()) {
                    getChartList().add(cd);
                }

                for (ChartDetails cd : client.getChartDetailsList()) {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                    String currentDateString = simpleDateFormat.format(new Date());
                    if (recordDateString.equals(currentDateString)) {
                        selectedChart = cd;
                        selectedChart.setAge(client.getAge());
                        familyhistory = cd.getFamilyhistory();
                        medicalhistory = cd.getMedicalhistory();
                        illnesshistory = cd.getIllnesshistory();
                        height = cd.getHeight();
                        weight = cd.getWeight();
                        bloodpressure = cd.getBloodpressure();
                        pulse = cd.getPulse();
                        referredby = cd.getReferredby();
                        recorddatetime = cd.getRecorddatetime();
                        chiefcomplaint = cd.getChiefcomplaint();
                        //billing
                        billingList = new ArrayList<>();
                        billingList = cd.getBillingDetailList();

                        break;
                    }
                }
            }
        }
    }

    public void onChartSelect(SelectEvent event) {
        thisChart = (ChartDetails) event.getObject();
        EntityManager em = factory.createEntityManager();
        thisChart = em.merge(thisChart);
        em.refresh(thisChart);
        em.close();
    }

    public void onRowSelect(SelectEvent event) {
        Appointment selObject = ((Appointment) event.getObject());
        if (selObject != null) {
            client = selObject.getClient();
            prepareClientChart();
        }
    }

    public void doctorRowSelect(SelectEvent event) {
        Appointment selObject = ((Appointment) event.getObject());
        if (selObject != null) {
            client = selObject.getClient();
            if (client != null) {
                EntityManager em = factory.createEntityManager();
                client = em.merge(client);
                em.refresh(client);
                em.close();

                ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
                if (client.getChartDetailsList().isEmpty()) {
                    selectedChart = new ChartDetails();
                    selectedChart.setClient(client);
                    selectedChart.setRecorddatetime(new Date());
                    selectedChart.setFamilyhistory("");
                    selectedChart.setMedicalhistory("");
                    selectedChart.setIllnesshistory("");
                    selectedChart.setHeight("");
                    selectedChart.setWeight("");
                    selectedChart.setBloodpressure("");
                    selectedChart.setPulse("");
                    selectedChart.setReferredby("");
                    selectedChart.setChiefcomplaint("");
                    try {
                        chartDetailController.create(selectedChart);
                    } catch (Exception ex) {
                        Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    for (ChartDetails cd : client.getChartDetailsList()) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                        String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                        String currentDateString = simpleDateFormat.format(new Date());
                        if (recordDateString.equals(currentDateString)) {
                            selectedChart = cd;
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the familyhistory
     */
    public String getFamilyhistory() {
        return familyhistory;
    }

    /**
     * @param familyhistory the familyhistory to set
     */
    public void setFamilyhistory(String familyhistory) {
        this.familyhistory = familyhistory;
    }

    /**
     * @return the medicalhistory
     */
    public String getMedicalhistory() {
        return medicalhistory;
    }

    /**
     * @param medicalhistory the medicalhistory to set
     */
    public void setMedicalhistory(String medicalhistory) {
        this.medicalhistory = medicalhistory;
    }

    /**
     * @return the illnesshistory
     */
    public String getIllnesshistory() {
        return illnesshistory;
    }

    /**
     * @param illnesshistory the illnesshistory to set
     */
    public void setIllnesshistory(String illnesshistory) {
        this.illnesshistory = illnesshistory;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the bloodpressure
     */
    public String getBloodpressure() {
        return bloodpressure;
    }

    /**
     * @param bloodpressure the bloodpressure to set
     */
    public void setBloodpressure(String bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    /**
     * @return the pulse
     */
    public String getPulse() {
        return pulse;
    }

    /**
     * @param pulse the pulse to set
     */
    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    /**
     * @return the referredby
     */
    public String getReferredby() {
        return referredby;
    }

    /**
     * @param referredby the referredby to set
     */
    public void setReferredby(String referredby) {
        this.referredby = referredby;
    }

    /**
     * @return the recorddatetime
     */
    public Date getRecorddatetime() {
        return recorddatetime;
    }

    /**
     * @param recorddatetime the recorddatetime to set
     */
    public void setRecorddatetime(Date recorddatetime) {
        this.recorddatetime = recorddatetime;
    }

    /**
     * @return the chiefcomplaint
     */
    public String getChiefcomplaint() {
        return chiefcomplaint;
    }

    /**
     * @param chiefcomplaint the chiefcomplaint to set
     */
    public void setChiefcomplaint(String chiefcomplaint) {
        this.chiefcomplaint = chiefcomplaint;
    }

    /**
     * @return the selectedChart
     */
    public ChartDetails getSelectedChart() {
        return selectedChart;
    }

    /**
     * @param selectedChart the selectedChart to set
     */
    public void setSelectedChart(ChartDetails selectedChart) {
        this.selectedChart = selectedChart;
    }

    /**
     * @return the chartList
     */
    public List<ChartDetails> getChartList() {
        return chartList;
    }

    /**
     * @param chartList the chartList to set
     */
    public void setChartList(List<ChartDetails> chartList) {
        this.chartList = chartList;
    }

    /**
     * @return the thisChart
     */
    public ChartDetails getThisChart() {
        return thisChart;
    }

    /**
     * @param thisChart the thisChart to set
     */
    public void setThisChart(ChartDetails thisChart) {
        this.thisChart = thisChart;
    }

    /**
     * @return the billingList
     */
    public List<BillingDetail> getBillingList() {
        return billingList;
    }

    /**
     * @param billingList the billingList to set
     */
    public void setBillingList(List<BillingDetail> billingList) {
        this.billingList = billingList;
    }

    /**
     * @return the totalBilling
     */
    public double getTotalBilling() {
        if (selectedChart != null) {
            EntityManager em = factory.createEntityManager();
            selectedChart = em.merge(selectedChart);
            em.refresh(selectedChart);
            em.close();

            totalBilling = 0;
            List<BillingDetail> billDetail = selectedChart.getBillingDetailList();
            if (!billDetail.isEmpty()) {
                for (BillingDetail bd : billDetail) {
                    totalBilling = totalBilling + bd.getAmount();
                }
            }
        }

        return totalBilling;
    }

    /**
     * @param totalBilling the totalBilling to set
     */
    public void setTotalBilling(double totalBilling) {
        this.totalBilling = totalBilling;
    }
}
