/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import java.io.Serializable;
import java.time.Year;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author apple
 */
@Named(value = "appConstants")
@ApplicationScoped
public class AppConstants implements Serializable{

    private Date maxDate;
    private Date maxMonthDate;
    private Date maxYearDate;

    /**
     * @return the maxDate
     */
    public Date getMaxDate() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
        maxDate = c.getTime();
        return maxDate;
    }

    /**
     * @param maxDate the maxDate to set
     */
    public void setMaxDate(Date maxDate) {
        this.maxDate = maxDate;
    }

    /**
     * @return the maxMonthDate
     */
    public Date getMaxMonthDate() {
        return maxMonthDate;
    }

    /**
     * @param maxMonthDate the maxMonthDate to set
     */
    public void setMaxMonthDate(Date maxMonthDate) {
        this.maxMonthDate = maxMonthDate;
    }

    /**
     * @return the maxYearDate
     */
    public Date getMaxYearDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 1900);
        cal.set(Calendar.DAY_OF_YEAR, 1);
        Date start = cal.getTime();

        cal.set(Calendar.YEAR, Year.now().getValue());
        cal.set(Calendar.MONTH, 11); // 11 = december
        cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve

        Date end = cal.getTime();
        GregorianCalendar gcal = new GregorianCalendar();
        gcal.setTime(start);
        while (gcal.getTime().before(end)) {
            gcal.add(Calendar.DAY_OF_YEAR, 1);

        }
        maxYearDate = gcal.getTime();
        return maxYearDate;
    }

    /**
     * @param maxYearDate the maxYearDate to set
     */
    public void setMaxYearDate(Date maxYearDate) {
        this.maxYearDate = maxYearDate;
    }

}
