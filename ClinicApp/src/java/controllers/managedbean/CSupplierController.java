/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.CSupplierJpaController;
import controllers.jpa.SupplierJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.CSupplier;
import models.Supplier;

/**
 *
 * @author apple
 */
@ManagedBean(name = "csupplierController")
@ViewScoped
public class CSupplierController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String suppliername;
    private String supplieraddress;
    private String suppliercontact;
    private String supplieragent;

    private CSupplier thisSupplier;
    private List<CSupplier> supplierList;

    private String searchkey = "";

    private CSupplierJpaController controller = new CSupplierJpaController(utx, factory);

    @PostConstruct
    public void init() {
        controller = new CSupplierJpaController(utx, factory);
        setSupplierList(controller.findCSupplierEntities());
    }

    public void prepareForm() {
        suppliername = "";
        supplieraddress = "";
        suppliercontact = "";
        supplieragent = "";
    }

    public void refreshSuppliers() {
        controller = new CSupplierJpaController(utx, factory);
        setSupplierList(controller.findCSupplierEntities());
    }

    public void update() {
        controller = new CSupplierJpaController(utx, factory);
        try {
            controller.edit(getThisSupplier());
            JsfUtil.addSuccessMessage("Supplier details updated!");
            refreshSuppliers();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(CSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        if (suppliername.isEmpty()) {
            JsfUtil.addErrorMessage("Please input supplier name");
            return;
        }

        if (suppliercontact.isEmpty()) {
            JsfUtil.addErrorMessage("Please input supplier contact info");
            return;
        }
        controller = new CSupplierJpaController(utx, factory);
        CSupplier s = new CSupplier();
        s.setSuppliername(suppliername.trim().toUpperCase());
        s.setSupplieraddress(supplieraddress.trim().toUpperCase());
        s.setSuppliercontact(suppliercontact.trim().toUpperCase());
        s.setSupplieragent(supplieragent.trim().toUpperCase());
        try {
            controller.create(s);
            JsfUtil.addSuccessMessage("Added new supplier!");
            refreshSuppliers();
        } catch (Exception ex) {
            Logger.getLogger(CSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<CSupplier> search() {
        searchkey = searchkey.toLowerCase();
        System.out.println(searchkey);
        supplierList.clear();
        controller = new CSupplierJpaController(utx, factory);
        if (searchkey.trim().isEmpty()) {
            supplierList = controller.findCSupplierEntities();
        } else {
            EntityManager em = factory.createEntityManager();
            for (CSupplier s : controller.findCSupplierEntities()) {
                //System.out.println(s.getFirstname().toLowerCase().contains(searchKey) || s.getLastname().toLowerCase().contains(searchKey));
                if (s.getSuppliername().toLowerCase().contains(searchkey) || s.getSupplieragent().toLowerCase().contains(searchkey)) {
                    s = em.merge(s);
                    em.refresh(s);
                    supplierList.add(s);
                }
            }
            em.close();
        }
        return supplierList;
    }

    public void prepareSupplierInfo() {
        if (getThisSupplier() != null) {
            EntityManager em = factory.createEntityManager();
            setThisSupplier(em.merge(getThisSupplier()));
            em.refresh(getThisSupplier());
            em.close();
        }
    }

    /**
     * @return the suppliername
     */
    public String getSuppliername() {
        return suppliername;
    }

    /**
     * @param suppliername the suppliername to set
     */
    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    /**
     * @return the supplieraddress
     */
    public String getSupplieraddress() {
        return supplieraddress;
    }

    /**
     * @param supplieraddress the supplieraddress to set
     */
    public void setSupplieraddress(String supplieraddress) {
        this.supplieraddress = supplieraddress;
    }

    /**
     * @return the suppliercontact
     */
    public String getSuppliercontact() {
        return suppliercontact;
    }

    /**
     * @param suppliercontact the suppliercontact to set
     */
    public void setSuppliercontact(String suppliercontact) {
        this.suppliercontact = suppliercontact;
    }

    /**
     * @return the supplieragent
     */
    public String getSupplieragent() {
        return supplieragent;
    }

    /**
     * @param supplieragent the supplieragent to set
     */
    public void setSupplieragent(String supplieragent) {
        this.supplieragent = supplieragent;
    }

    /**
     * @return the supplierList
     */
    public List<CSupplier> getSupplierList() {
        return supplierList;
    }

    /**
     * @param supplierList the supplierList to set
     */
    public void setSupplierList(List<CSupplier> supplierList) {
        this.supplierList = supplierList;
    }

    /**
     * @return the thisSupplier
     */
    public CSupplier getThisSupplier() {
        return thisSupplier;
    }

    /**
     * @param thisSupplier the thisSupplier to set
     */
    public void setThisSupplier(CSupplier thisSupplier) {
        this.thisSupplier = thisSupplier;
    }

    /**
     * @return the searchkey
     */
    public String getSearchkey() {
        return searchkey;
    }

    /**
     * @param searchkey the searchkey to set
     */
    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

}
