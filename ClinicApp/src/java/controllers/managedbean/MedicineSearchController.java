/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import models.Client;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean
@ViewScoped
public class MedicineSearchController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String searchKey = "";

    public List<Medicineitem> completeClient(String q) {
        List<Medicineitem> suggestions = new ArrayList<Medicineitem>();
        EntityManager em = factory.createEntityManager();
        //em.createQuery("SELECT m FROM Medicineitem m WHERE m.");
        Query query = em.createQuery("SELECT m FROM Medicineitem m WHERE m.brandname LIKE '" + q.trim().toLowerCase() + "%' OR m.genericname LIKE '" + q.trim().toLowerCase() + "%' ORDER BY m.selling").setMaxResults(5);
        for (Object c : query.getResultList()) {
                suggestions.add(((Medicineitem) c));
        }
        em.close();
        return suggestions;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }

    /**
     * @param searchKey the searchKey to set
     */
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

}
