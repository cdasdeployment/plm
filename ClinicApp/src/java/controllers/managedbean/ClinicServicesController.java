/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ClinicservicesJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Clinicservices;

/**
 *
 * @author apple
 */
@ManagedBean(name = "clinicServicesController")
@ViewScoped
public class ClinicServicesController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String servicename;
    private String department;
    private Double price;

    private ClinicservicesJpaController clinicController;

    private List<Clinicservices> serviceList;
    private List<Clinicservices> entServiceList;
    private List<Clinicservices> hearingServiceList;
    private List<Clinicservices> cosmeticServiceList;

    private Clinicservices thisItem;

    @PostConstruct
    public void init() {
        clinicController = new ClinicservicesJpaController(utx, factory);
        entServiceList = new ArrayList();
        serviceList = new ArrayList();
        hearingServiceList = new ArrayList();
        cosmeticServiceList = new ArrayList();

        serviceList = clinicController.findClinicservicesEntities();

        for (Clinicservices c : serviceList) {
            if (c.getDepartment().equals("ENT")) {
                entServiceList.add(c);
            }
            if (c.getDepartment().equals("Hearing")) {
                hearingServiceList.add(c);
            }
            if (c.getDepartment().equals("Cosmetics")) {
                cosmeticServiceList.add(c);
            }
        }

    }

    public void prepareNewServiceEntry() {
        servicename = "";
        department = "";
        price = 0.0;
    }

    public void prepareServiceInfo() {
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            thisItem = em.merge(thisItem);
            em.refresh(thisItem);
            em.close();
        }
    }

    public void refreshServiceList() {
        clinicController = new ClinicservicesJpaController(utx, factory);
        entServiceList = new ArrayList();
        serviceList = new ArrayList();
        hearingServiceList = new ArrayList();
        cosmeticServiceList = new ArrayList();

        serviceList = clinicController.findClinicservicesEntities();

        for (Clinicservices c : serviceList) {
            if (c.getDepartment().equals("ENT")) {
                entServiceList.add(c);
            }
            if (c.getDepartment().equals("Hearing")) {
                hearingServiceList.add(c);
            }
            if (c.getDepartment().equals("Cosmetics")) {
                cosmeticServiceList.add(c);
            }
        }

    }

    public void update() {
        try {
            clinicController = new ClinicservicesJpaController(utx, factory);
            thisItem.setServicename(thisItem.getServicename().toUpperCase());
            clinicController.edit(thisItem);
            refreshServiceList();
            JsfUtil.addNotifMessage("Service updated!");
        } catch (RollbackFailureException ex) {
            Logger.getLogger(ClinicServicesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ClinicServicesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void save() {
        if (servicename.isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate name of service. (Required)");
            return;
        }
        if (department.isEmpty()) {
            JsfUtil.addErrorMessage("Please assign a department. (Required)");
            return;
        }
        if (price < 0) {
            JsfUtil.addErrorMessage("Price should be grater than zero!");
            return;
        }
        if (servicename == null) {
            JsfUtil.addErrorMessage("Please indicate name of service. (Required)");
            return;
        }
        if (department == null) {
            JsfUtil.addErrorMessage("Please assign a department. (Required)");
            return;
        }
        if (price == null) {
            JsfUtil.addErrorMessage("Price should be grater than zero!");
            return;
        }

        clinicController = new ClinicservicesJpaController(utx, factory);
        Clinicservices newservice = new Clinicservices();
        newservice.setServicename(servicename.toUpperCase());
        newservice.setDepartment(department);
        newservice.setPrice(price);
        try {
            clinicController.create(newservice);
            JsfUtil.addSuccessMessage("New service added!");
            refreshServiceList();
            prepareNewServiceEntry();
        } catch (Exception ex) {
            Logger.getLogger(ClinicServicesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the servicename
     */
    public String getServicename() {
        return servicename;
    }

    /**
     * @param servicename the servicename to set
     */
    public void setServicename(String servicename) {
        this.servicename = servicename;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * @return the entServiceList
     */
    public List<Clinicservices> getEntServiceList() {
        return entServiceList;
    }

    /**
     * @param entServiceList the entServiceList to set
     */
    public void setEntServiceList(List<Clinicservices> entServiceList) {
        this.entServiceList = entServiceList;
    }

    /**
     * @return the hearingServiceList
     */
    public List<Clinicservices> getHearingServiceList() {
        return hearingServiceList;
    }

    /**
     * @param hearingServiceList the hearingServiceList to set
     */
    public void setHearingServiceList(List<Clinicservices> hearingServiceList) {
        this.hearingServiceList = hearingServiceList;
    }

    /**
     * @return the cosmeticServiceList
     */
    public List<Clinicservices> getCosmeticServiceList() {
        return cosmeticServiceList;
    }

    /**
     * @param cosmeticServiceList the cosmeticServiceList to set
     */
    public void setCosmeticServiceList(List<Clinicservices> cosmeticServiceList) {
        this.cosmeticServiceList = cosmeticServiceList;
    }

    /**
     * @return the serviceList
     */
    public List<Clinicservices> getServiceList() {
        return serviceList;
    }

    /**
     * @param serviceList the serviceList to set
     */
    public void setServiceList(List<Clinicservices> serviceList) {
        this.serviceList = serviceList;
    }

    /**
     * @return the thisItem
     */
    public Clinicservices getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(Clinicservices thisItem) {
        this.thisItem = thisItem;
    }

}
