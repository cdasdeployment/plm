/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.CDruginventoryJpaController;
import controllers.jpa.DruginventoryJpaController;
import controllers.jpa.MedicineitemJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.CDruginventory;
import models.CMedicineitem;
import models.Druginventory;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "cosmeticStockViewController")
@ViewScoped
public class CosmeticStockViewController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private CMedicineitem thisItem;

    private CDruginventoryJpaController controller;
    private List<CDruginventory> inventoryList = new ArrayList<CDruginventory>();

    @PostConstruct
    public void init() {
        inventoryList = new ArrayList<CDruginventory>();
        controller = new CDruginventoryJpaController(utx, factory);
        inventoryList = controller.findCDruginventoryEntities();
    }

    public void refreshInventoryList() {
        inventoryList = new ArrayList<CDruginventory>();
        controller = new CDruginventoryJpaController(utx, factory);
        inventoryList = controller.findCDruginventoryEntities();
    }

    public void prepareItemInventory() {
        inventoryList = new ArrayList<CDruginventory>();
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(thisItem));
            em.refresh(thisItem);
            em.close();
        }
        inventoryList = thisItem.getCdruginventoryList();
        
    }

    /**
     * @return the thisItem
     */
    public CMedicineitem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(CMedicineitem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the inventoryList
     */
    public List<CDruginventory> getInventoryList() {
        return inventoryList;
    }

    /**
     * @param inventoryList the inventoryList to set
     */
    public void setInventoryList(List<CDruginventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

}
