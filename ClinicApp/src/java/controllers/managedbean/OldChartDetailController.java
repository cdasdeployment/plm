/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ChartDetailsJpaController;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Appointment;
import models.ChartDetails;
import models.Client;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author apple
 */
@ManagedBean(name = "oldchartDetailController")
@ViewScoped
public class OldChartDetailController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Client client;
    private String familyhistory = "";
    private String medicalhistory = "";
    private String illnesshistory = "";
    private String height = "";
    private String weight = "";
    private String bloodpressure = "";
    private String pulse = "";
    private String referredby = "";
    private Date recorddatetime = null;
    private String chiefcomplaint = "";
    private String diagnosis = "";
    private String followupplan = "";
    private String generic1 = "";
    private String brand1 = "";
    private String preparation1 = "";
    private String generic2 = "";
    private String brand2 = "";
    private String preparation2 = "";
    private String generic3 = "";
    private String brand3 = "";
    private String preparation3 = "";
    private String direction1 = "";
    private String direction2 = "";
    private String direction3 = "";
    private String quantity1 = "";
    private String quantity2 = "";
    private String quantity3 = "";
    private ChartDetails selectedChart;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");

    public void saveInitialChart() {
        System.out.println(client);
        boolean isFound = false;
        if (client != null) {
            if (recorddatetime == null) {
                JsfUtil.addErrorMessage("Chart date cannot be blank!");
                return;
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
            String recordDateString = simpleDateFormat.format(recorddatetime);
            String currentDateString = simpleDateFormat.format(new Date());
            if (recordDateString.equals(currentDateString)) {
                JsfUtil.addErrorMessage("Please enter correct chart date.");
                return;
            }

            System.out.println("client not null");
            ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
            if (client.getChartDetailsList().isEmpty()) {
                System.out.println("empty chart details");
                ChartDetails chartDetail = new ChartDetails();
                chartDetail.setClient(client);
                chartDetail.setFamilyhistory(familyhistory);
                chartDetail.setMedicalhistory(medicalhistory);
                chartDetail.setIllnesshistory(illnesshistory);
                chartDetail.setHeight(height);
                chartDetail.setWeight(weight);
                chartDetail.setBloodpressure(bloodpressure);
                chartDetail.setPulse(pulse);
                chartDetail.setReferredby(referredby);
                chartDetail.setRecorddatetime(recorddatetime);
                chartDetail.setChiefcomplaint(chiefcomplaint);
                chartDetail.setFollowup(followupplan);
                chartDetail.setDiagnosis(diagnosis);
                chartDetail.setGeneric1(generic1);
                chartDetail.setBrand1(brand1);
                chartDetail.setPreparation1(preparation1);
                chartDetail.setDirection1(direction1);
                chartDetail.setGeneric2(generic2);
                chartDetail.setBrand2(brand2);
                chartDetail.setPreparation2(preparation2);
                chartDetail.setDirection2(direction2);
                chartDetail.setQuantity1(quantity1);
                chartDetail.setQuantity2(quantity2);
                chartDetail.setGeneric3(generic3);
                chartDetail.setBrand3(brand3);
                chartDetail.setPreparation3(preparation3);
                chartDetail.setDirection3(direction3);
                chartDetail.setChartCode(simpleDateFormat.format(recorddatetime));
                try {
                    chartDetailController.create(chartDetail);
                    JsfUtil.addSuccessMessage("Patient chart created!");
                    isFound = true;
                } catch (Exception ex) {
                    Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("chart details not empty");
                for (ChartDetails cd : client.getChartDetailsList()) {
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                    currentDateString = simpleDateFormat.format(new Date());
                    System.out.println(recordDateString + " --> " + currentDateString);
                    if (recordDateString.equals(currentDateString)) {
                        if (recorddatetime == null) {
                            JsfUtil.addErrorMessage("Chart date cannot be blank!");
                            return;
                        }

                        cd.setFamilyhistory(familyhistory);
                        cd.setMedicalhistory(medicalhistory);
                        cd.setIllnesshistory(illnesshistory);
                        cd.setHeight(height);
                        cd.setWeight(weight);
                        cd.setBloodpressure(bloodpressure);
                        cd.setPulse(pulse);
                        cd.setReferredby(referredby);
                        cd.setRecorddatetime(recorddatetime);
                        cd.setChiefcomplaint(chiefcomplaint);
                        cd.setFollowup(followupplan);
                        cd.setDiagnosis(diagnosis);
                        cd.setGeneric1(generic1);
                        cd.setBrand1(brand1);
                        cd.setPreparation1(preparation1);
                        cd.setDirection1(direction1);
                        cd.setGeneric2(generic2);
                        cd.setBrand2(brand2);
                        cd.setPreparation2(preparation2);
                        cd.setDirection2(direction2);
                        cd.setQuantity1(quantity1);
                        cd.setQuantity2(quantity2);
                        cd.setQuantity2(quantity3);
                        cd.setGeneric3(generic3);
                        cd.setBrand3(brand3);
                        cd.setPreparation3(preparation3);
                        cd.setDirection3(direction3);
                        cd.setChartCode(simpleDateFormat.format(recorddatetime));
                        try {
                            chartDetailController.edit(cd);
                            JsfUtil.addSuccessMessage("Patient chart updated!");
                            isFound = true;
                        } catch (NonexistentEntityException ex) {
                            Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (RollbackFailureException ex) {
                            Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (Exception ex) {
                            Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                }
                if (isFound == false) {

                    if (recorddatetime == null) {
                        JsfUtil.addErrorMessage("Chart date cannot be blank!");
                        return;
                    }
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    recordDateString = simpleDateFormat.format(recorddatetime);
                    currentDateString = simpleDateFormat.format(new Date());
                    if (recordDateString.equals(currentDateString)) {
                        JsfUtil.addErrorMessage("Please enter correct chart date.");
                        return;
                    }
                    //JsfUtil.addNotifMessage("Creating new chart..");
                    chartDetailController = new ChartDetailsJpaController(utx, factory);
                    ChartDetails chartDetail = new ChartDetails();
                    chartDetail.setClient(client);
                    chartDetail.setFamilyhistory(familyhistory);
                    chartDetail.setMedicalhistory(medicalhistory);
                    chartDetail.setIllnesshistory(illnesshistory);
                    chartDetail.setHeight(height);
                    chartDetail.setWeight(weight);
                    chartDetail.setBloodpressure(bloodpressure);
                    chartDetail.setPulse(pulse);
                    chartDetail.setReferredby(referredby);
                    chartDetail.setRecorddatetime(recorddatetime);
                    chartDetail.setChiefcomplaint(chiefcomplaint);
                    chartDetail.setFollowup(followupplan);
                    chartDetail.setDiagnosis(diagnosis);
                    chartDetail.setGeneric1(generic1);
                    chartDetail.setBrand1(brand1);
                    chartDetail.setPreparation1(preparation1);
                    chartDetail.setDirection1(direction1);
                    chartDetail.setGeneric2(generic2);
                    chartDetail.setBrand2(brand2);
                    chartDetail.setPreparation2(preparation2);
                    chartDetail.setDirection2(direction2);
                    chartDetail.setQuantity1(quantity1);
                    chartDetail.setQuantity2(quantity2);
                    chartDetail.setQuantity3(quantity3);
                    chartDetail.setGeneric3(generic3);
                    chartDetail.setBrand3(brand3);
                    chartDetail.setPreparation3(preparation3);
                    chartDetail.setDirection3(direction3);
                    chartDetail.setChartCode(simpleDateFormat.format(recorddatetime));

                    try {
                        chartDetailController.create(chartDetail);
                        JsfUtil.addSuccessMessage("Patient chart created!");
                    } catch (Exception ex) {
                        Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    public void prepareOldChart() {
        //clearFields();
        if (client != null) {
            EntityManager em = factory.createEntityManager();
            client = em.merge(client);
            em.refresh(client);
            em.close();

            if (client.getChartDetailsList().isEmpty()) {
                familyhistory = "";
                medicalhistory = "";
                illnesshistory = "";
                height = "";
                weight = "";
                bloodpressure = "";
                pulse = "";
                referredby = "";
                //recorddatetime = null;
                chiefcomplaint = "";
                diagnosis = "";
                followupplan = "";
                diagnosis = "";
                followupplan = "";
                generic1 = "";
                brand1 = "";
                preparation1 = "";
                generic2 = "";
                brand2 = "";
                preparation2 = "";
                generic3 = "";
                brand3 = "";
                preparation3 = "";
                direction1 = "";
                direction2 = "";
                direction3 = "";
                quantity1 = "";
                quantity2 = "";
                quantity3 = "";
                JsfUtil.addNotifMessage("Please complete all necessary chart data.");
            } else {
                for (ChartDetails cd : client.getChartDetailsList()) {
                    if (recorddatetime == null) {
                        return;
                    }
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                    String currentDateString = simpleDateFormat.format(recorddatetime);
                    if (recordDateString.equals(currentDateString)) {
                        familyhistory = cd.getFamilyhistory();
                        medicalhistory = cd.getMedicalhistory();
                        illnesshistory = cd.getIllnesshistory();
                        height = cd.getHeight();
                        weight = cd.getWeight();
                        bloodpressure = cd.getBloodpressure();
                        pulse = cd.getPulse();
                        referredby = cd.getReferredby();
                        recorddatetime = cd.getRecorddatetime();
                        chiefcomplaint = cd.getChiefcomplaint();
                        diagnosis = cd.getDiagnosis();
                        followupplan = cd.getFollowup();
                        generic1 = cd.getGeneric1();
                        generic2 = cd.getGeneric2();
                        generic3 = cd.getGeneric3();
                        brand1 = cd.getBrand1();
                        brand2 = cd.getBrand2();
                        brand3 = cd.getBrand3();
                        quantity1 = cd.getQuantity1();
                        quantity2 = cd.getQuantity2();
                        quantity3 = cd.getQuantity3();
                        preparation1 = cd.getPreparation1();
                        preparation2 = cd.getPreparation2();
                        preparation3 = cd.getPreparation3();
                        direction1 = cd.getDirection1();
                        direction2 = cd.getDirection2();
                        direction3 = cd.getDirection3();

                        JsfUtil.addNotifMessage("Please complete all necessary chart data.");
                        break;
                    }
                }
            }
        }
    }

    public void prepareClientChart() {
        clearFields();
        if (client != null) {
            EntityManager em = factory.createEntityManager();
            client = em.merge(client);
            em.refresh(client);
            em.close();

            if (client.getChartDetailsList().isEmpty()) {
                familyhistory = "";
                medicalhistory = "";
                illnesshistory = "";
                height = "";
                weight = "";
                bloodpressure = "";
                pulse = "";
                referredby = "";
                recorddatetime = null;
                chiefcomplaint = "";
                diagnosis = "";
                followupplan = "";
                diagnosis = "";
                followupplan = "";
                generic1 = "";
                brand1 = "";
                preparation1 = "";
                generic2 = "";
                brand2 = "";
                preparation2 = "";
                generic3 = "";
                brand3 = "";
                preparation3 = "";
                direction1 = "";
                direction2 = "";
                direction3 = "";
                quantity1 = "";
                quantity2 = "";
                quantity3 = "";
                JsfUtil.addNotifMessage("Please complete all necessary chart data.");
            } else {
                for (ChartDetails cd : client.getChartDetailsList()) {
                    simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                    String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                    String currentDateString = simpleDateFormat.format(new Date());
                    if (recordDateString.equals(currentDateString)) {
                        familyhistory = cd.getFamilyhistory();
                        medicalhistory = cd.getMedicalhistory();
                        illnesshistory = cd.getIllnesshistory();
                        height = cd.getHeight();
                        weight = cd.getWeight();
                        bloodpressure = cd.getBloodpressure();
                        pulse = cd.getPulse();
                        referredby = cd.getReferredby();
                        recorddatetime = cd.getRecorddatetime();
                        chiefcomplaint = cd.getChiefcomplaint();
                        diagnosis = cd.getDiagnosis();
                        followupplan = cd.getFollowup();
                        generic1 = cd.getGeneric1();
                        generic2 = cd.getGeneric2();
                        generic3 = cd.getGeneric3();
                        brand1 = cd.getBrand1();
                        brand2 = cd.getBrand2();
                        brand3 = cd.getBrand3();
                        quantity1 = cd.getQuantity1();
                        quantity2 = cd.getQuantity2();
                        quantity3 = cd.getQuantity3();
                        preparation1 = cd.getPreparation1();
                        preparation2 = cd.getPreparation2();
                        preparation3 = cd.getPreparation3();
                        direction1 = cd.getDirection1();
                        direction2 = cd.getDirection2();
                        direction3 = cd.getDirection3();

                        JsfUtil.addNotifMessage("Please complete all necessary chart data.");
                        break;
                    }
                }
            }
        }
    }

    public void onRowSelect(SelectEvent event) {
        Appointment selObject = ((Appointment) event.getObject());
        if (selObject != null) {
            client = selObject.getClient();
            prepareClientChart();
        }
    }

    public void doctorRowSelect(SelectEvent event) {
        Appointment selObject = ((Appointment) event.getObject());
        if (selObject != null) {
            client = selObject.getClient();
            if (client != null) {
                EntityManager em = factory.createEntityManager();
                client = em.merge(client);
                em.refresh(client);
                em.close();

                ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
                if (client.getChartDetailsList().isEmpty()) {
                    selectedChart = new ChartDetails();
                    selectedChart.setClient(client);
                    selectedChart.setRecorddatetime(null);
                    selectedChart.setFamilyhistory("");
                    selectedChart.setMedicalhistory("");
                    selectedChart.setIllnesshistory("");
                    selectedChart.setHeight("");
                    selectedChart.setWeight("");
                    selectedChart.setBloodpressure("");
                    selectedChart.setPulse("");
                    selectedChart.setReferredby("");
                    selectedChart.setChiefcomplaint("");
                    selectedChart.setFollowup("");
                    selectedChart.setDiagnosis("");
                    selectedChart.setGeneric1("");
                    selectedChart.setBrand1("");
                    selectedChart.setPreparation1("");
                    selectedChart.setDirection1("");
                    selectedChart.setGeneric2("");
                    selectedChart.setBrand2("");
                    selectedChart.setPreparation2("");
                    selectedChart.setDirection2("");
                    selectedChart.setQuantity1("");
                    selectedChart.setQuantity2("");
                    selectedChart.setQuantity3("");
                    selectedChart.setGeneric3("");
                    selectedChart.setBrand3("");
                    selectedChart.setPreparation3("");
                    selectedChart.setDirection3("");
                    selectedChart.setChartCode(simpleDateFormat.format(new Date()));
                    try {
                        chartDetailController.create(selectedChart);
                    } catch (Exception ex) {
                        Logger.getLogger(OldChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    for (ChartDetails cd : client.getChartDetailsList()) {
                        simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                        String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                        String currentDateString = simpleDateFormat.format(new Date());
                        if (recordDateString.equals(currentDateString)) {
                            selectedChart = cd;
                        }
                        break;
                    }
                }
            }
        }
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the familyhistory
     */
    public String getFamilyhistory() {
        return familyhistory;
    }

    /**
     * @param familyhistory the familyhistory to set
     */
    public void setFamilyhistory(String familyhistory) {
        this.familyhistory = familyhistory;
    }

    /**
     * @return the medicalhistory
     */
    public String getMedicalhistory() {
        return medicalhistory;
    }

    /**
     * @param medicalhistory the medicalhistory to set
     */
    public void setMedicalhistory(String medicalhistory) {
        this.medicalhistory = medicalhistory;
    }

    /**
     * @return the illnesshistory
     */
    public String getIllnesshistory() {
        return illnesshistory;
    }

    /**
     * @param illnesshistory the illnesshistory to set
     */
    public void setIllnesshistory(String illnesshistory) {
        this.illnesshistory = illnesshistory;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the bloodpressure
     */
    public String getBloodpressure() {
        return bloodpressure;
    }

    /**
     * @param bloodpressure the bloodpressure to set
     */
    public void setBloodpressure(String bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    /**
     * @return the pulse
     */
    public String getPulse() {
        return pulse;
    }

    /**
     * @param pulse the pulse to set
     */
    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    /**
     * @return the referredby
     */
    public String getReferredby() {
        return referredby;
    }

    /**
     * @param referredby the referredby to set
     */
    public void setReferredby(String referredby) {
        this.referredby = referredby;
    }

    /**
     * @return the recorddatetime
     */
    public Date getRecorddatetime() {
        return recorddatetime;
    }

    /**
     * @param recorddatetime the recorddatetime to set
     */
    public void setRecorddatetime(Date recorddatetime) {
        this.recorddatetime = recorddatetime;
    }

    /**
     * @return the chiefcomplaint
     */
    public String getChiefcomplaint() {
        return chiefcomplaint;
    }

    /**
     * @param chiefcomplaint the chiefcomplaint to set
     */
    public void setChiefcomplaint(String chiefcomplaint) {
        this.chiefcomplaint = chiefcomplaint;
    }

    /**
     * @return the selectedChart
     */
    public ChartDetails getSelectedChart() {
        return selectedChart;
    }

    /**
     * @param selectedChart the selectedChart to set
     */
    public void setSelectedChart(ChartDetails selectedChart) {
        this.selectedChart = selectedChart;
    }

    /**
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis the diagnosis to set
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return the followupplan
     */
    public String getFollowupplan() {
        return followupplan;
    }

    /**
     * @param followupplan the followupplan to set
     */
    public void setFollowupplan(String followupplan) {
        this.followupplan = followupplan;
    }

    /**
     * @return the generic1
     */
    public String getGeneric1() {
        return generic1;
    }

    /**
     * @param generic1 the generic1 to set
     */
    public void setGeneric1(String generic1) {
        this.generic1 = generic1;
    }

    /**
     * @return the brand1
     */
    public String getBrand1() {
        return brand1;
    }

    /**
     * @param brand1 the brand1 to set
     */
    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    /**
     * @return the preparation1
     */
    public String getPreparation1() {
        return preparation1;
    }

    /**
     * @param preparation1 the preparation1 to set
     */
    public void setPreparation1(String preparation1) {
        this.preparation1 = preparation1;
    }

    /**
     * @return the generic2
     */
    public String getGeneric2() {
        return generic2;
    }

    /**
     * @param generic2 the generic2 to set
     */
    public void setGeneric2(String generic2) {
        this.generic2 = generic2;
    }

    /**
     * @return the brand2
     */
    public String getBrand2() {
        return brand2;
    }

    /**
     * @param brand2 the brand2 to set
     */
    public void setBrand2(String brand2) {
        this.brand2 = brand2;
    }

    /**
     * @return the preparation2
     */
    public String getPreparation2() {
        return preparation2;
    }

    /**
     * @param preparation2 the preparation2 to set
     */
    public void setPreparation2(String preparation2) {
        this.preparation2 = preparation2;
    }

    /**
     * @return the generic3
     */
    public String getGeneric3() {
        return generic3;
    }

    /**
     * @param generic3 the generic3 to set
     */
    public void setGeneric3(String generic3) {
        this.generic3 = generic3;
    }

    /**
     * @return the brand3
     */
    public String getBrand3() {
        return brand3;
    }

    /**
     * @param brand3 the brand3 to set
     */
    public void setBrand3(String brand3) {
        this.brand3 = brand3;
    }

    /**
     * @return the preparation3
     */
    public String getPreparation3() {
        return preparation3;
    }

    /**
     * @param preparation3 the preparation3 to set
     */
    public void setPreparation3(String preparation3) {
        this.preparation3 = preparation3;
    }

    /**
     * @return the direction1
     */
    public String getDirection1() {
        return direction1;
    }

    /**
     * @param direction1 the direction1 to set
     */
    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    /**
     * @return the direction2
     */
    public String getDirection2() {
        return direction2;
    }

    /**
     * @param direction2 the direction2 to set
     */
    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    /**
     * @return the direction3
     */
    public String getDirection3() {
        return direction3;
    }

    /**
     * @param direction3 the direction3 to set
     */
    public void setDirection3(String direction3) {
        this.direction3 = direction3;
    }

    /**
     * @return the quantity1
     */
    public String getQuantity1() {
        return quantity1;
    }

    /**
     * @param quantity1 the quantity1 to set
     */
    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    /**
     * @return the quantity2
     */
    public String getQuantity2() {
        return quantity2;
    }

    /**
     * @param quantity2 the quantity2 to set
     */
    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    /**
     * @return the quantity3
     */
    public String getQuantity3() {
        return quantity3;
    }

    /**
     * @param quantity3 the quantity3 to set
     */
    public void setQuantity3(String quantity3) {
        this.quantity3 = quantity3;
    }

    private void clearFields() {
        familyhistory = "";
        medicalhistory = "";
        illnesshistory = "";
        height = "";
        weight = "";
        bloodpressure = "";
        pulse = "";
        referredby = "";
        recorddatetime = null;
        chiefcomplaint = "";
        diagnosis = "";
        followupplan = "";
        diagnosis = "";
        followupplan = "";
        generic1 = "";
        brand1 = "";
        preparation1 = "";
        generic2 = "";
        brand2 = "";
        preparation2 = "";
        generic3 = "";
        brand3 = "";
        preparation3 = "";
        direction1 = "";
        direction2 = "";
        direction3 = "";
        quantity1 = "";
        quantity2 = "";
        quantity3 = "";
    }
}
