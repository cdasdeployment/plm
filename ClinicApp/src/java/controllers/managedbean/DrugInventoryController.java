/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.DruginventoryJpaController;
import controllers.jpa.MedicineitemJpaController;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Druginventory;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "drugInventoryController")
@ViewScoped
public class DrugInventoryController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String remarks = "";
    private String updateType = "";
    private Date servicetimestamp = new Date();
    private Integer pullout = 0;
    private Integer purchase = 0;
    private Integer returned = 0;
    private Integer updated = 0;
    private Integer quantity = 0;
    private Double buying = 0.0;
    private Double selling = 0.0;
    private Medicineitem drug;
    private Integer currentStock = 0;

    private MedicineitemJpaController medController;
    private DruginventoryJpaController inventoryController;

    @PostConstruct
    public void init() {
        medController = new MedicineitemJpaController(utx, factory);
        inventoryController = new DruginventoryJpaController(utx, factory);
    }

    public void resetInventoryEntry() {
        remarks = "";
        updateType = "";
        servicetimestamp = new Date();
        pullout = 0;
        purchase = 0;
        returned = 0;
        updated = 0;
        quantity = 0;
        buying = 0.0;
        selling = 0.0;
        currentStock = 0;
    }

    public void prepareDrugInfo() {
        //System.out.println(drug);
        if (drug != null) {
            resetInventoryEntry();
            EntityManager em = factory.createEntityManager();
            drug = em.merge(drug);
            em.refresh(drug);
            em.close();
        }
    }

    public void deleteDrugInfo() {
        if (drug != null) {
            resetInventoryEntry();
            EntityManager em = factory.createEntityManager();
            drug = em.merge(drug);
            em.refresh(drug);
            em.close();
            boolean isFound = false;
            inventoryController = new DruginventoryJpaController(utx, factory);
            List<Druginventory> inventoryList = inventoryController.findDruginventoryEntities();
            //System.out.println(inventoryList);
            for (Druginventory med : inventoryList) {
                if (med.getDrug().getId().equals(drug.getId())) {
                    //ang drug kay naay inventory record, so dili na mag initial stock
                    isFound = true;
                    try {
                        System.out.println(med);
                        inventoryController.destroy(med.getId());
                        MedicineitemJpaController medItemController = new MedicineitemJpaController(utx, factory);
                        medItemController.destroy(drug.getId());
                        System.out.println(drug);
                    } catch (RollbackFailureException ex) {
                        Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                } else {
                    isFound = false;
                    MedicineitemJpaController medItemController = new MedicineitemJpaController(utx, factory);
                    try {
                        medItemController.destroy(drug.getId());
                    } catch (NonexistentEntityException ex) {
                        Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (RollbackFailureException ex) {
                        Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (Exception ex) {
                        Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
    }

    private Druginventory dInventory;
    private Druginventory drugInventory;

    public void update() {
        //////////////////////////////////////////////////////////////////////////////
        boolean isFound = false;
        inventoryController = new DruginventoryJpaController(utx, factory);
        List<Druginventory> inventoryList = inventoryController.findDruginventoryEntities();
        //System.out.println(inventoryList);
        for (Druginventory med : inventoryList) {
            if (med.getDrug().getId().equals(drug.getId())) {
                //ang drug kay naay inventory record, so dili na mag initial stock
                isFound = true;
                break;
            } else {
                isFound = false;
            }
        }

        if (!isFound) {
            try {
                dInventory = new Druginventory();
                dInventory.setDrug(drug);
                dInventory.setRemarks("Initial stock");
                dInventory.setUpdated(drug.getStocks());
                dInventory.setBuying(drug.getCapital());
                dInventory.setSelling(drug.getSelling());
                dInventory.setPullout(0);
                dInventory.setPurchase(0);
                dInventory.setReturned(0);
                dInventory.setServicetimestamp(new Date());
                inventoryController.create(dInventory);
            } catch (Exception ex) {
                Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        /////////////////////////////////////////////////////////////////////////////

        String sysMsg = "";
        pullout = 0;
        purchase = 0;
        returned = 0;
        updated = 0;

        if (updateType.isEmpty()) {
            JsfUtil.addErrorMessage("Please select update type.\nItem update cancelled.");
            return;
        }
        if (updateType == null) {
            JsfUtil.addErrorMessage("Please select update type.\nItem update cancelled.");
            return;
        }
        if (quantity == 0 || quantity < 0) {
            JsfUtil.addErrorMessage("Item quantity cannot be zero.\nItem update cancelled.");
            return;
        }

        if (updateType.equals("Pullout")) {
            if (quantity > drug.getStocks()) {
                JsfUtil.addErrorMessage("Pull-out quantity exceeds quantity in-stock.\nItem update cancelled.");
                return;
            }
        }

        medController = new MedicineitemJpaController(utx, factory);
        inventoryController = new DruginventoryJpaController(utx, factory);

        drugInventory = new Druginventory();

        if (updateType.equals("Update")) {
            drugInventory.setRemarks("Update stocks");
            drugInventory.setUpdated(quantity);
            updated = quantity;
            sysMsg = "Inventory updated for item " + drug.getGenericname() + " (" + drug.getBrandname() + ")";

        } else if (updateType.equals("Pullout")) {
            drugInventory.setRemarks("Pull-out stocks");
            if (quantity <= drug.getStocks()) {
                drugInventory.setPullout(quantity);
                pullout = quantity;
                sysMsg = "Stocks pull-out for item " + drug.getGenericname() + " (" + drug.getBrandname() + ")";
            }

        } else if (updateType.equals("Return")) {
            drugInventory.setRemarks("Return stocks");
            drugInventory.setReturned(quantity);
            returned = quantity;
            sysMsg = "Stocks returned for item " + drug.getGenericname() + " (" + drug.getBrandname() + ")";

        }

        drug.setBrandname(drug.getBrandname().toUpperCase());
        drug.setGenericname(drug.getGenericname().toUpperCase());
        drug.setStocks(((drug.getStocks() + returned + updated) - (pullout + purchase)));
        if (updateType.equals("Update")) {
            drug.setReorder((int) (drug.getStocks() * 0.20));
        }

        drugInventory.setDrug(drug);
        drugInventory.setUpdated(updated);
        drugInventory.setPullout(pullout);
        drugInventory.setReturned(returned);
        drugInventory.setPurchase(0);//always zero kay wala man mamalit dri
        drugInventory.setServicetimestamp(servicetimestamp);
        drugInventory.setBuying(drug.getCapital());
        drugInventory.setSelling(drug.getSelling());
        try {
            inventoryController.create(drugInventory);
            JsfUtil.addSuccessMessage(sysMsg);
        } catch (Exception ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            medController.edit(drug);
            dInventory.setDrug(drug);
            drugInventory.setDrug(drug);

            inventoryController.edit(dInventory);
            inventoryController.edit(drugInventory);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        JsfUtil.refreshPage();
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the servicetimestamp
     */
    public Date getServicetimestamp() {
        return servicetimestamp;
    }

    /**
     * @param servicetimestamp the servicetimestamp to set
     */
    public void setServicetimestamp(Date servicetimestamp) {
        this.servicetimestamp = servicetimestamp;
    }

    /**
     * @return the pullout
     */
    public Integer getPullout() {
        return pullout;
    }

    /**
     * @param pullout the pullout to set
     */
    public void setPullout(Integer pullout) {
        this.pullout = pullout;
    }

    /**
     * @return the purchase
     */
    public Integer getPurchase() {
        return purchase;
    }

    /**
     * @param purchase the purchase to set
     */
    public void setPurchase(Integer purchase) {
        this.purchase = purchase;
    }

    /**
     * @return the returned
     */
    public Integer getReturned() {
        return returned;
    }

    /**
     * @param returned the returned to set
     */
    public void setReturned(Integer returned) {
        this.returned = returned;
    }

    /**
     * @return the updated
     */
    public Integer getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    /**
     * @return the buying
     */
    public Double getBuying() {
        return buying;
    }

    /**
     * @param buying the buying to set
     */
    public void setBuying(Double buying) {
        this.buying = buying;
    }

    /**
     * @return the selling
     */
    public Double getSelling() {
        return selling;
    }

    /**
     * @param selling the selling to set
     */
    public void setSelling(Double selling) {
        this.selling = selling;
    }

    /**
     * @return the drug
     */
    public Medicineitem getDrug() {
        return drug;
    }

    /**
     * @param drug the drug to set
     */
    public void setDrug(Medicineitem drug) {
        this.drug = drug;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the updateType
     */
    public String getUpdateType() {
        return updateType;
    }

    /**
     * @param updateType the updateType to set
     */
    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    /**
     * @return the currentStock
     */
    public Integer getCurrentStock() {
        currentStock = 0;
        if (drug != null) {
            currentStock = (drug.getStocks() + (updated + returned)) - (purchase + pullout);
        }
        return currentStock;
    }

    /**
     * @param currentStock the currentStock to set
     */
    public void setCurrentStock(Integer currentStock) {
        this.currentStock = currentStock;
    }

}
