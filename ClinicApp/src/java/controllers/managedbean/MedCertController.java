/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.MedcertJpaController;
import controllers.util.Config;
import controllers.util.JsfUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.ServletContext;
import javax.transaction.UserTransaction;
import models.Medcert;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import reports.datasource.MedCertDataSource;

/**
 *
 * @author apple
 */
@ManagedBean
@ViewScoped
public class MedCertController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;
    private MedcertJpaController controller;

    private Date today = new Date();
    private String dateString = "";

    private Medcert thisMedcert;
    private String message = "";
    private String pdfViewPath = "";

    @PostConstruct
    public void init() {
        today = new Date();
        dateString = "";
        thisMedcert = null;
        message = "";
        pdfViewPath = "";
    }

    public void newCert() {
        today = new Date();
        dateString = "";
        thisMedcert = null;
        message = "";
        pdfViewPath = "";

    }

    /**
     * @return the dateString
     */
    public String getDateString() {
        String d = "";
        Calendar c = Calendar.getInstance();
        c.setTime(today);

        switch (c.get(Calendar.DAY_OF_MONTH)) {
            case 1:
                d = "1st";
                break;
            case 2:
                d = "2nd";
                break;
            case 3:
                d = "3rd";
                break;
            case 4:
                d = "4th";
                break;
            case 5:
                d = "5th";
                break;
            case 6:
                d = "6th";
                break;
            case 7:
                d = "7th";
                break;
            case 8:
                d = "8th";
                break;
            case 9:
                d = "9th";
                break;
            case 10:
                d = "10th";
                break;
            case 11:
                d = "11th";
                break;
            case 12:
                d = "12th";
                break;
            case 13:
                d = "13th";
                break;
            case 14:
                d = "16th";
                break;
            case 15:
                d = "17th";
                break;
            case 16:
                d = "16th";
                break;
            case 17:
                d = "17th";
                break;
            case 18:
                d = "18th";
                break;
            case 19:
                d = "19th";
                break;
            case 20:
                d = "20th";
                break;
            case 21:
                d = "21st";
                break;
            case 23:
                d = "23rd";
                break;
            case 24:
                d = "24th";
                break;
            case 25:
                d = "25th";
                break;
            case 26:
                d = "26th";
                break;
            case 27:
                d = "27th";
                break;
            case 28:
                d = "28th";
                break;
            case 29:
                d = "29th";
                break;
            case 30:
                d = "30th";
                break;
            case 31:
                d = "31st";
                break;
        }
        dateString = "Given this " + d + " day of " + new SimpleDateFormat("MMMM").format(today) + ", " + c.get(Calendar.YEAR) + ".";
        return dateString;
    }

    public void save() {
        controller = new MedcertJpaController(utx, factory);
        thisMedcert = new Medcert();
        thisMedcert.setMessage(message.trim() + "\n\n" + "This certifcation is issued for whatever purpose it may serve.\n" + getDateString().trim());
        thisMedcert.setDateissued(getDateString());
        try {
            controller.create(thisMedcert);

            EntityManager em = factory.createEntityManager();
            thisMedcert = em.merge(thisMedcert);
            em.refresh(thisMedcert);
            em.close();
            JsfUtil.addNotifMessage("Please wait...");
        } catch (Exception ex) {
            Logger.getLogger(MedCertController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void downloadMedCert() {
        try {
            save();

            List<Medcert> list = new ArrayList<Medcert>();

            list.add(thisMedcert);
            MedCertDataSource dataSource = new MedCertDataSource(list);
            JasperPrint jprint = JasperFillManager.fillReport(Config.reportTemplateDirectory + File.separator + "medcert.jasper", new HashMap(), dataSource);
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            File tempfile = new File(servletContext.getRealPath("/webapp") + File.separator + "TEMP");

            if (!tempfile.exists()) {
                tempfile.mkdir();
            }
            String pdfFile = tempfile + File.separator + "MedCert" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(today) + ".pdf";

            pdfViewPath = File.separator + "TEMP" + File.separator + "MedCert" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(today) + ".pdf";

            File f = new File(pdfFile);
            if (f.exists()) {
                f.delete();
            }
            if (f.createNewFile()) {
                JasperExportManager.exportReportToPdfFile(jprint, pdfFile);
            }

        } catch (Exception ex) {
            Logger.getLogger(MedCertController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the thisMedcert
     */
    public Medcert getThisMedcert() {
        return thisMedcert;
    }

    /**
     * @param thisMedcert the thisMedcert to set
     */
    public void setThisMedcert(Medcert thisMedcert) {
        this.thisMedcert = thisMedcert;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the pdfViewPath
     */
    public String getPdfViewPath() {
        return pdfViewPath;
    }

    /**
     * @param pdfViewPath the pdfViewPath to set
     */
    public void setPdfViewPath(String pdfViewPath) {
        this.pdfViewPath = pdfViewPath;
    }
}
