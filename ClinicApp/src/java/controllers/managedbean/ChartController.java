/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ClientJpaController;
import controllers.jpa.PatientChartJpaController;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;
import models.PatientChart;

/**
 *
 * @author apple
 */
@ManagedBean(name = "chartController")
@ViewScoped
public class ChartController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Client thisClient;
    private ClientJpaController clientController;
    private List<PatientChart> entChartList;
    private List<PatientChart> hearingChartList;
    private List<PatientChart> cosmeticChartList;

    private PatientChartJpaController chartController;
    
    private PatientChart thisENTChart;
    private PatientChart thisHearingChart;
    private PatientChart thisCosmeticChart;

    @PostConstruct
    public void init() {
        clientController = new ClientJpaController(utx, factory);
        chartController = new PatientChartJpaController(utx, factory);
    }

    /**
     * @return the thisClient
     */
    public Client getThisClient() {
        return thisClient;
    }

    /**
     * @param thisClient the thisClient to set
     */
    public void setThisClient(Client thisClient) {
        this.thisClient = thisClient;
    }

    public void editClientInfo() {
        if (thisClient != null) {
            EntityManager em = factory.createEntityManager();
            thisClient = em.merge(thisClient);
            em.refresh(thisClient);
            em.close();
        }
    }

    /**
     * @return the entChartList
     */
    public List<PatientChart> getEntChartList() {
        return entChartList;
    }

    /**
     * @param entChartList the entChartList to set
     */
    public void setEntChartList(List<PatientChart> entChartList) {
        this.entChartList = entChartList;
    }

    /**
     * @return the hearingChartList
     */
    public List<PatientChart> getHearingChartList() {
        return hearingChartList;
    }

    /**
     * @param hearingChartList the hearingChartList to set
     */
    public void setHearingChartList(List<PatientChart> hearingChartList) {
        this.hearingChartList = hearingChartList;
    }

    /**
     * @return the cosmeticChartList
     */
    public List<PatientChart> getCosmeticChartList() {
        return cosmeticChartList;
    }

    /**
     * @param cosmeticChartList the cosmeticChartList to set
     */
    public void setCosmeticChartList(List<PatientChart> cosmeticChartList) {
        this.cosmeticChartList = cosmeticChartList;
    }

    /**
     * @return the thisENTChart
     */
    public PatientChart getThisENTChart() {
        return thisENTChart;
    }

    /**
     * @param thisENTChart the thisENTChart to set
     */
    public void setThisENTChart(PatientChart thisENTChart) {
        this.thisENTChart = thisENTChart;
    }

    /**
     * @return the thisHearingChart
     */
    public PatientChart getThisHearingChart() {
        return thisHearingChart;
    }

    /**
     * @param thisHearingChart the thisHearingChart to set
     */
    public void setThisHearingChart(PatientChart thisHearingChart) {
        this.thisHearingChart = thisHearingChart;
    }

    /**
     * @return the thisCosmeticChart
     */
    public PatientChart getThisCosmeticChart() {
        return thisCosmeticChart;
    }

    /**
     * @param thisCosmeticChart the thisCosmeticChart to set
     */
    public void setThisCosmeticChart(PatientChart thisCosmeticChart) {
        this.thisCosmeticChart = thisCosmeticChart;
    }

}
