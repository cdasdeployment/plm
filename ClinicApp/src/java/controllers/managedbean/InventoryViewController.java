/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.AccHearingaiditemJpaController;
import controllers.jpa.CMedicineitemJpaController;
import controllers.jpa.HearingaiditemJpaController;
import controllers.jpa.MedicineitemJpaController;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.AccHearingaiditem;
import models.CMedicineitem;
import models.Hearingaiditem;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "inventoryViewController")
@ViewScoped
public class InventoryViewController {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private List<Medicineitem> lowInventory;
    private List<CMedicineitem> lowCosmeticInventory;
    private List<CMedicineitem> expiringCosmeticInventory;
    private List<Medicineitem> expiringInventory;
    private List<Hearingaiditem> lowHearingAidInventory;
    private List<AccHearingaiditem> lowAccHearingAidInventory;

    private MedicineitemJpaController medController;
    private CMedicineitemJpaController cmedController;
    private HearingaiditemJpaController hearingaidController;
    private AccHearingaiditemJpaController acchearingaidController;

    private Medicineitem thisItem;
    private CMedicineitem thiscItem;
    private Hearingaiditem thishItem;
    private AccHearingaiditem thisacchItem;

    @PostConstruct
    public void init() {
        medController = new MedicineitemJpaController(utx, factory);
        cmedController = new CMedicineitemJpaController(utx, factory);
        hearingaidController = new HearingaiditemJpaController(utx, factory);
        acchearingaidController = new AccHearingaiditemJpaController(utx, factory);

        lowInventory = new ArrayList();
        expiringInventory = new ArrayList();
        lowCosmeticInventory = new ArrayList();
        expiringCosmeticInventory = new ArrayList();
        lowHearingAidInventory = new ArrayList();
        lowAccHearingAidInventory = new ArrayList();

        List<Medicineitem> medList = medController.findMedicineitemEntities();
        List<CMedicineitem> cmedList = cmedController.findCMedicineitemEntities();
        List<Hearingaiditem> haList = hearingaidController.findHearingaiditemEntities();
        List<AccHearingaiditem> haccList = acchearingaidController.findAccHearingaiditemEntities();

        SimpleDateFormat sdfMonth = new SimpleDateFormat("MMMM");
        SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");

        for (Medicineitem m : medList) {
            if (m.getStocks() <= m.getReorder()) {
                if (m.getIsrx()) {
                    lowInventory.add(m);
                }
            }
            if (m.getExpiremonth().trim().equals(sdfMonth.format(new Date())) && m.getExpireyear().trim().equals(sdfYear.format(new Date()))) {
                expiringInventory.add(m);
            }
        }

        //cosmetics
        for (CMedicineitem m : cmedList) {
            if (m.getStocks() <= m.getReorder()) {
                lowCosmeticInventory.add(m);
            }

            if (m.getExpiremonth().trim().equals(sdfMonth.format(new Date())) && m.getExpireyear().trim().equals(sdfYear.format(new Date()))) {
                expiringCosmeticInventory.add(m);
            }
        }

        //hearing aid
        for (Hearingaiditem m : haList) {
            if (m.getStocks() <= m.getReorder()) {
                lowHearingAidInventory.add(m);
            }
        }
        for (AccHearingaiditem m : haccList) {
            if (m.getStocks() <= m.getReorder()) {
                lowAccHearingAidInventory.add(m);
            }
        }

    }

    /**
     * @return the lowInventory
     */
    public List<Medicineitem> getLowInventory() {
        return lowInventory;
    }

    /**
     * @param lowInventory the lowInventory to set
     */
    public void setLowInventory(List<Medicineitem> lowInventory) {
        this.lowInventory = lowInventory;
    }

    /**
     * @return the expiringInventory
     */
    public List<Medicineitem> getExpiringInventory() {
        return expiringInventory;
    }

    /**
     * @param expiringInventory the expiringInventory to set
     */
    public void setExpiringInventory(List<Medicineitem> expiringInventory) {
        this.expiringInventory = expiringInventory;
    }

    /**
     * @return the thisItem
     */
    public Medicineitem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(Medicineitem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the lowCosmeticInventory
     */
    public List<CMedicineitem> getLowCosmeticInventory() {
        return lowCosmeticInventory;
    }

    /**
     * @param lowCosmeticInventory the lowCosmeticInventory to set
     */
    public void setLowCosmeticInventory(List<CMedicineitem> lowCosmeticInventory) {
        this.lowCosmeticInventory = lowCosmeticInventory;
    }

    /**
     * @return the expiringCosmeticInventory
     */
    public List<CMedicineitem> getExpiringCosmeticInventory() {
        return expiringCosmeticInventory;
    }

    /**
     * @param expiringCosmeticInventory the expiringCosmeticInventory to set
     */
    public void setExpiringCosmeticInventory(List<CMedicineitem> expiringCosmeticInventory) {
        this.expiringCosmeticInventory = expiringCosmeticInventory;
    }

    /**
     * @return the thiscItem
     */
    public CMedicineitem getThiscItem() {
        return thiscItem;
    }

    /**
     * @param thiscItem the thiscItem to set
     */
    public void setThiscItem(CMedicineitem thiscItem) {
        this.thiscItem = thiscItem;
    }

    /**
     * @return the lowHearingAidInventory
     */
    public List<Hearingaiditem> getLowHearingAidInventory() {
        return lowHearingAidInventory;
    }

    /**
     * @param lowHearingAidInventory the lowHearingAidInventory to set
     */
    public void setLowHearingAidInventory(List<Hearingaiditem> lowHearingAidInventory) {
        this.lowHearingAidInventory = lowHearingAidInventory;
    }

    /**
     * @return the thishItem
     */
    public Hearingaiditem getThishItem() {
        return thishItem;
    }

    /**
     * @param thishItem the thishItem to set
     */
    public void setThishItem(Hearingaiditem thishItem) {
        this.thishItem = thishItem;
    }

    /**
     * @return the hearingaidController
     */
    public HearingaiditemJpaController getHearingaidController() {
        return hearingaidController;
    }

    /**
     * @param hearingaidController the hearingaidController to set
     */
    public void setHearingaidController(HearingaiditemJpaController hearingaidController) {
        this.hearingaidController = hearingaidController;
    }

    /**
     * @return the thisacchItem
     */
    public AccHearingaiditem getThisacchItem() {
        return thisacchItem;
    }

    /**
     * @param thisacchItem the thisacchItem to set
     */
    public void setThisacchItem(AccHearingaiditem thisacchItem) {
        this.thisacchItem = thisacchItem;
    }

    /**
     * @return the lowAccHearingAidInventory
     */
    public List<AccHearingaiditem> getLowAccHearingAidInventory() {
        return lowAccHearingAidInventory;
    }

    /**
     * @param lowAccHearingAidInventory the lowAccHearingAidInventory to set
     */
    public void setLowAccHearingAidInventory(List<AccHearingaiditem> lowAccHearingAidInventory) {
        this.lowAccHearingAidInventory = lowAccHearingAidInventory;
    }

}
