/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import java.util.Map;
import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;

/**
 *
 * @author jordy
 */
public abstract class AbstractController {
    
    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;
    
    protected static final String PARAM_VIEW_ID = "viewId";
    
    protected Map<String, String> paramMap;
    
    public void init() {
        FacesContext fcontext = FacesContext.getCurrentInstance();
        paramMap = fcontext.getExternalContext().getRequestParameterMap();
    }
    
    protected Client getClientById(int id) {
        EntityManager em = factory.createEntityManager();

        Client client = null;
        try {
            client = em.find(Client.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return client;
    }
    
    
}
