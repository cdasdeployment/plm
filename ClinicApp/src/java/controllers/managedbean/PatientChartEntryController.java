/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ClientJpaController;
import controllers.jpa.PatientChartJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.AppUtil;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;
import models.PatientChart;

/**
 *
 * @author apple
 */
@ManagedBean(name = "patientChartEntry")
@ViewScoped
public class PatientChartEntryController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    //Data Entry
    private Client thisClient;
    private PatientChart entChart;
    private PatientChart hearingChart;
    private PatientChart cosmeticChart;

    private Date recordDate = new Date();
    private String height = "";
    private String weight = "";
    private String bp = "";
    private String pulse = "";
    private String referredBy = "";
    private String maintenanceMeds = "";
    private String recentMeds = "";
    private String additionalNotes = "";
    private String chiefComplaint = "";
    private String illnessHistory = "";
    private String diagnosis = "";
    private String physicalExam = "";
    private String procedureDone = "";
    private String eprocedureDoneBy = "";
    private String hprocedureDoneBy = "";
    private String cprocedureDoneBy = "";

    //HEARING START
    private String hearinglossnotice = "";
    private String betterear = "";
    private String noiseexposure = "";
    private String tinnitus = "";
    private String tinnitusear = "";
    private String earfamilyhistory = "";
    private String earsurgery = "";
    private String earsurgerynote = "";
    private String tympStaticLeft = "";
    private String tympStaticRight = "";
    private String tympMiddleLeft = "";
    private String tympMiddleRight = "";
    private String tympVolumeLeft = "";
    private String tympTypeRight = "";
    private String tympTypeLeft = "";
    private String audioAcLeft1 = "0";
    private String audioAcLeft2 = "0";
    private String audioAcLeft3 = "0";
    private String audioAcLeft4 = "0";
    private String audioAcLeft5 = "0";
    private String audioAcLeft6 = "0";
    private String audioAcLeftAvg = "0";
    private String audioAcRight1 = "0";
    private String audioAcRight2 = "0";
    private String audioAcRight3 = "0";
    private String audioAcRight4 = "0";
    private String audioAcRight5 = "0";
    private String audioAcRight6 = "0";
    private String audioAcRightAvg = "0";
    private String audioBcLeft1 = "0";
    private String audioBcLeft2 = "0";
    private String audioBcLeft3 = "0";
    private String audioBcLeft4 = "0";
    private String audioBcLeft5 = "0";
    private String audioBcLeft6 = "0";
    private String audioBcLeftAvg = "0";
    private String audioBcRight1 = "0";
    private String audioBcRight2 = "0";
    private String audioBcRight3 = "0";
    private String audioBcRight4 = "0";
    private String audioBcRight5 = "0";
    private String audioBcRight6 = "0";
    private String audioBcRightAvg = "0";
    private String hearingrecommendation = "";
    private String tympVolumeRight = "";
    private String hearingAidUse = "";
    private String hearingAidNote = "";
    private String hearingLossDuration = "";
    private String hearingLossOnset = "";
    private String leftEarAssessment = "";
    private String rightEarAssessment = "";
    private String hearingassessment = "";
    private String doctororder = "";

    //HEARING END
    //COSMETIC START
    private String previousfacialtreatment = "";
    private String chemicalpeel = "";
    private String skinproducts = "";
    private boolean is_shaving = false;
    private boolean is_waxing = false;
    private boolean is_electro = false;
    private boolean is_pluck = false;
    private boolean is_tweeze = false;
    private boolean is_string = false;
    private boolean is_depilatories = false;
    private String sunscreen_use = "";
    private String sunscreen_notes = "";

    //COSMETIC END
    //ADDITIONAL
    private String productsused;
    private String coughcolds;
    private Integer age;

    public void saveENTChart() {

        if (chiefComplaint.trim().isEmpty()) {
            JsfUtil.addWarningMessage("Please indicate chief complaint. (Required)");
            return;
        }

        if (thisClient.getAge() == null) {
            JsfUtil.addWarningMessage("Please indicate age of patient. (Required)");
            return;
        }

        ClientJpaController cc = new ClientJpaController(utx, factory);
        PatientChartJpaController c = new PatientChartJpaController(utx, factory);
        PatientChart p = new PatientChart();
        p.setActive(1);
        p.setAdditionalNotes(additionalNotes.trim());
        p.setBloodpressure(bp.trim());
        p.setCharttype(1);//ENT
        p.setChiefcomplaint(chiefComplaint.trim());
        p.setClient(thisClient);
        p.setDiagnosis(diagnosis.trim());
        p.setHeight(height.trim());
        p.setIllnesshistory(illnessHistory.trim());
        p.setMaintenancemeds(maintenanceMeds.trim());
        p.setPhysicalexam(physicalExam.trim());
        p.setProcedures(procedureDone.trim());
        p.setPulse(pulse.trim());
        p.setRecentmeds(recentMeds.trim());
        p.setRecorddatetime(recordDate);
        p.setReferredby(referredBy.trim());
        p.setWeight(weight.trim());
        p.setDateOpen(new Date());
        p.setDateRecord(new Date());
        p.setDateUpdate(new Date());
        p.setDoctororder(doctororder.trim());
        p.setEProceduredoneby(eprocedureDoneBy.trim());

        try {
            thisClient.setLastprocedure("ENT: " + chiefComplaint);
            thisClient.setLastcheckup(new Date());
            cc.edit(thisClient);

            c.create(p);
            JsfUtil.addSuccessMessage("New ENT Chart saved!");
        } catch (Exception ex) {
            JsfUtil.addErrorMessage(ex, ex.getMessage());
            Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void saveHearingChart() {

        if (chiefComplaint.trim().isEmpty()) {
            JsfUtil.addWarningMessage("Please indicate chief complaint. (Required)");
            return;
        }
        ClientJpaController cc = new ClientJpaController(utx, factory);
        PatientChartJpaController c = new PatientChartJpaController(utx, factory);
        PatientChart p = new PatientChart();
        p.setActive(1);
        p.setAdditionalNotes(additionalNotes.trim());
        p.setBloodpressure(bp.trim());
        p.setCharttype(2);//Hearing
        p.setChiefcomplaint(chiefComplaint.trim());
        p.setClient(thisClient);
        p.setDiagnosis(diagnosis.trim());
        p.setHeight(height.trim());
        p.setIllnesshistory(illnessHistory.trim());
        p.setMaintenancemeds(maintenanceMeds.trim());
        p.setPhysicalexam(physicalExam.trim());
        p.setProcedures(procedureDone.trim());
        p.setPulse(pulse.trim());
        p.setRecentmeds(recentMeds.trim());
        p.setRecorddatetime(recordDate);
        p.setReferredby(referredBy.trim());
        p.setWeight(weight.trim());
        p.setDateOpen(new Date());
        p.setDateRecord(new Date());
        p.setDateUpdate(new Date());

        //SPECIFIC FIELDS BEGIN
        p.setAudioAcLeft1(audioAcLeft1);
        p.setAudioAcLeft2(audioAcLeft2);
        p.setAudioAcLeft3(audioAcLeft3);
        p.setAudioAcLeft4(audioAcLeft4);
        p.setAudioAcLeft5(audioAcLeft5);
        p.setAudioAcLeft6(audioAcLeft6);
        p.setAudioBcLeft1(audioBcLeft1);
        p.setAudioBcLeft2(audioBcLeft2);
        p.setAudioBcLeft3(audioBcLeft3);
        p.setAudioBcLeft4(audioBcLeft4);
        p.setAudioBcLeft5(audioBcLeft5);
        p.setAudioBcLeft6(audioBcLeft6);

        p.setAudioAcRight1(audioAcRight1);
        p.setAudioAcRight2(audioAcRight2);
        p.setAudioAcRight3(audioAcRight3);
        p.setAudioAcRight4(audioAcRight4);
        p.setAudioAcRight5(audioAcRight5);
        p.setAudioAcRight6(audioAcRight6);
        p.setAudioBcRight1(audioBcRight1);
        p.setAudioBcRight2(audioBcRight2);
        p.setAudioBcRight3(audioBcRight3);
        p.setAudioBcRight4(audioBcRight4);
        p.setAudioBcRight5(audioBcRight5);
        p.setAudioBcRight6(audioBcRight6);

        p.setAudioAcLeftAvg(audioAcLeftAvg);
        p.setAudioAcRightAvg(audioAcRightAvg);
        p.setAudioBcLeftAvg(audioBcLeftAvg);
        p.setAudioBcRightAvg(audioBcRightAvg);

        p.setTympStaticLeft(tympStaticLeft);
        p.setTympStaticRight(tympStaticRight);
        p.setTympMiddleLeft(tympMiddleLeft);
        p.setTympMiddleRight(tympMiddleRight);
        p.setTympVolumeLeft(tympVolumeLeft);
        p.setTympVolumeRight(tympVolumeRight);
        p.setTympTypeLeft(tympTypeLeft);
        p.setTympTypeRight(tympTypeRight);
        p.setHearinglossnotice(hearinglossnotice);
        p.setHearingAidUse(hearingAidUse);
        p.setHearingAidNote(hearingAidNote);
        p.setBetterear(betterear);
        p.setTinnitus(tinnitus);
        p.setTinnitusear(tinnitusear);
        p.setEarfamilyhistory(earfamilyhistory);
        p.setHearingLossDuration(hearingLossDuration);
        p.setHearingLossOnset(hearingLossOnset);
        p.setLeftEarAssessment(leftEarAssessment);
        p.setRightEarAssessment(rightEarAssessment);
        p.setHearingassessment(hearingassessment);
        p.setDoctororder(doctororder.trim());
        p.setHProceduredoneby(hprocedureDoneBy.trim());

        //SPECIFIC FIELDS END
        try {
            thisClient.setLastprocedure("Hearing: " + chiefComplaint);
            thisClient.setLastcheckup(new Date());
            cc.edit(thisClient);

            c.create(p);
            JsfUtil.addSuccessMessage("New Hearing Chart saved!");
        } catch (Exception ex) {
            JsfUtil.addErrorMessage(ex, ex.getMessage());
            Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void saveCosmeticChart() {
        if (chiefComplaint.trim().isEmpty()) {
            JsfUtil.addWarningMessage("Please indicate chief complaint. (Required)");
            return;
        }
        ClientJpaController cc = new ClientJpaController(utx, factory);
        PatientChartJpaController c = new PatientChartJpaController(utx, factory);
        PatientChart p = new PatientChart();
        p.setActive(1);
        p.setAdditionalNotes(additionalNotes.trim());
        p.setBloodpressure(bp.trim());
        p.setCharttype(3);//Cosmetic
        p.setChiefcomplaint(chiefComplaint.trim());
        p.setClient(thisClient);
        p.setDiagnosis(diagnosis.trim());
        p.setHeight(height.trim());
        p.setIllnesshistory(illnessHistory.trim());
        p.setMaintenancemeds(maintenanceMeds.trim());
        p.setPhysicalexam(physicalExam.trim());
        p.setProcedures(procedureDone.trim());
        p.setPulse(pulse.trim());
        p.setRecentmeds(recentMeds.trim());
        p.setRecorddatetime(recordDate);
        p.setReferredby(referredBy.trim());
        p.setWeight(weight.trim());
        p.setDateOpen(new Date());
        p.setDateRecord(new Date());
        p.setDateUpdate(new Date());
        p.setDoctororder(doctororder);
        p.setPreviousfacialtreatment(previousfacialtreatment.trim());
        p.setChemicalpeel(chemicalpeel);
        p.setSkinproducts(skinproducts);
        p.setIsDepilatories(is_depilatories);
        p.setIsElectro(is_electro);
        p.setIsPluck(is_pluck);
        p.setIsShaving(is_shaving);
        p.setIsString(is_string);
        p.setIsTweeze(is_tweeze);
        p.setIsWaxing(is_waxing);
        p.setSunscreenUse(sunscreen_use.trim());
        p.setSunscreenNotes(sunscreen_notes.trim());
        p.setCProceduredoneby(cprocedureDoneBy.trim());

        try {
            thisClient.setLastprocedure("Cosmetics: " + chiefComplaint);
            thisClient.setLastcheckup(new Date());
            cc.edit(thisClient);

            c.create(p);

            JsfUtil.addSuccessMessage("New Cosmetic Chart saved!");
        } catch (Exception ex) {
            JsfUtil.addErrorMessage(ex, ex.getMessage());
            Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * @return the recordDate
     */
    public Date getRecordDate() {
        return recordDate;
    }

    /**
     * @param recordDate the recordDate to set
     */
    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    /**
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * @return the weight
     */
    public String getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public void setWeight(String weight) {
        this.weight = weight;
    }

    /**
     * @return the bp
     */
    public String getBp() {
        return bp;
    }

    /**
     * @param bp the bp to set
     */
    public void setBp(String bp) {
        this.bp = bp;
    }

    /**
     * @return the pulse
     */
    public String getPulse() {
        return pulse;
    }

    /**
     * @param pulse the pulse to set
     */
    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    /**
     * @return the maintenanceMeds
     */
    public String getMaintenanceMeds() {
        return maintenanceMeds;
    }

    /**
     * @param maintenanceMeds the maintenanceMeds to set
     */
    public void setMaintenanceMeds(String maintenanceMeds) {
        this.maintenanceMeds = maintenanceMeds;
    }

    /**
     * @return the recentMeds
     */
    public String getRecentMeds() {
        return recentMeds;
    }

    /**
     * @param recentMeds the recentMeds to set
     */
    public void setRecentMeds(String recentMeds) {
        this.recentMeds = recentMeds;
    }

    /**
     * @return the chiefComplaint
     */
    public String getChiefComplaint() {
        return chiefComplaint;
    }

    /**
     * @param chiefComplaint the chiefComplaint to set
     */
    public void setChiefComplaint(String chiefComplaint) {
        this.chiefComplaint = chiefComplaint;
    }

    /**
     * @return the illnessHistory
     */
    public String getIllnessHistory() {
        return illnessHistory;
    }

    /**
     * @param illnessHistory the illnessHistory to set
     */
    public void setIllnessHistory(String illnessHistory) {
        this.illnessHistory = illnessHistory;
    }

    /**
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis the diagnosis to set
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return the physicalExam
     */
    public String getPhysicalExam() {
        return physicalExam;
    }

    /**
     * @param physicalExam the physicalExam to set
     */
    public void setPhysicalExam(String physicalExam) {
        this.physicalExam = physicalExam;
    }

    /**
     * @return the procedureDone
     */
    public String getProcedureDone() {
        return procedureDone;
    }

    /**
     * @param procedureDone the procedureDone to set
     */
    public void setProcedureDone(String procedureDone) {
        this.procedureDone = procedureDone;
    }

    /**
     * @return the thisClient
     */
    public Client getThisClient() {
        return thisClient;
    }

    /**
     * @param thisClient the thisClient to set
     */
    public void setThisClient(Client thisClient) {
        this.thisClient = thisClient;
    }

    public void prepareClient() {
        if (thisClient != null) {
            EntityManager em = factory.createEntityManager();
            thisClient = em.merge(thisClient);
            em.refresh(thisClient);
            em.close();
        }
        recordDate = new Date();
        height = "";
        weight = "";
        bp = "";
        pulse = "";
        referredBy = "";
        maintenanceMeds = "";
        recentMeds = "";
        additionalNotes = "";
        chiefComplaint = "";
        illnessHistory = "";
        diagnosis = "";
        physicalExam = "";
        procedureDone = "";
        //hearing start
        hearinglossnotice = "";
        betterear = "";
        noiseexposure = "";
        tinnitus = "";
        tinnitusear = "";
        earfamilyhistory = "";
        earsurgery = "";
        earsurgerynote = "";
        tympStaticLeft = "";
        tympStaticRight = "";
        tympMiddleLeft = "";
        tympMiddleRight = "";
        tympVolumeLeft = "";
        tympTypeRight = "";
        tympTypeLeft = "";
        audioAcLeft1 = "0";
        audioAcLeft2 = "0";
        audioAcLeft3 = "0";
        audioAcLeft4 = "0";
        audioAcLeft5 = "0";
        audioAcLeft6 = "0";
        audioAcLeftAvg = "0";
        audioAcRight1 = "0";
        audioAcRight2 = "0";
        audioAcRight3 = "0";
        audioAcRight4 = "0";
        audioAcRight5 = "0";
        audioAcRight6 = "0";
        audioAcRightAvg = "0";
        audioBcLeft1 = "0";
        audioBcLeft2 = "0";
        audioBcLeft3 = "0";
        audioBcLeft4 = "0";
        audioBcLeft5 = "0";
        audioBcLeft6 = "0";
        audioBcLeftAvg = "0";
        audioBcRight1 = "0";
        audioBcRight2 = "0";
        audioBcRight3 = "0";
        audioBcRight4 = "0";
        audioBcRight5 = "0";
        audioBcRight6 = "0";
        audioBcRightAvg = "0";
        hearingassessment = "";
        hearingrecommendation = "";
        tympVolumeRight = "";
        hearingAidUse = "";
        hearingAidNote = "";
        hearingLossDuration = "";
        hearingLossOnset = "";
        doctororder = "";
        previousfacialtreatment = "";
        chemicalpeel = "";
        skinproducts = "";
        is_shaving = false;
        is_waxing = false;
        is_electro = false;
        is_pluck = false;
        is_tweeze = false;
        is_string = false;
        is_depilatories = false;
        sunscreen_use = "";
        sunscreen_notes = "";
        eprocedureDoneBy = "";
        hprocedureDoneBy = "";
        cprocedureDoneBy = "";
        //hearing end
    }

    /**
     * @return the additionalNotes
     */
    public String getAdditionalNotes() {
        return additionalNotes;
    }

    /**
     * @param additionalNotes the additionalNotes to set
     */
    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    /**
     * @return the referredBy
     */
    public String getReferredBy() {
        return referredBy;
    }

    /**
     * @param referredBy the referredBy to set
     */
    public void setReferredBy(String referredBy) {
        this.referredBy = referredBy;
    }

    /**
     * @return the hearinglossnotice
     */
    public String getHearinglossnotice() {
        return hearinglossnotice;
    }

    /**
     * @param hearinglossnotice the hearinglossnotice to set
     */
    public void setHearinglossnotice(String hearinglossnotice) {
        this.hearinglossnotice = hearinglossnotice;
    }

    /**
     * @return the betterear
     */
    public String getBetterear() {
        return betterear;
    }

    /**
     * @param betterear the betterear to set
     */
    public void setBetterear(String betterear) {
        this.betterear = betterear;
    }

    /**
     * @return the noiseexposure
     */
    public String getNoiseexposure() {
        return noiseexposure;
    }

    /**
     * @param noiseexposure the noiseexposure to set
     */
    public void setNoiseexposure(String noiseexposure) {
        this.noiseexposure = noiseexposure;
    }

    /**
     * @return the tinnitus
     */
    public String getTinnitus() {
        return tinnitus;
    }

    /**
     * @param tinnitus the tinnitus to set
     */
    public void setTinnitus(String tinnitus) {
        this.tinnitus = tinnitus;
    }

    /**
     * @return the tinnitusear
     */
    public String getTinnitusear() {
        return tinnitusear;
    }

    /**
     * @param tinnitusear the tinnitusear to set
     */
    public void setTinnitusear(String tinnitusear) {
        this.tinnitusear = tinnitusear;
    }

    /**
     * @return the earfamilyhistory
     */
    public String getEarfamilyhistory() {
        return earfamilyhistory;
    }

    /**
     * @param earfamilyhistory the earfamilyhistory to set
     */
    public void setEarfamilyhistory(String earfamilyhistory) {
        this.earfamilyhistory = earfamilyhistory;
    }

    /**
     * @return the earsurgery
     */
    public String getEarsurgery() {
        return earsurgery;
    }

    /**
     * @param earsurgery the earsurgery to set
     */
    public void setEarsurgery(String earsurgery) {
        this.earsurgery = earsurgery;
    }

    /**
     * @return the earsurgerynote
     */
    public String getEarsurgerynote() {
        return earsurgerynote;
    }

    /**
     * @param earsurgerynote the earsurgerynote to set
     */
    public void setEarsurgerynote(String earsurgerynote) {
        this.earsurgerynote = earsurgerynote;
    }

    /**
     * @return the tympStaticLeft
     */
    public String getTympStaticLeft() {
        return tympStaticLeft;
    }

    /**
     * @param tympStaticLeft the tympStaticLeft to set
     */
    public void setTympStaticLeft(String tympStaticLeft) {
        this.tympStaticLeft = tympStaticLeft;
    }

    /**
     * @return the tympStaticRight
     */
    public String getTympStaticRight() {
        return tympStaticRight;
    }

    /**
     * @param tympStaticRight the tympStaticRight to set
     */
    public void setTympStaticRight(String tympStaticRight) {
        this.tympStaticRight = tympStaticRight;
    }

    /**
     * @return the tympMiddleLeft
     */
    public String getTympMiddleLeft() {
        return tympMiddleLeft;
    }

    /**
     * @param tympMiddleLeft the tympMiddleLeft to set
     */
    public void setTympMiddleLeft(String tympMiddleLeft) {
        this.tympMiddleLeft = tympMiddleLeft;
    }

    /**
     * @return the tympMiddleRight
     */
    public String getTympMiddleRight() {
        return tympMiddleRight;
    }

    /**
     * @param tympMiddleRight the tympMiddleRight to set
     */
    public void setTympMiddleRight(String tympMiddleRight) {
        this.tympMiddleRight = tympMiddleRight;
    }

    /**
     * @return the tympVolumeLeft
     */
    public String getTympVolumeLeft() {
        return tympVolumeLeft;
    }

    /**
     * @param tympVolumeLeft the tympVolumeLeft to set
     */
    public void setTympVolumeLeft(String tympVolumeLeft) {
        this.tympVolumeLeft = tympVolumeLeft;
    }

    /**
     * @return the tympTypeRight
     */
    public String getTympTypeRight() {
        return tympTypeRight;
    }

    /**
     * @param tympTypeRight the tympTypeRight to set
     */
    public void setTympTypeRight(String tympTypeRight) {
        this.tympTypeRight = tympTypeRight;
    }

    /**
     * @return the tympTypeLeft
     */
    public String getTympTypeLeft() {
        return tympTypeLeft;
    }

    /**
     * @param tympTypeLeft the tympTypeLeft to set
     */
    public void setTympTypeLeft(String tympTypeLeft) {
        this.tympTypeLeft = tympTypeLeft;
    }

    /**
     * @return the audioAcLeft1
     */
    public String getAudioAcLeft1() {
        return audioAcLeft1;
    }

    /**
     * @param audioAcLeft1 the audioAcLeft1 to set
     */
    public void setAudioAcLeft1(String audioAcLeft1) {
        this.audioAcLeft1 = audioAcLeft1;
    }

    /**
     * @return the audioAcLeft2
     */
    public String getAudioAcLeft2() {
        return audioAcLeft2;
    }

    /**
     * @param audioAcLeft2 the audioAcLeft2 to set
     */
    public void setAudioAcLeft2(String audioAcLeft2) {
        this.audioAcLeft2 = audioAcLeft2;
    }

    /**
     * @return the audioAcLeft3
     */
    public String getAudioAcLeft3() {
        return audioAcLeft3;
    }

    /**
     * @param audioAcLeft3 the audioAcLeft3 to set
     */
    public void setAudioAcLeft3(String audioAcLeft3) {
        this.audioAcLeft3 = audioAcLeft3;
    }

    /**
     * @return the audioAcLeft4
     */
    public String getAudioAcLeft4() {
        return audioAcLeft4;
    }

    /**
     * @param audioAcLeft4 the audioAcLeft4 to set
     */
    public void setAudioAcLeft4(String audioAcLeft4) {
        this.audioAcLeft4 = audioAcLeft4;
    }

    /**
     * @return the audioAcLeft5
     */
    public String getAudioAcLeft5() {
        return audioAcLeft5;
    }

    /**
     * @param audioAcLeft5 the audioAcLeft5 to set
     */
    public void setAudioAcLeft5(String audioAcLeft5) {
        this.audioAcLeft5 = audioAcLeft5;
    }

    /**
     * @return the audioAcLeft6
     */
    public String getAudioAcLeft6() {
        return audioAcLeft6;
    }

    /**
     * @param audioAcLeft6 the audioAcLeft6 to set
     */
    public void setAudioAcLeft6(String audioAcLeft6) {
        this.audioAcLeft6 = audioAcLeft6;
    }

    /**
     * @return the audioAcLeftAvg
     */
    public String getAudioAcLeftAvg() {
        return audioAcLeftAvg;
    }

    /**
     * @param audioAcLeftAvg the audioAcLeftAvg to set
     */
    public void setAudioAcLeftAvg(String audioAcLeftAvg) {
        this.audioAcLeftAvg = audioAcLeftAvg;
    }

    /**
     * @return the audioAcRight1
     */
    public String getAudioAcRight1() {
        return audioAcRight1;
    }

    /**
     * @param audioAcRight1 the audioAcRight1 to set
     */
    public void setAudioAcRight1(String audioAcRight1) {
        this.audioAcRight1 = audioAcRight1;
    }

    /**
     * @return the audioAcRight2
     */
    public String getAudioAcRight2() {
        return audioAcRight2;
    }

    /**
     * @param audioAcRight2 the audioAcRight2 to set
     */
    public void setAudioAcRight2(String audioAcRight2) {
        this.audioAcRight2 = audioAcRight2;
    }

    /**
     * @return the audioAcRight3
     */
    public String getAudioAcRight3() {
        return audioAcRight3;
    }

    /**
     * @param audioAcRight3 the audioAcRight3 to set
     */
    public void setAudioAcRight3(String audioAcRight3) {
        this.audioAcRight3 = audioAcRight3;
    }

    /**
     * @return the audioAcRight4
     */
    public String getAudioAcRight4() {
        return audioAcRight4;
    }

    /**
     * @param audioAcRight4 the audioAcRight4 to set
     */
    public void setAudioAcRight4(String audioAcRight4) {
        this.audioAcRight4 = audioAcRight4;
    }

    /**
     * @return the audioAcRight5
     */
    public String getAudioAcRight5() {
        return audioAcRight5;
    }

    /**
     * @param audioAcRight5 the audioAcRight5 to set
     */
    public void setAudioAcRight5(String audioAcRight5) {
        this.audioAcRight5 = audioAcRight5;
    }

    /**
     * @return the audioAcRight6
     */
    public String getAudioAcRight6() {
        return audioAcRight6;
    }

    /**
     * @param audioAcRight6 the audioAcRight6 to set
     */
    public void setAudioAcRight6(String audioAcRight6) {
        this.audioAcRight6 = audioAcRight6;
    }

    /**
     * @return the audioAcRightAvg
     */
    public String getAudioAcRightAvg() {
        return audioAcRightAvg;
    }

    /**
     * @param audioAcRightAvg the audioAcRightAvg to set
     */
    public void setAudioAcRightAvg(String audioAcRightAvg) {
        this.audioAcRightAvg = audioAcRightAvg;
    }

    /**
     * @return the audioBcLeft1
     */
    public String getAudioBcLeft1() {
        return audioBcLeft1;
    }

    /**
     * @param audioBcLeft1 the audioBcLeft1 to set
     */
    public void setAudioBcLeft1(String audioBcLeft1) {
        this.audioBcLeft1 = audioBcLeft1;
    }

    /**
     * @return the audioBcLeft2
     */
    public String getAudioBcLeft2() {
        return audioBcLeft2;
    }

    /**
     * @param audioBcLeft2 the audioBcLeft2 to set
     */
    public void setAudioBcLeft2(String audioBcLeft2) {
        this.audioBcLeft2 = audioBcLeft2;
    }

    /**
     * @return the audioBcLeft3
     */
    public String getAudioBcLeft3() {
        return audioBcLeft3;
    }

    /**
     * @param audioBcLeft3 the audioBcLeft3 to set
     */
    public void setAudioBcLeft3(String audioBcLeft3) {
        this.audioBcLeft3 = audioBcLeft3;
    }

    /**
     * @return the audioBcLeft4
     */
    public String getAudioBcLeft4() {
        return audioBcLeft4;
    }

    /**
     * @param audioBcLeft4 the audioBcLeft4 to set
     */
    public void setAudioBcLeft4(String audioBcLeft4) {
        this.audioBcLeft4 = audioBcLeft4;
    }

    /**
     * @return the audioBcLeft5
     */
    public String getAudioBcLeft5() {
        return audioBcLeft5;
    }

    /**
     * @param audioBcLeft5 the audioBcLeft5 to set
     */
    public void setAudioBcLeft5(String audioBcLeft5) {
        this.audioBcLeft5 = audioBcLeft5;
    }

    /**
     * @return the audioBcLeft6
     */
    public String getAudioBcLeft6() {
        return audioBcLeft6;
    }

    /**
     * @param audioBcLeft6 the audioBcLeft6 to set
     */
    public void setAudioBcLeft6(String audioBcLeft6) {
        this.audioBcLeft6 = audioBcLeft6;
    }

    /**
     * @return the audioBcLeftAvg
     */
    public String getAudioBcLeftAvg() {
        return audioBcLeftAvg;
    }

    /**
     * @param audioBcLeftAvg the audioBcLeftAvg to set
     */
    public void setAudioBcLeftAvg(String audioBcLeftAvg) {
        this.audioBcLeftAvg = audioBcLeftAvg;
    }

    /**
     * @return the audioBcRight1
     */
    public String getAudioBcRight1() {
        return audioBcRight1;
    }

    /**
     * @param audioBcRight1 the audioBcRight1 to set
     */
    public void setAudioBcRight1(String audioBcRight1) {
        this.audioBcRight1 = audioBcRight1;
    }

    /**
     * @return the audioBcRight2
     */
    public String getAudioBcRight2() {
        return audioBcRight2;
    }

    /**
     * @param audioBcRight2 the audioBcRight2 to set
     */
    public void setAudioBcRight2(String audioBcRight2) {
        this.audioBcRight2 = audioBcRight2;
    }

    /**
     * @return the audioBcRight3
     */
    public String getAudioBcRight3() {
        return audioBcRight3;
    }

    /**
     * @param audioBcRight3 the audioBcRight3 to set
     */
    public void setAudioBcRight3(String audioBcRight3) {
        this.audioBcRight3 = audioBcRight3;
    }

    /**
     * @return the audioBcRight4
     */
    public String getAudioBcRight4() {
        return audioBcRight4;
    }

    /**
     * @param audioBcRight4 the audioBcRight4 to set
     */
    public void setAudioBcRight4(String audioBcRight4) {
        this.audioBcRight4 = audioBcRight4;
    }

    /**
     * @return the audioBcRight5
     */
    public String getAudioBcRight5() {
        return audioBcRight5;
    }

    /**
     * @param audioBcRight5 the audioBcRight5 to set
     */
    public void setAudioBcRight5(String audioBcRight5) {
        this.audioBcRight5 = audioBcRight5;
    }

    /**
     * @return the audioBcRight6
     */
    public String getAudioBcRight6() {
        return audioBcRight6;
    }

    /**
     * @param audioBcRight6 the audioBcRight6 to set
     */
    public void setAudioBcRight6(String audioBcRight6) {
        this.audioBcRight6 = audioBcRight6;
    }

    /**
     * @return the audioBcRightAvg
     */
    public String getAudioBcRightAvg() {
        return audioBcRightAvg;
    }

    /**
     * @param audioBcRightAvg the audioBcRightAvg to set
     */
    public void setAudioBcRightAvg(String audioBcRightAvg) {
        this.audioBcRightAvg = audioBcRightAvg;
    }

    /**
     * @return the hearingrecommendation
     */
    public String getHearingrecommendation() {
        return hearingrecommendation;
    }

    /**
     * @param hearingrecommendation the hearingrecommendation to set
     */
    public void setHearingrecommendation(String hearingrecommendation) {
        this.hearingrecommendation = hearingrecommendation;
    }

    /**
     * @return the tympVolumeRight
     */
    public String getTympVolumeRight() {
        return tympVolumeRight;
    }

    /**
     * @param tympVolumeRight the tympVolumeRight to set
     */
    public void setTympVolumeRight(String tympVolumeRight) {
        this.tympVolumeRight = tympVolumeRight;
    }

    /**
     * @return the hearingAidUse
     */
    public String getHearingAidUse() {
        return hearingAidUse;
    }

    /**
     * @param hearingAidUse the hearingAidUse to set
     */
    public void setHearingAidUse(String hearingAidUse) {
        this.hearingAidUse = hearingAidUse;
    }

    /**
     * @return the hearingAidNote
     */
    public String getHearingAidNote() {
        return hearingAidNote;
    }

    /**
     * @param hearingAidNote the hearingAidNote to set
     */
    public void setHearingAidNote(String hearingAidNote) {
        this.hearingAidNote = hearingAidNote;
    }

    public void calcACAvgLeft() {
        try {
            if (!audioAcLeft2.isEmpty() && !audioAcLeft3.isEmpty() && !audioAcLeft4.isEmpty()) {
                double val1 = Double.parseDouble(audioAcLeft2.trim());
                double val2 = Double.parseDouble(audioAcLeft3.trim());
                double val3 = Double.parseDouble(audioAcLeft4.trim());

                audioAcLeftAvg = String.format("%.1f", (val1 + val2 + val3) / 3);

            } else {
                audioAcLeftAvg = "0";
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void updateACAvgLeft() {
        try {
            if (!hearingChart.getAudioAcLeft2().isEmpty() && !hearingChart.getAudioAcLeft3().isEmpty() && !hearingChart.getAudioAcLeft4().isEmpty()) {
                double val1 = Double.parseDouble(hearingChart.getAudioAcLeft2().trim());
                double val2 = Double.parseDouble(hearingChart.getAudioAcLeft3().trim());
                double val3 = Double.parseDouble(hearingChart.getAudioAcLeft4().trim());

                hearingChart.setAudioAcLeftAvg(String.format("%.1f", (val1 + val2 + val3) / 3));

            } else {
                hearingChart.setAudioAcLeftAvg("0");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void calcACAvgRight() {
        try {
            if (!audioAcRight2.isEmpty() && !audioAcRight3.isEmpty() && !audioAcRight4.isEmpty()) {
                double val1 = Double.parseDouble(audioAcRight2.trim());
                double val2 = Double.parseDouble(audioAcRight3.trim());
                double val3 = Double.parseDouble(audioAcRight4.trim());

                audioAcRightAvg = String.format("%.1f", (val1 + val2 + val3) / 3);

            } else {
                audioAcRightAvg = "0";
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void updateACAvgRight() {
        try {
            if (!hearingChart.getAudioAcRight2().isEmpty() && !hearingChart.getAudioAcRight3().isEmpty() && !hearingChart.getAudioAcRight4().isEmpty()) {
                double val1 = Double.parseDouble(hearingChart.getAudioAcRight2().trim());
                double val2 = Double.parseDouble(hearingChart.getAudioAcRight3().trim());
                double val3 = Double.parseDouble(hearingChart.getAudioAcRight4().trim());

                hearingChart.setAudioAcRightAvg(String.format("%.1f", (val1 + val2 + val3) / 3));

            } else {
                hearingChart.setAudioAcRightAvg("0");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void calcBCAvgLeft() {
        try {
            if (!audioBcLeft2.isEmpty() && !audioBcLeft3.isEmpty() && !audioBcLeft4.isEmpty()) {
                double val1 = Double.parseDouble(audioBcLeft2.trim());
                double val2 = Double.parseDouble(audioBcLeft3.trim());
                double val3 = Double.parseDouble(audioBcLeft4.trim());

                audioBcLeftAvg = String.format("%.1f", (val1 + val2 + val3) / 3);

            } else {
                audioBcLeftAvg = "0";
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void updateBCAvgLeft() {
        try {
            if (!hearingChart.getAudioBcLeft2().isEmpty() && !hearingChart.getAudioBcLeft3().isEmpty() && !hearingChart.getAudioBcLeft4().isEmpty()) {
                double val1 = Double.parseDouble(hearingChart.getAudioBcLeft2().trim());
                double val2 = Double.parseDouble(hearingChart.getAudioBcLeft3().trim());
                double val3 = Double.parseDouble(hearingChart.getAudioBcLeft4().trim());

                hearingChart.setAudioBcLeftAvg(String.format("%.1f", (val1 + val2 + val3) / 3));

            } else {
                hearingChart.setAudioBcLeftAvg("0");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void calcBCAvgRight() {
        try {
            if (!audioBcRight2.isEmpty() && !audioBcRight3.isEmpty() && !audioBcRight4.isEmpty()) {
                double val1 = Double.parseDouble(audioBcRight2.trim());
                double val2 = Double.parseDouble(audioBcRight3.trim());
                double val3 = Double.parseDouble(audioBcRight4.trim());

                audioBcRightAvg = String.format("%.1f", (val1 + val2 + val3) / 3);

            } else {
                audioBcRightAvg = "0";
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    public void updateBCAvgRight() {
        try {
            if (!hearingChart.getAudioBcRight2().isEmpty() && !hearingChart.getAudioBcRight3().isEmpty() && !hearingChart.getAudioBcRight4().isEmpty()) {
                double val1 = Double.parseDouble(hearingChart.getAudioBcRight2().trim());
                double val2 = Double.parseDouble(hearingChart.getAudioBcRight3().trim());
                double val3 = Double.parseDouble(hearingChart.getAudioBcRight4().trim());

                hearingChart.setAudioBcRightAvg(String.format("%.1f", (val1 + val2 + val3) / 3));

            } else {
                hearingChart.setAudioBcRightAvg("0");
            }
        } catch (Exception e) {
            JsfUtil.addErrorMessage("Input not valid!");
        }
    }

    /**
     * @return the hearingLossDuration
     */
    public String getHearingLossDuration() {
        return hearingLossDuration;
    }

    /**
     * @param hearingLossDuration the hearingLossDuration to set
     */
    public void setHearingLossDuration(String hearingLossDuration) {
        this.hearingLossDuration = hearingLossDuration;
    }

    /**
     * @return the hearingLossOnset
     */
    public String getHearingLossOnset() {
        return hearingLossOnset;
    }

    /**
     * @param hearingLossOnset the hearingLossOnset to set
     */
    public void setHearingLossOnset(String hearingLossOnset) {
        this.hearingLossOnset = hearingLossOnset;
    }

    public void prepareENTChart() {
        if (entChart != null) {
            EntityManager em = factory.createEntityManager();
            entChart = em.merge(entChart);
            em.refresh(entChart);
            em.close();
            JsfUtil.addNotifMessage("Last Edited: " + AppUtil.simpleDateFormat.format(entChart.getDateUpdate()));
        }
    }

    public void prepareHearingChart() {
        if (hearingChart != null) {
            EntityManager em = factory.createEntityManager();
            hearingChart = em.merge(hearingChart);
            em.refresh(hearingChart);
            em.close();
            JsfUtil.addNotifMessage("Last Edited: " + AppUtil.simpleDateFormat.format(hearingChart.getDateUpdate()));
        }
    }

    public void prepareCosmeticChart() {
        if (cosmeticChart != null) {
            EntityManager em = factory.createEntityManager();
            cosmeticChart = em.merge(cosmeticChart);
            em.refresh(cosmeticChart);
            em.close();
            JsfUtil.addNotifMessage("Last Edited: " + AppUtil.simpleDateFormat.format(cosmeticChart.getDateUpdate()));
        }
    }

    public void updateENTChart() {
        if (entChart != null) {
            entChart.setDateUpdate(new Date());
            PatientChartJpaController editor = new PatientChartJpaController(utx, factory);
            try {
                editor.edit(entChart);
                JsfUtil.addSuccessMessage("ENT Chart updated!");
                JsfUtil.refreshPage();
            } catch (RollbackFailureException ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            } catch (Exception ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            }
        } else {
            JsfUtil.addErrorMessage("Empty chart!");
        }
    }

    public void updateHearingChart() {
        if (hearingChart != null) {
            hearingChart.setDateUpdate(new Date());
            PatientChartJpaController editor = new PatientChartJpaController(utx, factory);
            try {
                editor.edit(hearingChart);
                JsfUtil.addSuccessMessage("Hearing Chart updated!");
                JsfUtil.refreshPage();
            } catch (RollbackFailureException ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            } catch (Exception ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            }
        } else {
            JsfUtil.addErrorMessage("Empty chart!");
        }
    }

    public void updateCosmeticChart() {
        if (cosmeticChart != null) {
            cosmeticChart.setDateUpdate(new Date());
            PatientChartJpaController editor = new PatientChartJpaController(utx, factory);
            try {
                editor.edit(cosmeticChart);
                JsfUtil.addSuccessMessage("Cosmetic Chart updated!");
                JsfUtil.refreshPage();
            } catch (RollbackFailureException ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            } catch (Exception ex) {
                Logger.getLogger(PatientChartEntryController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ex.getMessage());
            }
        } else {
            JsfUtil.addErrorMessage("Empty chart!");
        }
    }

    /**
     * @return the entChart
     */
    public PatientChart getEntChart() {
        return entChart;
    }

    /**
     * @param entChart the entChart to set
     */
    public void setEntChart(PatientChart entChart) {
        this.entChart = entChart;
    }

    /**
     * @return the hearingChart
     */
    public PatientChart getHearingChart() {
        return hearingChart;
    }

    /**
     * @param hearingChart the hearingChart to set
     */
    public void setHearingChart(PatientChart hearingChart) {
        this.hearingChart = hearingChart;
    }

    /**
     * @return the cosmeticChart
     */
    public PatientChart getCosmeticChart() {
        return cosmeticChart;
    }

    /**
     * @param cosmeticChart the cosmeticChart to set
     */
    public void setCosmeticChart(PatientChart cosmeticChart) {
        this.cosmeticChart = cosmeticChart;
    }

    /**
     * @return the leftEarAssessment
     */
    public String getLeftEarAssessment() {
        return leftEarAssessment;
    }

    /**
     * @param leftEarAssessment the leftEarAssessment to set
     */
    public void setLeftEarAssessment(String leftEarAssessment) {
        this.leftEarAssessment = leftEarAssessment;
    }

    /**
     * @return the rightEarAssessment
     */
    public String getRightEarAssessment() {
        return rightEarAssessment;
    }

    /**
     * @param rightEarAssessment the rightEarAssessment to set
     */
    public void setRightEarAssessment(String rightEarAssessment) {
        this.rightEarAssessment = rightEarAssessment;
    }

    /**
     * @return the hearingassessment
     */
    public String getHearingassessment() {
        return hearingassessment;
    }

    /**
     * @param hearingassessment the hearingassessment to set
     */
    public void setHearingassessment(String hearingassessment) {
        this.hearingassessment = hearingassessment;
    }

    /**
     * @return the doctororder
     */
    public String getDoctororder() {
        return doctororder;
    }

    /**
     * @param doctororder the doctororder to set
     */
    public void setDoctororder(String doctororder) {
        this.doctororder = doctororder;
    }

    /**
     * @return the previousfacialtreatment
     */
    public String getPreviousfacialtreatment() {
        return previousfacialtreatment;
    }

    /**
     * @param previousfacialtreatment the previousfacialtreatment to set
     */
    public void setPreviousfacialtreatment(String previousfacialtreatment) {
        this.previousfacialtreatment = previousfacialtreatment;
    }

    /**
     * @return the chemicalpeel
     */
    public String getChemicalpeel() {
        return chemicalpeel;
    }

    /**
     * @param chemicalpeel the chemicalpeel to set
     */
    public void setChemicalpeel(String chemicalpeel) {
        this.chemicalpeel = chemicalpeel;
    }

    /**
     * @return the skinproducts
     */
    public String getSkinproducts() {
        return skinproducts;
    }

    /**
     * @param skinproducts the skinproducts to set
     */
    public void setSkinproducts(String skinproducts) {
        this.skinproducts = skinproducts;
    }

    /**
     * @return the is_shaving
     */
    public boolean isIs_shaving() {
        return is_shaving;
    }

    /**
     * @param is_shaving the is_shaving to set
     */
    public void setIs_shaving(boolean is_shaving) {
        this.is_shaving = is_shaving;
    }

    /**
     * @return the is_waxing
     */
    public boolean isIs_waxing() {
        return is_waxing;
    }

    /**
     * @param is_waxing the is_waxing to set
     */
    public void setIs_waxing(boolean is_waxing) {
        this.is_waxing = is_waxing;
    }

    /**
     * @return the is_electro
     */
    public boolean isIs_electro() {
        return is_electro;
    }

    /**
     * @param is_electro the is_electro to set
     */
    public void setIs_electro(boolean is_electro) {
        this.is_electro = is_electro;
    }

    /**
     * @return the is_pluck
     */
    public boolean isIs_pluck() {
        return is_pluck;
    }

    /**
     * @param is_pluck the is_pluck to set
     */
    public void setIs_pluck(boolean is_pluck) {
        this.is_pluck = is_pluck;
    }

    /**
     * @return the is_tweeze
     */
    public boolean isIs_tweeze() {
        return is_tweeze;
    }

    /**
     * @param is_tweeze the is_tweeze to set
     */
    public void setIs_tweeze(boolean is_tweeze) {
        this.is_tweeze = is_tweeze;
    }

    /**
     * @return the is_string
     */
    public boolean isIs_string() {
        return is_string;
    }

    /**
     * @param is_string the is_string to set
     */
    public void setIs_string(boolean is_string) {
        this.is_string = is_string;
    }

    /**
     * @return the is_depilatories
     */
    public boolean isIs_depilatories() {
        return is_depilatories;
    }

    /**
     * @param is_depilatories the is_depilatories to set
     */
    public void setIs_depilatories(boolean is_depilatories) {
        this.is_depilatories = is_depilatories;
    }

    /**
     * @return the sunscreen_use
     */
    public String getSunscreen_use() {
        return sunscreen_use;
    }

    /**
     * @param sunscreen_use the sunscreen_use to set
     */
    public void setSunscreen_use(String sunscreen_use) {
        this.sunscreen_use = sunscreen_use;
    }

    /**
     * @return the sunscreen_notes
     */
    public String getSunscreen_notes() {
        return sunscreen_notes;
    }

    /**
     * @param sunscreen_notes the sunscreen_notes to set
     */
    public void setSunscreen_notes(String sunscreen_notes) {
        this.sunscreen_notes = sunscreen_notes;
    }

    /**
     * @return the eprocedureDoneBy
     */
    public String getEprocedureDoneBy() {
        return eprocedureDoneBy;
    }

    /**
     * @param eprocedureDoneBy the eprocedureDoneBy to set
     */
    public void setEprocedureDoneBy(String eprocedureDoneBy) {
        this.eprocedureDoneBy = eprocedureDoneBy;
    }

    /**
     * @return the hprocedureDoneBy
     */
    public String getHprocedureDoneBy() {
        return hprocedureDoneBy;
    }

    /**
     * @param hprocedureDoneBy the hprocedureDoneBy to set
     */
    public void setHprocedureDoneBy(String hprocedureDoneBy) {
        this.hprocedureDoneBy = hprocedureDoneBy;
    }

    /**
     * @return the cprocedureDoneBy
     */
    public String getCprocedureDoneBy() {
        return cprocedureDoneBy;
    }

    /**
     * @param cprocedureDoneBy the cprocedureDoneBy to set
     */
    public void setCprocedureDoneBy(String cprocedureDoneBy) {
        this.cprocedureDoneBy = cprocedureDoneBy;
    }

    /**
     * @return the productsused
     */
    public String getProductsused() {
        return productsused;
    }

    /**
     * @param productsused the productsused to set
     */
    public void setProductsused(String productsused) {
        this.productsused = productsused;
    }

    /**
     * @return the coughcolds
     */
    public String getCoughcolds() {
        return coughcolds;
    }

    /**
     * @param coughcolds the coughcolds to set
     */
    public void setCoughcolds(String coughcolds) {
        this.coughcolds = coughcolds;
    }

    /**
     * @return the age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

}
