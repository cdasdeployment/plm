/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.ClientJpaController;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;

@ManagedBean(name = "clientController")
@ViewScoped
public class ClientController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private List<Client> clientList = new ArrayList();
    private Client thisClient;
    private ClientJpaController controller;
    private String searchKey;

//fields
    private String lastname = "";
    private String firstname = "";
    private Date birthday;
    private String gender = "";
    private String civilstatus = "";
    private String occupation = "";
    private String contactinfo = "-";
    private String streetaddress = "";
    private String brgy = "";
    private String town = "";
    private String province = "Davao del Sur";
    private String email = "";
    private String profilepic = "";
    private String insurance = "None";
    private String creditcard = "None";
    private String debitcard = "None";
    private String asthma = "None";
    private String foodallergy = "None";
    private String medicineallergy = "None";
    private String otherallergies = "None";
    private String hypertension = "None";
    private String diabetismellitus = "None";
    private String cancer = "None";
    private String smoking = "None";
    private String smokingsticksperday = "0";
    private String smokingyears = "0";
    private String alcoholic = "None";
    private String alcoholiclen = "0";
    private String alcoholicfreq = "None";
    private String surgery = "None";
    private String surgeryName = "None";
    private Date surgeryDate = null;
    private String emergencyname = "";
    private String emergencyaddress = "";
    private String emergencycontact = "";
    private String company = "";
    private Integer age = 0;
    private Integer months = 0;
    private Integer days = 0;
    private String latexAllergy = "None";
    private String coughcolds = "None";
    private Date firstCheckUp = new Date();
    private String maintenanceMeds = "";

    @PostConstruct
    public void init() {
        ClientJpaController clientController = new ClientJpaController(utx, factory);
        clientList = clientController.findClientEntities();
    }

    public void editClientInfo() {
        if (thisClient != null) {
            EntityManager em = factory.createEntityManager();
            thisClient = em.merge(thisClient);
            em.refresh(thisClient);
            em.close();            
            if (thisClient.getBirthday() != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(thisClient.getBirthday());
                LocalDate today = LocalDate.now();
                LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

                Period period = Period.between(bday, today);
                thisClient.setAge(period.getYears());
                thisClient.setMonths(period.getMonths());
                thisClient.setDays(period.getDays());
            }
        }
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the civilstatus
     */
    public String getCivilstatus() {
        return civilstatus;
    }

    /**
     * @param civilstatus the civilstatus to set
     */
    public void setCivilstatus(String civilstatus) {
        this.civilstatus = civilstatus;
    }

    /**
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation the occupation to set
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return the contactinfo
     */
    public String getContactinfo() {
        return contactinfo;
    }

    /**
     * @param contactinfo the contactinfo to set
     */
    public void setContactinfo(String contactinfo) {
        this.contactinfo = contactinfo;
    }

    /**
     * @return the streetaddress
     */
    public String getStreetaddress() {
        return streetaddress;
    }

    /**
     * @param streetaddress the streetaddress to set
     */
    public void setStreetaddress(String streetaddress) {
        this.streetaddress = streetaddress;
    }

    /**
     * @return the brgy
     */
    public String getBrgy() {
        return brgy;
    }

    /**
     * @param brgy the brgy to set
     */
    public void setBrgy(String brgy) {
        this.brgy = brgy;
    }

    /**
     * @return the town
     */
    public String getTown() {
        return town;
    }

    /**
     * @param town the town to set
     */
    public void setTown(String town) {
        this.town = town;
    }

    /**
     * @return the province
     */
    public String getProvince() {
        return province;
    }

    /**
     * @param province the province to set
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the profilepic
     */
    public String getProfilepic() {
        return profilepic;
    }

    /**
     * @param profilepic the profilepic to set
     */
    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    /**
     * @return the insurance
     */
    public String getInsurance() {
        return insurance;
    }

    /**
     * @param insurance the insurance to set
     */
    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    /**
     * @return the creditcard
     */
    public String getCreditcard() {
        return creditcard;
    }

    /**
     * @param creditcard the creditcard to set
     */
    public void setCreditcard(String creditcard) {
        this.creditcard = creditcard;
    }

    /**
     * @return the debitcard
     */
    public String getDebitcard() {
        return debitcard;
    }

    /**
     * @param debitcard the debitcard to set
     */
    public void setDebitcard(String debitcard) {
        this.debitcard = debitcard;
    }

    /**
     * @return the asthma
     */
    public String getAsthma() {
        return asthma;
    }

    /**
     * @param asthma the asthma to set
     */
    public void setAsthma(String asthma) {
        this.asthma = asthma;
    }

    /**
     * @return the foodallergy
     */
    public String getFoodallergy() {
        return foodallergy;
    }

    /**
     * @param foodallergy the foodallergy to set
     */
    public void setFoodallergy(String foodallergy) {
        this.foodallergy = foodallergy;
    }

    /**
     * @return the medicineallergy
     */
    public String getMedicineallergy() {
        return medicineallergy;
    }

    /**
     * @param medicineallergy the medicineallergy to set
     */
    public void setMedicineallergy(String medicineallergy) {
        this.medicineallergy = medicineallergy;
    }

    /**
     * @return the otherallergies
     */
    public String getOtherallergies() {
        return otherallergies;
    }

    /**
     * @param otherallergies the otherallergies to set
     */
    public void setOtherallergies(String otherallergies) {
        this.otherallergies = otherallergies;
    }

    /**
     * @return the hypertension
     */
    public String getHypertension() {
        return hypertension;
    }

    /**
     * @param hypertension the hypertension to set
     */
    public void setHypertension(String hypertension) {
        this.hypertension = hypertension;
    }

    /**
     * @return the diabetismellitus
     */
    public String getDiabetismellitus() {
        return diabetismellitus;
    }

    /**
     * @param diabetismellitus the diabetismellitus to set
     */
    public void setDiabetismellitus(String diabetismellitus) {
        this.diabetismellitus = diabetismellitus;
    }

    /**
     * @return the cancer
     */
    public String getCancer() {
        return cancer;
    }

    /**
     * @param cancer the cancer to set
     */
    public void setCancer(String cancer) {
        this.cancer = cancer;
    }

    /**
     * @return the smoking
     */
    public String getSmoking() {
        return smoking;
    }

    /**
     * @param smoking the smoking to set
     */
    public void setSmoking(String smoking) {
        this.smoking = smoking;
    }

    /**
     * @return the smokingsticksperday
     */
    public String getSmokingsticksperday() {
        return smokingsticksperday;
    }

    /**
     * @param smokingsticksperday the smokingsticksperday to set
     */
    public void setSmokingsticksperday(String smokingsticksperday) {
        this.smokingsticksperday = smokingsticksperday;
    }

    /**
     * @return the smokingyears
     */
    public String getSmokingyears() {
        return smokingyears;
    }

    /**
     * @param smokingyears the smokingyears to set
     */
    public void setSmokingyears(String smokingyears) {
        this.smokingyears = smokingyears;
    }

    /**
     * @return the alcoholic
     */
    public String getAlcoholic() {
        return alcoholic;
    }

    /**
     * @param alcoholic the alcoholic to set
     */
    public void setAlcoholic(String alcoholic) {
        this.alcoholic = alcoholic;
    }

    /**
     * @return the alcoholiclen
     */
    public String getAlcoholiclen() {
        return alcoholiclen;
    }

    /**
     * @param alcoholiclen the alcoholiclen to set
     */
    public void setAlcoholiclen(String alcoholiclen) {
        this.alcoholiclen = alcoholiclen;
    }

    /**
     * @return the alcoholicfreq
     */
    public String getAlcoholicfreq() {
        return alcoholicfreq;
    }

    /**
     * @param alcoholicfreq the alcoholicfreq to set
     */
    public void setAlcoholicfreq(String alcoholicfreq) {
        this.alcoholicfreq = alcoholicfreq;
    }

    /**
     * @return the surgery
     */
    public String getSurgery() {
        return surgery;
    }

    /**
     * @param surgery the surgery to set
     */
    public void setSurgery(String surgery) {
        this.surgery = surgery;
    }

    /**
     * @return the surgeryName
     */
    public String getSurgeryName() {
        return surgeryName;
    }

    /**
     * @param surgeryName the surgeryName to set
     */
    public void setSurgeryName(String surgeryName) {
        this.surgeryName = surgeryName;
    }

    /**
     * @return the surgeryDate
     */
    public Date getSurgeryDate() {
        return surgeryDate;
    }

    /**
     * @param surgeryDate the surgeryDate to set
     */
    public void setSurgeryDate(Date surgeryDate) {
        this.surgeryDate = surgeryDate;
    }

    /////////////////////////////////////////////////////////////////
    public void newEntry() {
        lastname = "";
        firstname = "";
        birthday = null;
        gender = "";
        civilstatus = "";
        occupation = "";
        contactinfo = "";
        streetaddress = "";
        brgy = "";
        town = "";
        province = "";
        email = "";
        profilepic = "";
        insurance = "None";
        creditcard = "None";
        debitcard = "None";
        asthma = "None";
        foodallergy = "None";
        medicineallergy = "None";
        otherallergies = "None";
        hypertension = "None";
        diabetismellitus = "None";
        cancer = "None";
        smoking = "None";
        smokingsticksperday = "0";
        smokingyears = "0";
        alcoholic = "None";
        alcoholiclen = "0";
        alcoholicfreq = "None";
        surgery = "None";
        surgeryName = "None";
        coughcolds = "None";
        surgeryDate = null;
        emergencyname = "";
        emergencyaddress = "";
        emergencycontact = "";
        company = "";
        age = 0;
        months = 0;
        days = 0;
        maintenanceMeds = "None";
        firstCheckUp = new Date();
        latexAllergy = "None";
        coughcolds = "None";
    }

    public void saveProfile() {
        controller = new ClientJpaController(utx, factory);
        clientList = controller.findClientEntities();
        if (clientList != null) {
            for (Client p : clientList) {
                if (p.getFirstname().trim().toLowerCase().equals(firstname.toLowerCase().trim())
                        && p.getLastname().trim().toLowerCase().equals(lastname.toLowerCase().trim())
                        && p.getContactinfo().trim().toLowerCase().contains(contactinfo.toLowerCase().trim())) {
                    JsfUtil.addErrorMessage("A duplicate record is found for: " + firstname + " " + lastname + " (" + contactinfo + ")\nPlease verify the input before proceeding.\nData not saved.");
                    return;
                }
            }
        }

        if (birthday != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(birthday);
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            if (period.getYears() < 18) {
                JsfUtil.addWarningMessage("Patient age is below 18 years. \nPlease update emergency contacts.");
            }
        }

        if (birthday != null) {
            Calendar clientYear = Calendar.getInstance();
            clientYear.setTime(birthday);

            Calendar rightNow = Calendar.getInstance();
            int thisYear = rightNow.get(Calendar.YEAR);

            if (clientYear.get(Calendar.YEAR) == thisYear) {

                if (civilstatus.equals("Infant")) {
                    if (age == null) {
                        JsfUtil.addErrorMessage("Please input birthdate or age.");
                        return;
                    }
                } else if (birthday == null || (age == null || age == 0)) {
                    JsfUtil.addErrorMessage("Please input birthdate or age.");
                    return;
                }
            }
        }

        if (age < 18) {
            JsfUtil.addWarningMessage("Patient age is below 18 years. \nPlease update emergency contacts.");
        }

        if (gender == null && (gender == null || gender.trim().isEmpty())) {
            JsfUtil.addErrorMessage("Patient gender is required!");
            return;
        }

        if (civilstatus.isEmpty()) {
            JsfUtil.addErrorMessage("Civil status is required!");
            return;
        }

        if (civilstatus == null) {
            JsfUtil.addErrorMessage("Civil status is required!");
            return;
        }

        if (birthday == null) {
            JsfUtil.addErrorMessage("Please input birthdate or age.");
            return;
        }

        Client client = new Client();
        client.setFirstname(firstname.trim().toUpperCase());
        client.setLastname(lastname.trim().toUpperCase());
        client.setBirthday(birthday);
        client.setGender(gender.trim().toUpperCase());
        client.setCivilstatus(civilstatus.trim().toUpperCase());
        client.setOccupation(occupation.trim().toUpperCase());
        client.setContactinfo(contactinfo.trim());
        client.setStreetaddress(streetaddress.trim().toUpperCase());
        client.setBrgy(brgy.trim().toUpperCase());
        client.setTown(town.trim().toUpperCase());
        client.setProvince(province.trim().toUpperCase());
        client.setEmail(email.trim().toLowerCase());
        client.setProfilepic(profilepic.trim());
        client.setInsurance(insurance.trim().toUpperCase());
        client.setCreditcard(creditcard.trim());
        client.setDebitcard(debitcard.trim());
        client.setAsthma(asthma.trim());
        client.setFoodallergy(foodallergy.trim().toUpperCase());
        client.setMedicineallergy(medicineallergy.trim().toUpperCase());
        client.setOtherallergies(otherallergies.trim().toUpperCase());
        client.setHypertension(hypertension.trim());
        client.setDiabetesmellitus(diabetismellitus.trim());
        client.setCancer(cancer.trim());
        client.setSmoking(smoking.trim());
        client.setSmokingsticksperday(smokingsticksperday.trim());
        client.setSmokingfrequency(smokingyears.trim());
        client.setAlcoholic(alcoholic.trim());
        client.setAlcoholiclength(alcoholiclen.trim());
        client.setAlcoholicfrequency(alcoholicfreq.trim());
        client.setSurgery(surgery.trim());
        client.setSurgerydone(surgeryName.trim().toUpperCase());
        client.setSurgerydate(surgeryDate);
        client.setEmergencyname(emergencyname.trim().toUpperCase());
        client.setEmergencyaddress((emergencyaddress.trim().isEmpty() ? (streetaddress.trim() + " " + brgy.trim() + " " + town.trim() + " " + province.trim()).trim().toUpperCase() : emergencyaddress.trim()).toUpperCase());
        client.setEmergencycontact(emergencycontact.trim().toUpperCase());
        client.setCompany(company.trim().toUpperCase());
        client.setAge(age);
        client.setMonths(months);
        client.setDays(days);
        client.setDiabetesmellitus(diabetismellitus.trim());
        client.setCheckupdate(firstCheckUp);
        client.setIsActive(true);
        client.setLatexallergy(latexAllergy);
        client.setCoughcolds(coughcolds);
        client.setMaintenanceMeds(maintenanceMeds.trim().toUpperCase());

        if (client.getBirthday() != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(client.getBirthday());
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            client.setAge(period.getYears());
            client.setMonths(period.getMonths());
            client.setDays(period.getDays());
        }

        ClientJpaController c = new ClientJpaController(utx, factory);
        try {
            c.create(client);
            JsfUtil.addSuccessMessage("Patient Profile successfully saved!");
            JsfUtil.refreshPage();
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, "Something went wrong!");
        }
    }

    public void updateProfile() {
        controller = new ClientJpaController(utx, factory);
        clientList = controller.findClientEntities();

        if (thisClient.getBirthday() != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(thisClient.getBirthday());
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            if (period.getYears() < 18) {
                JsfUtil.addWarningMessage("Please update emergency contacts.");
            }
        }
        if (thisClient.getBirthday() != null) {
            Calendar clientYear = Calendar.getInstance();
            clientYear.setTime(thisClient.getBirthday());

            Calendar rightNow = Calendar.getInstance();
            int thisYear = rightNow.get(Calendar.YEAR);

            if (clientYear.get(Calendar.YEAR) == thisYear) {

                if (thisClient.getCivilstatus().equals("Infant")) {
                    if (thisClient.getAge() == null) {
                        JsfUtil.addErrorMessage("Please input birthdate or age.");
                        return;
                    }
                } else if (thisClient.getBirthday() == null || (thisClient.getAge() == null || thisClient.getAge() == 0)) {
                    JsfUtil.addErrorMessage("Please input birthdate or age.");
                    return;
                }
            }
        }

        if (thisClient.getCivilstatus().isEmpty()) {
            JsfUtil.addErrorMessage("Civil status is required!");
            return;
        }

        if (thisClient.getCivilstatus() == null) {
            JsfUtil.addErrorMessage("Civil status is required!");
            return;
        }

        if (thisClient.getBirthday() != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(thisClient.getBirthday());
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            thisClient.setAge(period.getYears());
            thisClient.setMonths(period.getMonths());
            thisClient.setDays(period.getDays());
        }

        ClientJpaController c = new ClientJpaController(utx, factory);
        try {
            thisClient.setIsActive(true);
            c.edit(thisClient);
            JsfUtil.addSuccessMessage("Patient Profile successfully updated!");
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, "Something went wrong!");
        }
    }

    public void deactivateProfile() {
        ClientJpaController c = new ClientJpaController(utx, factory);
        try {
            thisClient.setIsActive(false);
            c.edit(thisClient);
            JsfUtil.addSuccessMessage("Patient Profile successfully deleted!");
            JsfUtil.refreshPage();
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, "Something went wrong!");
        }
    }

    public List<Client> searchClient() {
        searchKey = searchKey.toLowerCase().trim();
        clientList.clear();
        ClientJpaController clientController = new ClientJpaController(utx, factory);
        if (searchKey.trim().isEmpty()) {
            clientList = clientController.findClientEntities();
        } else {
            EntityManager em = factory.createEntityManager();
            for (Client c : clientController.findClientEntities()) {
                System.out.println(c.getFirstname().toLowerCase().contains(searchKey) || c.getLastname().toLowerCase().contains(searchKey));
                if (c.getFirstname().toLowerCase().contains(searchKey) || c.getLastname().toLowerCase().contains(searchKey)) {
                    c = em.merge(c);
                    em.refresh(c);
                    clientList.add(c);
                }
            }
            em.close();
        }
        return clientList;
    }

    /**
     * @return the clientList
     */
    public List<Client> getClientList() {
        return clientList;
    }

    /**
     * @param clientList the clientList to set
     */
    public void setClientList(List<Client> clientList) {
        this.clientList = clientList;
    }

    /**
     * @return the thisClient
     */
    public Client getThisClient() {
        return thisClient;
    }

    /**
     * @param thisClient the thisClient to set
     */
    public void setThisClient(Client thisClient) {
        this.thisClient = thisClient;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }

    /**
     * @param searchKey the searchKey to set
     */
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

    /**
     * @return the emergencyname
     */
    public String getEmergencyname() {
        return emergencyname;
    }

    /**
     * @param emergencyname the emergencyname to set
     */
    public void setEmergencyname(String emergencyname) {
        this.emergencyname = emergencyname;
    }

    /**
     * @return the emergencyaddress
     */
    public String getEmergencyaddress() {
        return emergencyaddress;
    }

    /**
     * @param emergencyaddress the emergencyaddress to set
     */
    public void setEmergencyaddress(String emergencyaddress) {
        this.emergencyaddress = emergencyaddress;
    }

    /**
     * @return the emergencycontact
     */
    public String getEmergencycontact() {
        return emergencycontact;
    }

    /**
     * @param emergencycontact the emergencycontact to set
     */
    public void setEmergencycontact(String emergencycontact) {
        this.emergencycontact = emergencycontact;
    }

    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }

    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }

    /**
     * @return the age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

    /**
     * @return the firstCheckUp
     */
    public Date getFirstCheckUp() {
        return firstCheckUp;
    }

    /**
     * @param firstCheckUp the firstCheckUp to set
     */
    public void setFirstCheckUp(Date firstCheckUp) {
        this.firstCheckUp = firstCheckUp;
    }

    public void calculateAge() {
        if (birthday != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(birthday);
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            age = period.getYears();
            months = period.getMonths();
            days = period.getDays();
        } else {
            age = 0;
            months = 0;
            days = 0;
        }
    }

    public void calculateAgeEdit() {
        if (thisClient != null) {
            if (thisClient.getBirthday() != null) {
                Calendar c = Calendar.getInstance();
                c.setTime(thisClient.getBirthday());
                LocalDate today = LocalDate.now();
                LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

                Period period = Period.between(bday, today);
                thisClient.setAge(period.getYears());
                thisClient.setMonths(period.getMonths());
                thisClient.setDays(period.getDays());
            } else {
                thisClient.setAge(0);
                thisClient.setMonths(0);
                thisClient.setDays(0);

            }
        }
    }

    public void calculateDayAge() {
        if (birthday != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(birthday);
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            days = period.getDays();
        } else {
            days = 0;
        }
    }

    public void calculatePatientAge() {
        if (thisClient.getBirthday() != null) {
            Calendar c = Calendar.getInstance();
            c.setTime(thisClient.getBirthday());
            LocalDate today = LocalDate.now();
            LocalDate bday = LocalDate.of(c.get(Calendar.YEAR), c.get(Calendar.MONTH) + 1, c.get(Calendar.DAY_OF_MONTH));

            Period period = Period.between(bday, today);
            thisClient.setAge(period.getYears());
        } else {
            thisClient.setAge(0);
        }
    }

    /**
     * @return the latexAllergy
     */
    public String getLatexAllergy() {
        return latexAllergy;
    }

    /**
     * @param latexAllergy the latexAllergy to set
     */
    public void setLatexAllergy(String latexAllergy) {
        this.latexAllergy = latexAllergy;
    }

    /**
     * @return the coughcolds
     */
    public String getCoughcolds() {
        return coughcolds;
    }

    /**
     * @param coughcolds the coughcolds to set
     */
    public void setCoughcolds(String coughcolds) {
        this.coughcolds = coughcolds;
    }

    /**
     * @return the months
     */
    public Integer getMonths() {
        return months;
    }

    /**
     * @param months the months to set
     */
    public void setMonths(Integer months) {
        this.months = months;
    }

    /**
     * @return the days
     */
    public Integer getDays() {
        return days;
    }

    /**
     * @param days the days to set
     */
    public void setDays(Integer days) {
        this.days = days;
    }

    /**
     * @return the maintenanceMeds
     */
    public String getMaintenanceMeds() {
        return maintenanceMeds;
    }

    /**
     * @param maintenanceMeds the maintenanceMeds to set
     */
    public void setMaintenanceMeds(String maintenanceMeds) {
        this.maintenanceMeds = maintenanceMeds;
    }
}
