/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import models.Client;

/**
 *
 * @author apple
 */
@ManagedBean
@ViewScoped
public class ClientSearchController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String searchKey = "";

    public List<Client> completeClient(String q) {
        List<Client> suggestions = new ArrayList<Client>();
        EntityManager em = factory.createEntityManager();
        Query query = em.createQuery("SELECT c FROM Client c WHERE c.lastname LIKE '" + q.trim().toLowerCase() + "%' OR c.firstname LIKE '" + q.trim().toLowerCase() + "%' ORDER BY c.lastname,c.firstname").setMaxResults(5);
        for (Object c : query.getResultList()) {
            if (((Client) c).getIsActive()) {
                suggestions.add(((Client) c));
            }
        }
        em.close();
        return suggestions;
    }

    /**
     * @return the searchKey
     */
    public String getSearchKey() {
        return searchKey;
    }

    /**
     * @param searchKey the searchKey to set
     */
    public void setSearchKey(String searchKey) {
        this.searchKey = searchKey;
    }

}
