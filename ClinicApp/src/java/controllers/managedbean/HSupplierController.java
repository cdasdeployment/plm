/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.HSupplierJpaController;
import controllers.jpa.SupplierJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.AppUtil;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.HSupplier;
import models.Supplier;

/**
 *
 * @author apple
 */
@ManagedBean(name = "hsupplierController")
@ViewScoped
public class HSupplierController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String suppliername;
    private String supplieraddress;
    private String suppliercontact;
    private String supplieragent;

    private HSupplier thisSupplier;
    private List<HSupplier> supplierList;

    private String searchkey = "";

    private HSupplierJpaController controller = new HSupplierJpaController(utx, factory);

    @PostConstruct
    public void init() {
        controller = new HSupplierJpaController(utx, factory);
        setSupplierList(controller.findHSupplierEntities());
    }

    public void prepareForm() {
        suppliername = "";
        supplieraddress = "";
        suppliercontact = "";
        supplieragent = "";
    }

    public void refreshSuppliers() {
        controller = new HSupplierJpaController(utx, factory);
        setSupplierList(controller.findHSupplierEntities());
    }

    public void update() {
        controller = new HSupplierJpaController(utx, factory);
        try {
            controller.edit(getThisSupplier());
            JsfUtil.addSuccessMessage("Supplier details updated!");
            refreshSuppliers();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(HSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(HSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void save() {
        if (suppliername.isEmpty()) {
            JsfUtil.addErrorMessage("Please input supplier name");
            return;
        }

        if (suppliercontact.isEmpty()) {
            JsfUtil.addErrorMessage("Please input supplier contact info");
            return;
        }
        controller = new HSupplierJpaController(utx, factory);
        HSupplier s = new HSupplier();
        s.setSuppliername(suppliername.trim().toUpperCase());
        s.setSupplieraddress(supplieraddress.trim().toUpperCase());
        s.setSuppliercontact(suppliercontact.trim().toUpperCase());
        s.setSupplieragent(supplieragent.trim().toUpperCase());
        try {
            controller.create(s);
            JsfUtil.addSuccessMessage("Added new supplier!");
            refreshSuppliers();
        } catch (Exception ex) {
            Logger.getLogger(HSupplierController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<HSupplier> search() {
        searchkey = searchkey.toLowerCase();
        System.out.println(searchkey);
        supplierList.clear();
        controller = new HSupplierJpaController(utx, factory);
        if (searchkey.trim().isEmpty()) {
            supplierList = controller.findHSupplierEntities();
        } else {
            EntityManager em = factory.createEntityManager();
            for (HSupplier s : controller.findHSupplierEntities()) {
                //System.out.println(s.getFirstname().toLowerCase().contains(searchKey) || s.getLastname().toLowerCase().contains(searchKey));
                if (s.getSuppliername().toLowerCase().contains(searchkey) || s.getSupplieragent().toLowerCase().contains(searchkey)) {
                    s = em.merge(s);
                    em.refresh(s);
                    supplierList.add(s);
                }
            }
            em.close();
        }
        return supplierList;
    }

    public void prepareSupplierInfo() {
        if (getThisSupplier() != null) {
            EntityManager em = factory.createEntityManager();
            setThisSupplier(em.merge(getThisSupplier()));
            em.refresh(getThisSupplier());
            em.close();
        }
    }

    /**
     * @return the suppliername
     */
    public String getSuppliername() {
        return suppliername;
    }

    /**
     * @param suppliername the suppliername to set
     */
    public void setSuppliername(String suppliername) {
        this.suppliername = suppliername;
    }

    /**
     * @return the supplieraddress
     */
    public String getSupplieraddress() {
        return supplieraddress;
    }

    /**
     * @param supplieraddress the supplieraddress to set
     */
    public void setSupplieraddress(String supplieraddress) {
        this.supplieraddress = supplieraddress;
    }

    /**
     * @return the suppliercontact
     */
    public String getSuppliercontact() {
        return suppliercontact;
    }

    /**
     * @param suppliercontact the suppliercontact to set
     */
    public void setSuppliercontact(String suppliercontact) {
        this.suppliercontact = suppliercontact;
    }

    /**
     * @return the supplieragent
     */
    public String getSupplieragent() {
        return supplieragent;
    }

    /**
     * @param supplieragent the supplieragent to set
     */
    public void setSupplieragent(String supplieragent) {
        this.supplieragent = supplieragent;
    }

    /**
     * @return the supplierList
     */
    public List<HSupplier> getSupplierList() {
        return supplierList;
    }

    /**
     * @param supplierList the supplierList to set
     */
    public void setSupplierList(List<HSupplier> supplierList) {
        this.supplierList = supplierList;
    }

    /**
     * @return the thisSupplier
     */
    public HSupplier getThisSupplier() {
        return thisSupplier;
    }

    /**
     * @param thisSupplier the thisSupplier to set
     */
    public void setThisSupplier(HSupplier thisSupplier) {
        this.thisSupplier = thisSupplier;
    }

    /**
     * @return the searchkey
     */
    public String getSearchkey() {
        return searchkey;
    }

    /**
     * @param searchkey the searchkey to set
     */
    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

}
