/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.HearingaidinventoryJpaController;
import controllers.jpa.DruginventoryJpaController;
import controllers.jpa.MedicineitemJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Hearingaidinventory;
import models.Hearingaiditem;
import models.Druginventory;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "hearingAidStockViewController")
@ViewScoped
public class HearingAidStockViewController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Hearingaiditem thisItem;

    private HearingaidinventoryJpaController controller;
    private List<Hearingaidinventory> inventoryList = new ArrayList<Hearingaidinventory>();

    @PostConstruct
    public void init() {
        inventoryList = new ArrayList<Hearingaidinventory>();
        controller = new HearingaidinventoryJpaController(utx, factory);
        inventoryList = controller.findHearingaidinventoryEntities();
    }

    public void refreshInventoryList() {
        inventoryList = new ArrayList<Hearingaidinventory>();
        controller = new HearingaidinventoryJpaController(utx, factory);
        inventoryList = controller.findHearingaidinventoryEntities();
    }

    public void prepareItemInventory() {
        inventoryList = new ArrayList<Hearingaidinventory>();
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(thisItem));
            em.refresh(thisItem);
            em.close();
        }
        inventoryList = thisItem.getHearingaidinventoryList();
        
    }

    /**
     * @return the thisItem
     */
    public Hearingaiditem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(Hearingaiditem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the inventoryList
     */
    public List<Hearingaidinventory> getInventoryList() {
        return inventoryList;
    }

    /**
     * @param inventoryList the inventoryList to set
     */
    public void setInventoryList(List<Hearingaidinventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

}
