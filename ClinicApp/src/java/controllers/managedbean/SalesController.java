/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.DrugsaledetailJpaController;
import controllers.jpa.DrugsalesummaryJpaController;
import controllers.jpa.MedicineitemJpaController;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Client;
import models.Drugsaledetail;
import models.Drugsalesummary;
import models.Medicineitem;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author apple
 */
@ManagedBean(name = "salesController")
@ViewScoped
public class SalesController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    //summary
    private Client thisClient;
    private Medicineitem thisMedicine;
    private List<Drugsaledetail> medicineList = new ArrayList();
    private Drugsaledetail thisMedicineItem;
    private Drugsaledetail editItem;
    private Drugsalesummary saleSummary;

    private int stocks;
    private double price;
    private int quantity;
    private double totalAmount;

    public void refreshSalesList() {

        medicineList = new ArrayList();
        DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);

        for (Drugsaledetail d : detailController.findDrugsaledetailEntities()) {
            if (d.getSalesummary().equals(saleSummary)) {
                medicineList.add(d);
            }
        }
    }

    //details
    public void updateItem() {
        if (editItem != null) {
            DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
            try {
                detailController.edit(editItem);
                JsfUtil.addNotifMessage("Item updated!");
                refreshSalesList();
            } catch (RollbackFailureException ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public void deleteItem() {
        if (editItem != null) {
            DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
            MedicineitemJpaController mjc = new MedicineitemJpaController(utx, factory);
            try {
                Medicineitem editMedicine = editItem.getDrug();
                editMedicine.setStocks(editMedicine.getStocks() + editItem.getQuantity());
                mjc.edit(editMedicine);
                
                detailController.destroy(editItem.getId());
                JsfUtil.addNotifMessage("Item deleted!");
                refreshSalesList();
            } catch (RollbackFailureException ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void addMedicineList() {

        if (thisClient == null) {
            JsfUtil.addErrorMessage("Please specify customer.");
            return;
        }

        if (thisMedicine == null) {
            JsfUtil.addErrorMessage("Please specify medicine to purchase.");
            return;
        }

        if (quantity <= 0) {
            JsfUtil.addErrorMessage("Please specify correct quantity.");
            return;
        }

        System.out.println("Quantity = " + quantity);
        System.out.println("Price = " + thisMedicine.getSelling());

        if (saleSummary == null) {
            System.out.println("Sale Summary is empty");
            saleSummary = new Drugsalesummary();
            saleSummary.setAmountchange(0.0);
            saleSummary.setAmountreceived(0.0);
            saleSummary.setAuthcode("");
            saleSummary.setCardexpiry("");
            saleSummary.setCardname("");
            saleSummary.setCardnumber("");
            saleSummary.setChecknumber("");
            saleSummary.setClient(thisClient);
            saleSummary.setClientname(thisClient.getFirstname() + " " + thisClient.getLastname());
            saleSummary.setDescription("");
            saleSummary.setHmo("");
            saleSummary.setHmodiscount(0.0);
            saleSummary.setHmodiscountamount(0.0);
            saleSummary.setMidcode("");
            saleSummary.setPaymentmode("");
            saleSummary.setPrecode("");
            saleSummary.setSalediscount(0.0);
            saleSummary.setSalediscountamount(0.0);
            saleSummary.setSaletotal(0.0);
            saleSummary.setSeqcode("");
            saleSummary.setServicedatetime(new Date());
            saleSummary.setTotalamount(0.0);
            saleSummary.setTransactioncode("");

            DrugsalesummaryJpaController summaryController = new DrugsalesummaryJpaController(utx, factory);
            try {
                summaryController.create(saleSummary);
                JsfUtil.addNotifMessage("Sales Summary created for this transaction");
            } catch (Exception ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            }

            Drugsaledetail detail = new Drugsaledetail();
            detail.setClient(thisClient.getId());
            detail.setClientname(thisClient.getFirstname() + " " + thisClient.getLastname());
            detail.setDrug(thisMedicine);
            detail.setDescription(thisMedicine.getGenericname() + " (" + thisMedicine.getBrandname() + ") " + thisMedicine.getPreparation() + " " + thisMedicine.getMtype());
            detail.setHmo("");
            detail.setHmodiscount(0.0);
            detail.setHmodiscountamount(0.0);
            detail.setItemdiscount(0.0);
            detail.setItemdiscountamount(0.0);
            detail.setPrice(thisMedicine.getSelling());
            detail.setQuantity(quantity);
            detail.setTotal((quantity * thisMedicine.getSelling()) - (detail.getHmodiscountamount() + detail.getItemdiscountamount()));
            detail.setServicedatetime(new Date());
            detail.setSalesummary(saleSummary);
            detail.setTransactioncode("");
            DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
            try {
                detailController.create(detail);
                totalAmount = quantity * thisMedicine.getSelling();
                JsfUtil.addNotifMessage("New item added!");
                refreshSalesList();
            } catch (Exception ex) {
                Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
            }

        } else {
            System.out.println("Refresh Sale Summary");
            EntityManager em = factory.createEntityManager();
            saleSummary = em.merge(saleSummary);
            em.refresh(saleSummary);

            if (!saleSummary.getDrugsaledetailList().isEmpty()) {

                for (Drugsaledetail dDetail : saleSummary.getDrugsaledetailList()) {
                    if (dDetail.getDrug().equals(thisMedicine)) {
                        dDetail.setQuantity(dDetail.getQuantity() + quantity);
                        dDetail.setTotal((dDetail.getQuantity() * dDetail.getDrug().getSelling()) - (dDetail.getHmodiscountamount() + dDetail.getItemdiscountamount()));
                        dDetail.setServicedatetime(new Date());
                        dDetail.setSalesummary(saleSummary);
                        dDetail.setTransactioncode("");
                        DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
                        try {
                            detailController.edit(dDetail);
                            totalAmount = totalAmount + (quantity * dDetail.getDrug().getSelling());
                            JsfUtil.addNotifMessage("Item updated!");
                            refreshSalesList();
                        } catch (Exception ex) {
                            Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
                        } finally {
                            em.close();
                        }
                        return;
                    }//if
                    else {
                        Drugsaledetail detail = new Drugsaledetail();
                        detail.setClient(thisClient.getId());
                        detail.setClientname(thisClient.getFirstname() + " " + thisClient.getLastname());
                        detail.setDrug(thisMedicine);
                        detail.setDescription(thisMedicine.getGenericname() + " (" + thisMedicine.getBrandname() + ") " + thisMedicine.getPreparation() + " " + thisMedicine.getMtype());
                        detail.setHmo("");
                        detail.setHmodiscount(0.0);
                        detail.setHmodiscountamount(0.0);
                        detail.setItemdiscount(0.0);
                        detail.setItemdiscountamount(0.0);
                        detail.setPrice(thisMedicine.getSelling());
                        detail.setQuantity(quantity);
                        detail.setTotal((quantity * thisMedicine.getSelling()) - (detail.getHmodiscountamount() + detail.getItemdiscountamount()));
                        detail.setServicedatetime(new Date());
                        detail.setSalesummary(saleSummary);
                        detail.setTransactioncode("");
                        DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
                        try {
                            detailController.create(detail);
                            totalAmount = totalAmount + detail.getTotal();
                            JsfUtil.addNotifMessage("New item added!");
                            refreshSalesList();
                        } catch (Exception ex) {
                            Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
                        } finally {
                            em.close();
                        }
                    }
                    return;
                }//for
            } else {
                Drugsaledetail detail = new Drugsaledetail();
                detail.setClient(thisClient.getId());
                detail.setClientname(thisClient.getFirstname() + " " + thisClient.getLastname());
                detail.setDrug(thisMedicine);
                detail.setDescription(thisMedicine.getGenericname() + " (" + thisMedicine.getBrandname() + ") " + thisMedicine.getPreparation() + " " + thisMedicine.getMtype());
                detail.setHmo("");
                detail.setHmodiscount(0.0);
                detail.setHmodiscountamount(0.0);
                detail.setItemdiscount(0.0);
                detail.setItemdiscountamount(0.0);
                detail.setPrice(thisMedicine.getSelling());
                detail.setQuantity(quantity);
                detail.setTotal((quantity * thisMedicine.getSelling()) - (detail.getHmodiscountamount() + detail.getItemdiscountamount()));
                detail.setServicedatetime(new Date());
                detail.setSalesummary(saleSummary);
                detail.setTransactioncode("");
                DrugsaledetailJpaController detailController = new DrugsaledetailJpaController(utx, factory);
                try {
                    detailController.create(detail);
                    totalAmount = quantity * thisMedicine.getSelling();
                    JsfUtil.addNotifMessage("New item added!");
                    refreshSalesList();
                } catch (Exception ex) {
                    Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    em.close();
                }

            }//else

        }
        //update medicine quantity
        MedicineitemJpaController mjc = new MedicineitemJpaController(utx, factory);
        thisMedicine.setStocks(thisMedicine.getStocks() - quantity);
        try {
            mjc.edit(thisMedicine);
            JsfUtil.addNotifMessage("Item stocks updated!");
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(SalesController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void handleSelectDrug(SelectEvent event) {
        thisMedicine = (Medicineitem) event.getObject();
        stocks = thisMedicine.getStocks();
        price = thisMedicine.getSelling();
    }

    public void updateQuantity() {
        if (editItem.getQuantity() < 0) {
            JsfUtil.addErrorMessage("Please specify correct amount.");
            return;
        }

        if (editItem != null) {
            if (editItem.getQuantity() > 0) {
                editItem.setTotal(editItem.getQuantity() * editItem.getDrug().getSelling());
                updateItem();
            }

            if (editItem.getQuantity() == 0) {
                deleteItem();
            }

            if ((editItem.getHmodiscount() != null && editItem.getHmodiscountamount() != null)) {

                if (((editItem.getHmodiscount() == 0 && editItem.getHmodiscountamount() > 0))
                        || ((editItem.getHmodiscount() > 0 && editItem.getHmodiscountamount() == 0))) {

                    if ((editItem.getHmodiscount() == 0 && editItem.getHmodiscountamount() > 0)) {
                        if (editItem.getHmodiscountamount() <= editItem.getTotal()) {
                            editItem.setTotal(editItem.getTotal() - editItem.getHmodiscountamount());
                            editItem.setHmodiscount((editItem.getHmodiscountamount() / editItem.getTotal()) * 100);
                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                    if ((editItem.getHmodiscount() > 0 && editItem.getHmodiscountamount() == 0)) {
                        if ((editItem.getTotal() * (editItem.getHmodiscount() / 100)) <= editItem.getTotal()) {
                            editItem.setTotal(editItem.getTotal() - (editItem.getTotal() * (editItem.getHmodiscount() / 100)));
                            editItem.setHmodiscount(editItem.getHmodiscount() / 100);
                            editItem.setHmodiscountamount((editItem.getTotal() * (editItem.getHmodiscount() / 100)));
                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                }
            }

        }
    }

    public void updateHMODiscount() {
        if (editItem != null) {
            if (editItem.getHmo() == null) {
                JsfUtil.addErrorMessage("Please specify HMO provider.");
                return;

            }
            if (editItem.getHmo().isEmpty()) {
                JsfUtil.addErrorMessage("Please specify HMO provider.");
                return;
            }
            if (editItem.getHmodiscount() == null && editItem.getHmodiscountamount() == null) {
                JsfUtil.addErrorMessage("Please specify correct amount.");
                return;

            }

            if (editItem.getHmodiscount() > 0 && editItem.getHmodiscountamount() > 0) {
                JsfUtil.addErrorMessage("Use only one field to specify amount.");
                return;
            }

            if (editItem.getHmodiscount() <= 0 && editItem.getHmodiscountamount() <= 0) {
                JsfUtil.addErrorMessage("Please specify correct amount.");
                return;
            }

            if ((editItem.getHmodiscount() != null && editItem.getHmodiscountamount() != null)) {

                if (((editItem.getHmodiscount() == 0 && editItem.getHmodiscountamount() > 0))
                        || ((editItem.getHmodiscount() > 0 && editItem.getHmodiscountamount() == 0))) {

                    if ((editItem.getHmodiscount() == 0 && editItem.getHmodiscountamount() > 0)) {
                        if (editItem.getHmodiscountamount() <= editItem.getTotal()) {
                            editItem.setTotal(editItem.getTotal() - editItem.getHmodiscountamount());
                            editItem.setHmodiscount((editItem.getHmodiscountamount() / editItem.getTotal()) * 100);
                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                    if ((editItem.getHmodiscount() > 0 && editItem.getHmodiscountamount() == 0)) {
                        if ((editItem.getTotal() * (editItem.getHmodiscount() / 100)) <= editItem.getTotal()) {

                            editItem.setHmodiscount(editItem.getHmodiscount() / 100);
                            editItem.setHmodiscountamount(editItem.getTotal() * editItem.getHmodiscount());
                            editItem.setTotal(editItem.getTotal() - editItem.getHmodiscountamount());

                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                }
            }
        }
    }

    public void updateItemDiscount() {
        if (editItem != null) {

            if (editItem.getItemdiscount() == null && editItem.getItemdiscountamount() == null) {
                JsfUtil.addErrorMessage("Please specify correct amount.");
                return;

            }

            if (editItem.getItemdiscount() > 0 && editItem.getItemdiscountamount() > 0) {
                JsfUtil.addErrorMessage("Use only one field to specify amount.");
                return;
            }

            if (editItem.getItemdiscount() <= 0 && editItem.getItemdiscountamount() <= 0) {
                JsfUtil.addErrorMessage("Please specify correct amount.");
                return;
            }

            if ((editItem.getItemdiscount() != null && editItem.getItemdiscountamount() != null)) {

                if (((editItem.getItemdiscount() == 0 && editItem.getItemdiscountamount() > 0))
                        || ((editItem.getItemdiscount() > 0 && editItem.getItemdiscountamount() == 0))) {

                    if ((editItem.getItemdiscount() == 0 && editItem.getItemdiscountamount() > 0)) {
                        if (editItem.getItemdiscountamount() <= editItem.getTotal()) {
                            editItem.setTotal(editItem.getTotal() - editItem.getItemdiscountamount());
                            editItem.setItemdiscount((editItem.getItemdiscountamount() / editItem.getTotal()) * 100);
                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                    if ((editItem.getItemdiscount() > 0 && editItem.getItemdiscountamount() == 0)) {
                        if ((editItem.getTotal() * (editItem.getItemdiscount() / 100)) <= editItem.getTotal()) {

                            editItem.setItemdiscount(editItem.getItemdiscount() / 100);
                            editItem.setItemdiscountamount(editItem.getTotal() * editItem.getItemdiscount());
                            editItem.setTotal(editItem.getTotal() - editItem.getItemdiscountamount());

                            updateItem();
                            refreshSalesList();
                        } else {
                            JsfUtil.addErrorMessage("Please specify correct amount.");
                            return;
                        }
                    }

                }
            }
        }
    }

    /**
     * @return the thisClient
     */
    public Client getThisClient() {
        return thisClient;
    }

    /**
     * @param thisClient the thisClient to set
     */
    public void setThisClient(Client thisClient) {
        this.thisClient = thisClient;
    }

    /**
     * @return the thisMedicine
     */
    public Medicineitem getThisMedicine() {
        return thisMedicine;
    }

    /**
     * @param thisMedicine the thisMedicine to set
     */
    public void setThisMedicine(Medicineitem thisMedicine) {
        this.thisMedicine = thisMedicine;
    }

    /**
     * @return the stocks
     */
    public int getStocks() {
        return stocks;
    }

    /**
     * @param stocks the stocks to set
     */
    public void setStocks(int stocks) {
        this.stocks = stocks;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the medicineList
     */
    public List<Drugsaledetail> getMedicineList() {
        return medicineList;
    }

    /**
     * @param medicineList the medicineList to set
     */
    public void setMedicineList(List<Drugsaledetail> medicineList) {
        this.medicineList = medicineList;
    }

    /**
     * @return the thisMedicineItem
     */
    public Drugsaledetail getThisMedicineItem() {
        return thisMedicineItem;
    }

    /**
     * @param thisMedicineItem the thisMedicineItem to set
     */
    public void setThisMedicineItem(Drugsaledetail thisMedicineItem) {
        this.thisMedicineItem = thisMedicineItem;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the editItem
     */
    public Drugsaledetail getEditItem() {
        return editItem;
    }

    /**
     * @param editItem the editItem to set
     */
    public void setEditItem(Drugsaledetail editItem) {
        this.editItem = editItem;
    }

    /**
     * @return the totalAmount
     */
    public double getTotalAmount() {
        totalAmount = 0;

        if (!medicineList.isEmpty()) {
            for (Drugsaledetail temp : medicineList) {
                totalAmount = totalAmount + temp.getTotal().doubleValue();
            }
        }

        return totalAmount;
    }

    /**
     * @param totalAmount the totalAmount to set
     */
    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

}
