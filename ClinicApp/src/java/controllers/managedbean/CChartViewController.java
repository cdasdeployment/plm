/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.BillingDetailJpaController;
import controllers.jpa.ChartDetailsJpaController;
import controllers.jpa.ClientJpaController;
import controllers.jpa.DiagnosticreferenceJpaController;
import controllers.jpa.MedicineitemJpaController;
import controllers.jpa.PrescriptionJpaController;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.Config;
import controllers.util.JsfUtil;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.transaction.UserTransaction;
import models.Appointment;
import models.BillingDetail;
import models.ChartDetails;
import models.Client;
import models.Clinicservices;
import models.Diagnosticreference;
import models.Medicineitem;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.primefaces.event.SelectEvent;
import reports.datasource.RxDataSourcev2;

/**
 *
 * @author apple
 */
@ManagedBean(name = "cchartViewController")
@ViewScoped
public class CChartViewController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Client client;
    private ChartDetails selectedChart;
    private Appointment selObject;
    private String diagnosis;
    private List<ChartDetails> chartList = new ArrayList<>();
    private List<String> diagnosisList = new ArrayList<>();
    private List<BillingDetail> billingList = new ArrayList<>();
    private BillingDetail billItem;
    private Clinicservices selectedService;
    private String serviceDescription = "";
    private double serviceAmount = 0.0;
    private String diagnosisNote;
    private String medcertmessage;
    private boolean isRxVisible = false;
    private boolean isCertVisible = false;
    private Medicineitem drug1;
    private String generic1 = "";
    private String brand1 = "";
    private String preparation1 = "";

    private Medicineitem drug2;
    private String generic2 = "";
    private String brand2 = "";
    private String preparation2 = "";

    private Medicineitem drug3;
    private String generic3 = "";
    private String brand3 = "";
    private String preparation3 = "";

    private Medicineitem drug4;
    private String generic4 = "";
    private String brand4 = "";
    private String preparation4 = "";

    private Medicineitem drug5;
    private String generic5 = "";
    private String brand5 = "";
    private String preparation5 = "";

    private Medicineitem drug6;
    private String generic6 = "";
    private String brand6 = "";
    private String preparation6 = "";

    private String quantity1 = "";
    private String quantity2 = "";
    private String quantity3 = "";
    private String quantity4 = "";
    private String quantity5 = "";
    private String quantity6 = "";

    private String direction1 = "";
    private String direction2 = "";
    private String direction3 = "";
    private String direction4 = "";
    private String direction5 = "";
    private String direction6 = "";

    //for maintenance meds
    private Medicineitem mdrug1;
    private String mgeneric1;
    private String mbrand1;
    private String mpreparation1;

    private Medicineitem mdrug2;
    private String mgeneric2;
    private String mbrand2;
    private String mpreparation2;

    private Medicineitem mdrug3;
    private String mgeneric3;
    private String mbrand3;
    private String mpreparation3;

    private String mquantity1;
    private String mquantity2;
    private String mquantity3;

    private String mdirection1;
    private String mdirection2;
    private String mdirection3;
    private String pdfViewPath;

    @PostConstruct
    public void init() {
        FacesContext fcontext = FacesContext.getCurrentInstance();
        Map<String, String> paramMap = fcontext.getExternalContext().getRequestParameterMap();
        if (paramMap.containsKey("viewId")) {
            String chartId = paramMap.get("viewId");
            System.out.println("Chart ID = " + chartId);

            EntityManager em = factory.createEntityManager();
            selectedChart = em.find(ChartDetails.class, Integer.parseInt(chartId));
            selectedChart = em.merge(selectedChart);
            em.refresh(selectedChart);
            String chartCode = selectedChart.getChartCode();
            System.out.println("Found the chart code: " + chartCode);
            client = em.find(Client.class, selectedChart.getClient().getId());
            client = em.merge(client);
            em.refresh(client);
            em.close();
            JsfUtil.addSuccessMessage("Displaying related information...");
            System.out.println(client);
            //load sequence start
            chartList = new ArrayList();
            boolean isFound = false;

            if (client != null) {
                System.out.println("client is not null");
                ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
                if (client.getChartDetailsList().isEmpty()) {
                } else {
                    em = factory.createEntityManager();
                    client = em.merge(client);
                    em.refresh(client);
                    em.close();
                    if (!client.getChartDetailsList().isEmpty()) {
                        System.out.println("Found existing charts!");
                        chartList = new ArrayList();
                        for (ChartDetails cd : client.getChartDetailsList()) {
                            chartList.add(cd);
                        }
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                        if (selectedChart.getChartCode().equals(simpleDateFormat.format(new Date()))) {
                            System.out.println("Initiate redirection");
                            JsfUtil.moveTo("/webapp/pages/chart.xhtml?viewId=" + client.getId());
                        }
//                        for (ChartDetails cd : client.getChartDetailsList()) {
//                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
//                            String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
//                            String currentDateString = chartCode;
//                            System.out.println(recordDateString + " --> " + currentDateString);
//                            if (recordDateString.equals(currentDateString)) {
//                                System.out.println("Initiate redirection");
//                                JsfUtil.moveTo("/webapp/pages/chart.xhtml?viewId="+client.getId());
//                                break;
//                            }else{
//                                JsfUtil.moveTo("/webapp/pages/chartt.xhtml?viewId="+selectedChart.getId());
//                                break;
//                            }
//                        }
                    }
                }

                medcertmessage = "This is to certify that " + client.getLastname() + ", " + client.getFirstname() + " from " + (client.getStreetaddress().trim() + " ") + (client.getBrgy() + " ")
                        + (client.getTown() + " ") + client.getProvince() + " was seen and examined by the undersigned for the following: ";

                diagnosisList = new ArrayList<>();
                diagnosisNote = selectedChart.getDiagnosis();
                List<ChartDetails> chartDetailDiagnosis = chartDetailController.findChartDetailsEntities();
                for (ChartDetails chartDiagnosis : chartDetailDiagnosis) {
                    if (!chartDiagnosis.getDiagnosis().trim().isEmpty()) {
                        diagnosisList.add(chartDiagnosis.getDiagnosis().toUpperCase());
                    }
                }
                DiagnosticreferenceJpaController drController = new DiagnosticreferenceJpaController(utx, factory);
                List<Diagnosticreference> referenceList = drController.findDiagnosticreferenceEntities();
                for (Diagnosticreference dRef : referenceList) {
                    diagnosisList.add(dRef.getDiseasename().toUpperCase());
                }
                //reload meds begin
                if (!selectedChart.getGeneric1().trim().isEmpty()) {
                    //drug1 = getMedicineObj(selectedChart.getGeneric1());
                    generic1 = selectedChart.getGeneric1();
                    brand1 = selectedChart.getBrand1();
                    preparation1 = selectedChart.getPreparation1().toLowerCase();
                    direction1 = selectedChart.getDirection1();
                    quantity1 = selectedChart.getQuantity1();
                }
                if (!selectedChart.getGeneric2().trim().isEmpty()) {
                    //drug2 = getMedicineObj(selectedChart.getGeneric2());
                    generic2 = selectedChart.getGeneric2();
                    brand2 = selectedChart.getBrand2();
                    preparation2 = selectedChart.getPreparation2().toLowerCase();
                    direction2 = selectedChart.getDirection2();
                    quantity2 = selectedChart.getQuantity2();
                }
                if (!selectedChart.getGeneric3().trim().isEmpty()) {
                    //drug3 = getMedicineObj(selectedChart.getGeneric3());
                    generic3 = selectedChart.getGeneric3();
                    brand3 = selectedChart.getBrand3();
                    preparation3 = selectedChart.getPreparation3().toLowerCase();
                    direction3 = selectedChart.getDirection3();
                    quantity3 = selectedChart.getQuantity3();
                }
                if (!selectedChart.getGeneric4().trim().isEmpty()) {
                    //drug4 = getMedicineObj(selectedChart.getGeneric4());
                    generic4 = selectedChart.getGeneric5();
                    brand4 = selectedChart.getBrand4();
                    preparation4 = selectedChart.getPreparation4().toLowerCase();
                    direction4 = selectedChart.getDirection4();
                    quantity4 = selectedChart.getQuantity4();
                }
                if (!selectedChart.getGeneric5().trim().isEmpty()) {
                    //drug5 = getMedicineObj(selectedChart.getGeneric5());
                    generic5 = selectedChart.getGeneric5();
                    brand5 = selectedChart.getBrand5();
                    preparation5 = selectedChart.getPreparation5().toLowerCase();
                    direction5 = selectedChart.getDirection5();
                    quantity5 = selectedChart.getQuantity5();
                }
                if (!selectedChart.getGeneric6().trim().isEmpty()) {
                    //drug6 = getMedicineObj(selectedChart.getGeneric6());
                    generic6 = selectedChart.getGeneric6();
                    brand6 = selectedChart.getBrand6();
                    preparation6 = selectedChart.getPreparation6().toLowerCase();
                    direction6 = selectedChart.getDirection6();
                    quantity6 = selectedChart.getQuantity6();
                }

            }
        }
    }

    private Medicineitem getMedicineObj(String generic) {
        MedicineitemJpaController medController = new MedicineitemJpaController(utx, factory);
        for (Medicineitem m : medController.findMedicineitemEntitiesv2()) {
            if (m.getGenericname().trim().toLowerCase().equals(generic.trim().toLowerCase())) {
                return m;
            }
        }
        return null;
    }

    public void onRowSelect() {
        if (selObject != null) {
            setClient(selObject.getClient());
            if (getClient() != null) {
                EntityManager em = factory.createEntityManager();
                setClient(em.merge(client));
                em.refresh(client);
                em.close();

                ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
                if (getClient().getChartDetailsList().isEmpty()) {
                    selectedChart = new ChartDetails();
                    selectedChart.setClient(client);
                    selectedChart.setRecorddatetime(new Date());
                    selectedChart.setFamilyhistory("");
                    selectedChart.setMedicalhistory("");
                    selectedChart.setIllnesshistory("");
                    selectedChart.setHeight("");
                    selectedChart.setWeight("");
                    selectedChart.setBloodpressure("");
                    selectedChart.setPulse("");
                    selectedChart.setReferredby("");
                    selectedChart.setChiefcomplaint("");
                    try {
                        chartDetailController.create(selectedChart);
                    } catch (Exception ex) {
                        Logger.getLogger(ChartDetailController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    for (ChartDetails cd : getClient().getChartDetailsList()) {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
                        String recordDateString = simpleDateFormat.format(cd.getRecorddatetime());
                        String currentDateString = simpleDateFormat.format(new Date());
                        if (recordDateString.equals(currentDateString)) {
                            selectedChart = cd;
                            break;
                        }

                    }
                }
            }
        }
    }

    public List<Medicineitem> completeDrug(String q) {
        List<Medicineitem> suggestions = new ArrayList<Medicineitem>();
        EntityManager em = factory.createEntityManager();
        //Query query = em.createQuery("SELECT m from Medicineitem m WHERE m.genericname ORDER BY m.genericname").setMaxResults(5);
        Query query = em.createQuery("SELECT m from Medicineitem m WHERE m.genericname LIKE '" + q.trim().toLowerCase() + "%' ORDER BY m.genericname").setMaxResults(5);
        for (Object c : query.getResultList()) {
            suggestions.add(((Medicineitem) c));
        }
        em.close();
        return suggestions;
    }

    public List<Clinicservices> completeService(String q) {
        List<Clinicservices> suggestions = new ArrayList<Clinicservices>();
        EntityManager em = factory.createEntityManager();
        //Query query = em.createQuery("SELECT m from Clinicservices m WHERE m.servicename ORDER BY m.genericname").setMaxResults(5);
        Query query = em.createQuery("SELECT m from Clinicservices m WHERE m.servicename LIKE '" + q.trim().toLowerCase() + "%' ORDER BY m.servicename;").setMaxResults(5);
        for (Object c : query.getResultList()) {
            suggestions.add(((Clinicservices) c));
        }
        em.close();
        return suggestions;
    }

    public void handleSelectDrug1(SelectEvent event) {
        drug1 = (Medicineitem) event.getObject();
        generic1 = drug1.getGenericname();
        brand1 = drug1.getBrandname();
        preparation1 = drug1.getPreparation() + " " + drug1.getMtype();
    }

    public void handleSelectDrug2(SelectEvent event) {
        drug2 = (Medicineitem) event.getObject();
        generic2 = drug2.getGenericname();
        brand2 = drug2.getBrandname();
        preparation2 = drug2.getPreparation() + " " + drug2.getMtype();
    }

    public void handleSelectDrug3(SelectEvent event) {
        drug3 = (Medicineitem) event.getObject();
        generic3 = drug3.getGenericname();
        brand3 = drug3.getBrandname();
        preparation3 = drug3.getPreparation() + " " + drug3.getMtype();
    }

    public void handleSelectMDrug1(SelectEvent event) {
        mdrug1 = (Medicineitem) event.getObject();
        mgeneric1 = mdrug1.getGenericname();
        mbrand1 = mdrug1.getBrandname();
        mpreparation1 = mdrug1.getPreparation() + " " + mdrug1.getMtype();
    }

    public void handleSelectMDrug2(SelectEvent event) {
        mdrug2 = (Medicineitem) event.getObject();
        mgeneric2 = mdrug2.getGenericname();
        mbrand2 = mdrug2.getBrandname();
        mpreparation2 = mdrug2.getPreparation() + " " + mdrug2.getMtype();
    }

    public void handleSelectMDrug3(SelectEvent event) {
        mdrug3 = (Medicineitem) event.getObject();
        mgeneric3 = mdrug3.getGenericname();
        mbrand3 = mdrug3.getBrandname();
        mpreparation3 = mdrug3.getPreparation() + " " + mdrug3.getMtype();
    }

    public void updateChart() {
        if (selectedChart != null) {
            ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
            try {
                selectedChart.setDiagnosis(diagnosisNote);
                chartDetailController.edit(selectedChart);
                JsfUtil.addNotifMessage("Chart updated.");

            } catch (NonexistentEntityException ex) {
                Logger.getLogger(CChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(CChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(CChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
            ClientJpaController clientController = new ClientJpaController(utx, factory);
            client.setLastprocedure(selectedChart.getChiefcomplaint().toUpperCase() + "\n" + selectedChart.getDiagnosis().toUpperCase() + "\n" + new SimpleDateFormat("MMM dd, yyyy").format(selectedChart.getRecorddatetime()));
            try {
                clientController.edit(client);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void savePrescriptionDrugs() {
        String pdfFile = "";
        if (selectedChart != null) {
            ChartDetailsJpaController chartDetailController = new ChartDetailsJpaController(utx, factory);
            if (drug1 == null && drug2 == null && drug3 == null) {
                JsfUtil.addErrorMessage("Please specify medicines for prescription.");
                return;
            }

            if (drug1 != null) {
                generic1 = drug1.getGenericname();
                brand1 = drug1.getBrandname();
                preparation1 = drug1.getPreparation() + " " + drug1.getMtype().toLowerCase();

                selectedChart.setGeneric1(generic1);
                selectedChart.setBrand1(brand1);
                selectedChart.setPreparation1(preparation1);
                selectedChart.setQuantity1(quantity1);
                selectedChart.setDirection1(direction1);

            }

            if (drug2 != null) {
                generic2 = drug2.getGenericname();
                brand2 = drug2.getBrandname();
                preparation2 = drug2.getPreparation() + " " + drug2.getMtype().toLowerCase();

                selectedChart.setGeneric2(generic2);
                selectedChart.setBrand2(brand2);
                selectedChart.setPreparation2(preparation2);
                selectedChart.setQuantity2(quantity2);
                selectedChart.setDirection2(direction2);
            }

            if (drug3 != null) {
                generic3 = drug3.getGenericname();
                brand3 = drug3.getBrandname();
                preparation3 = drug3.getPreparation() + " " + drug3.getMtype().toLowerCase();

                selectedChart.setGeneric3(generic3);
                selectedChart.setBrand3(brand3);
                selectedChart.setPreparation3(preparation3);
                selectedChart.setQuantity3(quantity3);
                selectedChart.setDirection3(direction3);
            }
            try {
                chartDetailController.edit(selectedChart);
                JsfUtil.addNotifMessage("Updating chart details...");
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            }

            List<ChartDetails> list = new ArrayList<ChartDetails>();
            list.add(selectedChart);
            try {
                RxDataSourcev2 dataSource = new RxDataSourcev2(list);
                JasperPrint jprint = JasperFillManager.fillReport(Config.reportTemplateDirectory + File.separator + "rx.jasper", new HashMap(), dataSource);
                ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
                File tempfile = new File(servletContext.getRealPath("/webapp") + File.separator + "TEMP");

                if (!tempfile.exists()) {
                    tempfile.mkdir();
                }
                pdfFile = tempfile + File.separator + "Rx" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(new Date()) + ".pdf";
                System.out.println(pdfFile);
                pdfViewPath = File.separator + "TEMP" + File.separator + "Rx" + new SimpleDateFormat("yyyy-MMdd-hhmmss").format(new Date()) + ".pdf";

                File f = new File(pdfFile);
                if (f.exists()) {
                    f.delete();
                }

                if (f.createNewFile()) {
                    JasperExportManager.exportReportToPdfFile(jprint, pdfFile);
                }
            } catch (IOException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (JRException ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(ChartViewController.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(pdfViewPath);
            System.out.println(drug1);
            System.out.println(generic1);
            System.out.println(brand1);
            System.out.println(quantity1);
            System.out.println(preparation1);
            System.out.println(direction1);
            System.out.println("-----------------------------------------------------");
            System.out.println(drug2);
            System.out.println(generic2);
            System.out.println(brand2);
            System.out.println(quantity2);
            System.out.println(preparation2);
            System.out.println(direction2);
            System.out.println("-----------------------------------------------------");
            System.out.println(drug3);
            System.out.println(generic3);
            System.out.println(brand3);
            System.out.println(quantity3);
            System.out.println(preparation3);
            System.out.println(direction3);
            System.out.println("-----------------------------------------------------");
        }
        System.out.println(pdfViewPath);
        System.out.println(pdfFile);
    }

    public void saveMaintenanceDrugs() {
        System.out.println(mdrug1);
        System.out.println(mgeneric1);
        System.out.println(mbrand1);
        System.out.println(mquantity1);
        System.out.println(mpreparation1);
        System.out.println(mdirection1);
        System.out.println("-----------------------------------------------------");
        System.out.println(mdrug2);
        System.out.println(mgeneric2);
        System.out.println(mbrand2);
        System.out.println(mquantity2);
        System.out.println(mpreparation2);
        System.out.println(mdirection2);
        System.out.println("-----------------------------------------------------");
        System.out.println(mdrug3);
        System.out.println(mgeneric3);
        System.out.println(mbrand3);
        System.out.println(mquantity3);
        System.out.println(mpreparation3);
        System.out.println(mdirection3);
        System.out.println("-----------------------------------------------------");
    }

    public void updateDiagnosisList() {
        if (diagnosisNote == null) {
            diagnosisNote = "";
        }
        diagnosisNote = diagnosisNote + diagnosis + "\n";
    }

    public void addRx() {
        isRxVisible = !isRxVisible;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    public void addServiceCharge() {
        BillingDetailJpaController billingController = new BillingDetailJpaController(utx, factory);
        BillingDetail item = new BillingDetail();
        item.setChart(selectedChart);
        item.setCharge(selectedService.getServicename());
        item.setAmount((serviceAmount));
        try {
            billingController.create(item);
            JsfUtil.addNotifMessage("Addedd to billing: " + selectedService.getServicename() + " (" + String.format("%,.2f", serviceAmount) + ")");
            JsfUtil.addSuccessMessage("Billing updated!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the selectedChart
     */
    public ChartDetails getSelectedChart() {
        return selectedChart;
    }

    /**
     * @param selectedChart the selectedChart to set
     */
    public void setSelectedChart(ChartDetails selectedChart) {
        this.selectedChart = selectedChart;
    }

    /**
     * @return the selObject
     */
    public Appointment getSelObject() {
        return selObject;
    }

    /**
     * @param selObject the selObject to set
     */
    public void setSelObject(Appointment selObject) {
        this.selObject = selObject;
    }

    /**
     * @return the chartList
     */
    public List<ChartDetails> getChartList() {
        return chartList;
    }

    /**
     * @param chartList the chartList to set
     */
    public void setChartList(List<ChartDetails> chartList) {
        this.chartList = chartList;
    }

    /**
     * @return the diagnosisList
     */
    public List<String> getDiagnosisList() {
        return diagnosisList;
    }

    /**
     * @param diagnosisList the diagnosisList to set
     */
    public void setDiagnosisList(List<String> diagnosisList) {
        this.diagnosisList = diagnosisList;
    }

    /**
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis the diagnosis to set
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return the diagnosisNote
     */
    public String getDiagnosisNote() {
        return diagnosisNote;
    }

    /**
     * @param diagnosisNote the diagnosisNote to set
     */
    public void setDiagnosisNote(String diagnosisNote) {
        this.diagnosisNote = diagnosisNote;
    }

    /**
     * @return the isRxVisible
     */
    public boolean isIsRxVisible() {
        return isRxVisible;
    }

    /**
     * @param isRxVisible the isRxVisible to set
     */
    public void setIsRxVisible(boolean isRxVisible) {
        this.isRxVisible = isRxVisible;
    }

    /**
     * @return the drug1
     */
    public Medicineitem getDrug1() {
        return drug1;
    }

    /**
     * @param drug1 the drug1 to set
     */
    public void setDrug1(Medicineitem drug1) {
        this.drug1 = drug1;
    }

    /**
     * @return the generic1
     */
    public String getGeneric1() {
        if (generic1 != null) {
            return generic1.toUpperCase();
        }
        return "";
    }

    /**
     * @param generic1 the generic1 to set
     */
    public void setGeneric1(String generic1) {
        this.generic1 = generic1;
    }

    /**
     * @return the brand1
     */
    public String getBrand1() {
        return brand1.toUpperCase();
    }

    /**
     * @param brand1 the brand1 to set
     */
    public void setBrand1(String brand1) {
        this.brand1 = brand1;
    }

    /**
     * @return the preparation1
     */
    public String getPreparation1() {
        return preparation1.toLowerCase();
    }

    /**
     * @param preparation1 the preparation1 to set
     */
    public void setPreparation1(String preparation1) {
        this.preparation1 = preparation1;
    }

    /**
     * @return the drug2
     */
    public Medicineitem getDrug2() {
        return drug2;
    }

    /**
     * @param drug2 the drug2 to set
     */
    public void setDrug2(Medicineitem drug2) {
        this.drug2 = drug2;
    }

    /**
     * @return the generic2
     */
    public String getGeneric2() {
        if (generic2 != null) {
            return generic2.toUpperCase();
        }
        return "";
    }

    /**
     * @param generic2 the generic2 to set
     */
    public void setGeneric2(String generic2) {
        this.generic2 = generic2;
    }

    /**
     * @return the brand2
     */
    public String getBrand2() {
        return brand2.toUpperCase();
    }

    /**
     * @param brand2 the brand2 to set
     */
    public void setBrand2(String brand2) {
        this.brand2 = brand2;
    }

    /**
     * @return the preparation2
     */
    public String getPreparation2() {
        return preparation2.toLowerCase();
    }

    /**
     * @param preparation2 the preparation2 to set
     */
    public void setPreparation2(String preparation2) {
        this.preparation2 = preparation2;
    }

    /**
     * @return the drug3
     */
    public Medicineitem getDrug3() {
        return drug3;
    }

    /**
     * @param drug3 the drug3 to set
     */
    public void setDrug3(Medicineitem drug3) {
        this.drug3 = drug3;
    }

    /**
     * @return the generic3
     */
    public String getGeneric3() {
        return generic3.toUpperCase();
    }

    /**
     * @param generic3 the generic3 to set
     */
    public void setGeneric3(String generic3) {
        this.generic3 = generic3;
    }

    /**
     * @return the brand3
     */
    public String getBrand3() {
        return brand3.toUpperCase();
    }

    /**
     * @param brand3 the brand3 to set
     */
    public void setBrand3(String brand3) {
        this.brand3 = brand3;
    }

    /**
     * @return the preparation3
     */
    public String getPreparation3() {
        return preparation3.toLowerCase();
    }

    /**
     * @param preparation3 the preparation3 to set
     */
    public void setPreparation3(String preparation3) {
        this.preparation3 = preparation3;
    }

    /**
     * @return the quantity1
     */
    public String getQuantity1() {
        return quantity1.toUpperCase();
    }

    /**
     * @param quantity1 the quantity1 to set
     */
    public void setQuantity1(String quantity1) {
        this.quantity1 = quantity1;
    }

    /**
     * @return the quantity2
     */
    public String getQuantity2() {
        return quantity2.toUpperCase();
    }

    /**
     * @param quantity2 the quantity2 to set
     */
    public void setQuantity2(String quantity2) {
        this.quantity2 = quantity2;
    }

    /**
     * @return the quantity3
     */
    public String getQuantity3() {
        return quantity3.toUpperCase();
    }

    /**
     * @param quantity3 the quantity3 to set
     */
    public void setQuantity3(String quantity3) {
        this.quantity3 = quantity3;
    }

    /**
     * @return the direction1
     */
    public String getDirection1() {
        return direction1.toUpperCase();
    }

    /**
     * @param direction1 the direction1 to set
     */
    public void setDirection1(String direction1) {
        this.direction1 = direction1;
    }

    /**
     * @return the direction2
     */
    public String getDirection2() {
        return direction2.toUpperCase();
    }

    /**
     * @param direction2 the direction2 to set
     */
    public void setDirection2(String direction2) {
        this.direction2 = direction2;
    }

    /**
     * @return the direction3
     */
    public String getDirection3() {
        return direction3.toUpperCase();
    }

    /**
     * @param direction3 the direction3 to set
     */
    public void setDirection3(String direction3) {
        this.direction3 = direction3;
    }

    /**
     * @return the mdrug1
     */
    public Medicineitem getMdrug1() {
        return mdrug1;
    }

    /**
     * @param mdrug1 the mdrug1 to set
     */
    public void setMdrug1(Medicineitem mdrug1) {
        this.mdrug1 = mdrug1;
    }

    /**
     * @return the mgeneric1
     */
    public String getMgeneric1() {
        return mgeneric1;
    }

    /**
     * @param mgeneric1 the mgeneric1 to set
     */
    public void setMgeneric1(String mgeneric1) {
        this.mgeneric1 = mgeneric1;
    }

    /**
     * @return the mbrand1
     */
    public String getMbrand1() {
        return mbrand1;
    }

    /**
     * @param mbrand1 the mbrand1 to set
     */
    public void setMbrand1(String mbrand1) {
        this.mbrand1 = mbrand1;
    }

    /**
     * @return the mpreparation1
     */
    public String getMpreparation1() {
        return mpreparation1;
    }

    /**
     * @param mpreparation1 the mpreparation1 to set
     */
    public void setMpreparation1(String mpreparation1) {
        this.mpreparation1 = mpreparation1;
    }

    /**
     * @return the mdrug2
     */
    public Medicineitem getMdrug2() {
        return mdrug2;
    }

    /**
     * @param mdrug2 the mdrug2 to set
     */
    public void setMdrug2(Medicineitem mdrug2) {
        this.mdrug2 = mdrug2;
    }

    /**
     * @return the mgeneric2
     */
    public String getMgeneric2() {
        return mgeneric2;
    }

    /**
     * @param mgeneric2 the mgeneric2 to set
     */
    public void setMgeneric2(String mgeneric2) {
        this.mgeneric2 = mgeneric2;
    }

    /**
     * @return the mbrand2
     */
    public String getMbrand2() {
        return mbrand2;
    }

    /**
     * @param mbrand2 the mbrand2 to set
     */
    public void setMbrand2(String mbrand2) {
        this.mbrand2 = mbrand2;
    }

    /**
     * @return the mpreparation2
     */
    public String getMpreparation2() {
        return mpreparation2;
    }

    /**
     * @param mpreparation2 the mpreparation2 to set
     */
    public void setMpreparation2(String mpreparation2) {
        this.mpreparation2 = mpreparation2;
    }

    /**
     * @return the mdrug3
     */
    public Medicineitem getMdrug3() {
        return mdrug3;
    }

    /**
     * @param mdrug3 the mdrug3 to set
     */
    public void setMdrug3(Medicineitem mdrug3) {
        this.mdrug3 = mdrug3;
    }

    /**
     * @return the mgeneric3
     */
    public String getMgeneric3() {
        return mgeneric3;
    }

    /**
     * @param mgeneric3 the mgeneric3 to set
     */
    public void setMgeneric3(String mgeneric3) {
        this.mgeneric3 = mgeneric3;
    }

    /**
     * @return the mbrand3
     */
    public String getMbrand3() {
        return mbrand3;
    }

    /**
     * @param mbrand3 the mbrand3 to set
     */
    public void setMbrand3(String mbrand3) {
        this.mbrand3 = mbrand3;
    }

    /**
     * @return the mpreparation3
     */
    public String getMpreparation3() {
        return mpreparation3;
    }

    /**
     * @param mpreparation3 the mpreparation3 to set
     */
    public void setMpreparation3(String mpreparation3) {
        this.mpreparation3 = mpreparation3;
    }

    /**
     * @return the mquantity1
     */
    public String getMquantity1() {
        return mquantity1;
    }

    /**
     * @param mquantity1 the mquantity1 to set
     */
    public void setMquantity1(String mquantity1) {
        this.mquantity1 = mquantity1;
    }

    /**
     * @return the mquantity2
     */
    public String getMquantity2() {
        return mquantity2;
    }

    /**
     * @param mquantity2 the mquantity2 to set
     */
    public void setMquantity2(String mquantity2) {
        this.mquantity2 = mquantity2;
    }

    /**
     * @return the mquantity3
     */
    public String getMquantity3() {
        return mquantity3;
    }

    /**
     * @param mquantity3 the mquantity3 to set
     */
    public void setMquantity3(String mquantity3) {
        this.mquantity3 = mquantity3;
    }

    /**
     * @return the mdirection1
     */
    public String getMdirection1() {
        return mdirection1;
    }

    /**
     * @param mdirection1 the mdirection1 to set
     */
    public void setMdirection1(String mdirection1) {
        this.mdirection1 = mdirection1;
    }

    /**
     * @return the mdirection2
     */
    public String getMdirection2() {
        return mdirection2;
    }

    /**
     * @param mdirection2 the mdirection2 to set
     */
    public void setMdirection2(String mdirection2) {
        this.mdirection2 = mdirection2;
    }

    /**
     * @return the mdirection3
     */
    public String getMdirection3() {
        return mdirection3;
    }

    /**
     * @param mdirection3 the mdirection3 to set
     */
    public void setMdirection3(String mdirection3) {
        this.mdirection3 = mdirection3;
    }

    /**
     * @return the medcertmessage
     */
    public String getMedcertmessage() {
        return medcertmessage;
    }

    /**
     * @param medcertmessage the medcertmessage to set
     */
    public void setMedcertmessage(String medcertmessage) {
        this.medcertmessage = medcertmessage;
    }

    /**
     * @return the isCertVisible
     */
    public boolean isIsCertVisible() {
        return isCertVisible;
    }

    /**
     * @param isCertVisible the isCertVisible to set
     */
    public void setIsCertVisible(boolean isCertVisible) {
        this.isCertVisible = isCertVisible;
    }

    /**
     * @return the billingList
     */
    public List<BillingDetail> getBillingList() {
        return billingList;
    }

    /**
     * @param billingList the billingList to set
     */
    public void setBillingList(List<BillingDetail> billingList) {
        this.billingList = billingList;
    }

    /**
     * @return the billItem
     */
    public BillingDetail getBillItem() {
        return billItem;
    }

    /**
     * @param billItem the billItem to set
     */
    public void setBillItem(BillingDetail billItem) {
        this.billItem = billItem;
    }

    /**
     * @return the serviceDescription
     */
    public String getServiceDescription() {
        return serviceDescription;
    }

    /**
     * @param serviceDescription the serviceDescription to set
     */
    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }

    /**
     * @return the serviceAmount
     */
    public double getServiceAmount() {
        return serviceAmount;
    }

    /**
     * @param serviceAmount the serviceAmount to set
     */
    public void setServiceAmount(double serviceAmount) {
        this.serviceAmount = serviceAmount;
    }

    /**
     * @return the selectedService
     */
    public Clinicservices getSelectedService() {
        return selectedService;
    }

    /**
     * @param selectedService the selectedService to set
     */
    public void setSelectedService(Clinicservices selectedService) {
        this.selectedService = selectedService;
    }

    /**
     * @return the pdfViewPath
     */
    public String getPdfViewPath() {
        return pdfViewPath;
    }

    /**
     * @param pdfViewPath the pdfViewPath to set
     */
    public void setPdfViewPath(String pdfViewPath) {
        this.pdfViewPath = pdfViewPath;
    }

    /**
     * @return the drug4
     */
    public Medicineitem getDrug4() {
        return drug4;
    }

    /**
     * @param drug4 the drug4 to set
     */
    public void setDrug4(Medicineitem drug4) {
        this.drug4 = drug4;
    }

    /**
     * @return the generic4
     */
    public String getGeneric4() {
        return generic4.toUpperCase();
    }

    /**
     * @param generic4 the generic4 to set
     */
    public void setGeneric4(String generic4) {
        this.generic4 = generic4;
    }

    /**
     * @return the brand4
     */
    public String getBrand4() {
        return brand4.toUpperCase();
    }

    /**
     * @param brand4 the brand4 to set
     */
    public void setBrand4(String brand4) {
        this.brand4 = brand4;
    }

    /**
     * @return the preparation4
     */
    public String getPreparation4() {
        return preparation4.toLowerCase();
    }

    /**
     * @param preparation4 the preparation4 to set
     */
    public void setPreparation4(String preparation4) {
        this.preparation4 = preparation4;
    }

    /**
     * @return the drug5
     */
    public Medicineitem getDrug5() {
        return drug5;
    }

    /**
     * @param drug5 the drug5 to set
     */
    public void setDrug5(Medicineitem drug5) {
        this.drug5 = drug5;
    }

    /**
     * @return the generic5
     */
    public String getGeneric5() {
        return generic5.toUpperCase();
    }

    /**
     * @param generic5 the generic5 to set
     */
    public void setGeneric5(String generic5) {
        this.generic5 = generic5;
    }

    /**
     * @return the brand5
     */
    public String getBrand5() {
        return brand5.toUpperCase();
    }

    /**
     * @param brand5 the brand5 to set
     */
    public void setBrand5(String brand5) {
        this.brand5 = brand5;
    }

    /**
     * @return the preparation5
     */
    public String getPreparation5() {
        return preparation5.toLowerCase();
    }

    /**
     * @param preparation5 the preparation5 to set
     */
    public void setPreparation5(String preparation5) {
        this.preparation5 = preparation5;
    }

    /**
     * @return the drug6
     */
    public Medicineitem getDrug6() {
        return drug6;
    }

    /**
     * @param drug6 the drug6 to set
     */
    public void setDrug6(Medicineitem drug6) {
        this.drug6 = drug6;
    }

    /**
     * @return the generic6
     */
    public String getGeneric6() {
        return generic6.toUpperCase();
    }

    /**
     * @param generic6 the generic6 to set
     */
    public void setGeneric6(String generic6) {
        this.generic6 = generic6;
    }

    /**
     * @return the brand6
     */
    public String getBrand6() {
        return brand6.toUpperCase();
    }

    /**
     * @param brand6 the brand6 to set
     */
    public void setBrand6(String brand6) {
        this.brand6 = brand6;
    }

    /**
     * @return the preparation6
     */
    public String getPreparation6() {
        return preparation6.toLowerCase();
    }

    /**
     * @param preparation6 the preparation6 to set
     */
    public void setPreparation6(String preparation6) {
        this.preparation6 = preparation6;
    }

    /**
     * @return the quantity4
     */
    public String getQuantity4() {
        return quantity4.toUpperCase();
    }

    /**
     * @param quantity4 the quantity4 to set
     */
    public void setQuantity4(String quantity4) {
        this.quantity4 = quantity4;
    }

    /**
     * @return the quantity5
     */
    public String getQuantity5() {
        return quantity5.toUpperCase();
    }

    /**
     * @param quantity5 the quantity5 to set
     */
    public void setQuantity5(String quantity5) {
        this.quantity5 = quantity5;
    }

    /**
     * @return the quantity6
     */
    public String getQuantity6() {
        return quantity6.toUpperCase();
    }

    /**
     * @param quantity6 the quantity6 to set
     */
    public void setQuantity6(String quantity6) {
        this.quantity6 = quantity6;
    }

    /**
     * @return the direction4
     */
    public String getDirection4() {
        return direction4.toUpperCase();
    }

    /**
     * @param direction4 the direction4 to set
     */
    public void setDirection4(String direction4) {
        this.direction4 = direction4;
    }

    /**
     * @return the direction5
     */
    public String getDirection5() {
        return direction5.toUpperCase();
    }

    /**
     * @param direction5 the direction5 to set
     */
    public void setDirection5(String direction5) {
        this.direction5 = direction5;
    }

    /**
     * @return the direction6
     */
    public String getDirection6() {
        return direction6.toUpperCase();
    }

    /**
     * @param direction6 the direction6 to set
     */
    public void setDirection6(String direction6) {
        this.direction6 = direction6;
    }

}
