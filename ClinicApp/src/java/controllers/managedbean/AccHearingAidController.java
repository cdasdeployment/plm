/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.HSupplierJpaController;
import controllers.jpa.AccHearingaiditemJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.HSupplier;
import models.AccHearingaiditem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "acchearingAidController")
@ViewScoped
public class AccHearingAidController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String serialnumber = "";
    private String model = "";
    private Double buying = 0.0;
    private Double selling = 0.0;
    private String supplier = "";
    private Integer stocks = 0;
    private Integer reorder = 0;
    private String purchaseDate = "";

    private List<AccHearingaiditem> hearingAidList = new ArrayList<AccHearingaiditem>();
    private List<String> supplierList = new ArrayList<>();
    private String searchkey = "";
    private AccHearingaiditemJpaController controller;
    private HSupplierJpaController hcontroller;
    private AccHearingaiditem thisItem;

    @PostConstruct
    public void init() {
        controller = new AccHearingaiditemJpaController(utx, factory);
        hcontroller = new HSupplierJpaController(utx, factory);
        setHearingAidList(controller.findAccHearingaiditemEntities());
        for (HSupplier hsupplier : hcontroller.findHSupplierEntities()) {
            getSupplierList().add(hsupplier.getSuppliername());
        }

    }

    public List<AccHearingaiditem> search() {
        searchkey = searchkey.toLowerCase();
        hearingAidList.clear();
        controller = new AccHearingaiditemJpaController(utx, factory);
        if (searchkey.trim().isEmpty()) {
            hearingAidList = controller.findAccHearingaiditemEntities();
        } else {
            EntityManager em = factory.createEntityManager();
            for (AccHearingaiditem m : controller.findAccHearingaiditemEntities()) {
                if (m.getModel().toLowerCase().contains(searchkey) || m.getSerialnumber().toLowerCase().contains(searchkey)
                        || m.getSupplier().toLowerCase().contains(searchkey)) {
                    m = em.merge(m);
                    em.refresh(m);
                    hearingAidList.add(m);
                }
            }
            em.close();
        }
        return hearingAidList;
    }

    public void refreshInventoryList() {
        controller = new AccHearingaiditemJpaController(utx, factory);
        setHearingAidList(controller.findAccHearingaiditemEntities());
    }

    public void newInventoryItem() {
        serialnumber = "";
        model = "";
        buying = 0.0;
        selling = 0.0;
        supplier = "";
        stocks = 0;
        reorder = 0;
    }

    public void save() {
        if (supplier == null) {
            JsfUtil.addErrorMessage("Please indicate supplier (Required). Item not saved.");
            return;
        }

        if (supplier.isEmpty()) {
            JsfUtil.addErrorMessage("Please indicate supplier (Required). Item not saved.");
            return;
        }

        if (selling <= 0) {
            JsfUtil.addErrorMessage("Selling price should be greater than zero (Required). Item not saved.");
            return;
        }

        if (stocks <= 0) {
            JsfUtil.addErrorMessage("Quantity in-stock should be greater than zero (Required). Item not saved.");
            return;
        }

        AccHearingaiditem h = new AccHearingaiditem();
        h.setBuying(buying);
        h.setModel(model.toUpperCase());
        h.setReorder((int) (stocks * 0.20));
        h.setSelling(selling);
        h.setSerialnumber(serialnumber.toUpperCase());
        h.setStocks(stocks);
        h.setSupplier(supplier);
        h.setPurchasedate(purchaseDate);

        try {
            controller = new AccHearingaiditemJpaController(utx, factory);
            controller.create(h);
            JsfUtil.addSuccessMessage("New hearing aid accessory saved!");
            refreshInventoryList();
        } catch (Exception ex) {
            Logger.getLogger(AccHearingAidController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void prepareItemInfo() {
        if (getThisItem() != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(getThisItem()));
            em.refresh(getThisItem());
            em.close();
        }
    }

    public void update() {
        controller = new AccHearingaiditemJpaController(utx, factory);
        try {
            thisItem.setModel(thisItem.getModel().toUpperCase());
            thisItem.setSerialnumber(thisItem.getSerialnumber().toUpperCase());
            controller.edit(thisItem);
            JsfUtil.addSuccessMessage("Item updated!");
            refreshInventoryList();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(MedicineController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(MedicineController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the serialnumber
     */
    public String getSerialnumber() {
        return serialnumber;
    }

    /**
     * @param serialnumber the serialnumber to set
     */
    public void setSerialnumber(String serialnumber) {
        this.serialnumber = serialnumber;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @return the buying
     */
    public Double getBuying() {
        return buying;
    }

    /**
     * @param buying the buying to set
     */
    public void setBuying(Double buying) {
        this.buying = buying;
    }

    /**
     * @return the selling
     */
    public Double getSelling() {
        return selling;
    }

    /**
     * @param selling the selling to set
     */
    public void setSelling(Double selling) {
        this.selling = selling;
    }

    /**
     * @return the supplier
     */
    public String getSupplier() {
        return supplier;
    }

    /**
     * @param supplier the supplier to set
     */
    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    /**
     * @return the stocks
     */
    public Integer getStocks() {
        return stocks;
    }

    /**
     * @param stocks the stocks to set
     */
    public void setStocks(Integer stocks) {
        this.stocks = stocks;
    }

    /**
     * @return the reorder
     */
    public Integer getReorder() {
        return reorder;
    }

    /**
     * @param reorder the reorder to set
     */
    public void setReorder(Integer reorder) {
        this.reorder = reorder;
    }

    /**
     * @return the searchkey
     */
    public String getSearchkey() {
        return searchkey;
    }

    /**
     * @param searchkey the searchkey to set
     */
    public void setSearchkey(String searchkey) {
        this.searchkey = searchkey;
    }

    /**
     * @return the hearingAidList
     */
    public List<AccHearingaiditem> getHearingAidList() {
        return hearingAidList;
    }

    /**
     * @param hearingAidList the hearingAidList to set
     */
    public void setHearingAidList(List<AccHearingaiditem> hearingAidList) {
        this.hearingAidList = hearingAidList;
    }

    /**
     * @return the thisItem
     */
    public AccHearingaiditem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(AccHearingaiditem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the supplierList
     */
    public List<String> getSupplierList() {
        return supplierList;
    }

    /**
     * @param supplierList the supplierList to set
     */
    public void setSupplierList(List<String> supplierList) {
        this.supplierList = supplierList;
    }

    /**
     * @return the purchaseDate
     */
    public String getPurchaseDate() {
        return purchaseDate;
    }

    /**
     * @param purchaseDate the purchaseDate to set
     */
    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }
}
