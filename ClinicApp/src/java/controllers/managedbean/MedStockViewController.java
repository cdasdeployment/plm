/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.DruginventoryJpaController;
import controllers.jpa.MedicineitemJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Druginventory;
import models.Medicineitem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "medStockViewController")
@ViewScoped
public class MedStockViewController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private Medicineitem thisItem;

    private DruginventoryJpaController controller;
    private List<Druginventory> inventoryList = new ArrayList<Druginventory>();

    @PostConstruct
    public void init() {
        inventoryList = new ArrayList<Druginventory>();
        controller = new DruginventoryJpaController(utx, factory);
        inventoryList = controller.findDruginventoryEntities();
    }

    public void refreshInventoryList() {
        inventoryList = new ArrayList<Druginventory>();
        controller = new DruginventoryJpaController(utx, factory);
        inventoryList = controller.findDruginventoryEntities();
    }

    public void prepareItemInventory() {
        inventoryList = new ArrayList<Druginventory>();
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(thisItem));
            em.refresh(thisItem);
            em.close();
        }
        inventoryList = thisItem.getDruginventoryCollection();
        
    }

    /**
     * @return the thisItem
     */
    public Medicineitem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(Medicineitem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the inventoryList
     */
    public List<Druginventory> getInventoryList() {
        return inventoryList;
    }

    /**
     * @param inventoryList the inventoryList to set
     */
    public void setInventoryList(List<Druginventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

}
