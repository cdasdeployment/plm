/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.AccHearingaidinventoryJpaController;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.AccHearingaidinventory;
import models.AccHearingaiditem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "acchearingAidStockViewController")
@ViewScoped
public class AccHearingAidStockViewController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private AccHearingaiditem thisItem;

    private AccHearingaidinventoryJpaController controller;
    private List<AccHearingaidinventory> inventoryList = new ArrayList<AccHearingaidinventory>();

    @PostConstruct
    public void init() {
        inventoryList = new ArrayList<AccHearingaidinventory>();
        controller = new AccHearingaidinventoryJpaController(utx, factory);
        inventoryList = controller.findAccHearingaidinventoryEntities();
    }

    public void refreshInventoryList() {
        inventoryList = new ArrayList<AccHearingaidinventory>();
        controller = new AccHearingaidinventoryJpaController(utx, factory);
        inventoryList = controller.findAccHearingaidinventoryEntities();
    }

    public void prepareItemInventory() {
        inventoryList = new ArrayList<AccHearingaidinventory>();
        if (thisItem != null) {
            EntityManager em = factory.createEntityManager();
            setThisItem(em.merge(thisItem));
            em.refresh(thisItem);
            em.close();
        }
        inventoryList = thisItem.getAccHearingaidinventoryList();
        
    }

    /**
     * @return the thisItem
     */
    public AccHearingaiditem getThisItem() {
        return thisItem;
    }

    /**
     * @param thisItem the thisItem to set
     */
    public void setThisItem(AccHearingaiditem thisItem) {
        this.thisItem = thisItem;
    }

    /**
     * @return the inventoryList
     */
    public List<AccHearingaidinventory> getInventoryList() {
        return inventoryList;
    }

    /**
     * @param inventoryList the inventoryList to set
     */
    public void setInventoryList(List<AccHearingaidinventory> inventoryList) {
        this.inventoryList = inventoryList;
    }

}
