/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

/**
 *
 * @author apple
 */
import controllers.jpa.AppointmentJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.Appointment;
import models.Client;

@ManagedBean(name = "appointmentController")
@ViewScoped
public class AppointmentController implements Serializable {

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private AppointmentJpaController controller;
    private Client searchClient;
    private Date appointmentDate = new Date();
    private String procedure;
    private List<Appointment> appointmentList = new ArrayList<>();
    private List<Appointment> appointmentSessionList = new ArrayList<>();
    private List<Appointment> morningList = new ArrayList<>();
    private List<Appointment> afternoonList = new ArrayList<>();
    private String session = "";
    private String indexMessage = "";
    private Boolean isRun = false;
    private String department;
    private Boolean isPriority = false;

    private Appointment thisAppointment;

    //overallList
    private List<Appointment> patientList = new ArrayList<>();

    //main group
    private List<Appointment> hearingList = new ArrayList<>();
    private List<Appointment> cosmeticList = new ArrayList<>();
    private List<Appointment> entList = new ArrayList<>();

    //sub groups
    private List<Appointment> hearingMorning = new ArrayList<>();
    private List<Appointment> hearingAfternoon = new ArrayList<>();

    private List<Appointment> cosmeticMorning = new ArrayList<>();
    private List<Appointment> cosmeticAfternoon = new ArrayList<>();

    private List<Appointment> entMorning = new ArrayList<>();
    private List<Appointment> entAfternoon = new ArrayList<>();

    //for doctor's view
    private List<Appointment> hearingSessionList = new ArrayList<>();
    private List<Appointment> cosmeticSessionList = new ArrayList<>();
    private List<Appointment> entSessionList = new ArrayList<>();

    @PostConstruct
    public void init() {
        controller = new AppointmentJpaController(utx, factory);
        appointmentList = controller.findAppointmentEntities();
        hearingList = new ArrayList<>();
        cosmeticList = new ArrayList<>();
        entList = new ArrayList<>();
        hearingMorning = new ArrayList<>();
        hearingAfternoon = new ArrayList<>();
        cosmeticMorning = new ArrayList<>();
        cosmeticAfternoon = new ArrayList<>();
        entMorning = new ArrayList<>();
        entAfternoon = new ArrayList<>();

        for (Appointment a : appointmentList) {

            if (a.getProcedure().equals("Hearing Test") || a.getProcedure().equals("Regular Check-up (Hearing)")) {
                a.setDepartment("A");
                hearingList.add(a);
            }

            if (a.getProcedure().equals("Cosmetic Procedure") || a.getProcedure().equals("Regular Check-up (Cosmetics)")) {
                a.setDepartment("C");
                cosmeticList.add(a);
            }

            if (a.getProcedure().equals("Regular Check-up (ENT)")) {
                a.setDepartment("E");
                entList.add(a);
            }
        }

        int counter = 0;

        for (Appointment h : hearingList) {
            if (h.getSession().equals("Morning")) {
                counter = counter + 1;
                h.setIndex(counter);
                hearingMorning.add(h);
            }
            counter = 0;
            if (h.getSession().equals("Afternoon")) {
                counter = counter + 1;
                h.setIndex(counter);
                hearingAfternoon.add(h);

            }
        }
        counter = 0;
        for (Appointment c : cosmeticList) {
            if (c.getSession().equals("Morning")) {
                counter = counter + 1;
                c.setIndex(counter);
                cosmeticMorning.add(c);
            }
            counter = 0;
            if (c.getSession().equals("Afternoon")) {
                counter = counter + 1;
                c.setIndex(counter);
                cosmeticAfternoon.add(c);
            }
        }
        counter = 0;
        for (Appointment e : entList) {
            if (e.getSession().equals("Morning")) {
                counter = counter + 1;
                e.setIndex(counter);
                entMorning.add(e);
            }
            counter = 0;
            if (e.getSession().equals("Afternoon")) {
                counter = counter + 1;
                e.setIndex(counter);
                entAfternoon.add(e);
            }
        }
    }

    public void refreshAppointments() {
        controller = new AppointmentJpaController(utx, factory);
        appointmentList = controller.findAppointmentEntities();
        hearingList.clear();
        cosmeticList.clear();
        entList.clear();
        hearingMorning.clear();
        hearingAfternoon.clear();
        cosmeticMorning.clear();
        cosmeticAfternoon.clear();
        entMorning.clear();
        entAfternoon.clear();

        for (Appointment a : appointmentList) {
            if (a.getProcedure().equals("Hearing Test") || a.getProcedure().equals("Regular Check-up (Hearing)")) {
                a.setDepartment("A");
                hearingList.add(a);
            }

            if (a.getProcedure().equals("Cosmetic Procedure") || a.getProcedure().equals("Regular Check-up (Cosmetics)")) {
                a.setDepartment("C");
                cosmeticList.add(a);
            }

            if (a.getProcedure().equals("Regular Check-up (ENT)")) {
                a.setDepartment("E");
                entList.add(a);
            }
        }

        int counter = 0;

        for (Appointment h : hearingList) {
            if (h.getSession().equals("Morning")) {
                counter = counter + 1;
                h.setIndex(counter);
                hearingMorning.add(h);
            }
            counter = 0;
            if (h.getSession().equals("Afternoon")) {
                counter = counter + 1;
                h.setIndex(counter);
                hearingAfternoon.add(h);

            }
        }
        counter = 0;
        for (Appointment c : cosmeticList) {
            if (c.getSession().equals("Morning")) {
                counter = counter + 1;
                c.setIndex(counter);
                cosmeticMorning.add(c);
            }
            counter = 0;
            if (c.getSession().equals("Afternoon")) {
                counter = counter + 1;
                c.setIndex(counter);
                cosmeticAfternoon.add(c);
            }
        }
        counter = 0;
        for (Appointment e : entList) {
            if (e.getSession().equals("Morning")) {
                counter = counter + 1;
                e.setIndex(counter);
                entMorning.add(e);
            }
            counter = 0;
            if (e.getSession().equals("Afternoon")) {
                counter = counter + 1;
                e.setIndex(counter);
                entAfternoon.add(e);
            }
        }
    }

    /**
     * @return the searchClient
     */
    public Client getSearchClient() {
        return searchClient;
    }

    /**
     * @param searchClient the searchClient to set
     */
    public void setSearchClient(Client searchClient) {
        this.searchClient = searchClient;
    }

    /**
     * @return the appointmentDate
     */
    public Date getAppointmentDate() {
        return appointmentDate;
    }

    /**
     * @param appointmentDate the appointmentDate to set
     */
    public void setAppointmentDate(Date appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    /**
     * @return the procedure
     */
    public String getProcedure() {
        return procedure;
    }

    /**
     * @param procedure the procedure to set
     */
    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public void saveAppointment() {
        controller = new AppointmentJpaController(utx, factory);

        if (searchClient == null) {
            JsfUtil.addErrorMessage("A client is required to set an appointment.");
            return;
        }

        if (procedure == null) {
            JsfUtil.addErrorMessage("A transaction type is required to set an appointment.");
            return;
        }

        if (session == null) {
            JsfUtil.addErrorMessage("A session (morning or afternoon) is required to set an appointment.");
            return;
        }

        if (department == null) {
            JsfUtil.addErrorMessage("A department is required to set an appointment.");
            return;
        }

        if (procedure.isEmpty()) {
            JsfUtil.addErrorMessage("A transaction type is required to set an appointment.");
            return;
        }

        if (session.isEmpty()) {
            JsfUtil.addErrorMessage("A session (morning or afternoon) is required to set an appointment.");
            return;
        }

        if (department.isEmpty()) {
            JsfUtil.addErrorMessage("A department is required to set an appointment.");
            return;
        }

        if (searchClient != null) {
            if (searchClient.getBirthday() == null) {
                JsfUtil.addWarningMessage("Please update patient's birthday!");
            }
        }

        for(Appointment aa:controller.findAppointmentEntities()){
            
        }
        
        
        Appointment a = new Appointment();
        a.setClient(searchClient);
        a.setAppointmentdate(appointmentDate);
        a.setProcedure(procedure);
        a.setSession(session);
        a.setDepartment(department);
        a.setIsUpdated(false); //default value
        a.setIsPriority(isPriority);
        try {
            controller.create(a);

            refreshAppointments();
            JsfUtil.addSuccessMessage("New appointment successfully saved!");
            //JsfUtil.refreshPage();
        } catch (RollbackFailureException ex) {
            Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(AppointmentController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void editAppointmentInfo() {
        if (thisAppointment != null) {
            controller.getEntityManager().merge(thisAppointment);
            appointmentList.clear();
            morningList.clear();
            afternoonList.clear();
            appointmentList = controller.findAppointmentEntities();

            refreshAppointments();
        }
    }

    public void prepareAppointmentEntry() {
        searchClient = null;
        procedure = "";
        session = "";
        department = "";
        isPriority = false;
    }

    public void updateAppointment() {
        if (thisAppointment != null) {
            if (thisAppointment.getSession().trim().isEmpty() || thisAppointment.getSession() == null) {
                JsfUtil.addErrorMessage("Please specify appointment schedule.");
                return;
            }
            controller = new AppointmentJpaController(utx, factory);
            try {
                thisAppointment.setIsUpdated(false);
                controller.edit(thisAppointment);
                appointmentList.clear();
                morningList.clear();
                afternoonList.clear();
                appointmentList = controller.findAppointmentEntities();

                refreshAppointments();
                JsfUtil.addSuccessMessage("Appointment successfully updated!");

            } catch (Exception ex) {
                Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, "Something went wrong!");
            }
        }
    }

    public void cancelAppointment() {
        try {
            controller.destroy(thisAppointment.getId());
            appointmentList.clear();
            morningList.clear();
            afternoonList.clear();
            appointmentList = controller.findAppointmentEntities();

            refreshAppointments();

            JsfUtil.addSuccessMessage("Appointment has been cancelled!");
        } catch (Exception ex) {
            Logger.getLogger(ClientController.class.getName()).log(Level.SEVERE, null, ex);
            JsfUtil.addErrorMessage(ex, "Something went wrong!");
        }
    }

    /**
     * @return the thisAppointment
     */
    public Appointment getThisAppointment() {
        return thisAppointment;
    }

    /**
     * @param thisAppointment the thisAppointment to set
     */
    public void setThisAppointment(Appointment thisAppointment) {
        this.thisAppointment = thisAppointment;
    }

    /**
     * @return the appointmentList
     */
    public List<Appointment> getAppointmentList() {
        return appointmentList;
    }

    /**
     * @param appointmentList the appointmentList to set
     */
    public void setAppointmentList(List<Appointment> appointmentList) {
        this.appointmentList = appointmentList;
    }

    /**
     * @return the morningList
     */
    public List<Appointment> getMorningList() {
        return morningList;
    }

    /**
     * @param morningList the morningList to set
     */
    public void setMorningList(List<Appointment> morningList) {
        this.morningList = morningList;
    }

    /**
     * @return the afternoonList
     */
    public List<Appointment> getAfternoonList() {
        return afternoonList;
    }

    /**
     * @param afternoonList the afternoonList to set
     */
    public void setAfternoonList(List<Appointment> afternoonList) {
        this.afternoonList = afternoonList;
    }

    /**
     * @return the session
     */
    public String getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(String session) {
        this.session = session;
    }

    /**
     * @return the appointmentSessionList
     */
    public List<Appointment> getAppointmentSessionList() {
        controller = new AppointmentJpaController(utx, factory);

        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (hour >= 1 && hour <= 11) {
            //JsfUtil.addNotifMessage("Morning Queue: 8:00 AM to 11:00 AM");
            indexMessage = "Morning Queue: 8:00 AM to 11:00 AM";
            appointmentList = controller.findAppointmentEntities();
            appointmentSessionList.clear();
            for (Appointment a : appointmentList) {
                if (a.getSession().equals("Morning")) {
                    appointmentSessionList.add(a);
                }
            }
        }

        if (hour >= 12 && hour <= 23) {
            //JsfUtil.addNotifMessage("Afternoon Queue: 1:00 PM to 5:00 PM");
            indexMessage = "Afternoon Queue: 1:00 PM to 5:00 PM";
            appointmentList = controller.findAppointmentEntities();
            appointmentSessionList.clear();
            for (Appointment a : appointmentList) {
                if (a.getSession().equals("Afternoon")) {
                    appointmentSessionList.add(a);
                }
            }
        }

        return appointmentSessionList;
    }

    public void refreshAppointmentSessionList() {
        controller = new AppointmentJpaController(utx, factory);

        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (hour >= 1 && hour <= 11) {
            //JsfUtil.addNotifMessage("Morning Queue: 8:00 AM to 11:00 AM");
            indexMessage = "Morning Queue: 8:00 AM to 11:00 AM";
            appointmentList = controller.findAppointmentEntities();
            appointmentSessionList.clear();
            for (Appointment a : appointmentList) {
                if (a.getSession().equals("Morning")) {
                    appointmentSessionList.add(a);
                }
            }
            isRun = true;
        } else if (hour >= 12 && hour <= 23) {
            //JsfUtil.addNotifMessage("Afternoon Queue: 1:00 PM to 5:00 PM");
            indexMessage = "Afternoon Queue: 1:00 PM to 5:00 PM";
            appointmentList = controller.findAppointmentEntities();
            appointmentSessionList.clear();
            for (Appointment a : appointmentList) {
                if (a.getSession().equals("Afternoon")) {
                    appointmentSessionList.add(a);
                }
            }
            isRun = true;
        } else {
            isRun = false;
        }
    }

    /**
     * @param appointmentSessionList the appointmentSessionList to set
     */
    public void setAppointmentSessionList(List<Appointment> appointmentSessionList) {
        this.appointmentSessionList = appointmentSessionList;
    }

    /**
     * @return the indexMessage
     */
    public String getIndexMessage() {
        return indexMessage;
    }

    /**
     * @param indexMessage the indexMessage to set
     */
    public void setIndexMessage(String indexMessage) {
        this.indexMessage = indexMessage;
    }

    /**
     * @return the isRun
     */
    public Boolean getIsRun() {
        return isRun;
    }

    /**
     * @return the department
     */
    public String getDepartment() {
        return department;
    }

    /**
     * @param department the department to set
     */
    public void setDepartment(String department) {
        this.department = department;
    }

    /**
     * @return the hearingMorning
     */
    public List<Appointment> getHearingMorning() {
        refreshAppointments();
        return hearingMorning;
    }

    /**
     * @param hearingMorning the hearingMorning to set
     */
    public void setHearingMorning(List<Appointment> hearingMorning) {
        this.hearingMorning = hearingMorning;
    }

    /**
     * @return the hearingAfternoon
     */
    public List<Appointment> getHearingAfternoon() {
        refreshAppointments();
        return hearingAfternoon;
    }

    /**
     * @param hearingAfternoon the hearingAfternoon to set
     */
    public void setHearingAfternoon(List<Appointment> hearingAfternoon) {
        this.hearingAfternoon = hearingAfternoon;
    }

    /**
     * @return the cosmeticMorning
     */
    public List<Appointment> getCosmeticMorning() {
        return cosmeticMorning;
    }

    /**
     * @param cosmeticMorning the cosmeticMorning to set
     */
    public void setCosmeticMorning(List<Appointment> cosmeticMorning) {
        this.cosmeticMorning = cosmeticMorning;
    }

    /**
     * @return the cosmeticAfternoon
     */
    public List<Appointment> getCosmeticAfternoon() {
        refreshAppointments();
        return cosmeticAfternoon;
    }

    /**
     * @param cosmeticAfternoon the cosmeticAfternoon to set
     */
    public void setCosmeticAfternoon(List<Appointment> cosmeticAfternoon) {
        this.cosmeticAfternoon = cosmeticAfternoon;
    }

    /**
     * @return the entMorning
     */
    public List<Appointment> getEntMorning() {
        refreshAppointments();
        return entMorning;
    }

    /**
     * @param entMorning the entMorning to set
     */
    public void setEntMorning(List<Appointment> entMorning) {
        this.entMorning = entMorning;
    }

    /**
     * @return the entAfternoon
     */
    public List<Appointment> getEntAfternoon() {
        refreshAppointments();
        return entAfternoon;
    }

    /**
     * @param entAfternoon the entAfternoon to set
     */
    public void setEntAfternoon(List<Appointment> entAfternoon) {
        this.entAfternoon = entAfternoon;
    }

    /**
     * @return the hearingSessionList
     */
    public List<Appointment> getHearingSessionList() {
        refreshAppointments();
        hearingSessionList = new ArrayList<>();
        int counter = 0;
        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (!hearingList.isEmpty()) {

            for (Appointment h : hearingList) {
                if (hour >= 1 && hour <= 13) {
                    if (h.getSession().equals("Morning")) {
                        if (h.getIsUpdated() == false) {
                            indexMessage = "Morning Queue: 9:00 AM to 1:00 PM";
                            counter = counter + 1;
                            h.setIndex(counter);
                            hearingSessionList.add(h);
                        }
                    }
                }

                if (hour >= 14 && hour <= 23) {
                    if (h.getSession().equals("Afternoon")) {
                        if (h.getIsUpdated() == false) {
                            indexMessage = "Afternoon Queue: 2:00 PM to 5:00 PM";
                            counter = counter + 1;
                            h.setIndex(counter);
                            hearingSessionList.add(h);
                        }

                    }
                }
            }

        }

        return hearingSessionList;
    }

    /**
     * @param hearingSessionList the hearingSessionList to set
     */
    public void setHearingSessionList(List<Appointment> hearingSessionList) {
        this.hearingSessionList = hearingSessionList;
    }

    /**
     * @return the cosmeticSessionList
     */
    public List<Appointment> getCosmeticSessionList() {
        refreshAppointments();
        cosmeticSessionList = new ArrayList<>();
        int counter = 0;
        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (!cosmeticList.isEmpty()) {

            for (Appointment c : cosmeticList) {
                if (hour >= 1 && hour <= 13) {
                    if (c.getSession().equals("Morning")) {
                        if (c.getIsUpdated() == false) {
                            indexMessage = "Morning Queue: 10:00 AM to 1:00 PM";
                            counter = counter + 1;
                            c.setIndex(counter);
                            cosmeticSessionList.add(c);
                        }
                    }
                }
                if (hour >= 14 && hour <= 23) {
                    if (c.getSession().equals("Afternoon")) {
                        if (c.getIsUpdated() == false) {
                            indexMessage = "Afternoon Queue: 2:00 PM to 5:00 PM";
                            counter = counter + 1;
                            c.setIndex(counter);
                            cosmeticSessionList.add(c);
                        }
                    }
                }
            }

        }

        return cosmeticSessionList;
    }

    /**
     * @param cosmeticSessionList the cosmeticSessionList to set
     */
    public void setCosmeticSessionList(List<Appointment> cosmeticSessionList) {
        this.cosmeticSessionList = cosmeticSessionList;
    }

    /**
     * @return the entSessionList
     */
    public List<Appointment> getEntSessionList() {
        refreshAppointments();
        entSessionList = new ArrayList();
        int counter = 0;
        Calendar rightNow = Calendar.getInstance();
        int hour = rightNow.get(Calendar.HOUR_OF_DAY);

        if (!entList.isEmpty()) {
            for (Appointment e : entList) {
                if (hour >= 1 && hour <= 13) {
                    if (e.getSession().equals("Morning")) {
                        if (e.getIsUpdated() == false) {
                            indexMessage = "Morning Queue: 9:00 AM to 1:00 PM";
                            counter = counter + 1;
                            e.setIndex(counter);
                            entSessionList.add(e);
                        }
                    }
                }
                if (hour >= 14 && hour <= 23) {
                    if (e.getIsUpdated() == false) {
                        if (e.getSession().equals("Afternoon")) {
                            indexMessage = "Afternoon Queue: 2:00 PM to 5:00 PM";
                            counter = counter + 1;
                            e.setIndex(counter);
                            entSessionList.add(e);
                        }
                    }
                }
            }

        }
        return entSessionList;
    }

    /**
     * @param entSessionList the entSessionList to set
     */
    public void setEntSessionList(List<Appointment> entSessionList) {
        this.entSessionList = entSessionList;
    }

    /**
     * @return the patientList
     */
    public List<Appointment> getPatientList() {
        patientList = new ArrayList<>();

        patientList.addAll(getHearingSessionList());
        patientList.addAll(getCosmeticSessionList());
        patientList.addAll(getEntSessionList());

        return patientList;
    }

    /**
     * @param patientList the patientList to set
     */
    public void setPatientList(List<Appointment> patientList) {
        this.patientList = patientList;
    }

    /**
     * @return the isPriority
     */
    public Boolean getIsPriority() {
        return isPriority;
    }

    /**
     * @param isPriority the isPriority to set
     */
    public void setIsPriority(Boolean isPriority) {
        this.isPriority = isPriority;
    }
}
