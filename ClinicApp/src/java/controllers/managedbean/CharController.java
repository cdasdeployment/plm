/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import models.ChartDetails;
import models.Client;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jordy
 */
@ManagedBean
@ViewScoped
public class CharController extends AbstractController implements Serializable {

    private Client client;
    private List<ChartDetails> chartDetailsList;
    private ChartDetails selectedChart;

    public CharController() {
        chartDetailsList = new ArrayList<>();
        
        for (int x = 1; x <= 10; x++) {
            ChartDetails cd = new ChartDetails(x);
            chartDetailsList.add(cd);
                    
            selectedChart = cd;   
        }

        selectedChart = chartDetailsList.get(0);
    }
    
    @PostConstruct
    @Override
    public void init() {
        super.init();
        
        if (paramMap.containsKey(PARAM_VIEW_ID)) {
            String id = paramMap.get(PARAM_VIEW_ID);

            if (StringUtils.isNumeric(id)) {
                client = getClientById(Integer.parseInt(id));
            }
        }
    }
    
    public void updateChart() {
        System.out.println("WEW");
        System.out.println(selectedChart);
    }

    public Client getClient() {
        return client;
    }

    public ChartDetails getSelectedChart() {
        return selectedChart;
    }

    public void setSelectedChart(ChartDetails selectedChart) {
        this.selectedChart = selectedChart;
    }
    
    public List<ChartDetails> getChartDetailsList() {
        return chartDetailsList;
    }
    
}
