/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.managedbean;

import controllers.jpa.AccHearingaidinventoryJpaController;
import controllers.jpa.AccHearingaiditemJpaController;
import controllers.jpa.exceptions.RollbackFailureException;
import controllers.util.JsfUtil;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import models.AccHearingaidinventory;
import models.AccHearingaiditem;

/**
 *
 * @author apple
 */
@ManagedBean(name = "acchearingAidInventoryController")
@ViewScoped
public class AccHearingAidInventoryController implements Serializable{

    @PersistenceUnit(unitName = "ClinicAppPU")
    private EntityManagerFactory factory;
    @Resource
    private UserTransaction utx;

    private String remarks = "";
    private String updateType = "";
    private Integer currentStock = 0;
    private Integer quantity = 0;
    private Date servicetimestamp = new Date();
    private Integer pullout = 0;
    private Integer purchase = 0;
    private Integer returned = 0;
    private Integer updated = 0;
    private Double buying = 0.0;
    private Double selling = 0.0;
    private AccHearingaiditem hearingaid;

    private AccHearingaidinventoryJpaController inventoryController;
    private AccHearingaiditemJpaController hcontroller;

    @PostConstruct
    public void init() {
        inventoryController = new AccHearingaidinventoryJpaController(utx, factory);
        hcontroller = new AccHearingaiditemJpaController(utx, factory);
    }

    public void resetInventoryEntry() {
        remarks = "";
        setUpdateType("");
        servicetimestamp = new Date();
        pullout = 0;
        purchase = 0;
        returned = 0;
        updated = 0;
        setQuantity((Integer) 0);
        buying = 0.0;
        selling = 0.0;
        setCurrentStock((Integer) 0);
    }

    public void prepareItemInfo() {
        if (hearingaid != null) {
            resetInventoryEntry();
            EntityManager em = factory.createEntityManager();
            hearingaid = em.merge(hearingaid);
            em.refresh(hearingaid);
            em.close();
        }
    }

    private AccHearingaidinventory hInventory;
    private AccHearingaidinventory hearingAidInventory;
    public void update() {
        //////////////////////////////////////////////////////////////////////////////
        boolean isFound = false;
        
        inventoryController = new AccHearingaidinventoryJpaController(utx, factory);
        List<AccHearingaidinventory> inventoryList = inventoryController.findAccHearingaidinventoryEntities();
        System.out.println(inventoryList);
        for (AccHearingaidinventory h : inventoryList) {
            if (h.getHearingaid().getId().equals(hearingaid.getId())) {
                //ang drug kay naay inventory record, so dili na mag initial stock
                isFound = true;
                break;
            } else {
                isFound = false;
            }
        }

        if (!isFound) {
            try {
                hInventory = new AccHearingaidinventory();
                hInventory.setHearingaid(hearingaid);
                hInventory.setRemarks("Initial stock");
                hInventory.setUpdated(hearingaid.getStocks());
                hInventory.setBuying(hearingaid.getBuying());
                hInventory.setSelling(hearingaid.getSelling());
                hInventory.setPullout(0);
                hInventory.setPurchase(0);
                hInventory.setReturned(0);
                hInventory.setServicetimestamp(new Date());
                inventoryController.create(hInventory);
            } catch (Exception ex) {
                Logger.getLogger(AccHearingAidInventoryController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        /////////////////////////////////////////////////////////////////////////////

        String sysMsg = "";
        pullout = 0;
        purchase = 0;
        returned = 0;
        updated = 0;

        if (updateType.isEmpty()) {
            JsfUtil.addErrorMessage("Please select update type.\nItem update cancelled.");
            return;
        }
        if (updateType == null) {
            JsfUtil.addErrorMessage("Please select update type.\nItem update cancelled.");
            return;
        }
        if (quantity == 0 || quantity < 0) {
            JsfUtil.addErrorMessage("Item quantity cannot be zero.\nItem update cancelled.");
            return;
        }

        if (updateType.equals("Pullout")) {
            if (quantity > hearingaid.getStocks()) {
                JsfUtil.addErrorMessage("Pull-out quantity exceeds quantity in-stock.\nItem update cancelled.");
                return;
            }
        }

        hcontroller = new AccHearingaiditemJpaController(utx, factory);
        inventoryController = new AccHearingaidinventoryJpaController(utx, factory);

       hearingAidInventory = new AccHearingaidinventory();

        if (updateType.equals("Update")) {
            hearingAidInventory.setRemarks("Update stocks");
            hearingAidInventory.setUpdated(quantity);
            updated = quantity;
            sysMsg = "Inventory updated for item " + hearingaid.getModel() + " (" + hearingaid.getSerialnumber() + ")";

        } else if (updateType.equals("Pullout")) {
            hearingAidInventory.setRemarks("Pull-out stocks");
            if (quantity <= hearingaid.getStocks()) {
                hearingAidInventory.setPullout(quantity);
                pullout = quantity;
                sysMsg = "Stocks pull-out for item " + hearingaid.getModel() + " (" + hearingaid.getSerialnumber() + ")";
            }

        } else if (updateType.equals("Return")) {
            hearingAidInventory.setRemarks("Return stocks");
            hearingAidInventory.setReturned(quantity);
            returned = quantity;
            sysMsg = "Stocks returned for item " + hearingaid.getModel() + " (" + hearingaid.getSerialnumber() + ")";

        }

        hearingAidInventory.setHearingaid(hearingaid);
        hearingAidInventory.setUpdated(updated);
        hearingAidInventory.setPullout(pullout);
        hearingAidInventory.setReturned(returned);
        hearingAidInventory.setPurchase(0);//always zero kay wala man mamalit dri
        hearingAidInventory.setServicetimestamp(servicetimestamp);
        hearingAidInventory.setBuying(hearingaid.getBuying());
        hearingAidInventory.setSelling(hearingaid.getSelling());
        try {
            inventoryController.create(hearingAidInventory);
            JsfUtil.addSuccessMessage(sysMsg);
        } catch (Exception ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        }

        hearingaid.setStocks(((hearingaid.getStocks() + returned + updated) - (pullout + purchase)));
        if (updateType.equals("Update")) {
            hearingaid.setReorder((int) (hearingaid.getStocks() * 0.20));
        }
        try {
            hcontroller.edit(hearingaid);
            hInventory.setHearingaid(hearingaid);
            hearingAidInventory.setHearingaid(hearingaid);
            
            inventoryController.edit(hearingAidInventory);
            inventoryController.edit(hInventory);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(DrugInventoryController.class.getName()).log(Level.SEVERE, null, ex);
        }
        JsfUtil.refreshPage();
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    /**
     * @return the servicetimestamp
     */
    public Date getServicetimestamp() {
        return servicetimestamp;
    }

    /**
     * @param servicetimestamp the servicetimestamp to set
     */
    public void setServicetimestamp(Date servicetimestamp) {
        this.servicetimestamp = servicetimestamp;
    }

    /**
     * @return the pullout
     */
    public Integer getPullout() {
        return pullout;
    }

    /**
     * @param pullout the pullout to set
     */
    public void setPullout(Integer pullout) {
        this.pullout = pullout;
    }

    /**
     * @return the purchase
     */
    public Integer getPurchase() {
        return purchase;
    }

    /**
     * @param purchase the purchase to set
     */
    public void setPurchase(Integer purchase) {
        this.purchase = purchase;
    }

    /**
     * @return the returned
     */
    public Integer getReturned() {
        return returned;
    }

    /**
     * @param returned the returned to set
     */
    public void setReturned(Integer returned) {
        this.returned = returned;
    }

    /**
     * @return the updated
     */
    public Integer getUpdated() {
        return updated;
    }

    /**
     * @param updated the updated to set
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    /**
     * @return the buying
     */
    public Double getBuying() {
        return buying;
    }

    /**
     * @param buying the buying to set
     */
    public void setBuying(Double buying) {
        this.buying = buying;
    }

    /**
     * @return the selling
     */
    public Double getSelling() {
        return selling;
    }

    /**
     * @param selling the selling to set
     */
    public void setSelling(Double selling) {
        this.selling = selling;
    }

    /**
     * @return the hearingaid
     */
    public AccHearingaiditem getHearingaid() {
        return hearingaid;
    }

    /**
     * @param hearingaid the hearingaid to set
     */
    public void setHearingaid(AccHearingaiditem hearingaid) {
        this.hearingaid = hearingaid;
    }

    /**
     * @return the updateType
     */
    public String getUpdateType() {
        return updateType;
    }

    /**
     * @param updateType the updateType to set
     */
    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    /**
     * @return the currentStock
     */
    public Integer getCurrentStock() {
        return currentStock;
    }

    /**
     * @param currentStock the currentStock to set
     */
    public void setCurrentStock(Integer currentStock) {
        this.currentStock = currentStock;
    }

    /**
     * @return the quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
