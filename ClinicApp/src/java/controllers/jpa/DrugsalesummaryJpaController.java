/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Client;
import models.Drugsaledetail;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Drugsalesummary;

/**
 *
 * @author apple
 */
public class DrugsalesummaryJpaController implements Serializable {

    public DrugsalesummaryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Drugsalesummary drugsalesummary) throws RollbackFailureException, Exception {
        if (drugsalesummary.getDrugsaledetailList() == null) {
            drugsalesummary.setDrugsaledetailList(new ArrayList<Drugsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = drugsalesummary.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                drugsalesummary.setClient(client);
            }
            List<Drugsaledetail> attachedDrugsaledetailList = new ArrayList<Drugsaledetail>();
            for (Drugsaledetail drugsaledetailListDrugsaledetailToAttach : drugsalesummary.getDrugsaledetailList()) {
                drugsaledetailListDrugsaledetailToAttach = em.getReference(drugsaledetailListDrugsaledetailToAttach.getClass(), drugsaledetailListDrugsaledetailToAttach.getId());
                attachedDrugsaledetailList.add(drugsaledetailListDrugsaledetailToAttach);
            }
            drugsalesummary.setDrugsaledetailList(attachedDrugsaledetailList);
            em.persist(drugsalesummary);
            if (client != null) {
                client.getDrugsalesummaryList().add(drugsalesummary);
                client = em.merge(client);
            }
            for (Drugsaledetail drugsaledetailListDrugsaledetail : drugsalesummary.getDrugsaledetailList()) {
                Drugsalesummary oldSalesummaryOfDrugsaledetailListDrugsaledetail = drugsaledetailListDrugsaledetail.getSalesummary();
                drugsaledetailListDrugsaledetail.setSalesummary(drugsalesummary);
                drugsaledetailListDrugsaledetail = em.merge(drugsaledetailListDrugsaledetail);
                if (oldSalesummaryOfDrugsaledetailListDrugsaledetail != null) {
                    oldSalesummaryOfDrugsaledetailListDrugsaledetail.getDrugsaledetailList().remove(drugsaledetailListDrugsaledetail);
                    oldSalesummaryOfDrugsaledetailListDrugsaledetail = em.merge(oldSalesummaryOfDrugsaledetailListDrugsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Drugsalesummary drugsalesummary) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Drugsalesummary persistentDrugsalesummary = em.find(Drugsalesummary.class, drugsalesummary.getId());
            Client clientOld = persistentDrugsalesummary.getClient();
            Client clientNew = drugsalesummary.getClient();
            List<Drugsaledetail> drugsaledetailListOld = persistentDrugsalesummary.getDrugsaledetailList();
            List<Drugsaledetail> drugsaledetailListNew = drugsalesummary.getDrugsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Drugsaledetail drugsaledetailListOldDrugsaledetail : drugsaledetailListOld) {
                if (!drugsaledetailListNew.contains(drugsaledetailListOldDrugsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Drugsaledetail " + drugsaledetailListOldDrugsaledetail + " since its salesummary field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                drugsalesummary.setClient(clientNew);
            }
            List<Drugsaledetail> attachedDrugsaledetailListNew = new ArrayList<Drugsaledetail>();
            for (Drugsaledetail drugsaledetailListNewDrugsaledetailToAttach : drugsaledetailListNew) {
                drugsaledetailListNewDrugsaledetailToAttach = em.getReference(drugsaledetailListNewDrugsaledetailToAttach.getClass(), drugsaledetailListNewDrugsaledetailToAttach.getId());
                attachedDrugsaledetailListNew.add(drugsaledetailListNewDrugsaledetailToAttach);
            }
            drugsaledetailListNew = attachedDrugsaledetailListNew;
            drugsalesummary.setDrugsaledetailList(drugsaledetailListNew);
            drugsalesummary = em.merge(drugsalesummary);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getDrugsalesummaryList().remove(drugsalesummary);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getDrugsalesummaryList().add(drugsalesummary);
                clientNew = em.merge(clientNew);
            }
            for (Drugsaledetail drugsaledetailListNewDrugsaledetail : drugsaledetailListNew) {
                if (!drugsaledetailListOld.contains(drugsaledetailListNewDrugsaledetail)) {
                    Drugsalesummary oldSalesummaryOfDrugsaledetailListNewDrugsaledetail = drugsaledetailListNewDrugsaledetail.getSalesummary();
                    drugsaledetailListNewDrugsaledetail.setSalesummary(drugsalesummary);
                    drugsaledetailListNewDrugsaledetail = em.merge(drugsaledetailListNewDrugsaledetail);
                    if (oldSalesummaryOfDrugsaledetailListNewDrugsaledetail != null && !oldSalesummaryOfDrugsaledetailListNewDrugsaledetail.equals(drugsalesummary)) {
                        oldSalesummaryOfDrugsaledetailListNewDrugsaledetail.getDrugsaledetailList().remove(drugsaledetailListNewDrugsaledetail);
                        oldSalesummaryOfDrugsaledetailListNewDrugsaledetail = em.merge(oldSalesummaryOfDrugsaledetailListNewDrugsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = drugsalesummary.getId();
                if (findDrugsalesummary(id) == null) {
                    throw new NonexistentEntityException("The drugsalesummary with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Drugsalesummary drugsalesummary;
            try {
                drugsalesummary = em.getReference(Drugsalesummary.class, id);
                drugsalesummary.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The drugsalesummary with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Drugsaledetail> drugsaledetailListOrphanCheck = drugsalesummary.getDrugsaledetailList();
            for (Drugsaledetail drugsaledetailListOrphanCheckDrugsaledetail : drugsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Drugsalesummary (" + drugsalesummary + ") cannot be destroyed since the Drugsaledetail " + drugsaledetailListOrphanCheckDrugsaledetail + " in its drugsaledetailList field has a non-nullable salesummary field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client client = drugsalesummary.getClient();
            if (client != null) {
                client.getDrugsalesummaryList().remove(drugsalesummary);
                client = em.merge(client);
            }
            em.remove(drugsalesummary);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Drugsalesummary> findDrugsalesummaryEntities() {
        return findDrugsalesummaryEntities(true, -1, -1);
    }

    public List<Drugsalesummary> findDrugsalesummaryEntities(int maxResults, int firstResult) {
        return findDrugsalesummaryEntities(false, maxResults, firstResult);
    }

    private List<Drugsalesummary> findDrugsalesummaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Drugsalesummary.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Drugsalesummary findDrugsalesummary(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Drugsalesummary.class, id);
        } finally {
            em.close();
        }
    }

    public int getDrugsalesummaryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Drugsalesummary> rt = cq.from(Drugsalesummary.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
