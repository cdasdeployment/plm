/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Hearingaidsalesummary;
import models.Hearingaiditem;
import models.Hearingaidsaledetail;

/**
 *
 * @author apple
 */
public class HearingaidsaledetailJpaController implements Serializable {

    public HearingaidsaledetailJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hearingaidsaledetail hearingaidsaledetail) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidsalesummary salesummary = hearingaidsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary = em.getReference(salesummary.getClass(), salesummary.getId());
                hearingaidsaledetail.setSalesummary(salesummary);
            }
            Hearingaiditem hearingaid = hearingaidsaledetail.getHearingaid();
            if (hearingaid != null) {
                hearingaid = em.getReference(hearingaid.getClass(), hearingaid.getId());
                hearingaidsaledetail.setHearingaid(hearingaid);
            }
            em.persist(hearingaidsaledetail);
            if (salesummary != null) {
                salesummary.getHearingaidsaledetailList().add(hearingaidsaledetail);
                salesummary = em.merge(salesummary);
            }
            if (hearingaid != null) {
                hearingaid.getHearingaidsaledetailList().add(hearingaidsaledetail);
                hearingaid = em.merge(hearingaid);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hearingaidsaledetail hearingaidsaledetail) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidsaledetail persistentHearingaidsaledetail = em.find(Hearingaidsaledetail.class, hearingaidsaledetail.getId());
            Hearingaidsalesummary salesummaryOld = persistentHearingaidsaledetail.getSalesummary();
            Hearingaidsalesummary salesummaryNew = hearingaidsaledetail.getSalesummary();
            Hearingaiditem hearingaidOld = persistentHearingaidsaledetail.getHearingaid();
            Hearingaiditem hearingaidNew = hearingaidsaledetail.getHearingaid();
            if (salesummaryNew != null) {
                salesummaryNew = em.getReference(salesummaryNew.getClass(), salesummaryNew.getId());
                hearingaidsaledetail.setSalesummary(salesummaryNew);
            }
            if (hearingaidNew != null) {
                hearingaidNew = em.getReference(hearingaidNew.getClass(), hearingaidNew.getId());
                hearingaidsaledetail.setHearingaid(hearingaidNew);
            }
            hearingaidsaledetail = em.merge(hearingaidsaledetail);
            if (salesummaryOld != null && !salesummaryOld.equals(salesummaryNew)) {
                salesummaryOld.getHearingaidsaledetailList().remove(hearingaidsaledetail);
                salesummaryOld = em.merge(salesummaryOld);
            }
            if (salesummaryNew != null && !salesummaryNew.equals(salesummaryOld)) {
                salesummaryNew.getHearingaidsaledetailList().add(hearingaidsaledetail);
                salesummaryNew = em.merge(salesummaryNew);
            }
            if (hearingaidOld != null && !hearingaidOld.equals(hearingaidNew)) {
                hearingaidOld.getHearingaidsaledetailList().remove(hearingaidsaledetail);
                hearingaidOld = em.merge(hearingaidOld);
            }
            if (hearingaidNew != null && !hearingaidNew.equals(hearingaidOld)) {
                hearingaidNew.getHearingaidsaledetailList().add(hearingaidsaledetail);
                hearingaidNew = em.merge(hearingaidNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = hearingaidsaledetail.getId();
                if (findHearingaidsaledetail(id) == null) {
                    throw new NonexistentEntityException("The hearingaidsaledetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidsaledetail hearingaidsaledetail;
            try {
                hearingaidsaledetail = em.getReference(Hearingaidsaledetail.class, id);
                hearingaidsaledetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hearingaidsaledetail with id " + id + " no longer exists.", enfe);
            }
            Hearingaidsalesummary salesummary = hearingaidsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary.getHearingaidsaledetailList().remove(hearingaidsaledetail);
                salesummary = em.merge(salesummary);
            }
            Hearingaiditem hearingaid = hearingaidsaledetail.getHearingaid();
            if (hearingaid != null) {
                hearingaid.getHearingaidsaledetailList().remove(hearingaidsaledetail);
                hearingaid = em.merge(hearingaid);
            }
            em.remove(hearingaidsaledetail);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hearingaidsaledetail> findHearingaidsaledetailEntities() {
        return findHearingaidsaledetailEntities(true, -1, -1);
    }

    public List<Hearingaidsaledetail> findHearingaidsaledetailEntities(int maxResults, int firstResult) {
        return findHearingaidsaledetailEntities(false, maxResults, firstResult);
    }

    private List<Hearingaidsaledetail> findHearingaidsaledetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hearingaidsaledetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hearingaidsaledetail findHearingaidsaledetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hearingaidsaledetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getHearingaidsaledetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hearingaidsaledetail> rt = cq.from(Hearingaidsaledetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
