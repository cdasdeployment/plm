/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Drugsaledetail;
import models.Drugsalesummary;
import models.Medicineitem;

/**
 *
 * @author apple
 */
public class DrugsaledetailJpaController implements Serializable {

    public DrugsaledetailJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Drugsaledetail drugsaledetail) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Drugsalesummary salesummary = drugsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary = em.getReference(salesummary.getClass(), salesummary.getId());
                drugsaledetail.setSalesummary(salesummary);
            }
            Medicineitem drug = drugsaledetail.getDrug();
            if (drug != null) {
                drug = em.getReference(drug.getClass(), drug.getId());
                drugsaledetail.setDrug(drug);
            }
            em.persist(drugsaledetail);
            if (salesummary != null) {
                salesummary.getDrugsaledetailList().add(drugsaledetail);
                salesummary = em.merge(salesummary);
            }
            if (drug != null) {
                drug.getDrugsaledetailList().add(drugsaledetail);
                drug = em.merge(drug);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Drugsaledetail drugsaledetail) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Drugsaledetail persistentDrugsaledetail = em.find(Drugsaledetail.class, drugsaledetail.getId());
            Drugsalesummary salesummaryOld = persistentDrugsaledetail.getSalesummary();
            Drugsalesummary salesummaryNew = drugsaledetail.getSalesummary();
            Medicineitem drugOld = persistentDrugsaledetail.getDrug();
            Medicineitem drugNew = drugsaledetail.getDrug();
            if (salesummaryNew != null) {
                salesummaryNew = em.getReference(salesummaryNew.getClass(), salesummaryNew.getId());
                drugsaledetail.setSalesummary(salesummaryNew);
            }
            if (drugNew != null) {
                drugNew = em.getReference(drugNew.getClass(), drugNew.getId());
                drugsaledetail.setDrug(drugNew);
            }
            drugsaledetail = em.merge(drugsaledetail);
            if (salesummaryOld != null && !salesummaryOld.equals(salesummaryNew)) {
                salesummaryOld.getDrugsaledetailList().remove(drugsaledetail);
                salesummaryOld = em.merge(salesummaryOld);
            }
            if (salesummaryNew != null && !salesummaryNew.equals(salesummaryOld)) {
                salesummaryNew.getDrugsaledetailList().add(drugsaledetail);
                salesummaryNew = em.merge(salesummaryNew);
            }
            if (drugOld != null && !drugOld.equals(drugNew)) {
                drugOld.getDrugsaledetailList().remove(drugsaledetail);
                drugOld = em.merge(drugOld);
            }
            if (drugNew != null && !drugNew.equals(drugOld)) {
                drugNew.getDrugsaledetailList().add(drugsaledetail);
                drugNew = em.merge(drugNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = drugsaledetail.getId();
                if (findDrugsaledetail(id) == null) {
                    throw new NonexistentEntityException("The drugsaledetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Drugsaledetail drugsaledetail;
            try {
                drugsaledetail = em.getReference(Drugsaledetail.class, id);
                drugsaledetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The drugsaledetail with id " + id + " no longer exists.", enfe);
            }
            Drugsalesummary salesummary = drugsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary.getDrugsaledetailList().remove(drugsaledetail);
                salesummary = em.merge(salesummary);
            }
            Medicineitem drug = drugsaledetail.getDrug();
            if (drug != null) {
                drug.getDrugsaledetailList().remove(drugsaledetail);
                drug = em.merge(drug);
            }
            em.remove(drugsaledetail);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Drugsaledetail> findDrugsaledetailEntities() {
        return findDrugsaledetailEntities(true, -1, -1);
    }

    public List<Drugsaledetail> findDrugsaledetailEntities(int maxResults, int firstResult) {
        return findDrugsaledetailEntities(false, maxResults, firstResult);
    }

    private List<Drugsaledetail> findDrugsaledetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Drugsaledetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Drugsaledetail findDrugsaledetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Drugsaledetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getDrugsaledetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Drugsaledetail> rt = cq.from(Drugsaledetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
