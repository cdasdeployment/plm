/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Druginventory;
import models.Medicineitem;

/**
 *
 * @author apple
 */
public class DruginventoryJpaController implements Serializable {

    public DruginventoryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Druginventory druginventory) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Medicineitem drug = druginventory.getDrug();
            if (drug != null) {
                drug = em.getReference(drug.getClass(), drug.getId());
                druginventory.setDrug(drug);
            }
            em.persist(druginventory);
            if (drug != null) {
                drug.getDruginventoryCollection().add(druginventory);
                drug = em.merge(drug);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Druginventory druginventory) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Druginventory persistentDruginventory = em.find(Druginventory.class, druginventory.getId());
            Medicineitem drugOld = persistentDruginventory.getDrug();
            Medicineitem drugNew = druginventory.getDrug();
            if (drugNew != null) {
                drugNew = em.getReference(drugNew.getClass(), drugNew.getId());
                druginventory.setDrug(drugNew);
            }
            druginventory = em.merge(druginventory);
            if (drugOld != null && !drugOld.equals(drugNew)) {
                drugOld.getDruginventoryCollection().remove(druginventory);
                drugOld = em.merge(drugOld);
            }
            if (drugNew != null && !drugNew.equals(drugOld)) {
                drugNew.getDruginventoryCollection().add(druginventory);
                drugNew = em.merge(drugNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = druginventory.getId();
                if (findDruginventory(id) == null) {
                    throw new NonexistentEntityException("The druginventory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Druginventory druginventory;
            try {
                druginventory = em.getReference(Druginventory.class, id);
                druginventory.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The druginventory with id " + id + " no longer exists.", enfe);
            }
            Medicineitem drug = druginventory.getDrug();
            if (drug != null) {
                drug.getDruginventoryCollection().remove(druginventory);
                drug = em.merge(drug);
            }
            em.remove(druginventory);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Druginventory> findDruginventoryEntities() {
        return findDruginventoryEntities(true, -1, -1);
    }

    public List<Druginventory> findDruginventoryEntities(int maxResults, int firstResult) {
        return findDruginventoryEntities(false, maxResults, firstResult);
    }

    private List<Druginventory> findDruginventoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Druginventory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Druginventory findDruginventory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Druginventory.class, id);
        } finally {
            em.close();
        }
    }

    public int getDruginventoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Druginventory> rt = cq.from(Druginventory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
