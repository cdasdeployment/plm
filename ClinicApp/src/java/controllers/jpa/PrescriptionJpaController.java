/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.PreexistingEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Client;
import models.Prescription;

/**
 *
 * @author apple
 */
public class PrescriptionJpaController implements Serializable {

    public PrescriptionJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Prescription prescription) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = prescription.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                prescription.setClient(client);
            }
            em.persist(prescription);
            if (client != null) {
                client.getPrescriptionList().add(prescription);
                client = em.merge(client);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findPrescription(prescription.getId()) != null) {
                throw new PreexistingEntityException("Prescription " + prescription + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Prescription prescription) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Prescription persistentPrescription = em.find(Prescription.class, prescription.getId());
            Client clientOld = persistentPrescription.getClient();
            Client clientNew = prescription.getClient();
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                prescription.setClient(clientNew);
            }
            prescription = em.merge(prescription);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getPrescriptionList().remove(prescription);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getPrescriptionList().add(prescription);
                clientNew = em.merge(clientNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = prescription.getId();
                if (findPrescription(id) == null) {
                    throw new NonexistentEntityException("The prescription with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Prescription prescription;
            try {
                prescription = em.getReference(Prescription.class, id);
                prescription.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The prescription with id " + id + " no longer exists.", enfe);
            }
            Client client = prescription.getClient();
            if (client != null) {
                client.getPrescriptionList().remove(prescription);
                client = em.merge(client);
            }
            em.remove(prescription);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Prescription> findPrescriptionEntities() {
        return findPrescriptionEntities(true, -1, -1);
    }

    public List<Prescription> findPrescriptionEntities(int maxResults, int firstResult) {
        return findPrescriptionEntities(false, maxResults, firstResult);
    }

    private List<Prescription> findPrescriptionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Prescription.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Prescription findPrescription(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Prescription.class, id);
        } finally {
            em.close();
        }
    }

    public int getPrescriptionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Prescription> rt = cq.from(Prescription.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
