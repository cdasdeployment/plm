/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Hearingaidinventory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Hearingaiditem;
import models.Hearingaidsaledetail;

/**
 *
 * @author apple
 */
public class HearingaiditemJpaController implements Serializable {

    public HearingaiditemJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hearingaiditem hearingaiditem) throws RollbackFailureException, Exception {
        if (hearingaiditem.getHearingaidinventoryList() == null) {
            hearingaiditem.setHearingaidinventoryList(new ArrayList<Hearingaidinventory>());
        }
        if (hearingaiditem.getHearingaidsaledetailList() == null) {
            hearingaiditem.setHearingaidsaledetailList(new ArrayList<Hearingaidsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Hearingaidinventory> attachedHearingaidinventoryList = new ArrayList<Hearingaidinventory>();
            for (Hearingaidinventory hearingaidinventoryListHearingaidinventoryToAttach : hearingaiditem.getHearingaidinventoryList()) {
                hearingaidinventoryListHearingaidinventoryToAttach = em.getReference(hearingaidinventoryListHearingaidinventoryToAttach.getClass(), hearingaidinventoryListHearingaidinventoryToAttach.getId());
                attachedHearingaidinventoryList.add(hearingaidinventoryListHearingaidinventoryToAttach);
            }
            hearingaiditem.setHearingaidinventoryList(attachedHearingaidinventoryList);
            List<Hearingaidsaledetail> attachedHearingaidsaledetailList = new ArrayList<Hearingaidsaledetail>();
            for (Hearingaidsaledetail hearingaidsaledetailListHearingaidsaledetailToAttach : hearingaiditem.getHearingaidsaledetailList()) {
                hearingaidsaledetailListHearingaidsaledetailToAttach = em.getReference(hearingaidsaledetailListHearingaidsaledetailToAttach.getClass(), hearingaidsaledetailListHearingaidsaledetailToAttach.getId());
                attachedHearingaidsaledetailList.add(hearingaidsaledetailListHearingaidsaledetailToAttach);
            }
            hearingaiditem.setHearingaidsaledetailList(attachedHearingaidsaledetailList);
            em.persist(hearingaiditem);
            for (Hearingaidinventory hearingaidinventoryListHearingaidinventory : hearingaiditem.getHearingaidinventoryList()) {
                Hearingaiditem oldHearingaidOfHearingaidinventoryListHearingaidinventory = hearingaidinventoryListHearingaidinventory.getHearingaid();
                hearingaidinventoryListHearingaidinventory.setHearingaid(hearingaiditem);
                hearingaidinventoryListHearingaidinventory = em.merge(hearingaidinventoryListHearingaidinventory);
                if (oldHearingaidOfHearingaidinventoryListHearingaidinventory != null) {
                    oldHearingaidOfHearingaidinventoryListHearingaidinventory.getHearingaidinventoryList().remove(hearingaidinventoryListHearingaidinventory);
                    oldHearingaidOfHearingaidinventoryListHearingaidinventory = em.merge(oldHearingaidOfHearingaidinventoryListHearingaidinventory);
                }
            }
            for (Hearingaidsaledetail hearingaidsaledetailListHearingaidsaledetail : hearingaiditem.getHearingaidsaledetailList()) {
                Hearingaiditem oldHearingaidOfHearingaidsaledetailListHearingaidsaledetail = hearingaidsaledetailListHearingaidsaledetail.getHearingaid();
                hearingaidsaledetailListHearingaidsaledetail.setHearingaid(hearingaiditem);
                hearingaidsaledetailListHearingaidsaledetail = em.merge(hearingaidsaledetailListHearingaidsaledetail);
                if (oldHearingaidOfHearingaidsaledetailListHearingaidsaledetail != null) {
                    oldHearingaidOfHearingaidsaledetailListHearingaidsaledetail.getHearingaidsaledetailList().remove(hearingaidsaledetailListHearingaidsaledetail);
                    oldHearingaidOfHearingaidsaledetailListHearingaidsaledetail = em.merge(oldHearingaidOfHearingaidsaledetailListHearingaidsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hearingaiditem hearingaiditem) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaiditem persistentHearingaiditem = em.find(Hearingaiditem.class, hearingaiditem.getId());
            List<Hearingaidinventory> hearingaidinventoryListOld = persistentHearingaiditem.getHearingaidinventoryList();
            List<Hearingaidinventory> hearingaidinventoryListNew = hearingaiditem.getHearingaidinventoryList();
            List<Hearingaidsaledetail> hearingaidsaledetailListOld = persistentHearingaiditem.getHearingaidsaledetailList();
            List<Hearingaidsaledetail> hearingaidsaledetailListNew = hearingaiditem.getHearingaidsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Hearingaidsaledetail hearingaidsaledetailListOldHearingaidsaledetail : hearingaidsaledetailListOld) {
                if (!hearingaidsaledetailListNew.contains(hearingaidsaledetailListOldHearingaidsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Hearingaidsaledetail " + hearingaidsaledetailListOldHearingaidsaledetail + " since its hearingaid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Hearingaidinventory> attachedHearingaidinventoryListNew = new ArrayList<Hearingaidinventory>();
            for (Hearingaidinventory hearingaidinventoryListNewHearingaidinventoryToAttach : hearingaidinventoryListNew) {
                hearingaidinventoryListNewHearingaidinventoryToAttach = em.getReference(hearingaidinventoryListNewHearingaidinventoryToAttach.getClass(), hearingaidinventoryListNewHearingaidinventoryToAttach.getId());
                attachedHearingaidinventoryListNew.add(hearingaidinventoryListNewHearingaidinventoryToAttach);
            }
            hearingaidinventoryListNew = attachedHearingaidinventoryListNew;
            hearingaiditem.setHearingaidinventoryList(hearingaidinventoryListNew);
            List<Hearingaidsaledetail> attachedHearingaidsaledetailListNew = new ArrayList<Hearingaidsaledetail>();
            for (Hearingaidsaledetail hearingaidsaledetailListNewHearingaidsaledetailToAttach : hearingaidsaledetailListNew) {
                hearingaidsaledetailListNewHearingaidsaledetailToAttach = em.getReference(hearingaidsaledetailListNewHearingaidsaledetailToAttach.getClass(), hearingaidsaledetailListNewHearingaidsaledetailToAttach.getId());
                attachedHearingaidsaledetailListNew.add(hearingaidsaledetailListNewHearingaidsaledetailToAttach);
            }
            hearingaidsaledetailListNew = attachedHearingaidsaledetailListNew;
            hearingaiditem.setHearingaidsaledetailList(hearingaidsaledetailListNew);
            hearingaiditem = em.merge(hearingaiditem);
            for (Hearingaidinventory hearingaidinventoryListOldHearingaidinventory : hearingaidinventoryListOld) {
                if (!hearingaidinventoryListNew.contains(hearingaidinventoryListOldHearingaidinventory)) {
                    hearingaidinventoryListOldHearingaidinventory.setHearingaid(null);
                    hearingaidinventoryListOldHearingaidinventory = em.merge(hearingaidinventoryListOldHearingaidinventory);
                }
            }
            for (Hearingaidinventory hearingaidinventoryListNewHearingaidinventory : hearingaidinventoryListNew) {
                if (!hearingaidinventoryListOld.contains(hearingaidinventoryListNewHearingaidinventory)) {
                    Hearingaiditem oldHearingaidOfHearingaidinventoryListNewHearingaidinventory = hearingaidinventoryListNewHearingaidinventory.getHearingaid();
                    hearingaidinventoryListNewHearingaidinventory.setHearingaid(hearingaiditem);
                    hearingaidinventoryListNewHearingaidinventory = em.merge(hearingaidinventoryListNewHearingaidinventory);
                    if (oldHearingaidOfHearingaidinventoryListNewHearingaidinventory != null && !oldHearingaidOfHearingaidinventoryListNewHearingaidinventory.equals(hearingaiditem)) {
                        oldHearingaidOfHearingaidinventoryListNewHearingaidinventory.getHearingaidinventoryList().remove(hearingaidinventoryListNewHearingaidinventory);
                        oldHearingaidOfHearingaidinventoryListNewHearingaidinventory = em.merge(oldHearingaidOfHearingaidinventoryListNewHearingaidinventory);
                    }
                }
            }
            for (Hearingaidsaledetail hearingaidsaledetailListNewHearingaidsaledetail : hearingaidsaledetailListNew) {
                if (!hearingaidsaledetailListOld.contains(hearingaidsaledetailListNewHearingaidsaledetail)) {
                    Hearingaiditem oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail = hearingaidsaledetailListNewHearingaidsaledetail.getHearingaid();
                    hearingaidsaledetailListNewHearingaidsaledetail.setHearingaid(hearingaiditem);
                    hearingaidsaledetailListNewHearingaidsaledetail = em.merge(hearingaidsaledetailListNewHearingaidsaledetail);
                    if (oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail != null && !oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail.equals(hearingaiditem)) {
                        oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail.getHearingaidsaledetailList().remove(hearingaidsaledetailListNewHearingaidsaledetail);
                        oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail = em.merge(oldHearingaidOfHearingaidsaledetailListNewHearingaidsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = hearingaiditem.getId();
                if (findHearingaiditem(id) == null) {
                    throw new NonexistentEntityException("The hearingaiditem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaiditem hearingaiditem;
            try {
                hearingaiditem = em.getReference(Hearingaiditem.class, id);
                hearingaiditem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hearingaiditem with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Hearingaidsaledetail> hearingaidsaledetailListOrphanCheck = hearingaiditem.getHearingaidsaledetailList();
            for (Hearingaidsaledetail hearingaidsaledetailListOrphanCheckHearingaidsaledetail : hearingaidsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Hearingaiditem (" + hearingaiditem + ") cannot be destroyed since the Hearingaidsaledetail " + hearingaidsaledetailListOrphanCheckHearingaidsaledetail + " in its hearingaidsaledetailList field has a non-nullable hearingaid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Hearingaidinventory> hearingaidinventoryList = hearingaiditem.getHearingaidinventoryList();
            for (Hearingaidinventory hearingaidinventoryListHearingaidinventory : hearingaidinventoryList) {
                hearingaidinventoryListHearingaidinventory.setHearingaid(null);
                hearingaidinventoryListHearingaidinventory = em.merge(hearingaidinventoryListHearingaidinventory);
            }
            em.remove(hearingaiditem);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hearingaiditem> findHearingaiditemEntities() {
        return findHearingaiditemEntities(true, -1, -1);
    }

    public List<Hearingaiditem> findHearingaiditemEntities(int maxResults, int firstResult) {
        return findHearingaiditemEntities(false, maxResults, firstResult);
    }

    private List<Hearingaiditem> findHearingaiditemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hearingaiditem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hearingaiditem findHearingaiditem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hearingaiditem.class, id);
        } finally {
            em.close();
        }
    }

    public int getHearingaiditemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hearingaiditem> rt = cq.from(Hearingaiditem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
