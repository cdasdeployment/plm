/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.AccHearingaidinventory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.AccHearingaiditem;
import models.Acchearingaidsaledetail;

/**
 *
 * @author apple
 */
public class AccHearingaiditemJpaController implements Serializable {

    public AccHearingaiditemJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AccHearingaiditem accHearingaiditem) throws RollbackFailureException, Exception {
        if (accHearingaiditem.getAccHearingaidinventoryList() == null) {
            accHearingaiditem.setAccHearingaidinventoryList(new ArrayList<AccHearingaidinventory>());
        }
        if (accHearingaiditem.getAcchearingaidsaledetailList() == null) {
            accHearingaiditem.setAcchearingaidsaledetailList(new ArrayList<Acchearingaidsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<AccHearingaidinventory> attachedAccHearingaidinventoryList = new ArrayList<AccHearingaidinventory>();
            for (AccHearingaidinventory accHearingaidinventoryListAccHearingaidinventoryToAttach : accHearingaiditem.getAccHearingaidinventoryList()) {
                accHearingaidinventoryListAccHearingaidinventoryToAttach = em.getReference(accHearingaidinventoryListAccHearingaidinventoryToAttach.getClass(), accHearingaidinventoryListAccHearingaidinventoryToAttach.getId());
                attachedAccHearingaidinventoryList.add(accHearingaidinventoryListAccHearingaidinventoryToAttach);
            }
            accHearingaiditem.setAccHearingaidinventoryList(attachedAccHearingaidinventoryList);
            List<Acchearingaidsaledetail> attachedAcchearingaidsaledetailList = new ArrayList<Acchearingaidsaledetail>();
            for (Acchearingaidsaledetail acchearingaidsaledetailListAcchearingaidsaledetailToAttach : accHearingaiditem.getAcchearingaidsaledetailList()) {
                acchearingaidsaledetailListAcchearingaidsaledetailToAttach = em.getReference(acchearingaidsaledetailListAcchearingaidsaledetailToAttach.getClass(), acchearingaidsaledetailListAcchearingaidsaledetailToAttach.getId());
                attachedAcchearingaidsaledetailList.add(acchearingaidsaledetailListAcchearingaidsaledetailToAttach);
            }
            accHearingaiditem.setAcchearingaidsaledetailList(attachedAcchearingaidsaledetailList);
            em.persist(accHearingaiditem);
            for (AccHearingaidinventory accHearingaidinventoryListAccHearingaidinventory : accHearingaiditem.getAccHearingaidinventoryList()) {
                AccHearingaiditem oldHearingaidOfAccHearingaidinventoryListAccHearingaidinventory = accHearingaidinventoryListAccHearingaidinventory.getHearingaid();
                accHearingaidinventoryListAccHearingaidinventory.setHearingaid(accHearingaiditem);
                accHearingaidinventoryListAccHearingaidinventory = em.merge(accHearingaidinventoryListAccHearingaidinventory);
                if (oldHearingaidOfAccHearingaidinventoryListAccHearingaidinventory != null) {
                    oldHearingaidOfAccHearingaidinventoryListAccHearingaidinventory.getAccHearingaidinventoryList().remove(accHearingaidinventoryListAccHearingaidinventory);
                    oldHearingaidOfAccHearingaidinventoryListAccHearingaidinventory = em.merge(oldHearingaidOfAccHearingaidinventoryListAccHearingaidinventory);
                }
            }
            for (Acchearingaidsaledetail acchearingaidsaledetailListAcchearingaidsaledetail : accHearingaiditem.getAcchearingaidsaledetailList()) {
                AccHearingaiditem oldAcchearingaidOfAcchearingaidsaledetailListAcchearingaidsaledetail = acchearingaidsaledetailListAcchearingaidsaledetail.getAcchearingaid();
                acchearingaidsaledetailListAcchearingaidsaledetail.setAcchearingaid(accHearingaiditem);
                acchearingaidsaledetailListAcchearingaidsaledetail = em.merge(acchearingaidsaledetailListAcchearingaidsaledetail);
                if (oldAcchearingaidOfAcchearingaidsaledetailListAcchearingaidsaledetail != null) {
                    oldAcchearingaidOfAcchearingaidsaledetailListAcchearingaidsaledetail.getAcchearingaidsaledetailList().remove(acchearingaidsaledetailListAcchearingaidsaledetail);
                    oldAcchearingaidOfAcchearingaidsaledetailListAcchearingaidsaledetail = em.merge(oldAcchearingaidOfAcchearingaidsaledetailListAcchearingaidsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AccHearingaiditem accHearingaiditem) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AccHearingaiditem persistentAccHearingaiditem = em.find(AccHearingaiditem.class, accHearingaiditem.getId());
            List<AccHearingaidinventory> accHearingaidinventoryListOld = persistentAccHearingaiditem.getAccHearingaidinventoryList();
            List<AccHearingaidinventory> accHearingaidinventoryListNew = accHearingaiditem.getAccHearingaidinventoryList();
            List<Acchearingaidsaledetail> acchearingaidsaledetailListOld = persistentAccHearingaiditem.getAcchearingaidsaledetailList();
            List<Acchearingaidsaledetail> acchearingaidsaledetailListNew = accHearingaiditem.getAcchearingaidsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Acchearingaidsaledetail acchearingaidsaledetailListOldAcchearingaidsaledetail : acchearingaidsaledetailListOld) {
                if (!acchearingaidsaledetailListNew.contains(acchearingaidsaledetailListOldAcchearingaidsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Acchearingaidsaledetail " + acchearingaidsaledetailListOldAcchearingaidsaledetail + " since its acchearingaid field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<AccHearingaidinventory> attachedAccHearingaidinventoryListNew = new ArrayList<AccHearingaidinventory>();
            for (AccHearingaidinventory accHearingaidinventoryListNewAccHearingaidinventoryToAttach : accHearingaidinventoryListNew) {
                accHearingaidinventoryListNewAccHearingaidinventoryToAttach = em.getReference(accHearingaidinventoryListNewAccHearingaidinventoryToAttach.getClass(), accHearingaidinventoryListNewAccHearingaidinventoryToAttach.getId());
                attachedAccHearingaidinventoryListNew.add(accHearingaidinventoryListNewAccHearingaidinventoryToAttach);
            }
            accHearingaidinventoryListNew = attachedAccHearingaidinventoryListNew;
            accHearingaiditem.setAccHearingaidinventoryList(accHearingaidinventoryListNew);
            List<Acchearingaidsaledetail> attachedAcchearingaidsaledetailListNew = new ArrayList<Acchearingaidsaledetail>();
            for (Acchearingaidsaledetail acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach : acchearingaidsaledetailListNew) {
                acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach = em.getReference(acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach.getClass(), acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach.getId());
                attachedAcchearingaidsaledetailListNew.add(acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach);
            }
            acchearingaidsaledetailListNew = attachedAcchearingaidsaledetailListNew;
            accHearingaiditem.setAcchearingaidsaledetailList(acchearingaidsaledetailListNew);
            accHearingaiditem = em.merge(accHearingaiditem);
            for (AccHearingaidinventory accHearingaidinventoryListOldAccHearingaidinventory : accHearingaidinventoryListOld) {
                if (!accHearingaidinventoryListNew.contains(accHearingaidinventoryListOldAccHearingaidinventory)) {
                    accHearingaidinventoryListOldAccHearingaidinventory.setHearingaid(null);
                    accHearingaidinventoryListOldAccHearingaidinventory = em.merge(accHearingaidinventoryListOldAccHearingaidinventory);
                }
            }
            for (AccHearingaidinventory accHearingaidinventoryListNewAccHearingaidinventory : accHearingaidinventoryListNew) {
                if (!accHearingaidinventoryListOld.contains(accHearingaidinventoryListNewAccHearingaidinventory)) {
                    AccHearingaiditem oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory = accHearingaidinventoryListNewAccHearingaidinventory.getHearingaid();
                    accHearingaidinventoryListNewAccHearingaidinventory.setHearingaid(accHearingaiditem);
                    accHearingaidinventoryListNewAccHearingaidinventory = em.merge(accHearingaidinventoryListNewAccHearingaidinventory);
                    if (oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory != null && !oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory.equals(accHearingaiditem)) {
                        oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory.getAccHearingaidinventoryList().remove(accHearingaidinventoryListNewAccHearingaidinventory);
                        oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory = em.merge(oldHearingaidOfAccHearingaidinventoryListNewAccHearingaidinventory);
                    }
                }
            }
            for (Acchearingaidsaledetail acchearingaidsaledetailListNewAcchearingaidsaledetail : acchearingaidsaledetailListNew) {
                if (!acchearingaidsaledetailListOld.contains(acchearingaidsaledetailListNewAcchearingaidsaledetail)) {
                    AccHearingaiditem oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail = acchearingaidsaledetailListNewAcchearingaidsaledetail.getAcchearingaid();
                    acchearingaidsaledetailListNewAcchearingaidsaledetail.setAcchearingaid(accHearingaiditem);
                    acchearingaidsaledetailListNewAcchearingaidsaledetail = em.merge(acchearingaidsaledetailListNewAcchearingaidsaledetail);
                    if (oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail != null && !oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail.equals(accHearingaiditem)) {
                        oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail.getAcchearingaidsaledetailList().remove(acchearingaidsaledetailListNewAcchearingaidsaledetail);
                        oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail = em.merge(oldAcchearingaidOfAcchearingaidsaledetailListNewAcchearingaidsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = accHearingaiditem.getId();
                if (findAccHearingaiditem(id) == null) {
                    throw new NonexistentEntityException("The accHearingaiditem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AccHearingaiditem accHearingaiditem;
            try {
                accHearingaiditem = em.getReference(AccHearingaiditem.class, id);
                accHearingaiditem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The accHearingaiditem with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Acchearingaidsaledetail> acchearingaidsaledetailListOrphanCheck = accHearingaiditem.getAcchearingaidsaledetailList();
            for (Acchearingaidsaledetail acchearingaidsaledetailListOrphanCheckAcchearingaidsaledetail : acchearingaidsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This AccHearingaiditem (" + accHearingaiditem + ") cannot be destroyed since the Acchearingaidsaledetail " + acchearingaidsaledetailListOrphanCheckAcchearingaidsaledetail + " in its acchearingaidsaledetailList field has a non-nullable acchearingaid field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<AccHearingaidinventory> accHearingaidinventoryList = accHearingaiditem.getAccHearingaidinventoryList();
            for (AccHearingaidinventory accHearingaidinventoryListAccHearingaidinventory : accHearingaidinventoryList) {
                accHearingaidinventoryListAccHearingaidinventory.setHearingaid(null);
                accHearingaidinventoryListAccHearingaidinventory = em.merge(accHearingaidinventoryListAccHearingaidinventory);
            }
            em.remove(accHearingaiditem);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AccHearingaiditem> findAccHearingaiditemEntities() {
        return findAccHearingaiditemEntities(true, -1, -1);
    }

    public List<AccHearingaiditem> findAccHearingaiditemEntities(int maxResults, int firstResult) {
        return findAccHearingaiditemEntities(false, maxResults, firstResult);
    }

    private List<AccHearingaiditem> findAccHearingaiditemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AccHearingaiditem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AccHearingaiditem findAccHearingaiditem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AccHearingaiditem.class, id);
        } finally {
            em.close();
        }
    }

    public int getAccHearingaiditemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AccHearingaiditem> rt = cq.from(AccHearingaiditem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
