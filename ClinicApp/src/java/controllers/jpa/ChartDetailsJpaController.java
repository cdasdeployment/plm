/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Client;
import models.DiagnosisList;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.BillingDetail;
import models.ChartDetails;
import models.ChartRX;
import models.MaintenanceRX;

/**
 *
 * @author apple
 */
public class ChartDetailsJpaController implements Serializable {

    public ChartDetailsJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ChartDetails chartDetails) throws RollbackFailureException, Exception {
        if (chartDetails.getDiagnosisListList() == null) {
            chartDetails.setDiagnosisListList(new ArrayList<DiagnosisList>());
        }
        if (chartDetails.getBillingDetailList() == null) {
            chartDetails.setBillingDetailList(new ArrayList<BillingDetail>());
        }
        if (chartDetails.getChartRXList() == null) {
            chartDetails.setChartRXList(new ArrayList<ChartRX>());
        }
        if (chartDetails.getMaintenanceRXList() == null) {
            chartDetails.setMaintenanceRXList(new ArrayList<MaintenanceRX>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = chartDetails.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                chartDetails.setClient(client);
            }
            List<DiagnosisList> attachedDiagnosisListList = new ArrayList<DiagnosisList>();
            for (DiagnosisList diagnosisListListDiagnosisListToAttach : chartDetails.getDiagnosisListList()) {
                diagnosisListListDiagnosisListToAttach = em.getReference(diagnosisListListDiagnosisListToAttach.getClass(), diagnosisListListDiagnosisListToAttach.getId());
                attachedDiagnosisListList.add(diagnosisListListDiagnosisListToAttach);
            }
            chartDetails.setDiagnosisListList(attachedDiagnosisListList);
            List<BillingDetail> attachedBillingDetailList = new ArrayList<BillingDetail>();
            for (BillingDetail billingDetailListBillingDetailToAttach : chartDetails.getBillingDetailList()) {
                billingDetailListBillingDetailToAttach = em.getReference(billingDetailListBillingDetailToAttach.getClass(), billingDetailListBillingDetailToAttach.getId());
                attachedBillingDetailList.add(billingDetailListBillingDetailToAttach);
            }
            chartDetails.setBillingDetailList(attachedBillingDetailList);
            List<ChartRX> attachedChartRXList = new ArrayList<ChartRX>();
            for (ChartRX chartRXListChartRXToAttach : chartDetails.getChartRXList()) {
                chartRXListChartRXToAttach = em.getReference(chartRXListChartRXToAttach.getClass(), chartRXListChartRXToAttach.getId());
                attachedChartRXList.add(chartRXListChartRXToAttach);
            }
            chartDetails.setChartRXList(attachedChartRXList);
            List<MaintenanceRX> attachedMaintenanceRXList = new ArrayList<MaintenanceRX>();
            for (MaintenanceRX maintenanceRXListMaintenanceRXToAttach : chartDetails.getMaintenanceRXList()) {
                maintenanceRXListMaintenanceRXToAttach = em.getReference(maintenanceRXListMaintenanceRXToAttach.getClass(), maintenanceRXListMaintenanceRXToAttach.getId());
                attachedMaintenanceRXList.add(maintenanceRXListMaintenanceRXToAttach);
            }
            chartDetails.setMaintenanceRXList(attachedMaintenanceRXList);
            em.persist(chartDetails);
            if (client != null) {
                client.getChartDetailsList().add(chartDetails);
                client = em.merge(client);
            }
            for (DiagnosisList diagnosisListListDiagnosisList : chartDetails.getDiagnosisListList()) {
                ChartDetails oldChartOfDiagnosisListListDiagnosisList = diagnosisListListDiagnosisList.getChart();
                diagnosisListListDiagnosisList.setChart(chartDetails);
                diagnosisListListDiagnosisList = em.merge(diagnosisListListDiagnosisList);
                if (oldChartOfDiagnosisListListDiagnosisList != null) {
                    oldChartOfDiagnosisListListDiagnosisList.getDiagnosisListList().remove(diagnosisListListDiagnosisList);
                    oldChartOfDiagnosisListListDiagnosisList = em.merge(oldChartOfDiagnosisListListDiagnosisList);
                }
            }
            for (BillingDetail billingDetailListBillingDetail : chartDetails.getBillingDetailList()) {
                ChartDetails oldChartOfBillingDetailListBillingDetail = billingDetailListBillingDetail.getChart();
                billingDetailListBillingDetail.setChart(chartDetails);
                billingDetailListBillingDetail = em.merge(billingDetailListBillingDetail);
                if (oldChartOfBillingDetailListBillingDetail != null) {
                    oldChartOfBillingDetailListBillingDetail.getBillingDetailList().remove(billingDetailListBillingDetail);
                    oldChartOfBillingDetailListBillingDetail = em.merge(oldChartOfBillingDetailListBillingDetail);
                }
            }
            for (ChartRX chartRXListChartRX : chartDetails.getChartRXList()) {
                ChartDetails oldChartOfChartRXListChartRX = chartRXListChartRX.getChart();
                chartRXListChartRX.setChart(chartDetails);
                chartRXListChartRX = em.merge(chartRXListChartRX);
                if (oldChartOfChartRXListChartRX != null) {
                    oldChartOfChartRXListChartRX.getChartRXList().remove(chartRXListChartRX);
                    oldChartOfChartRXListChartRX = em.merge(oldChartOfChartRXListChartRX);
                }
            }
            for (MaintenanceRX maintenanceRXListMaintenanceRX : chartDetails.getMaintenanceRXList()) {
                ChartDetails oldChartOfMaintenanceRXListMaintenanceRX = maintenanceRXListMaintenanceRX.getChart();
                maintenanceRXListMaintenanceRX.setChart(chartDetails);
                maintenanceRXListMaintenanceRX = em.merge(maintenanceRXListMaintenanceRX);
                if (oldChartOfMaintenanceRXListMaintenanceRX != null) {
                    oldChartOfMaintenanceRXListMaintenanceRX.getMaintenanceRXList().remove(maintenanceRXListMaintenanceRX);
                    oldChartOfMaintenanceRXListMaintenanceRX = em.merge(oldChartOfMaintenanceRXListMaintenanceRX);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ChartDetails chartDetails) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails persistentChartDetails = em.find(ChartDetails.class, chartDetails.getId());
            Client clientOld = persistentChartDetails.getClient();
            Client clientNew = chartDetails.getClient();
            List<DiagnosisList> diagnosisListListOld = persistentChartDetails.getDiagnosisListList();
            List<DiagnosisList> diagnosisListListNew = chartDetails.getDiagnosisListList();
            List<BillingDetail> billingDetailListOld = persistentChartDetails.getBillingDetailList();
            List<BillingDetail> billingDetailListNew = chartDetails.getBillingDetailList();
            List<ChartRX> chartRXListOld = persistentChartDetails.getChartRXList();
            List<ChartRX> chartRXListNew = chartDetails.getChartRXList();
            List<MaintenanceRX> maintenanceRXListOld = persistentChartDetails.getMaintenanceRXList();
            List<MaintenanceRX> maintenanceRXListNew = chartDetails.getMaintenanceRXList();
            List<String> illegalOrphanMessages = null;
            for (DiagnosisList diagnosisListListOldDiagnosisList : diagnosisListListOld) {
                if (!diagnosisListListNew.contains(diagnosisListListOldDiagnosisList)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain DiagnosisList " + diagnosisListListOldDiagnosisList + " since its chart field is not nullable.");
                }
            }
            for (BillingDetail billingDetailListOldBillingDetail : billingDetailListOld) {
                if (!billingDetailListNew.contains(billingDetailListOldBillingDetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain BillingDetail " + billingDetailListOldBillingDetail + " since its chart field is not nullable.");
                }
            }
            for (ChartRX chartRXListOldChartRX : chartRXListOld) {
                if (!chartRXListNew.contains(chartRXListOldChartRX)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ChartRX " + chartRXListOldChartRX + " since its chart field is not nullable.");
                }
            }
            for (MaintenanceRX maintenanceRXListOldMaintenanceRX : maintenanceRXListOld) {
                if (!maintenanceRXListNew.contains(maintenanceRXListOldMaintenanceRX)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain MaintenanceRX " + maintenanceRXListOldMaintenanceRX + " since its chart field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                chartDetails.setClient(clientNew);
            }
            List<DiagnosisList> attachedDiagnosisListListNew = new ArrayList<DiagnosisList>();
            for (DiagnosisList diagnosisListListNewDiagnosisListToAttach : diagnosisListListNew) {
                diagnosisListListNewDiagnosisListToAttach = em.getReference(diagnosisListListNewDiagnosisListToAttach.getClass(), diagnosisListListNewDiagnosisListToAttach.getId());
                attachedDiagnosisListListNew.add(diagnosisListListNewDiagnosisListToAttach);
            }
            diagnosisListListNew = attachedDiagnosisListListNew;
            chartDetails.setDiagnosisListList(diagnosisListListNew);
            List<BillingDetail> attachedBillingDetailListNew = new ArrayList<BillingDetail>();
            for (BillingDetail billingDetailListNewBillingDetailToAttach : billingDetailListNew) {
                billingDetailListNewBillingDetailToAttach = em.getReference(billingDetailListNewBillingDetailToAttach.getClass(), billingDetailListNewBillingDetailToAttach.getId());
                attachedBillingDetailListNew.add(billingDetailListNewBillingDetailToAttach);
            }
            billingDetailListNew = attachedBillingDetailListNew;
            chartDetails.setBillingDetailList(billingDetailListNew);
            List<ChartRX> attachedChartRXListNew = new ArrayList<ChartRX>();
            for (ChartRX chartRXListNewChartRXToAttach : chartRXListNew) {
                chartRXListNewChartRXToAttach = em.getReference(chartRXListNewChartRXToAttach.getClass(), chartRXListNewChartRXToAttach.getId());
                attachedChartRXListNew.add(chartRXListNewChartRXToAttach);
            }
            chartRXListNew = attachedChartRXListNew;
            chartDetails.setChartRXList(chartRXListNew);
            List<MaintenanceRX> attachedMaintenanceRXListNew = new ArrayList<MaintenanceRX>();
            for (MaintenanceRX maintenanceRXListNewMaintenanceRXToAttach : maintenanceRXListNew) {
                maintenanceRXListNewMaintenanceRXToAttach = em.getReference(maintenanceRXListNewMaintenanceRXToAttach.getClass(), maintenanceRXListNewMaintenanceRXToAttach.getId());
                attachedMaintenanceRXListNew.add(maintenanceRXListNewMaintenanceRXToAttach);
            }
            maintenanceRXListNew = attachedMaintenanceRXListNew;
            chartDetails.setMaintenanceRXList(maintenanceRXListNew);
            chartDetails = em.merge(chartDetails);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getChartDetailsList().remove(chartDetails);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getChartDetailsList().add(chartDetails);
                clientNew = em.merge(clientNew);
            }
            for (DiagnosisList diagnosisListListNewDiagnosisList : diagnosisListListNew) {
                if (!diagnosisListListOld.contains(diagnosisListListNewDiagnosisList)) {
                    ChartDetails oldChartOfDiagnosisListListNewDiagnosisList = diagnosisListListNewDiagnosisList.getChart();
                    diagnosisListListNewDiagnosisList.setChart(chartDetails);
                    diagnosisListListNewDiagnosisList = em.merge(diagnosisListListNewDiagnosisList);
                    if (oldChartOfDiagnosisListListNewDiagnosisList != null && !oldChartOfDiagnosisListListNewDiagnosisList.equals(chartDetails)) {
                        oldChartOfDiagnosisListListNewDiagnosisList.getDiagnosisListList().remove(diagnosisListListNewDiagnosisList);
                        oldChartOfDiagnosisListListNewDiagnosisList = em.merge(oldChartOfDiagnosisListListNewDiagnosisList);
                    }
                }
            }
            for (BillingDetail billingDetailListNewBillingDetail : billingDetailListNew) {
                if (!billingDetailListOld.contains(billingDetailListNewBillingDetail)) {
                    ChartDetails oldChartOfBillingDetailListNewBillingDetail = billingDetailListNewBillingDetail.getChart();
                    billingDetailListNewBillingDetail.setChart(chartDetails);
                    billingDetailListNewBillingDetail = em.merge(billingDetailListNewBillingDetail);
                    if (oldChartOfBillingDetailListNewBillingDetail != null && !oldChartOfBillingDetailListNewBillingDetail.equals(chartDetails)) {
                        oldChartOfBillingDetailListNewBillingDetail.getBillingDetailList().remove(billingDetailListNewBillingDetail);
                        oldChartOfBillingDetailListNewBillingDetail = em.merge(oldChartOfBillingDetailListNewBillingDetail);
                    }
                }
            }
            for (ChartRX chartRXListNewChartRX : chartRXListNew) {
                if (!chartRXListOld.contains(chartRXListNewChartRX)) {
                    ChartDetails oldChartOfChartRXListNewChartRX = chartRXListNewChartRX.getChart();
                    chartRXListNewChartRX.setChart(chartDetails);
                    chartRXListNewChartRX = em.merge(chartRXListNewChartRX);
                    if (oldChartOfChartRXListNewChartRX != null && !oldChartOfChartRXListNewChartRX.equals(chartDetails)) {
                        oldChartOfChartRXListNewChartRX.getChartRXList().remove(chartRXListNewChartRX);
                        oldChartOfChartRXListNewChartRX = em.merge(oldChartOfChartRXListNewChartRX);
                    }
                }
            }
            for (MaintenanceRX maintenanceRXListNewMaintenanceRX : maintenanceRXListNew) {
                if (!maintenanceRXListOld.contains(maintenanceRXListNewMaintenanceRX)) {
                    ChartDetails oldChartOfMaintenanceRXListNewMaintenanceRX = maintenanceRXListNewMaintenanceRX.getChart();
                    maintenanceRXListNewMaintenanceRX.setChart(chartDetails);
                    maintenanceRXListNewMaintenanceRX = em.merge(maintenanceRXListNewMaintenanceRX);
                    if (oldChartOfMaintenanceRXListNewMaintenanceRX != null && !oldChartOfMaintenanceRXListNewMaintenanceRX.equals(chartDetails)) {
                        oldChartOfMaintenanceRXListNewMaintenanceRX.getMaintenanceRXList().remove(maintenanceRXListNewMaintenanceRX);
                        oldChartOfMaintenanceRXListNewMaintenanceRX = em.merge(oldChartOfMaintenanceRXListNewMaintenanceRX);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = chartDetails.getId();
                if (findChartDetails(id) == null) {
                    throw new NonexistentEntityException("The chartDetails with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails chartDetails;
            try {
                chartDetails = em.getReference(ChartDetails.class, id);
                chartDetails.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The chartDetails with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<DiagnosisList> diagnosisListListOrphanCheck = chartDetails.getDiagnosisListList();
            for (DiagnosisList diagnosisListListOrphanCheckDiagnosisList : diagnosisListListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ChartDetails (" + chartDetails + ") cannot be destroyed since the DiagnosisList " + diagnosisListListOrphanCheckDiagnosisList + " in its diagnosisListList field has a non-nullable chart field.");
            }
            List<BillingDetail> billingDetailListOrphanCheck = chartDetails.getBillingDetailList();
            for (BillingDetail billingDetailListOrphanCheckBillingDetail : billingDetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ChartDetails (" + chartDetails + ") cannot be destroyed since the BillingDetail " + billingDetailListOrphanCheckBillingDetail + " in its billingDetailList field has a non-nullable chart field.");
            }
            List<ChartRX> chartRXListOrphanCheck = chartDetails.getChartRXList();
            for (ChartRX chartRXListOrphanCheckChartRX : chartRXListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ChartDetails (" + chartDetails + ") cannot be destroyed since the ChartRX " + chartRXListOrphanCheckChartRX + " in its chartRXList field has a non-nullable chart field.");
            }
            List<MaintenanceRX> maintenanceRXListOrphanCheck = chartDetails.getMaintenanceRXList();
            for (MaintenanceRX maintenanceRXListOrphanCheckMaintenanceRX : maintenanceRXListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This ChartDetails (" + chartDetails + ") cannot be destroyed since the MaintenanceRX " + maintenanceRXListOrphanCheckMaintenanceRX + " in its maintenanceRXList field has a non-nullable chart field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client client = chartDetails.getClient();
            if (client != null) {
                client.getChartDetailsList().remove(chartDetails);
                client = em.merge(client);
            }
            em.remove(chartDetails);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ChartDetails> findChartDetailsEntities() {
        return findChartDetailsEntities(true, -1, -1);
    }

    public List<ChartDetails> findChartDetailsEntities(int maxResults, int firstResult) {
        return findChartDetailsEntities(false, maxResults, firstResult);
    }

    private List<ChartDetails> findChartDetailsEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ChartDetails.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ChartDetails findChartDetails(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ChartDetails.class, id);
        } finally {
            em.close();
        }
    }

    public int getChartDetailsCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ChartDetails> rt = cq.from(ChartDetails.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
