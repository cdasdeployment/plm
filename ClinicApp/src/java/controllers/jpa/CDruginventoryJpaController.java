/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.CDruginventory;
import models.CMedicineitem;

/**
 *
 * @author apple
 */
public class CDruginventoryJpaController implements Serializable {

    public CDruginventoryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CDruginventory CDruginventory) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CMedicineitem drug = CDruginventory.getDrug();
            if (drug != null) {
                drug = em.getReference(drug.getClass(), drug.getId());
                CDruginventory.setDrug(drug);
            }
            em.persist(CDruginventory);
            if (drug != null) {
                drug.getCdruginventoryList().add(CDruginventory);
                drug = em.merge(drug);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CDruginventory CDruginventory) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CDruginventory persistentCDruginventory = em.find(CDruginventory.class, CDruginventory.getId());
            CMedicineitem drugOld = persistentCDruginventory.getDrug();
            CMedicineitem drugNew = CDruginventory.getDrug();
            if (drugNew != null) {
                drugNew = em.getReference(drugNew.getClass(), drugNew.getId());
                CDruginventory.setDrug(drugNew);
            }
            CDruginventory = em.merge(CDruginventory);
            if (drugOld != null && !drugOld.equals(drugNew)) {
                drugOld.getCdruginventoryList().remove(CDruginventory);
                drugOld = em.merge(drugOld);
            }
            if (drugNew != null && !drugNew.equals(drugOld)) {
                drugNew.getCdruginventoryList().add(CDruginventory);
                drugNew = em.merge(drugNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = CDruginventory.getId();
                if (findCDruginventory(id) == null) {
                    throw new NonexistentEntityException("The cDruginventory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CDruginventory CDruginventory;
            try {
                CDruginventory = em.getReference(CDruginventory.class, id);
                CDruginventory.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The CDruginventory with id " + id + " no longer exists.", enfe);
            }
            CMedicineitem drug = CDruginventory.getDrug();
            if (drug != null) {
                drug.getCdruginventoryList().remove(CDruginventory);
                drug = em.merge(drug);
            }
            em.remove(CDruginventory);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CDruginventory> findCDruginventoryEntities() {
        return findCDruginventoryEntities(true, -1, -1);
    }

    public List<CDruginventory> findCDruginventoryEntities(int maxResults, int firstResult) {
        return findCDruginventoryEntities(false, maxResults, firstResult);
    }

    private List<CDruginventory> findCDruginventoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CDruginventory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CDruginventory findCDruginventory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CDruginventory.class, id);
        } finally {
            em.close();
        }
    }

    public int getCDruginventoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CDruginventory> rt = cq.from(CDruginventory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
