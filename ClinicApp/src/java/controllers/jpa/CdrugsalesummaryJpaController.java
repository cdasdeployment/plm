/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Client;
import models.Cdrugsaledetail;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Cdrugsalesummary;

/**
 *
 * @author apple
 */
public class CdrugsalesummaryJpaController implements Serializable {

    public CdrugsalesummaryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cdrugsalesummary cdrugsalesummary) throws RollbackFailureException, Exception {
        if (cdrugsalesummary.getCdrugsaledetailList() == null) {
            cdrugsalesummary.setCdrugsaledetailList(new ArrayList<Cdrugsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = cdrugsalesummary.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                cdrugsalesummary.setClient(client);
            }
            List<Cdrugsaledetail> attachedCdrugsaledetailList = new ArrayList<Cdrugsaledetail>();
            for (Cdrugsaledetail cdrugsaledetailListCdrugsaledetailToAttach : cdrugsalesummary.getCdrugsaledetailList()) {
                cdrugsaledetailListCdrugsaledetailToAttach = em.getReference(cdrugsaledetailListCdrugsaledetailToAttach.getClass(), cdrugsaledetailListCdrugsaledetailToAttach.getId());
                attachedCdrugsaledetailList.add(cdrugsaledetailListCdrugsaledetailToAttach);
            }
            cdrugsalesummary.setCdrugsaledetailList(attachedCdrugsaledetailList);
            em.persist(cdrugsalesummary);
            if (client != null) {
                client.getCdrugsalesummaryList().add(cdrugsalesummary);
                client = em.merge(client);
            }
            for (Cdrugsaledetail cdrugsaledetailListCdrugsaledetail : cdrugsalesummary.getCdrugsaledetailList()) {
                Cdrugsalesummary oldSalesummaryOfCdrugsaledetailListCdrugsaledetail = cdrugsaledetailListCdrugsaledetail.getSalesummary();
                cdrugsaledetailListCdrugsaledetail.setSalesummary(cdrugsalesummary);
                cdrugsaledetailListCdrugsaledetail = em.merge(cdrugsaledetailListCdrugsaledetail);
                if (oldSalesummaryOfCdrugsaledetailListCdrugsaledetail != null) {
                    oldSalesummaryOfCdrugsaledetailListCdrugsaledetail.getCdrugsaledetailList().remove(cdrugsaledetailListCdrugsaledetail);
                    oldSalesummaryOfCdrugsaledetailListCdrugsaledetail = em.merge(oldSalesummaryOfCdrugsaledetailListCdrugsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cdrugsalesummary cdrugsalesummary) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cdrugsalesummary persistentCdrugsalesummary = em.find(Cdrugsalesummary.class, cdrugsalesummary.getId());
            Client clientOld = persistentCdrugsalesummary.getClient();
            Client clientNew = cdrugsalesummary.getClient();
            List<Cdrugsaledetail> cdrugsaledetailListOld = persistentCdrugsalesummary.getCdrugsaledetailList();
            List<Cdrugsaledetail> cdrugsaledetailListNew = cdrugsalesummary.getCdrugsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Cdrugsaledetail cdrugsaledetailListOldCdrugsaledetail : cdrugsaledetailListOld) {
                if (!cdrugsaledetailListNew.contains(cdrugsaledetailListOldCdrugsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cdrugsaledetail " + cdrugsaledetailListOldCdrugsaledetail + " since its salesummary field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                cdrugsalesummary.setClient(clientNew);
            }
            List<Cdrugsaledetail> attachedCdrugsaledetailListNew = new ArrayList<Cdrugsaledetail>();
            for (Cdrugsaledetail cdrugsaledetailListNewCdrugsaledetailToAttach : cdrugsaledetailListNew) {
                cdrugsaledetailListNewCdrugsaledetailToAttach = em.getReference(cdrugsaledetailListNewCdrugsaledetailToAttach.getClass(), cdrugsaledetailListNewCdrugsaledetailToAttach.getId());
                attachedCdrugsaledetailListNew.add(cdrugsaledetailListNewCdrugsaledetailToAttach);
            }
            cdrugsaledetailListNew = attachedCdrugsaledetailListNew;
            cdrugsalesummary.setCdrugsaledetailList(cdrugsaledetailListNew);
            cdrugsalesummary = em.merge(cdrugsalesummary);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getCdrugsalesummaryList().remove(cdrugsalesummary);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getCdrugsalesummaryList().add(cdrugsalesummary);
                clientNew = em.merge(clientNew);
            }
            for (Cdrugsaledetail cdrugsaledetailListNewCdrugsaledetail : cdrugsaledetailListNew) {
                if (!cdrugsaledetailListOld.contains(cdrugsaledetailListNewCdrugsaledetail)) {
                    Cdrugsalesummary oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail = cdrugsaledetailListNewCdrugsaledetail.getSalesummary();
                    cdrugsaledetailListNewCdrugsaledetail.setSalesummary(cdrugsalesummary);
                    cdrugsaledetailListNewCdrugsaledetail = em.merge(cdrugsaledetailListNewCdrugsaledetail);
                    if (oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail != null && !oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail.equals(cdrugsalesummary)) {
                        oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail.getCdrugsaledetailList().remove(cdrugsaledetailListNewCdrugsaledetail);
                        oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail = em.merge(oldSalesummaryOfCdrugsaledetailListNewCdrugsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cdrugsalesummary.getId();
                if (findCdrugsalesummary(id) == null) {
                    throw new NonexistentEntityException("The cdrugsalesummary with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cdrugsalesummary cdrugsalesummary;
            try {
                cdrugsalesummary = em.getReference(Cdrugsalesummary.class, id);
                cdrugsalesummary.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cdrugsalesummary with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cdrugsaledetail> cdrugsaledetailListOrphanCheck = cdrugsalesummary.getCdrugsaledetailList();
            for (Cdrugsaledetail cdrugsaledetailListOrphanCheckCdrugsaledetail : cdrugsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Cdrugsalesummary (" + cdrugsalesummary + ") cannot be destroyed since the Cdrugsaledetail " + cdrugsaledetailListOrphanCheckCdrugsaledetail + " in its cdrugsaledetailList field has a non-nullable salesummary field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client client = cdrugsalesummary.getClient();
            if (client != null) {
                client.getCdrugsalesummaryList().remove(cdrugsalesummary);
                client = em.merge(client);
            }
            em.remove(cdrugsalesummary);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cdrugsalesummary> findCdrugsalesummaryEntities() {
        return findCdrugsalesummaryEntities(true, -1, -1);
    }

    public List<Cdrugsalesummary> findCdrugsalesummaryEntities(int maxResults, int firstResult) {
        return findCdrugsalesummaryEntities(false, maxResults, firstResult);
    }

    private List<Cdrugsalesummary> findCdrugsalesummaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cdrugsalesummary.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cdrugsalesummary findCdrugsalesummary(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cdrugsalesummary.class, id);
        } finally {
            em.close();
        }
    }

    public int getCdrugsalesummaryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cdrugsalesummary> rt = cq.from(Cdrugsalesummary.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
