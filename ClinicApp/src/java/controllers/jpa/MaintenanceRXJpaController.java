/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.ChartDetails;
import models.MaintenanceRX;

/**
 *
 * @author apple
 */
public class MaintenanceRXJpaController implements Serializable {

    public MaintenanceRXJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(MaintenanceRX maintenanceRX) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails chart = maintenanceRX.getChart();
            if (chart != null) {
                chart = em.getReference(chart.getClass(), chart.getId());
                maintenanceRX.setChart(chart);
            }
            em.persist(maintenanceRX);
            if (chart != null) {
                chart.getMaintenanceRXList().add(maintenanceRX);
                chart = em.merge(chart);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(MaintenanceRX maintenanceRX) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            MaintenanceRX persistentMaintenanceRX = em.find(MaintenanceRX.class, maintenanceRX.getId());
            ChartDetails chartOld = persistentMaintenanceRX.getChart();
            ChartDetails chartNew = maintenanceRX.getChart();
            if (chartNew != null) {
                chartNew = em.getReference(chartNew.getClass(), chartNew.getId());
                maintenanceRX.setChart(chartNew);
            }
            maintenanceRX = em.merge(maintenanceRX);
            if (chartOld != null && !chartOld.equals(chartNew)) {
                chartOld.getMaintenanceRXList().remove(maintenanceRX);
                chartOld = em.merge(chartOld);
            }
            if (chartNew != null && !chartNew.equals(chartOld)) {
                chartNew.getMaintenanceRXList().add(maintenanceRX);
                chartNew = em.merge(chartNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = maintenanceRX.getId();
                if (findMaintenanceRX(id) == null) {
                    throw new NonexistentEntityException("The maintenanceRX with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            MaintenanceRX maintenanceRX;
            try {
                maintenanceRX = em.getReference(MaintenanceRX.class, id);
                maintenanceRX.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The maintenanceRX with id " + id + " no longer exists.", enfe);
            }
            ChartDetails chart = maintenanceRX.getChart();
            if (chart != null) {
                chart.getMaintenanceRXList().remove(maintenanceRX);
                chart = em.merge(chart);
            }
            em.remove(maintenanceRX);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<MaintenanceRX> findMaintenanceRXEntities() {
        return findMaintenanceRXEntities(true, -1, -1);
    }

    public List<MaintenanceRX> findMaintenanceRXEntities(int maxResults, int firstResult) {
        return findMaintenanceRXEntities(false, maxResults, firstResult);
    }

    private List<MaintenanceRX> findMaintenanceRXEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(MaintenanceRX.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public MaintenanceRX findMaintenanceRX(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MaintenanceRX.class, id);
        } finally {
            em.close();
        }
    }

    public int getMaintenanceRXCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<MaintenanceRX> rt = cq.from(MaintenanceRX.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
