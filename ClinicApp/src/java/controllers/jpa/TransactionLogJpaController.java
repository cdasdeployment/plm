/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.PreexistingEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.TransactionLog;
import models.TransactionLogPK;

/**
 *
 * @author apple
 */
public class TransactionLogJpaController implements Serializable {

    public TransactionLogJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TransactionLog transactionLog) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (transactionLog.getTransactionLogPK() == null) {
            transactionLog.setTransactionLogPK(new TransactionLogPK());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(transactionLog);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findTransactionLog(transactionLog.getTransactionLogPK()) != null) {
                throw new PreexistingEntityException("TransactionLog " + transactionLog + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TransactionLog transactionLog) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            transactionLog = em.merge(transactionLog);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                TransactionLogPK id = transactionLog.getTransactionLogPK();
                if (findTransactionLog(id) == null) {
                    throw new NonexistentEntityException("The transactionLog with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(TransactionLogPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            TransactionLog transactionLog;
            try {
                transactionLog = em.getReference(TransactionLog.class, id);
                transactionLog.getTransactionLogPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The transactionLog with id " + id + " no longer exists.", enfe);
            }
            em.remove(transactionLog);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TransactionLog> findTransactionLogEntities() {
        return findTransactionLogEntities(true, -1, -1);
    }

    public List<TransactionLog> findTransactionLogEntities(int maxResults, int firstResult) {
        return findTransactionLogEntities(false, maxResults, firstResult);
    }

    private List<TransactionLog> findTransactionLogEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TransactionLog.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TransactionLog findTransactionLog(TransactionLogPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TransactionLog.class, id);
        } finally {
            em.close();
        }
    }

    public int getTransactionLogCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TransactionLog> rt = cq.from(TransactionLog.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
