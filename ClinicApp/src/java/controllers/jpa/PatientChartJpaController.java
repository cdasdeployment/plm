/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.transaction.UserTransaction;
import models.Client;
import models.PatientChart;

/**
 *
 * @author apple
 */
public class PatientChartJpaController implements Serializable {

    public PatientChartJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PatientChart patientChart) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = patientChart.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                patientChart.setClient(client);
            }
            em.persist(patientChart);
            if (client != null) {
                client.getPatientChartList().add(patientChart);
                client = em.merge(client);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PatientChart patientChart) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            PatientChart persistentPatientChart = em.find(PatientChart.class, patientChart.getId());
            Client clientOld = persistentPatientChart.getClient();
            Client clientNew = patientChart.getClient();
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                patientChart.setClient(clientNew);
            }
            patientChart = em.merge(patientChart);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getPatientChartList().remove(patientChart);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getPatientChartList().add(patientChart);
                clientNew = em.merge(clientNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = patientChart.getId();
                if (findPatientChart(id) == null) {
                    throw new NonexistentEntityException("The patientChart with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            PatientChart patientChart;
            try {
                patientChart = em.getReference(PatientChart.class, id);
                patientChart.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The patientChart with id " + id + " no longer exists.", enfe);
            }
            Client client = patientChart.getClient();
            if (client != null) {
                client.getPatientChartList().remove(patientChart);
                client = em.merge(client);
            }
            em.remove(patientChart);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PatientChart> findPatientChartEntities() {
        return findPatientChartEntities(true, -1, -1);
    }

    public List<PatientChart> findPatientChartEntities(int maxResults, int firstResult) {
        return findPatientChartEntities(false, maxResults, firstResult);
    }

    private List<PatientChart> findPatientChartEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PatientChart.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<PatientChart> findPatientChartEntities(int chartType) {
        EntityManager em = getEntityManager();
        try {
            
            Query q = em.createQuery("SELECT p FROM PatientChart p WHERE p.charttype = :c");
            q.setParameter("c", chartType);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PatientChart findPatientChart(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PatientChart.class, id);
        } finally {
            em.close();
        }
    }

    public int getPatientChartCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PatientChart> rt = cq.from(PatientChart.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
