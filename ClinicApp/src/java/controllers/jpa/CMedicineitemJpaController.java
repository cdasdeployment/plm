/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.CDruginventory;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.CMedicineitem;
import models.Cdrugsaledetail;

/**
 *
 * @author apple
 */
public class CMedicineitemJpaController implements Serializable {

    public CMedicineitemJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CMedicineitem CMedicineitem) throws RollbackFailureException, Exception {
        if (CMedicineitem.getCdruginventoryList() == null) {
            CMedicineitem.setCdruginventoryList(new ArrayList<CDruginventory>());
        }
        if (CMedicineitem.getCdrugsaledetailList() == null) {
            CMedicineitem.setCdrugsaledetailList(new ArrayList<Cdrugsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<CDruginventory> attachedCdruginventoryList = new ArrayList<CDruginventory>();
            for (CDruginventory cdruginventoryListCDruginventoryToAttach : CMedicineitem.getCdruginventoryList()) {
                cdruginventoryListCDruginventoryToAttach = em.getReference(cdruginventoryListCDruginventoryToAttach.getClass(), cdruginventoryListCDruginventoryToAttach.getId());
                attachedCdruginventoryList.add(cdruginventoryListCDruginventoryToAttach);
            }
            CMedicineitem.setCdruginventoryList(attachedCdruginventoryList);
            List<Cdrugsaledetail> attachedCdrugsaledetailList = new ArrayList<Cdrugsaledetail>();
            for (Cdrugsaledetail cdrugsaledetailListCdrugsaledetailToAttach : CMedicineitem.getCdrugsaledetailList()) {
                cdrugsaledetailListCdrugsaledetailToAttach = em.getReference(cdrugsaledetailListCdrugsaledetailToAttach.getClass(), cdrugsaledetailListCdrugsaledetailToAttach.getId());
                attachedCdrugsaledetailList.add(cdrugsaledetailListCdrugsaledetailToAttach);
            }
            CMedicineitem.setCdrugsaledetailList(attachedCdrugsaledetailList);
            em.persist(CMedicineitem);
            for (CDruginventory cdruginventoryListCDruginventory : CMedicineitem.getCdruginventoryList()) {
                CMedicineitem oldDrugOfCdruginventoryListCDruginventory = cdruginventoryListCDruginventory.getDrug();
                cdruginventoryListCDruginventory.setDrug(CMedicineitem);
                cdruginventoryListCDruginventory = em.merge(cdruginventoryListCDruginventory);
                if (oldDrugOfCdruginventoryListCDruginventory != null) {
                    oldDrugOfCdruginventoryListCDruginventory.getCdruginventoryList().remove(cdruginventoryListCDruginventory);
                    oldDrugOfCdruginventoryListCDruginventory = em.merge(oldDrugOfCdruginventoryListCDruginventory);
                }
            }
            for (Cdrugsaledetail cdrugsaledetailListCdrugsaledetail : CMedicineitem.getCdrugsaledetailList()) {
                CMedicineitem oldCdrugOfCdrugsaledetailListCdrugsaledetail = cdrugsaledetailListCdrugsaledetail.getCdrug();
                cdrugsaledetailListCdrugsaledetail.setCdrug(CMedicineitem);
                cdrugsaledetailListCdrugsaledetail = em.merge(cdrugsaledetailListCdrugsaledetail);
                if (oldCdrugOfCdrugsaledetailListCdrugsaledetail != null) {
                    oldCdrugOfCdrugsaledetailListCdrugsaledetail.getCdrugsaledetailList().remove(cdrugsaledetailListCdrugsaledetail);
                    oldCdrugOfCdrugsaledetailListCdrugsaledetail = em.merge(oldCdrugOfCdrugsaledetailListCdrugsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CMedicineitem CMedicineitem) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CMedicineitem persistentCMedicineitem = em.find(CMedicineitem.class, CMedicineitem.getId());
            List<CDruginventory> cdruginventoryListOld = persistentCMedicineitem.getCdruginventoryList();
            List<CDruginventory> cdruginventoryListNew = CMedicineitem.getCdruginventoryList();
            List<Cdrugsaledetail> cdrugsaledetailListOld = persistentCMedicineitem.getCdrugsaledetailList();
            List<Cdrugsaledetail> cdrugsaledetailListNew = CMedicineitem.getCdrugsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Cdrugsaledetail cdrugsaledetailListOldCdrugsaledetail : cdrugsaledetailListOld) {
                if (!cdrugsaledetailListNew.contains(cdrugsaledetailListOldCdrugsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cdrugsaledetail " + cdrugsaledetailListOldCdrugsaledetail + " since its cdrug field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CDruginventory> attachedCdruginventoryListNew = new ArrayList<CDruginventory>();
            for (CDruginventory cdruginventoryListNewCDruginventoryToAttach : cdruginventoryListNew) {
                cdruginventoryListNewCDruginventoryToAttach = em.getReference(cdruginventoryListNewCDruginventoryToAttach.getClass(), cdruginventoryListNewCDruginventoryToAttach.getId());
                attachedCdruginventoryListNew.add(cdruginventoryListNewCDruginventoryToAttach);
            }
            cdruginventoryListNew = attachedCdruginventoryListNew;
            CMedicineitem.setCdruginventoryList(cdruginventoryListNew);
            List<Cdrugsaledetail> attachedCdrugsaledetailListNew = new ArrayList<Cdrugsaledetail>();
            for (Cdrugsaledetail cdrugsaledetailListNewCdrugsaledetailToAttach : cdrugsaledetailListNew) {
                cdrugsaledetailListNewCdrugsaledetailToAttach = em.getReference(cdrugsaledetailListNewCdrugsaledetailToAttach.getClass(), cdrugsaledetailListNewCdrugsaledetailToAttach.getId());
                attachedCdrugsaledetailListNew.add(cdrugsaledetailListNewCdrugsaledetailToAttach);
            }
            cdrugsaledetailListNew = attachedCdrugsaledetailListNew;
            CMedicineitem.setCdrugsaledetailList(cdrugsaledetailListNew);
            CMedicineitem = em.merge(CMedicineitem);
            for (CDruginventory cdruginventoryListOldCDruginventory : cdruginventoryListOld) {
                if (!cdruginventoryListNew.contains(cdruginventoryListOldCDruginventory)) {
                    cdruginventoryListOldCDruginventory.setDrug(null);
                    cdruginventoryListOldCDruginventory = em.merge(cdruginventoryListOldCDruginventory);
                }
            }
            for (CDruginventory cdruginventoryListNewCDruginventory : cdruginventoryListNew) {
                if (!cdruginventoryListOld.contains(cdruginventoryListNewCDruginventory)) {
                    CMedicineitem oldDrugOfCdruginventoryListNewCDruginventory = cdruginventoryListNewCDruginventory.getDrug();
                    cdruginventoryListNewCDruginventory.setDrug(CMedicineitem);
                    cdruginventoryListNewCDruginventory = em.merge(cdruginventoryListNewCDruginventory);
                    if (oldDrugOfCdruginventoryListNewCDruginventory != null && !oldDrugOfCdruginventoryListNewCDruginventory.equals(CMedicineitem)) {
                        oldDrugOfCdruginventoryListNewCDruginventory.getCdruginventoryList().remove(cdruginventoryListNewCDruginventory);
                        oldDrugOfCdruginventoryListNewCDruginventory = em.merge(oldDrugOfCdruginventoryListNewCDruginventory);
                    }
                }
            }
            for (Cdrugsaledetail cdrugsaledetailListNewCdrugsaledetail : cdrugsaledetailListNew) {
                if (!cdrugsaledetailListOld.contains(cdrugsaledetailListNewCdrugsaledetail)) {
                    CMedicineitem oldCdrugOfCdrugsaledetailListNewCdrugsaledetail = cdrugsaledetailListNewCdrugsaledetail.getCdrug();
                    cdrugsaledetailListNewCdrugsaledetail.setCdrug(CMedicineitem);
                    cdrugsaledetailListNewCdrugsaledetail = em.merge(cdrugsaledetailListNewCdrugsaledetail);
                    if (oldCdrugOfCdrugsaledetailListNewCdrugsaledetail != null && !oldCdrugOfCdrugsaledetailListNewCdrugsaledetail.equals(CMedicineitem)) {
                        oldCdrugOfCdrugsaledetailListNewCdrugsaledetail.getCdrugsaledetailList().remove(cdrugsaledetailListNewCdrugsaledetail);
                        oldCdrugOfCdrugsaledetailListNewCdrugsaledetail = em.merge(oldCdrugOfCdrugsaledetailListNewCdrugsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = CMedicineitem.getId();
                if (findCMedicineitem(id) == null) {
                    throw new NonexistentEntityException("The cMedicineitem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            CMedicineitem CMedicineitem;
            try {
                CMedicineitem = em.getReference(CMedicineitem.class, id);
                CMedicineitem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The CMedicineitem with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Cdrugsaledetail> cdrugsaledetailListOrphanCheck = CMedicineitem.getCdrugsaledetailList();
            for (Cdrugsaledetail cdrugsaledetailListOrphanCheckCdrugsaledetail : cdrugsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CMedicineitem (" + CMedicineitem + ") cannot be destroyed since the Cdrugsaledetail " + cdrugsaledetailListOrphanCheckCdrugsaledetail + " in its cdrugsaledetailList field has a non-nullable cdrug field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<CDruginventory> cdruginventoryList = CMedicineitem.getCdruginventoryList();
            for (CDruginventory cdruginventoryListCDruginventory : cdruginventoryList) {
                cdruginventoryListCDruginventory.setDrug(null);
                cdruginventoryListCDruginventory = em.merge(cdruginventoryListCDruginventory);
            }
            em.remove(CMedicineitem);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<CMedicineitem> findCMedicineitemEntities() {
        return findCMedicineitemEntities(true, -1, -1);
    }

    public List<CMedicineitem> findCMedicineitemEntities(int maxResults, int firstResult) {
        return findCMedicineitemEntities(false, maxResults, firstResult);
    }

    private List<CMedicineitem> findCMedicineitemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(CMedicineitem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CMedicineitem findCMedicineitem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CMedicineitem.class, id);
        } finally {
            em.close();
        }
    }

    public int getCMedicineitemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<CMedicineitem> rt = cq.from(CMedicineitem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
