/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.ChartDetails;
import models.DiagnosisList;

/**
 *
 * @author apple
 */
public class DiagnosisListJpaController implements Serializable {

    public DiagnosisListJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DiagnosisList diagnosisList) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails chart = diagnosisList.getChart();
            if (chart != null) {
                chart = em.getReference(chart.getClass(), chart.getId());
                diagnosisList.setChart(chart);
            }
            em.persist(diagnosisList);
            if (chart != null) {
                chart.getDiagnosisListList().add(diagnosisList);
                chart = em.merge(chart);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DiagnosisList diagnosisList) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DiagnosisList persistentDiagnosisList = em.find(DiagnosisList.class, diagnosisList.getId());
            ChartDetails chartOld = persistentDiagnosisList.getChart();
            ChartDetails chartNew = diagnosisList.getChart();
            if (chartNew != null) {
                chartNew = em.getReference(chartNew.getClass(), chartNew.getId());
                diagnosisList.setChart(chartNew);
            }
            diagnosisList = em.merge(diagnosisList);
            if (chartOld != null && !chartOld.equals(chartNew)) {
                chartOld.getDiagnosisListList().remove(diagnosisList);
                chartOld = em.merge(chartOld);
            }
            if (chartNew != null && !chartNew.equals(chartOld)) {
                chartNew.getDiagnosisListList().add(diagnosisList);
                chartNew = em.merge(chartNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = diagnosisList.getId();
                if (findDiagnosisList(id) == null) {
                    throw new NonexistentEntityException("The diagnosisList with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            DiagnosisList diagnosisList;
            try {
                diagnosisList = em.getReference(DiagnosisList.class, id);
                diagnosisList.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The diagnosisList with id " + id + " no longer exists.", enfe);
            }
            ChartDetails chart = diagnosisList.getChart();
            if (chart != null) {
                chart.getDiagnosisListList().remove(diagnosisList);
                chart = em.merge(chart);
            }
            em.remove(diagnosisList);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DiagnosisList> findDiagnosisListEntities() {
        return findDiagnosisListEntities(true, -1, -1);
    }

    public List<DiagnosisList> findDiagnosisListEntities(int maxResults, int firstResult) {
        return findDiagnosisListEntities(false, maxResults, firstResult);
    }

    private List<DiagnosisList> findDiagnosisListEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DiagnosisList.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DiagnosisList findDiagnosisList(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DiagnosisList.class, id);
        } finally {
            em.close();
        }
    }

    public int getDiagnosisListCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DiagnosisList> rt = cq.from(DiagnosisList.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
