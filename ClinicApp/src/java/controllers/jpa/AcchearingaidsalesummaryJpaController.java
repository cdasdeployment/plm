/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Client;
import models.Acchearingaidsaledetail;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Acchearingaidsalesummary;

/**
 *
 * @author apple
 */
public class AcchearingaidsalesummaryJpaController implements Serializable {

    public AcchearingaidsalesummaryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Acchearingaidsalesummary acchearingaidsalesummary) throws RollbackFailureException, Exception {
        if (acchearingaidsalesummary.getAcchearingaidsaledetailList() == null) {
            acchearingaidsalesummary.setAcchearingaidsaledetailList(new ArrayList<Acchearingaidsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = acchearingaidsalesummary.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                acchearingaidsalesummary.setClient(client);
            }
            List<Acchearingaidsaledetail> attachedAcchearingaidsaledetailList = new ArrayList<Acchearingaidsaledetail>();
            for (Acchearingaidsaledetail acchearingaidsaledetailListAcchearingaidsaledetailToAttach : acchearingaidsalesummary.getAcchearingaidsaledetailList()) {
                acchearingaidsaledetailListAcchearingaidsaledetailToAttach = em.getReference(acchearingaidsaledetailListAcchearingaidsaledetailToAttach.getClass(), acchearingaidsaledetailListAcchearingaidsaledetailToAttach.getId());
                attachedAcchearingaidsaledetailList.add(acchearingaidsaledetailListAcchearingaidsaledetailToAttach);
            }
            acchearingaidsalesummary.setAcchearingaidsaledetailList(attachedAcchearingaidsaledetailList);
            em.persist(acchearingaidsalesummary);
            if (client != null) {
                client.getAcchearingaidsalesummaryList().add(acchearingaidsalesummary);
                client = em.merge(client);
            }
            for (Acchearingaidsaledetail acchearingaidsaledetailListAcchearingaidsaledetail : acchearingaidsalesummary.getAcchearingaidsaledetailList()) {
                Acchearingaidsalesummary oldSalesummaryOfAcchearingaidsaledetailListAcchearingaidsaledetail = acchearingaidsaledetailListAcchearingaidsaledetail.getSalesummary();
                acchearingaidsaledetailListAcchearingaidsaledetail.setSalesummary(acchearingaidsalesummary);
                acchearingaidsaledetailListAcchearingaidsaledetail = em.merge(acchearingaidsaledetailListAcchearingaidsaledetail);
                if (oldSalesummaryOfAcchearingaidsaledetailListAcchearingaidsaledetail != null) {
                    oldSalesummaryOfAcchearingaidsaledetailListAcchearingaidsaledetail.getAcchearingaidsaledetailList().remove(acchearingaidsaledetailListAcchearingaidsaledetail);
                    oldSalesummaryOfAcchearingaidsaledetailListAcchearingaidsaledetail = em.merge(oldSalesummaryOfAcchearingaidsaledetailListAcchearingaidsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Acchearingaidsalesummary acchearingaidsalesummary) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Acchearingaidsalesummary persistentAcchearingaidsalesummary = em.find(Acchearingaidsalesummary.class, acchearingaidsalesummary.getId());
            Client clientOld = persistentAcchearingaidsalesummary.getClient();
            Client clientNew = acchearingaidsalesummary.getClient();
            List<Acchearingaidsaledetail> acchearingaidsaledetailListOld = persistentAcchearingaidsalesummary.getAcchearingaidsaledetailList();
            List<Acchearingaidsaledetail> acchearingaidsaledetailListNew = acchearingaidsalesummary.getAcchearingaidsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Acchearingaidsaledetail acchearingaidsaledetailListOldAcchearingaidsaledetail : acchearingaidsaledetailListOld) {
                if (!acchearingaidsaledetailListNew.contains(acchearingaidsaledetailListOldAcchearingaidsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Acchearingaidsaledetail " + acchearingaidsaledetailListOldAcchearingaidsaledetail + " since its salesummary field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                acchearingaidsalesummary.setClient(clientNew);
            }
            List<Acchearingaidsaledetail> attachedAcchearingaidsaledetailListNew = new ArrayList<Acchearingaidsaledetail>();
            for (Acchearingaidsaledetail acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach : acchearingaidsaledetailListNew) {
                acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach = em.getReference(acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach.getClass(), acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach.getId());
                attachedAcchearingaidsaledetailListNew.add(acchearingaidsaledetailListNewAcchearingaidsaledetailToAttach);
            }
            acchearingaidsaledetailListNew = attachedAcchearingaidsaledetailListNew;
            acchearingaidsalesummary.setAcchearingaidsaledetailList(acchearingaidsaledetailListNew);
            acchearingaidsalesummary = em.merge(acchearingaidsalesummary);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getAcchearingaidsalesummaryList().remove(acchearingaidsalesummary);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getAcchearingaidsalesummaryList().add(acchearingaidsalesummary);
                clientNew = em.merge(clientNew);
            }
            for (Acchearingaidsaledetail acchearingaidsaledetailListNewAcchearingaidsaledetail : acchearingaidsaledetailListNew) {
                if (!acchearingaidsaledetailListOld.contains(acchearingaidsaledetailListNewAcchearingaidsaledetail)) {
                    Acchearingaidsalesummary oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail = acchearingaidsaledetailListNewAcchearingaidsaledetail.getSalesummary();
                    acchearingaidsaledetailListNewAcchearingaidsaledetail.setSalesummary(acchearingaidsalesummary);
                    acchearingaidsaledetailListNewAcchearingaidsaledetail = em.merge(acchearingaidsaledetailListNewAcchearingaidsaledetail);
                    if (oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail != null && !oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail.equals(acchearingaidsalesummary)) {
                        oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail.getAcchearingaidsaledetailList().remove(acchearingaidsaledetailListNewAcchearingaidsaledetail);
                        oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail = em.merge(oldSalesummaryOfAcchearingaidsaledetailListNewAcchearingaidsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = acchearingaidsalesummary.getId();
                if (findAcchearingaidsalesummary(id) == null) {
                    throw new NonexistentEntityException("The acchearingaidsalesummary with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Acchearingaidsalesummary acchearingaidsalesummary;
            try {
                acchearingaidsalesummary = em.getReference(Acchearingaidsalesummary.class, id);
                acchearingaidsalesummary.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The acchearingaidsalesummary with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Acchearingaidsaledetail> acchearingaidsaledetailListOrphanCheck = acchearingaidsalesummary.getAcchearingaidsaledetailList();
            for (Acchearingaidsaledetail acchearingaidsaledetailListOrphanCheckAcchearingaidsaledetail : acchearingaidsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Acchearingaidsalesummary (" + acchearingaidsalesummary + ") cannot be destroyed since the Acchearingaidsaledetail " + acchearingaidsaledetailListOrphanCheckAcchearingaidsaledetail + " in its acchearingaidsaledetailList field has a non-nullable salesummary field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client client = acchearingaidsalesummary.getClient();
            if (client != null) {
                client.getAcchearingaidsalesummaryList().remove(acchearingaidsalesummary);
                client = em.merge(client);
            }
            em.remove(acchearingaidsalesummary);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Acchearingaidsalesummary> findAcchearingaidsalesummaryEntities() {
        return findAcchearingaidsalesummaryEntities(true, -1, -1);
    }

    public List<Acchearingaidsalesummary> findAcchearingaidsalesummaryEntities(int maxResults, int firstResult) {
        return findAcchearingaidsalesummaryEntities(false, maxResults, firstResult);
    }

    private List<Acchearingaidsalesummary> findAcchearingaidsalesummaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Acchearingaidsalesummary.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Acchearingaidsalesummary findAcchearingaidsalesummary(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Acchearingaidsalesummary.class, id);
        } finally {
            em.close();
        }
    }

    public int getAcchearingaidsalesummaryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Acchearingaidsalesummary> rt = cq.from(Acchearingaidsalesummary.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
