/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Client;
import models.Hearingaidsaledetail;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Hearingaidsalesummary;

/**
 *
 * @author apple
 */
public class HearingaidsalesummaryJpaController implements Serializable {

    public HearingaidsalesummaryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hearingaidsalesummary hearingaidsalesummary) throws RollbackFailureException, Exception {
        if (hearingaidsalesummary.getHearingaidsaledetailList() == null) {
            hearingaidsalesummary.setHearingaidsaledetailList(new ArrayList<Hearingaidsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client = hearingaidsalesummary.getClient();
            if (client != null) {
                client = em.getReference(client.getClass(), client.getId());
                hearingaidsalesummary.setClient(client);
            }
            List<Hearingaidsaledetail> attachedHearingaidsaledetailList = new ArrayList<Hearingaidsaledetail>();
            for (Hearingaidsaledetail hearingaidsaledetailListHearingaidsaledetailToAttach : hearingaidsalesummary.getHearingaidsaledetailList()) {
                hearingaidsaledetailListHearingaidsaledetailToAttach = em.getReference(hearingaidsaledetailListHearingaidsaledetailToAttach.getClass(), hearingaidsaledetailListHearingaidsaledetailToAttach.getId());
                attachedHearingaidsaledetailList.add(hearingaidsaledetailListHearingaidsaledetailToAttach);
            }
            hearingaidsalesummary.setHearingaidsaledetailList(attachedHearingaidsaledetailList);
            em.persist(hearingaidsalesummary);
            if (client != null) {
                client.getHearingaidsalesummaryList().add(hearingaidsalesummary);
                client = em.merge(client);
            }
            for (Hearingaidsaledetail hearingaidsaledetailListHearingaidsaledetail : hearingaidsalesummary.getHearingaidsaledetailList()) {
                Hearingaidsalesummary oldSalesummaryOfHearingaidsaledetailListHearingaidsaledetail = hearingaidsaledetailListHearingaidsaledetail.getSalesummary();
                hearingaidsaledetailListHearingaidsaledetail.setSalesummary(hearingaidsalesummary);
                hearingaidsaledetailListHearingaidsaledetail = em.merge(hearingaidsaledetailListHearingaidsaledetail);
                if (oldSalesummaryOfHearingaidsaledetailListHearingaidsaledetail != null) {
                    oldSalesummaryOfHearingaidsaledetailListHearingaidsaledetail.getHearingaidsaledetailList().remove(hearingaidsaledetailListHearingaidsaledetail);
                    oldSalesummaryOfHearingaidsaledetailListHearingaidsaledetail = em.merge(oldSalesummaryOfHearingaidsaledetailListHearingaidsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hearingaidsalesummary hearingaidsalesummary) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidsalesummary persistentHearingaidsalesummary = em.find(Hearingaidsalesummary.class, hearingaidsalesummary.getId());
            Client clientOld = persistentHearingaidsalesummary.getClient();
            Client clientNew = hearingaidsalesummary.getClient();
            List<Hearingaidsaledetail> hearingaidsaledetailListOld = persistentHearingaidsalesummary.getHearingaidsaledetailList();
            List<Hearingaidsaledetail> hearingaidsaledetailListNew = hearingaidsalesummary.getHearingaidsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Hearingaidsaledetail hearingaidsaledetailListOldHearingaidsaledetail : hearingaidsaledetailListOld) {
                if (!hearingaidsaledetailListNew.contains(hearingaidsaledetailListOldHearingaidsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Hearingaidsaledetail " + hearingaidsaledetailListOldHearingaidsaledetail + " since its salesummary field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (clientNew != null) {
                clientNew = em.getReference(clientNew.getClass(), clientNew.getId());
                hearingaidsalesummary.setClient(clientNew);
            }
            List<Hearingaidsaledetail> attachedHearingaidsaledetailListNew = new ArrayList<Hearingaidsaledetail>();
            for (Hearingaidsaledetail hearingaidsaledetailListNewHearingaidsaledetailToAttach : hearingaidsaledetailListNew) {
                hearingaidsaledetailListNewHearingaidsaledetailToAttach = em.getReference(hearingaidsaledetailListNewHearingaidsaledetailToAttach.getClass(), hearingaidsaledetailListNewHearingaidsaledetailToAttach.getId());
                attachedHearingaidsaledetailListNew.add(hearingaidsaledetailListNewHearingaidsaledetailToAttach);
            }
            hearingaidsaledetailListNew = attachedHearingaidsaledetailListNew;
            hearingaidsalesummary.setHearingaidsaledetailList(hearingaidsaledetailListNew);
            hearingaidsalesummary = em.merge(hearingaidsalesummary);
            if (clientOld != null && !clientOld.equals(clientNew)) {
                clientOld.getHearingaidsalesummaryList().remove(hearingaidsalesummary);
                clientOld = em.merge(clientOld);
            }
            if (clientNew != null && !clientNew.equals(clientOld)) {
                clientNew.getHearingaidsalesummaryList().add(hearingaidsalesummary);
                clientNew = em.merge(clientNew);
            }
            for (Hearingaidsaledetail hearingaidsaledetailListNewHearingaidsaledetail : hearingaidsaledetailListNew) {
                if (!hearingaidsaledetailListOld.contains(hearingaidsaledetailListNewHearingaidsaledetail)) {
                    Hearingaidsalesummary oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail = hearingaidsaledetailListNewHearingaidsaledetail.getSalesummary();
                    hearingaidsaledetailListNewHearingaidsaledetail.setSalesummary(hearingaidsalesummary);
                    hearingaidsaledetailListNewHearingaidsaledetail = em.merge(hearingaidsaledetailListNewHearingaidsaledetail);
                    if (oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail != null && !oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail.equals(hearingaidsalesummary)) {
                        oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail.getHearingaidsaledetailList().remove(hearingaidsaledetailListNewHearingaidsaledetail);
                        oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail = em.merge(oldSalesummaryOfHearingaidsaledetailListNewHearingaidsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = hearingaidsalesummary.getId();
                if (findHearingaidsalesummary(id) == null) {
                    throw new NonexistentEntityException("The hearingaidsalesummary with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidsalesummary hearingaidsalesummary;
            try {
                hearingaidsalesummary = em.getReference(Hearingaidsalesummary.class, id);
                hearingaidsalesummary.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hearingaidsalesummary with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Hearingaidsaledetail> hearingaidsaledetailListOrphanCheck = hearingaidsalesummary.getHearingaidsaledetailList();
            for (Hearingaidsaledetail hearingaidsaledetailListOrphanCheckHearingaidsaledetail : hearingaidsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Hearingaidsalesummary (" + hearingaidsalesummary + ") cannot be destroyed since the Hearingaidsaledetail " + hearingaidsaledetailListOrphanCheckHearingaidsaledetail + " in its hearingaidsaledetailList field has a non-nullable salesummary field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Client client = hearingaidsalesummary.getClient();
            if (client != null) {
                client.getHearingaidsalesummaryList().remove(hearingaidsalesummary);
                client = em.merge(client);
            }
            em.remove(hearingaidsalesummary);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hearingaidsalesummary> findHearingaidsalesummaryEntities() {
        return findHearingaidsalesummaryEntities(true, -1, -1);
    }

    public List<Hearingaidsalesummary> findHearingaidsalesummaryEntities(int maxResults, int firstResult) {
        return findHearingaidsalesummaryEntities(false, maxResults, firstResult);
    }

    private List<Hearingaidsalesummary> findHearingaidsalesummaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hearingaidsalesummary.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hearingaidsalesummary findHearingaidsalesummary(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hearingaidsalesummary.class, id);
        } finally {
            em.close();
        }
    }

    public int getHearingaidsalesummaryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hearingaidsalesummary> rt = cq.from(Hearingaidsalesummary.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
