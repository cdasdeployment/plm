/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Cdrugsalesummary;
import models.CMedicineitem;
import models.Cdrugsaledetail;

/**
 *
 * @author apple
 */
public class CdrugsaledetailJpaController implements Serializable {

    public CdrugsaledetailJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cdrugsaledetail cdrugsaledetail) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cdrugsalesummary salesummary = cdrugsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary = em.getReference(salesummary.getClass(), salesummary.getId());
                cdrugsaledetail.setSalesummary(salesummary);
            }
            CMedicineitem cdrug = cdrugsaledetail.getCdrug();
            if (cdrug != null) {
                cdrug = em.getReference(cdrug.getClass(), cdrug.getId());
                cdrugsaledetail.setCdrug(cdrug);
            }
            em.persist(cdrugsaledetail);
            if (salesummary != null) {
                salesummary.getCdrugsaledetailList().add(cdrugsaledetail);
                salesummary = em.merge(salesummary);
            }
            if (cdrug != null) {
                cdrug.getCdrugsaledetailList().add(cdrugsaledetail);
                cdrug = em.merge(cdrug);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cdrugsaledetail cdrugsaledetail) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cdrugsaledetail persistentCdrugsaledetail = em.find(Cdrugsaledetail.class, cdrugsaledetail.getId());
            Cdrugsalesummary salesummaryOld = persistentCdrugsaledetail.getSalesummary();
            Cdrugsalesummary salesummaryNew = cdrugsaledetail.getSalesummary();
            CMedicineitem cdrugOld = persistentCdrugsaledetail.getCdrug();
            CMedicineitem cdrugNew = cdrugsaledetail.getCdrug();
            if (salesummaryNew != null) {
                salesummaryNew = em.getReference(salesummaryNew.getClass(), salesummaryNew.getId());
                cdrugsaledetail.setSalesummary(salesummaryNew);
            }
            if (cdrugNew != null) {
                cdrugNew = em.getReference(cdrugNew.getClass(), cdrugNew.getId());
                cdrugsaledetail.setCdrug(cdrugNew);
            }
            cdrugsaledetail = em.merge(cdrugsaledetail);
            if (salesummaryOld != null && !salesummaryOld.equals(salesummaryNew)) {
                salesummaryOld.getCdrugsaledetailList().remove(cdrugsaledetail);
                salesummaryOld = em.merge(salesummaryOld);
            }
            if (salesummaryNew != null && !salesummaryNew.equals(salesummaryOld)) {
                salesummaryNew.getCdrugsaledetailList().add(cdrugsaledetail);
                salesummaryNew = em.merge(salesummaryNew);
            }
            if (cdrugOld != null && !cdrugOld.equals(cdrugNew)) {
                cdrugOld.getCdrugsaledetailList().remove(cdrugsaledetail);
                cdrugOld = em.merge(cdrugOld);
            }
            if (cdrugNew != null && !cdrugNew.equals(cdrugOld)) {
                cdrugNew.getCdrugsaledetailList().add(cdrugsaledetail);
                cdrugNew = em.merge(cdrugNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = cdrugsaledetail.getId();
                if (findCdrugsaledetail(id) == null) {
                    throw new NonexistentEntityException("The cdrugsaledetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Cdrugsaledetail cdrugsaledetail;
            try {
                cdrugsaledetail = em.getReference(Cdrugsaledetail.class, id);
                cdrugsaledetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cdrugsaledetail with id " + id + " no longer exists.", enfe);
            }
            Cdrugsalesummary salesummary = cdrugsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary.getCdrugsaledetailList().remove(cdrugsaledetail);
                salesummary = em.merge(salesummary);
            }
            CMedicineitem cdrug = cdrugsaledetail.getCdrug();
            if (cdrug != null) {
                cdrug.getCdrugsaledetailList().remove(cdrugsaledetail);
                cdrug = em.merge(cdrug);
            }
            em.remove(cdrugsaledetail);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cdrugsaledetail> findCdrugsaledetailEntities() {
        return findCdrugsaledetailEntities(true, -1, -1);
    }

    public List<Cdrugsaledetail> findCdrugsaledetailEntities(int maxResults, int firstResult) {
        return findCdrugsaledetailEntities(false, maxResults, firstResult);
    }

    private List<Cdrugsaledetail> findCdrugsaledetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cdrugsaledetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cdrugsaledetail findCdrugsaledetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cdrugsaledetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getCdrugsaledetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cdrugsaledetail> rt = cq.from(Cdrugsaledetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
