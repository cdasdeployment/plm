/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Hearingaidinventory;
import models.Hearingaiditem;

/**
 *
 * @author apple
 */
public class HearingaidinventoryJpaController implements Serializable {

    public HearingaidinventoryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Hearingaidinventory hearingaidinventory) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaiditem hearingaid = hearingaidinventory.getHearingaid();
            if (hearingaid != null) {
                hearingaid = em.getReference(hearingaid.getClass(), hearingaid.getId());
                hearingaidinventory.setHearingaid(hearingaid);
            }
            em.persist(hearingaidinventory);
            if (hearingaid != null) {
                hearingaid.getHearingaidinventoryList().add(hearingaidinventory);
                hearingaid = em.merge(hearingaid);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Hearingaidinventory hearingaidinventory) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidinventory persistentHearingaidinventory = em.find(Hearingaidinventory.class, hearingaidinventory.getId());
            Hearingaiditem hearingaidOld = persistentHearingaidinventory.getHearingaid();
            Hearingaiditem hearingaidNew = hearingaidinventory.getHearingaid();
            if (hearingaidNew != null) {
                hearingaidNew = em.getReference(hearingaidNew.getClass(), hearingaidNew.getId());
                hearingaidinventory.setHearingaid(hearingaidNew);
            }
            hearingaidinventory = em.merge(hearingaidinventory);
            if (hearingaidOld != null && !hearingaidOld.equals(hearingaidNew)) {
                hearingaidOld.getHearingaidinventoryList().remove(hearingaidinventory);
                hearingaidOld = em.merge(hearingaidOld);
            }
            if (hearingaidNew != null && !hearingaidNew.equals(hearingaidOld)) {
                hearingaidNew.getHearingaidinventoryList().add(hearingaidinventory);
                hearingaidNew = em.merge(hearingaidNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = hearingaidinventory.getId();
                if (findHearingaidinventory(id) == null) {
                    throw new NonexistentEntityException("The hearingaidinventory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Hearingaidinventory hearingaidinventory;
            try {
                hearingaidinventory = em.getReference(Hearingaidinventory.class, id);
                hearingaidinventory.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The hearingaidinventory with id " + id + " no longer exists.", enfe);
            }
            Hearingaiditem hearingaid = hearingaidinventory.getHearingaid();
            if (hearingaid != null) {
                hearingaid.getHearingaidinventoryList().remove(hearingaidinventory);
                hearingaid = em.merge(hearingaid);
            }
            em.remove(hearingaidinventory);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Hearingaidinventory> findHearingaidinventoryEntities() {
        return findHearingaidinventoryEntities(true, -1, -1);
    }

    public List<Hearingaidinventory> findHearingaidinventoryEntities(int maxResults, int firstResult) {
        return findHearingaidinventoryEntities(false, maxResults, firstResult);
    }

    private List<Hearingaidinventory> findHearingaidinventoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Hearingaidinventory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Hearingaidinventory findHearingaidinventory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Hearingaidinventory.class, id);
        } finally {
            em.close();
        }
    }

    public int getHearingaidinventoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Hearingaidinventory> rt = cq.from(Hearingaidinventory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
