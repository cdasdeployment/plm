/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.ChartDetails;
import models.ChartRX;

/**
 *
 * @author apple
 */
public class ChartRXJpaController implements Serializable {

    public ChartRXJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(ChartRX chartRX) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails chart = chartRX.getChart();
            if (chart != null) {
                chart = em.getReference(chart.getClass(), chart.getId());
                chartRX.setChart(chart);
            }
            em.persist(chartRX);
            if (chart != null) {
                chart.getChartRXList().add(chartRX);
                chart = em.merge(chart);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(ChartRX chartRX) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartRX persistentChartRX = em.find(ChartRX.class, chartRX.getId());
            ChartDetails chartOld = persistentChartRX.getChart();
            ChartDetails chartNew = chartRX.getChart();
            if (chartNew != null) {
                chartNew = em.getReference(chartNew.getClass(), chartNew.getId());
                chartRX.setChart(chartNew);
            }
            chartRX = em.merge(chartRX);
            if (chartOld != null && !chartOld.equals(chartNew)) {
                chartOld.getChartRXList().remove(chartRX);
                chartOld = em.merge(chartOld);
            }
            if (chartNew != null && !chartNew.equals(chartOld)) {
                chartNew.getChartRXList().add(chartRX);
                chartNew = em.merge(chartNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = chartRX.getId();
                if (findChartRX(id) == null) {
                    throw new NonexistentEntityException("The chartRX with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartRX chartRX;
            try {
                chartRX = em.getReference(ChartRX.class, id);
                chartRX.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The chartRX with id " + id + " no longer exists.", enfe);
            }
            ChartDetails chart = chartRX.getChart();
            if (chart != null) {
                chart.getChartRXList().remove(chartRX);
                chart = em.merge(chart);
            }
            em.remove(chartRX);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<ChartRX> findChartRXEntities() {
        return findChartRXEntities(true, -1, -1);
    }

    public List<ChartRX> findChartRXEntities(int maxResults, int firstResult) {
        return findChartRXEntities(false, maxResults, firstResult);
    }

    private List<ChartRX> findChartRXEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(ChartRX.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public ChartRX findChartRX(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(ChartRX.class, id);
        } finally {
            em.close();
        }
    }

    public int getChartRXCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<ChartRX> rt = cq.from(ChartRX.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
