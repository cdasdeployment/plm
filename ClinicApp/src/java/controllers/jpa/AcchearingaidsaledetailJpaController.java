/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.Acchearingaidsalesummary;
import models.AccHearingaiditem;
import models.Acchearingaidsaledetail;

/**
 *
 * @author apple
 */
public class AcchearingaidsaledetailJpaController implements Serializable {

    public AcchearingaidsaledetailJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Acchearingaidsaledetail acchearingaidsaledetail) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Acchearingaidsalesummary salesummary = acchearingaidsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary = em.getReference(salesummary.getClass(), salesummary.getId());
                acchearingaidsaledetail.setSalesummary(salesummary);
            }
            AccHearingaiditem acchearingaid = acchearingaidsaledetail.getAcchearingaid();
            if (acchearingaid != null) {
                acchearingaid = em.getReference(acchearingaid.getClass(), acchearingaid.getId());
                acchearingaidsaledetail.setAcchearingaid(acchearingaid);
            }
            em.persist(acchearingaidsaledetail);
            if (salesummary != null) {
                salesummary.getAcchearingaidsaledetailList().add(acchearingaidsaledetail);
                salesummary = em.merge(salesummary);
            }
            if (acchearingaid != null) {
                acchearingaid.getAcchearingaidsaledetailList().add(acchearingaidsaledetail);
                acchearingaid = em.merge(acchearingaid);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Acchearingaidsaledetail acchearingaidsaledetail) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Acchearingaidsaledetail persistentAcchearingaidsaledetail = em.find(Acchearingaidsaledetail.class, acchearingaidsaledetail.getId());
            Acchearingaidsalesummary salesummaryOld = persistentAcchearingaidsaledetail.getSalesummary();
            Acchearingaidsalesummary salesummaryNew = acchearingaidsaledetail.getSalesummary();
            AccHearingaiditem acchearingaidOld = persistentAcchearingaidsaledetail.getAcchearingaid();
            AccHearingaiditem acchearingaidNew = acchearingaidsaledetail.getAcchearingaid();
            if (salesummaryNew != null) {
                salesummaryNew = em.getReference(salesummaryNew.getClass(), salesummaryNew.getId());
                acchearingaidsaledetail.setSalesummary(salesummaryNew);
            }
            if (acchearingaidNew != null) {
                acchearingaidNew = em.getReference(acchearingaidNew.getClass(), acchearingaidNew.getId());
                acchearingaidsaledetail.setAcchearingaid(acchearingaidNew);
            }
            acchearingaidsaledetail = em.merge(acchearingaidsaledetail);
            if (salesummaryOld != null && !salesummaryOld.equals(salesummaryNew)) {
                salesummaryOld.getAcchearingaidsaledetailList().remove(acchearingaidsaledetail);
                salesummaryOld = em.merge(salesummaryOld);
            }
            if (salesummaryNew != null && !salesummaryNew.equals(salesummaryOld)) {
                salesummaryNew.getAcchearingaidsaledetailList().add(acchearingaidsaledetail);
                salesummaryNew = em.merge(salesummaryNew);
            }
            if (acchearingaidOld != null && !acchearingaidOld.equals(acchearingaidNew)) {
                acchearingaidOld.getAcchearingaidsaledetailList().remove(acchearingaidsaledetail);
                acchearingaidOld = em.merge(acchearingaidOld);
            }
            if (acchearingaidNew != null && !acchearingaidNew.equals(acchearingaidOld)) {
                acchearingaidNew.getAcchearingaidsaledetailList().add(acchearingaidsaledetail);
                acchearingaidNew = em.merge(acchearingaidNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = acchearingaidsaledetail.getId();
                if (findAcchearingaidsaledetail(id) == null) {
                    throw new NonexistentEntityException("The acchearingaidsaledetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Acchearingaidsaledetail acchearingaidsaledetail;
            try {
                acchearingaidsaledetail = em.getReference(Acchearingaidsaledetail.class, id);
                acchearingaidsaledetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The acchearingaidsaledetail with id " + id + " no longer exists.", enfe);
            }
            Acchearingaidsalesummary salesummary = acchearingaidsaledetail.getSalesummary();
            if (salesummary != null) {
                salesummary.getAcchearingaidsaledetailList().remove(acchearingaidsaledetail);
                salesummary = em.merge(salesummary);
            }
            AccHearingaiditem acchearingaid = acchearingaidsaledetail.getAcchearingaid();
            if (acchearingaid != null) {
                acchearingaid.getAcchearingaidsaledetailList().remove(acchearingaidsaledetail);
                acchearingaid = em.merge(acchearingaid);
            }
            em.remove(acchearingaidsaledetail);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Acchearingaidsaledetail> findAcchearingaidsaledetailEntities() {
        return findAcchearingaidsaledetailEntities(true, -1, -1);
    }

    public List<Acchearingaidsaledetail> findAcchearingaidsaledetailEntities(int maxResults, int firstResult) {
        return findAcchearingaidsaledetailEntities(false, maxResults, firstResult);
    }

    private List<Acchearingaidsaledetail> findAcchearingaidsaledetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Acchearingaidsaledetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Acchearingaidsaledetail findAcchearingaidsaledetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Acchearingaidsaledetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getAcchearingaidsaledetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Acchearingaidsaledetail> rt = cq.from(Acchearingaidsaledetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
