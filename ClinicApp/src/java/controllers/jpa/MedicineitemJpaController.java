/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Druginventory;
import java.util.ArrayList;
import java.util.Collection;
import models.Drugsaledetail;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Medicineitem;

/**
 *
 * @author apple
 */
public class MedicineitemJpaController implements Serializable {

    public MedicineitemJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Medicineitem medicineitem) throws RollbackFailureException, Exception {
        if (medicineitem.getDruginventoryCollection() == null) {
            medicineitem.setDruginventoryCollection(new ArrayList<Druginventory>());
        }
        if (medicineitem.getDrugsaledetailList() == null) {
            medicineitem.setDrugsaledetailList(new ArrayList<Drugsaledetail>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Druginventory> attachedDruginventoryCollection = new ArrayList<Druginventory>();
            for (Druginventory druginventoryCollectionDruginventoryToAttach : medicineitem.getDruginventoryCollection()) {
                druginventoryCollectionDruginventoryToAttach = em.getReference(druginventoryCollectionDruginventoryToAttach.getClass(), druginventoryCollectionDruginventoryToAttach.getId());
                attachedDruginventoryCollection.add(druginventoryCollectionDruginventoryToAttach);
            }
            medicineitem.setDruginventoryCollection(attachedDruginventoryCollection);
            List<Drugsaledetail> attachedDrugsaledetailList = new ArrayList<Drugsaledetail>();
            for (Drugsaledetail drugsaledetailListDrugsaledetailToAttach : medicineitem.getDrugsaledetailList()) {
                drugsaledetailListDrugsaledetailToAttach = em.getReference(drugsaledetailListDrugsaledetailToAttach.getClass(), drugsaledetailListDrugsaledetailToAttach.getId());
                attachedDrugsaledetailList.add(drugsaledetailListDrugsaledetailToAttach);
            }
            medicineitem.setDrugsaledetailList(attachedDrugsaledetailList);
            em.persist(medicineitem);
            for (Druginventory druginventoryCollectionDruginventory : medicineitem.getDruginventoryCollection()) {
                Medicineitem oldDrugOfDruginventoryCollectionDruginventory = druginventoryCollectionDruginventory.getDrug();
                druginventoryCollectionDruginventory.setDrug(medicineitem);
                druginventoryCollectionDruginventory = em.merge(druginventoryCollectionDruginventory);
                if (oldDrugOfDruginventoryCollectionDruginventory != null) {
                    oldDrugOfDruginventoryCollectionDruginventory.getDruginventoryCollection().remove(druginventoryCollectionDruginventory);
                    oldDrugOfDruginventoryCollectionDruginventory = em.merge(oldDrugOfDruginventoryCollectionDruginventory);
                }
            }
            for (Drugsaledetail drugsaledetailListDrugsaledetail : medicineitem.getDrugsaledetailList()) {
                Medicineitem oldDrugOfDrugsaledetailListDrugsaledetail = drugsaledetailListDrugsaledetail.getDrug();
                drugsaledetailListDrugsaledetail.setDrug(medicineitem);
                drugsaledetailListDrugsaledetail = em.merge(drugsaledetailListDrugsaledetail);
                if (oldDrugOfDrugsaledetailListDrugsaledetail != null) {
                    oldDrugOfDrugsaledetailListDrugsaledetail.getDrugsaledetailList().remove(drugsaledetailListDrugsaledetail);
                    oldDrugOfDrugsaledetailListDrugsaledetail = em.merge(oldDrugOfDrugsaledetailListDrugsaledetail);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Medicineitem medicineitem) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Medicineitem persistentMedicineitem = em.find(Medicineitem.class, medicineitem.getId());
            List<Druginventory> druginventoryCollectionOld = persistentMedicineitem.getDruginventoryCollection();
            List<Druginventory> druginventoryCollectionNew = medicineitem.getDruginventoryCollection();
            List<Drugsaledetail> drugsaledetailListOld = persistentMedicineitem.getDrugsaledetailList();
            List<Drugsaledetail> drugsaledetailListNew = medicineitem.getDrugsaledetailList();
            List<String> illegalOrphanMessages = null;
            for (Drugsaledetail drugsaledetailListOldDrugsaledetail : drugsaledetailListOld) {
                if (!drugsaledetailListNew.contains(drugsaledetailListOldDrugsaledetail)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Drugsaledetail " + drugsaledetailListOldDrugsaledetail + " since its drug field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Druginventory> attachedDruginventoryCollectionNew = new ArrayList<Druginventory>();
            for (Druginventory druginventoryCollectionNewDruginventoryToAttach : druginventoryCollectionNew) {
                druginventoryCollectionNewDruginventoryToAttach = em.getReference(druginventoryCollectionNewDruginventoryToAttach.getClass(), druginventoryCollectionNewDruginventoryToAttach.getId());
                attachedDruginventoryCollectionNew.add(druginventoryCollectionNewDruginventoryToAttach);
            }
            druginventoryCollectionNew = attachedDruginventoryCollectionNew;
            medicineitem.setDruginventoryCollection(druginventoryCollectionNew);
            List<Drugsaledetail> attachedDrugsaledetailListNew = new ArrayList<Drugsaledetail>();
            for (Drugsaledetail drugsaledetailListNewDrugsaledetailToAttach : drugsaledetailListNew) {
                drugsaledetailListNewDrugsaledetailToAttach = em.getReference(drugsaledetailListNewDrugsaledetailToAttach.getClass(), drugsaledetailListNewDrugsaledetailToAttach.getId());
                attachedDrugsaledetailListNew.add(drugsaledetailListNewDrugsaledetailToAttach);
            }
            drugsaledetailListNew = attachedDrugsaledetailListNew;
            medicineitem.setDrugsaledetailList(drugsaledetailListNew);
            medicineitem = em.merge(medicineitem);
            for (Druginventory druginventoryCollectionOldDruginventory : druginventoryCollectionOld) {
                if (!druginventoryCollectionNew.contains(druginventoryCollectionOldDruginventory)) {
                    druginventoryCollectionOldDruginventory.setDrug(null);
                    druginventoryCollectionOldDruginventory = em.merge(druginventoryCollectionOldDruginventory);
                }
            }
            for (Druginventory druginventoryCollectionNewDruginventory : druginventoryCollectionNew) {
                if (!druginventoryCollectionOld.contains(druginventoryCollectionNewDruginventory)) {
                    Medicineitem oldDrugOfDruginventoryCollectionNewDruginventory = druginventoryCollectionNewDruginventory.getDrug();
                    druginventoryCollectionNewDruginventory.setDrug(medicineitem);
                    druginventoryCollectionNewDruginventory = em.merge(druginventoryCollectionNewDruginventory);
                    if (oldDrugOfDruginventoryCollectionNewDruginventory != null && !oldDrugOfDruginventoryCollectionNewDruginventory.equals(medicineitem)) {
                        oldDrugOfDruginventoryCollectionNewDruginventory.getDruginventoryCollection().remove(druginventoryCollectionNewDruginventory);
                        oldDrugOfDruginventoryCollectionNewDruginventory = em.merge(oldDrugOfDruginventoryCollectionNewDruginventory);
                    }
                }
            }
            for (Drugsaledetail drugsaledetailListNewDrugsaledetail : drugsaledetailListNew) {
                if (!drugsaledetailListOld.contains(drugsaledetailListNewDrugsaledetail)) {
                    Medicineitem oldDrugOfDrugsaledetailListNewDrugsaledetail = drugsaledetailListNewDrugsaledetail.getDrug();
                    drugsaledetailListNewDrugsaledetail.setDrug(medicineitem);
                    drugsaledetailListNewDrugsaledetail = em.merge(drugsaledetailListNewDrugsaledetail);
                    if (oldDrugOfDrugsaledetailListNewDrugsaledetail != null && !oldDrugOfDrugsaledetailListNewDrugsaledetail.equals(medicineitem)) {
                        oldDrugOfDrugsaledetailListNewDrugsaledetail.getDrugsaledetailList().remove(drugsaledetailListNewDrugsaledetail);
                        oldDrugOfDrugsaledetailListNewDrugsaledetail = em.merge(oldDrugOfDrugsaledetailListNewDrugsaledetail);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = medicineitem.getId();
                if (findMedicineitem(id) == null) {
                    throw new NonexistentEntityException("The medicineitem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Medicineitem medicineitem;
            try {
                medicineitem = em.getReference(Medicineitem.class, id);
                medicineitem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The medicineitem with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Drugsaledetail> drugsaledetailListOrphanCheck = medicineitem.getDrugsaledetailList();
            for (Drugsaledetail drugsaledetailListOrphanCheckDrugsaledetail : drugsaledetailListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Medicineitem (" + medicineitem + ") cannot be destroyed since the Drugsaledetail " + drugsaledetailListOrphanCheckDrugsaledetail + " in its drugsaledetailList field has a non-nullable drug field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Druginventory> druginventoryCollection = medicineitem.getDruginventoryCollection();
            for (Druginventory druginventoryCollectionDruginventory : druginventoryCollection) {
                druginventoryCollectionDruginventory.setDrug(null);
                druginventoryCollectionDruginventory = em.merge(druginventoryCollectionDruginventory);
            }
            em.remove(medicineitem);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Medicineitem> findMedicineitemEntitiesv2() {
        return findMedicineitemEntitiesv2(true, -1, -1);
    }

    public List<Medicineitem> findMedicineitemEntities() {
        return findMedicineitemEntities(true, -1, -1);
    }

    public List<Medicineitem> findMedicineitemEntities(int maxResults, int firstResult) {
        return findMedicineitemEntities(false, maxResults, firstResult);
    }

    private List<Medicineitem> findMedicineitemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        List<Medicineitem> medicineList = new ArrayList();
        try {
            Query q = em.createQuery("SELECT a FROM Medicineitem a WHERE a.isrx = :d ORDER BY a.genericname,a.brandname");
            q.setParameter("d", true);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }

            return q.getResultList().isEmpty() ? medicineList : q.getResultList();
        } finally {
            em.close();
        }
    }

    public Medicineitem findMedicineitem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Medicineitem.class, id);
        } finally {
            em.close();
        }
    }

    public int getMedicineitemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Medicineitem> rt = cq.from(Medicineitem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    private List<Medicineitem> findMedicineitemEntitiesv2(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        List<Medicineitem> medicineList = new ArrayList();
        try {
            Query q = em.createQuery("SELECT a FROM Medicineitem a ORDER BY a.genericname");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }

            return q.getResultList().isEmpty() ? medicineList : q.getResultList();
        } finally {
            em.close();
        }
    }

}
