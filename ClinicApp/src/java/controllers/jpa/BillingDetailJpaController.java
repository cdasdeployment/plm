/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.BillingDetail;
import models.ChartDetails;

/**
 *
 * @author apple
 */
public class BillingDetailJpaController implements Serializable {

    public BillingDetailJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(BillingDetail billingDetail) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            ChartDetails chart = billingDetail.getChart();
            if (chart != null) {
                chart = em.getReference(chart.getClass(), chart.getId());
                billingDetail.setChart(chart);
            }
            em.persist(billingDetail);
            if (chart != null) {
                chart.getBillingDetailList().add(billingDetail);
                chart = em.merge(chart);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(BillingDetail billingDetail) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            BillingDetail persistentBillingDetail = em.find(BillingDetail.class, billingDetail.getId());
            ChartDetails chartOld = persistentBillingDetail.getChart();
            ChartDetails chartNew = billingDetail.getChart();
            if (chartNew != null) {
                chartNew = em.getReference(chartNew.getClass(), chartNew.getId());
                billingDetail.setChart(chartNew);
            }
            billingDetail = em.merge(billingDetail);
            if (chartOld != null && !chartOld.equals(chartNew)) {
                chartOld.getBillingDetailList().remove(billingDetail);
                chartOld = em.merge(chartOld);
            }
            if (chartNew != null && !chartNew.equals(chartOld)) {
                chartNew.getBillingDetailList().add(billingDetail);
                chartNew = em.merge(chartNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = billingDetail.getId();
                if (findBillingDetail(id) == null) {
                    throw new NonexistentEntityException("The billingDetail with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            BillingDetail billingDetail;
            try {
                billingDetail = em.getReference(BillingDetail.class, id);
                billingDetail.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The billingDetail with id " + id + " no longer exists.", enfe);
            }
            ChartDetails chart = billingDetail.getChart();
            if (chart != null) {
                chart.getBillingDetailList().remove(billingDetail);
                chart = em.merge(chart);
            }
            em.remove(billingDetail);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<BillingDetail> findBillingDetailEntities() {
        return findBillingDetailEntities(true, -1, -1);
    }

    public List<BillingDetail> findBillingDetailEntities(int maxResults, int firstResult) {
        return findBillingDetailEntities(false, maxResults, firstResult);
    }

    private List<BillingDetail> findBillingDetailEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(BillingDetail.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public BillingDetail findBillingDetail(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(BillingDetail.class, id);
        } finally {
            em.close();
        }
    }

    public int getBillingDetailCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<BillingDetail> rt = cq.from(BillingDetail.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
