/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import models.AccHearingaidinventory;
import models.AccHearingaiditem;

/**
 *
 * @author apple
 */
public class AccHearingaidinventoryJpaController implements Serializable {

    public AccHearingaidinventoryJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AccHearingaidinventory accHearingaidinventory) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AccHearingaiditem hearingaid = accHearingaidinventory.getHearingaid();
            if (hearingaid != null) {
                hearingaid = em.getReference(hearingaid.getClass(), hearingaid.getId());
                accHearingaidinventory.setHearingaid(hearingaid);
            }
            em.persist(accHearingaidinventory);
            if (hearingaid != null) {
                hearingaid.getAccHearingaidinventoryList().add(accHearingaidinventory);
                hearingaid = em.merge(hearingaid);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AccHearingaidinventory accHearingaidinventory) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AccHearingaidinventory persistentAccHearingaidinventory = em.find(AccHearingaidinventory.class, accHearingaidinventory.getId());
            AccHearingaiditem hearingaidOld = persistentAccHearingaidinventory.getHearingaid();
            AccHearingaiditem hearingaidNew = accHearingaidinventory.getHearingaid();
            if (hearingaidNew != null) {
                hearingaidNew = em.getReference(hearingaidNew.getClass(), hearingaidNew.getId());
                accHearingaidinventory.setHearingaid(hearingaidNew);
            }
            accHearingaidinventory = em.merge(accHearingaidinventory);
            if (hearingaidOld != null && !hearingaidOld.equals(hearingaidNew)) {
                hearingaidOld.getAccHearingaidinventoryList().remove(accHearingaidinventory);
                hearingaidOld = em.merge(hearingaidOld);
            }
            if (hearingaidNew != null && !hearingaidNew.equals(hearingaidOld)) {
                hearingaidNew.getAccHearingaidinventoryList().add(accHearingaidinventory);
                hearingaidNew = em.merge(hearingaidNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = accHearingaidinventory.getId();
                if (findAccHearingaidinventory(id) == null) {
                    throw new NonexistentEntityException("The accHearingaidinventory with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            AccHearingaidinventory accHearingaidinventory;
            try {
                accHearingaidinventory = em.getReference(AccHearingaidinventory.class, id);
                accHearingaidinventory.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The accHearingaidinventory with id " + id + " no longer exists.", enfe);
            }
            AccHearingaiditem hearingaid = accHearingaidinventory.getHearingaid();
            if (hearingaid != null) {
                hearingaid.getAccHearingaidinventoryList().remove(accHearingaidinventory);
                hearingaid = em.merge(hearingaid);
            }
            em.remove(accHearingaidinventory);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AccHearingaidinventory> findAccHearingaidinventoryEntities() {
        return findAccHearingaidinventoryEntities(true, -1, -1);
    }

    public List<AccHearingaidinventory> findAccHearingaidinventoryEntities(int maxResults, int firstResult) {
        return findAccHearingaidinventoryEntities(false, maxResults, firstResult);
    }

    private List<AccHearingaidinventory> findAccHearingaidinventoryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AccHearingaidinventory.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AccHearingaidinventory findAccHearingaidinventory(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AccHearingaidinventory.class, id);
        } finally {
            em.close();
        }
    }

    public int getAccHearingaidinventoryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AccHearingaidinventory> rt = cq.from(AccHearingaidinventory.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
