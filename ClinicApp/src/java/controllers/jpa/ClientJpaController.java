/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.jpa;

import controllers.jpa.exceptions.IllegalOrphanException;
import controllers.jpa.exceptions.NonexistentEntityException;
import controllers.jpa.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import models.Appointment;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Prescription;
import models.PatientChart;
import models.Hearingaidsalesummary;
import models.Acchearingaidsalesummary;
import models.Cdrugsalesummary;
import models.Drugsalesummary;
import models.ChartDetails;
import models.Client;

/**
 *
 * @author apple
 */
public class ClientJpaController implements Serializable {

    public ClientJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Client client) throws RollbackFailureException, Exception {
        if (client.getAppointmentList() == null) {
            client.setAppointmentList(new ArrayList<Appointment>());
        }
        if (client.getPrescriptionList() == null) {
            client.setPrescriptionList(new ArrayList<Prescription>());
        }
        if (client.getPatientChartList() == null) {
            client.setPatientChartList(new ArrayList<PatientChart>());
        }
        if (client.getHearingaidsalesummaryList() == null) {
            client.setHearingaidsalesummaryList(new ArrayList<Hearingaidsalesummary>());
        }
        if (client.getAcchearingaidsalesummaryList() == null) {
            client.setAcchearingaidsalesummaryList(new ArrayList<Acchearingaidsalesummary>());
        }
        if (client.getCdrugsalesummaryList() == null) {
            client.setCdrugsalesummaryList(new ArrayList<Cdrugsalesummary>());
        }
        if (client.getDrugsalesummaryList() == null) {
            client.setDrugsalesummaryList(new ArrayList<Drugsalesummary>());
        }
        if (client.getChartDetailsList() == null) {
            client.setChartDetailsList(new ArrayList<ChartDetails>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            List<Appointment> attachedAppointmentList = new ArrayList<Appointment>();
            for (Appointment appointmentListAppointmentToAttach : client.getAppointmentList()) {
                appointmentListAppointmentToAttach = em.getReference(appointmentListAppointmentToAttach.getClass(), appointmentListAppointmentToAttach.getId());
                attachedAppointmentList.add(appointmentListAppointmentToAttach);
            }
            client.setAppointmentList(attachedAppointmentList);
            List<Prescription> attachedPrescriptionList = new ArrayList<Prescription>();
            for (Prescription prescriptionListPrescriptionToAttach : client.getPrescriptionList()) {
                prescriptionListPrescriptionToAttach = em.getReference(prescriptionListPrescriptionToAttach.getClass(), prescriptionListPrescriptionToAttach.getId());
                attachedPrescriptionList.add(prescriptionListPrescriptionToAttach);
            }
            client.setPrescriptionList(attachedPrescriptionList);
            List<PatientChart> attachedPatientChartList = new ArrayList<PatientChart>();
            for (PatientChart patientChartListPatientChartToAttach : client.getPatientChartList()) {
                patientChartListPatientChartToAttach = em.getReference(patientChartListPatientChartToAttach.getClass(), patientChartListPatientChartToAttach.getId());
                attachedPatientChartList.add(patientChartListPatientChartToAttach);
            }
            client.setPatientChartList(attachedPatientChartList);
            List<Hearingaidsalesummary> attachedHearingaidsalesummaryList = new ArrayList<Hearingaidsalesummary>();
            for (Hearingaidsalesummary hearingaidsalesummaryListHearingaidsalesummaryToAttach : client.getHearingaidsalesummaryList()) {
                hearingaidsalesummaryListHearingaidsalesummaryToAttach = em.getReference(hearingaidsalesummaryListHearingaidsalesummaryToAttach.getClass(), hearingaidsalesummaryListHearingaidsalesummaryToAttach.getId());
                attachedHearingaidsalesummaryList.add(hearingaidsalesummaryListHearingaidsalesummaryToAttach);
            }
            client.setHearingaidsalesummaryList(attachedHearingaidsalesummaryList);
            List<Acchearingaidsalesummary> attachedAcchearingaidsalesummaryList = new ArrayList<Acchearingaidsalesummary>();
            for (Acchearingaidsalesummary acchearingaidsalesummaryListAcchearingaidsalesummaryToAttach : client.getAcchearingaidsalesummaryList()) {
                acchearingaidsalesummaryListAcchearingaidsalesummaryToAttach = em.getReference(acchearingaidsalesummaryListAcchearingaidsalesummaryToAttach.getClass(), acchearingaidsalesummaryListAcchearingaidsalesummaryToAttach.getId());
                attachedAcchearingaidsalesummaryList.add(acchearingaidsalesummaryListAcchearingaidsalesummaryToAttach);
            }
            client.setAcchearingaidsalesummaryList(attachedAcchearingaidsalesummaryList);
            List<Cdrugsalesummary> attachedCdrugsalesummaryList = new ArrayList<Cdrugsalesummary>();
            for (Cdrugsalesummary cdrugsalesummaryListCdrugsalesummaryToAttach : client.getCdrugsalesummaryList()) {
                cdrugsalesummaryListCdrugsalesummaryToAttach = em.getReference(cdrugsalesummaryListCdrugsalesummaryToAttach.getClass(), cdrugsalesummaryListCdrugsalesummaryToAttach.getId());
                attachedCdrugsalesummaryList.add(cdrugsalesummaryListCdrugsalesummaryToAttach);
            }
            client.setCdrugsalesummaryList(attachedCdrugsalesummaryList);
            List<Drugsalesummary> attachedDrugsalesummaryList = new ArrayList<Drugsalesummary>();
            for (Drugsalesummary drugsalesummaryListDrugsalesummaryToAttach : client.getDrugsalesummaryList()) {
                drugsalesummaryListDrugsalesummaryToAttach = em.getReference(drugsalesummaryListDrugsalesummaryToAttach.getClass(), drugsalesummaryListDrugsalesummaryToAttach.getId());
                attachedDrugsalesummaryList.add(drugsalesummaryListDrugsalesummaryToAttach);
            }
            client.setDrugsalesummaryList(attachedDrugsalesummaryList);
            List<ChartDetails> attachedChartDetailsList = new ArrayList<ChartDetails>();
            for (ChartDetails chartDetailsListChartDetailsToAttach : client.getChartDetailsList()) {
                chartDetailsListChartDetailsToAttach = em.getReference(chartDetailsListChartDetailsToAttach.getClass(), chartDetailsListChartDetailsToAttach.getId());
                attachedChartDetailsList.add(chartDetailsListChartDetailsToAttach);
            }
            client.setChartDetailsList(attachedChartDetailsList);
            em.persist(client);
            for (Appointment appointmentListAppointment : client.getAppointmentList()) {
                Client oldClientOfAppointmentListAppointment = appointmentListAppointment.getClient();
                appointmentListAppointment.setClient(client);
                appointmentListAppointment = em.merge(appointmentListAppointment);
                if (oldClientOfAppointmentListAppointment != null) {
                    oldClientOfAppointmentListAppointment.getAppointmentList().remove(appointmentListAppointment);
                    oldClientOfAppointmentListAppointment = em.merge(oldClientOfAppointmentListAppointment);
                }
            }
            for (Prescription prescriptionListPrescription : client.getPrescriptionList()) {
                Client oldClientOfPrescriptionListPrescription = prescriptionListPrescription.getClient();
                prescriptionListPrescription.setClient(client);
                prescriptionListPrescription = em.merge(prescriptionListPrescription);
                if (oldClientOfPrescriptionListPrescription != null) {
                    oldClientOfPrescriptionListPrescription.getPrescriptionList().remove(prescriptionListPrescription);
                    oldClientOfPrescriptionListPrescription = em.merge(oldClientOfPrescriptionListPrescription);
                }
            }
            for (PatientChart patientChartListPatientChart : client.getPatientChartList()) {
                Client oldClientOfPatientChartListPatientChart = patientChartListPatientChart.getClient();
                patientChartListPatientChart.setClient(client);
                patientChartListPatientChart = em.merge(patientChartListPatientChart);
                if (oldClientOfPatientChartListPatientChart != null) {
                    oldClientOfPatientChartListPatientChart.getPatientChartList().remove(patientChartListPatientChart);
                    oldClientOfPatientChartListPatientChart = em.merge(oldClientOfPatientChartListPatientChart);
                }
            }
            for (Hearingaidsalesummary hearingaidsalesummaryListHearingaidsalesummary : client.getHearingaidsalesummaryList()) {
                Client oldClientOfHearingaidsalesummaryListHearingaidsalesummary = hearingaidsalesummaryListHearingaidsalesummary.getClient();
                hearingaidsalesummaryListHearingaidsalesummary.setClient(client);
                hearingaidsalesummaryListHearingaidsalesummary = em.merge(hearingaidsalesummaryListHearingaidsalesummary);
                if (oldClientOfHearingaidsalesummaryListHearingaidsalesummary != null) {
                    oldClientOfHearingaidsalesummaryListHearingaidsalesummary.getHearingaidsalesummaryList().remove(hearingaidsalesummaryListHearingaidsalesummary);
                    oldClientOfHearingaidsalesummaryListHearingaidsalesummary = em.merge(oldClientOfHearingaidsalesummaryListHearingaidsalesummary);
                }
            }
            for (Acchearingaidsalesummary acchearingaidsalesummaryListAcchearingaidsalesummary : client.getAcchearingaidsalesummaryList()) {
                Client oldClientOfAcchearingaidsalesummaryListAcchearingaidsalesummary = acchearingaidsalesummaryListAcchearingaidsalesummary.getClient();
                acchearingaidsalesummaryListAcchearingaidsalesummary.setClient(client);
                acchearingaidsalesummaryListAcchearingaidsalesummary = em.merge(acchearingaidsalesummaryListAcchearingaidsalesummary);
                if (oldClientOfAcchearingaidsalesummaryListAcchearingaidsalesummary != null) {
                    oldClientOfAcchearingaidsalesummaryListAcchearingaidsalesummary.getAcchearingaidsalesummaryList().remove(acchearingaidsalesummaryListAcchearingaidsalesummary);
                    oldClientOfAcchearingaidsalesummaryListAcchearingaidsalesummary = em.merge(oldClientOfAcchearingaidsalesummaryListAcchearingaidsalesummary);
                }
            }
            for (Cdrugsalesummary cdrugsalesummaryListCdrugsalesummary : client.getCdrugsalesummaryList()) {
                Client oldClientOfCdrugsalesummaryListCdrugsalesummary = cdrugsalesummaryListCdrugsalesummary.getClient();
                cdrugsalesummaryListCdrugsalesummary.setClient(client);
                cdrugsalesummaryListCdrugsalesummary = em.merge(cdrugsalesummaryListCdrugsalesummary);
                if (oldClientOfCdrugsalesummaryListCdrugsalesummary != null) {
                    oldClientOfCdrugsalesummaryListCdrugsalesummary.getCdrugsalesummaryList().remove(cdrugsalesummaryListCdrugsalesummary);
                    oldClientOfCdrugsalesummaryListCdrugsalesummary = em.merge(oldClientOfCdrugsalesummaryListCdrugsalesummary);
                }
            }
            for (Drugsalesummary drugsalesummaryListDrugsalesummary : client.getDrugsalesummaryList()) {
                Client oldClientOfDrugsalesummaryListDrugsalesummary = drugsalesummaryListDrugsalesummary.getClient();
                drugsalesummaryListDrugsalesummary.setClient(client);
                drugsalesummaryListDrugsalesummary = em.merge(drugsalesummaryListDrugsalesummary);
                if (oldClientOfDrugsalesummaryListDrugsalesummary != null) {
                    oldClientOfDrugsalesummaryListDrugsalesummary.getDrugsalesummaryList().remove(drugsalesummaryListDrugsalesummary);
                    oldClientOfDrugsalesummaryListDrugsalesummary = em.merge(oldClientOfDrugsalesummaryListDrugsalesummary);
                }
            }
            for (ChartDetails chartDetailsListChartDetails : client.getChartDetailsList()) {
                Client oldClientOfChartDetailsListChartDetails = chartDetailsListChartDetails.getClient();
                chartDetailsListChartDetails.setClient(client);
                chartDetailsListChartDetails = em.merge(chartDetailsListChartDetails);
                if (oldClientOfChartDetailsListChartDetails != null) {
                    oldClientOfChartDetailsListChartDetails.getChartDetailsList().remove(chartDetailsListChartDetails);
                    oldClientOfChartDetailsListChartDetails = em.merge(oldClientOfChartDetailsListChartDetails);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Client client) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client persistentClient = em.find(Client.class, client.getId());
            List<Appointment> appointmentListOld = persistentClient.getAppointmentList();
            List<Appointment> appointmentListNew = client.getAppointmentList();
            List<Prescription> prescriptionListOld = persistentClient.getPrescriptionList();
            List<Prescription> prescriptionListNew = client.getPrescriptionList();
            List<PatientChart> patientChartListOld = persistentClient.getPatientChartList();
            List<PatientChart> patientChartListNew = client.getPatientChartList();
            List<Hearingaidsalesummary> hearingaidsalesummaryListOld = persistentClient.getHearingaidsalesummaryList();
            List<Hearingaidsalesummary> hearingaidsalesummaryListNew = client.getHearingaidsalesummaryList();
            List<Acchearingaidsalesummary> acchearingaidsalesummaryListOld = persistentClient.getAcchearingaidsalesummaryList();
            List<Acchearingaidsalesummary> acchearingaidsalesummaryListNew = client.getAcchearingaidsalesummaryList();
            List<Cdrugsalesummary> cdrugsalesummaryListOld = persistentClient.getCdrugsalesummaryList();
            List<Cdrugsalesummary> cdrugsalesummaryListNew = client.getCdrugsalesummaryList();
            List<Drugsalesummary> drugsalesummaryListOld = persistentClient.getDrugsalesummaryList();
            List<Drugsalesummary> drugsalesummaryListNew = client.getDrugsalesummaryList();
            List<ChartDetails> chartDetailsListOld = persistentClient.getChartDetailsList();
            List<ChartDetails> chartDetailsListNew = client.getChartDetailsList();
            List<String> illegalOrphanMessages = null;
            for (Appointment appointmentListOldAppointment : appointmentListOld) {
                if (!appointmentListNew.contains(appointmentListOldAppointment)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Appointment " + appointmentListOldAppointment + " since its client field is not nullable.");
                }
            }
            for (Hearingaidsalesummary hearingaidsalesummaryListOldHearingaidsalesummary : hearingaidsalesummaryListOld) {
                if (!hearingaidsalesummaryListNew.contains(hearingaidsalesummaryListOldHearingaidsalesummary)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Hearingaidsalesummary " + hearingaidsalesummaryListOldHearingaidsalesummary + " since its client field is not nullable.");
                }
            }
            for (Acchearingaidsalesummary acchearingaidsalesummaryListOldAcchearingaidsalesummary : acchearingaidsalesummaryListOld) {
                if (!acchearingaidsalesummaryListNew.contains(acchearingaidsalesummaryListOldAcchearingaidsalesummary)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Acchearingaidsalesummary " + acchearingaidsalesummaryListOldAcchearingaidsalesummary + " since its client field is not nullable.");
                }
            }
            for (Cdrugsalesummary cdrugsalesummaryListOldCdrugsalesummary : cdrugsalesummaryListOld) {
                if (!cdrugsalesummaryListNew.contains(cdrugsalesummaryListOldCdrugsalesummary)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Cdrugsalesummary " + cdrugsalesummaryListOldCdrugsalesummary + " since its client field is not nullable.");
                }
            }
            for (Drugsalesummary drugsalesummaryListOldDrugsalesummary : drugsalesummaryListOld) {
                if (!drugsalesummaryListNew.contains(drugsalesummaryListOldDrugsalesummary)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Drugsalesummary " + drugsalesummaryListOldDrugsalesummary + " since its client field is not nullable.");
                }
            }
            for (ChartDetails chartDetailsListOldChartDetails : chartDetailsListOld) {
                if (!chartDetailsListNew.contains(chartDetailsListOldChartDetails)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain ChartDetails " + chartDetailsListOldChartDetails + " since its client field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Appointment> attachedAppointmentListNew = new ArrayList<Appointment>();
            for (Appointment appointmentListNewAppointmentToAttach : appointmentListNew) {
                appointmentListNewAppointmentToAttach = em.getReference(appointmentListNewAppointmentToAttach.getClass(), appointmentListNewAppointmentToAttach.getId());
                attachedAppointmentListNew.add(appointmentListNewAppointmentToAttach);
            }
            appointmentListNew = attachedAppointmentListNew;
            client.setAppointmentList(appointmentListNew);
            List<Prescription> attachedPrescriptionListNew = new ArrayList<Prescription>();
            for (Prescription prescriptionListNewPrescriptionToAttach : prescriptionListNew) {
                prescriptionListNewPrescriptionToAttach = em.getReference(prescriptionListNewPrescriptionToAttach.getClass(), prescriptionListNewPrescriptionToAttach.getId());
                attachedPrescriptionListNew.add(prescriptionListNewPrescriptionToAttach);
            }
            prescriptionListNew = attachedPrescriptionListNew;
            client.setPrescriptionList(prescriptionListNew);
            List<PatientChart> attachedPatientChartListNew = new ArrayList<PatientChart>();
            for (PatientChart patientChartListNewPatientChartToAttach : patientChartListNew) {
                patientChartListNewPatientChartToAttach = em.getReference(patientChartListNewPatientChartToAttach.getClass(), patientChartListNewPatientChartToAttach.getId());
                attachedPatientChartListNew.add(patientChartListNewPatientChartToAttach);
            }
            patientChartListNew = attachedPatientChartListNew;
            client.setPatientChartList(patientChartListNew);
            List<Hearingaidsalesummary> attachedHearingaidsalesummaryListNew = new ArrayList<Hearingaidsalesummary>();
            for (Hearingaidsalesummary hearingaidsalesummaryListNewHearingaidsalesummaryToAttach : hearingaidsalesummaryListNew) {
                hearingaidsalesummaryListNewHearingaidsalesummaryToAttach = em.getReference(hearingaidsalesummaryListNewHearingaidsalesummaryToAttach.getClass(), hearingaidsalesummaryListNewHearingaidsalesummaryToAttach.getId());
                attachedHearingaidsalesummaryListNew.add(hearingaidsalesummaryListNewHearingaidsalesummaryToAttach);
            }
            hearingaidsalesummaryListNew = attachedHearingaidsalesummaryListNew;
            client.setHearingaidsalesummaryList(hearingaidsalesummaryListNew);
            List<Acchearingaidsalesummary> attachedAcchearingaidsalesummaryListNew = new ArrayList<Acchearingaidsalesummary>();
            for (Acchearingaidsalesummary acchearingaidsalesummaryListNewAcchearingaidsalesummaryToAttach : acchearingaidsalesummaryListNew) {
                acchearingaidsalesummaryListNewAcchearingaidsalesummaryToAttach = em.getReference(acchearingaidsalesummaryListNewAcchearingaidsalesummaryToAttach.getClass(), acchearingaidsalesummaryListNewAcchearingaidsalesummaryToAttach.getId());
                attachedAcchearingaidsalesummaryListNew.add(acchearingaidsalesummaryListNewAcchearingaidsalesummaryToAttach);
            }
            acchearingaidsalesummaryListNew = attachedAcchearingaidsalesummaryListNew;
            client.setAcchearingaidsalesummaryList(acchearingaidsalesummaryListNew);
            List<Cdrugsalesummary> attachedCdrugsalesummaryListNew = new ArrayList<Cdrugsalesummary>();
            for (Cdrugsalesummary cdrugsalesummaryListNewCdrugsalesummaryToAttach : cdrugsalesummaryListNew) {
                cdrugsalesummaryListNewCdrugsalesummaryToAttach = em.getReference(cdrugsalesummaryListNewCdrugsalesummaryToAttach.getClass(), cdrugsalesummaryListNewCdrugsalesummaryToAttach.getId());
                attachedCdrugsalesummaryListNew.add(cdrugsalesummaryListNewCdrugsalesummaryToAttach);
            }
            cdrugsalesummaryListNew = attachedCdrugsalesummaryListNew;
            client.setCdrugsalesummaryList(cdrugsalesummaryListNew);
            List<Drugsalesummary> attachedDrugsalesummaryListNew = new ArrayList<Drugsalesummary>();
            for (Drugsalesummary drugsalesummaryListNewDrugsalesummaryToAttach : drugsalesummaryListNew) {
                drugsalesummaryListNewDrugsalesummaryToAttach = em.getReference(drugsalesummaryListNewDrugsalesummaryToAttach.getClass(), drugsalesummaryListNewDrugsalesummaryToAttach.getId());
                attachedDrugsalesummaryListNew.add(drugsalesummaryListNewDrugsalesummaryToAttach);
            }
            drugsalesummaryListNew = attachedDrugsalesummaryListNew;
            client.setDrugsalesummaryList(drugsalesummaryListNew);
            List<ChartDetails> attachedChartDetailsListNew = new ArrayList<ChartDetails>();
            for (ChartDetails chartDetailsListNewChartDetailsToAttach : chartDetailsListNew) {
                chartDetailsListNewChartDetailsToAttach = em.getReference(chartDetailsListNewChartDetailsToAttach.getClass(), chartDetailsListNewChartDetailsToAttach.getId());
                attachedChartDetailsListNew.add(chartDetailsListNewChartDetailsToAttach);
            }
            chartDetailsListNew = attachedChartDetailsListNew;
            client.setChartDetailsList(chartDetailsListNew);
            client = em.merge(client);
            for (Appointment appointmentListNewAppointment : appointmentListNew) {
                if (!appointmentListOld.contains(appointmentListNewAppointment)) {
                    Client oldClientOfAppointmentListNewAppointment = appointmentListNewAppointment.getClient();
                    appointmentListNewAppointment.setClient(client);
                    appointmentListNewAppointment = em.merge(appointmentListNewAppointment);
                    if (oldClientOfAppointmentListNewAppointment != null && !oldClientOfAppointmentListNewAppointment.equals(client)) {
                        oldClientOfAppointmentListNewAppointment.getAppointmentList().remove(appointmentListNewAppointment);
                        oldClientOfAppointmentListNewAppointment = em.merge(oldClientOfAppointmentListNewAppointment);
                    }
                }
            }
            for (Prescription prescriptionListOldPrescription : prescriptionListOld) {
                if (!prescriptionListNew.contains(prescriptionListOldPrescription)) {
                    prescriptionListOldPrescription.setClient(null);
                    prescriptionListOldPrescription = em.merge(prescriptionListOldPrescription);
                }
            }
            for (Prescription prescriptionListNewPrescription : prescriptionListNew) {
                if (!prescriptionListOld.contains(prescriptionListNewPrescription)) {
                    Client oldClientOfPrescriptionListNewPrescription = prescriptionListNewPrescription.getClient();
                    prescriptionListNewPrescription.setClient(client);
                    prescriptionListNewPrescription = em.merge(prescriptionListNewPrescription);
                    if (oldClientOfPrescriptionListNewPrescription != null && !oldClientOfPrescriptionListNewPrescription.equals(client)) {
                        oldClientOfPrescriptionListNewPrescription.getPrescriptionList().remove(prescriptionListNewPrescription);
                        oldClientOfPrescriptionListNewPrescription = em.merge(oldClientOfPrescriptionListNewPrescription);
                    }
                }
            }
            for (PatientChart patientChartListOldPatientChart : patientChartListOld) {
                if (!patientChartListNew.contains(patientChartListOldPatientChart)) {
                    patientChartListOldPatientChart.setClient(null);
                    patientChartListOldPatientChart = em.merge(patientChartListOldPatientChart);
                }
            }
            for (PatientChart patientChartListNewPatientChart : patientChartListNew) {
                if (!patientChartListOld.contains(patientChartListNewPatientChart)) {
                    Client oldClientOfPatientChartListNewPatientChart = patientChartListNewPatientChart.getClient();
                    patientChartListNewPatientChart.setClient(client);
                    patientChartListNewPatientChart = em.merge(patientChartListNewPatientChart);
                    if (oldClientOfPatientChartListNewPatientChart != null && !oldClientOfPatientChartListNewPatientChart.equals(client)) {
                        oldClientOfPatientChartListNewPatientChart.getPatientChartList().remove(patientChartListNewPatientChart);
                        oldClientOfPatientChartListNewPatientChart = em.merge(oldClientOfPatientChartListNewPatientChart);
                    }
                }
            }
            for (Hearingaidsalesummary hearingaidsalesummaryListNewHearingaidsalesummary : hearingaidsalesummaryListNew) {
                if (!hearingaidsalesummaryListOld.contains(hearingaidsalesummaryListNewHearingaidsalesummary)) {
                    Client oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary = hearingaidsalesummaryListNewHearingaidsalesummary.getClient();
                    hearingaidsalesummaryListNewHearingaidsalesummary.setClient(client);
                    hearingaidsalesummaryListNewHearingaidsalesummary = em.merge(hearingaidsalesummaryListNewHearingaidsalesummary);
                    if (oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary != null && !oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary.equals(client)) {
                        oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary.getHearingaidsalesummaryList().remove(hearingaidsalesummaryListNewHearingaidsalesummary);
                        oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary = em.merge(oldClientOfHearingaidsalesummaryListNewHearingaidsalesummary);
                    }
                }
            }
            for (Acchearingaidsalesummary acchearingaidsalesummaryListNewAcchearingaidsalesummary : acchearingaidsalesummaryListNew) {
                if (!acchearingaidsalesummaryListOld.contains(acchearingaidsalesummaryListNewAcchearingaidsalesummary)) {
                    Client oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary = acchearingaidsalesummaryListNewAcchearingaidsalesummary.getClient();
                    acchearingaidsalesummaryListNewAcchearingaidsalesummary.setClient(client);
                    acchearingaidsalesummaryListNewAcchearingaidsalesummary = em.merge(acchearingaidsalesummaryListNewAcchearingaidsalesummary);
                    if (oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary != null && !oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary.equals(client)) {
                        oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary.getAcchearingaidsalesummaryList().remove(acchearingaidsalesummaryListNewAcchearingaidsalesummary);
                        oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary = em.merge(oldClientOfAcchearingaidsalesummaryListNewAcchearingaidsalesummary);
                    }
                }
            }
            for (Cdrugsalesummary cdrugsalesummaryListNewCdrugsalesummary : cdrugsalesummaryListNew) {
                if (!cdrugsalesummaryListOld.contains(cdrugsalesummaryListNewCdrugsalesummary)) {
                    Client oldClientOfCdrugsalesummaryListNewCdrugsalesummary = cdrugsalesummaryListNewCdrugsalesummary.getClient();
                    cdrugsalesummaryListNewCdrugsalesummary.setClient(client);
                    cdrugsalesummaryListNewCdrugsalesummary = em.merge(cdrugsalesummaryListNewCdrugsalesummary);
                    if (oldClientOfCdrugsalesummaryListNewCdrugsalesummary != null && !oldClientOfCdrugsalesummaryListNewCdrugsalesummary.equals(client)) {
                        oldClientOfCdrugsalesummaryListNewCdrugsalesummary.getCdrugsalesummaryList().remove(cdrugsalesummaryListNewCdrugsalesummary);
                        oldClientOfCdrugsalesummaryListNewCdrugsalesummary = em.merge(oldClientOfCdrugsalesummaryListNewCdrugsalesummary);
                    }
                }
            }
            for (Drugsalesummary drugsalesummaryListNewDrugsalesummary : drugsalesummaryListNew) {
                if (!drugsalesummaryListOld.contains(drugsalesummaryListNewDrugsalesummary)) {
                    Client oldClientOfDrugsalesummaryListNewDrugsalesummary = drugsalesummaryListNewDrugsalesummary.getClient();
                    drugsalesummaryListNewDrugsalesummary.setClient(client);
                    drugsalesummaryListNewDrugsalesummary = em.merge(drugsalesummaryListNewDrugsalesummary);
                    if (oldClientOfDrugsalesummaryListNewDrugsalesummary != null && !oldClientOfDrugsalesummaryListNewDrugsalesummary.equals(client)) {
                        oldClientOfDrugsalesummaryListNewDrugsalesummary.getDrugsalesummaryList().remove(drugsalesummaryListNewDrugsalesummary);
                        oldClientOfDrugsalesummaryListNewDrugsalesummary = em.merge(oldClientOfDrugsalesummaryListNewDrugsalesummary);
                    }
                }
            }
            for (ChartDetails chartDetailsListNewChartDetails : chartDetailsListNew) {
                if (!chartDetailsListOld.contains(chartDetailsListNewChartDetails)) {
                    Client oldClientOfChartDetailsListNewChartDetails = chartDetailsListNewChartDetails.getClient();
                    chartDetailsListNewChartDetails.setClient(client);
                    chartDetailsListNewChartDetails = em.merge(chartDetailsListNewChartDetails);
                    if (oldClientOfChartDetailsListNewChartDetails != null && !oldClientOfChartDetailsListNewChartDetails.equals(client)) {
                        oldClientOfChartDetailsListNewChartDetails.getChartDetailsList().remove(chartDetailsListNewChartDetails);
                        oldClientOfChartDetailsListNewChartDetails = em.merge(oldClientOfChartDetailsListNewChartDetails);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = client.getId();
                if (findClient(id) == null) {
                    throw new NonexistentEntityException("The client with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Client client;
            try {
                client = em.getReference(Client.class, id);
                client.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The client with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Appointment> appointmentListOrphanCheck = client.getAppointmentList();
            for (Appointment appointmentListOrphanCheckAppointment : appointmentListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the Appointment " + appointmentListOrphanCheckAppointment + " in its appointmentList field has a non-nullable client field.");
            }
            List<Hearingaidsalesummary> hearingaidsalesummaryListOrphanCheck = client.getHearingaidsalesummaryList();
            for (Hearingaidsalesummary hearingaidsalesummaryListOrphanCheckHearingaidsalesummary : hearingaidsalesummaryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the Hearingaidsalesummary " + hearingaidsalesummaryListOrphanCheckHearingaidsalesummary + " in its hearingaidsalesummaryList field has a non-nullable client field.");
            }
            List<Acchearingaidsalesummary> acchearingaidsalesummaryListOrphanCheck = client.getAcchearingaidsalesummaryList();
            for (Acchearingaidsalesummary acchearingaidsalesummaryListOrphanCheckAcchearingaidsalesummary : acchearingaidsalesummaryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the Acchearingaidsalesummary " + acchearingaidsalesummaryListOrphanCheckAcchearingaidsalesummary + " in its acchearingaidsalesummaryList field has a non-nullable client field.");
            }
            List<Cdrugsalesummary> cdrugsalesummaryListOrphanCheck = client.getCdrugsalesummaryList();
            for (Cdrugsalesummary cdrugsalesummaryListOrphanCheckCdrugsalesummary : cdrugsalesummaryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the Cdrugsalesummary " + cdrugsalesummaryListOrphanCheckCdrugsalesummary + " in its cdrugsalesummaryList field has a non-nullable client field.");
            }
            List<Drugsalesummary> drugsalesummaryListOrphanCheck = client.getDrugsalesummaryList();
            for (Drugsalesummary drugsalesummaryListOrphanCheckDrugsalesummary : drugsalesummaryListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the Drugsalesummary " + drugsalesummaryListOrphanCheckDrugsalesummary + " in its drugsalesummaryList field has a non-nullable client field.");
            }
            List<ChartDetails> chartDetailsListOrphanCheck = client.getChartDetailsList();
            for (ChartDetails chartDetailsListOrphanCheckChartDetails : chartDetailsListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Client (" + client + ") cannot be destroyed since the ChartDetails " + chartDetailsListOrphanCheckChartDetails + " in its chartDetailsList field has a non-nullable client field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Prescription> prescriptionList = client.getPrescriptionList();
            for (Prescription prescriptionListPrescription : prescriptionList) {
                prescriptionListPrescription.setClient(null);
                prescriptionListPrescription = em.merge(prescriptionListPrescription);
            }
            List<PatientChart> patientChartList = client.getPatientChartList();
            for (PatientChart patientChartListPatientChart : patientChartList) {
                patientChartListPatientChart.setClient(null);
                patientChartListPatientChart = em.merge(patientChartListPatientChart);
            }
            em.remove(client);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Client> findClientEntities() {
        return findClientEntities(true, -1, -1);
    }

    public List<Client> findClientEntities(int maxResults, int firstResult) {
        return findClientEntities(false, maxResults, firstResult);
    }

    private List<Client> findClientEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM Client c WHERE c.isActive = :active ORDER BY c.lastname,c.firstname");
            q.setParameter("active", true);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Client findClient(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Client.class, id);
        } finally {
            em.close();
        }
    }

    public int getClientCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Client> rt = cq.from(Client.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
