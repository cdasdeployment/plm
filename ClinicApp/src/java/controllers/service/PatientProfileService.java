/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.service;

import controllers.dao.PatientProfileDAO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Client;

/**
 *
 * @author apple
 */
public class PatientProfileService implements Serializable {

    private PatientProfileDAO patientProfileDAO;

    public PatientProfileService(EntityManagerFactory factory, UserTransaction utx) {
        patientProfileDAO = new PatientProfileDAO(factory, utx);
    }

    public List<Client> getPatientProfileList() {
        return patientProfileDAO.findAll();
    }

}
