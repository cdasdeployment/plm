/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers.dao;

import java.io.Serializable;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;
import models.Client;

/**
 *
 * @author apple
 */
public class PatientProfileDAO extends AbstractDAO<Client>
        implements Serializable {

    public PatientProfileDAO(EntityManagerFactory factory, UserTransaction utx) {
        super.construct(factory, utx);
    }

    @Override
    public Class<Client> getEntityClass() {
        return Client.class;
    }

}
